<?php
// Heading
$_['heading_title']  = 'Свържете се с нас';

// Text 
$_['text_location']  = 'Местоположение';
$_['text_store']     = 'Нашите магазини';
$_['text_contact']   = 'Форма за контакт';
$_['text_address']   = 'Адрес:';
$_['text_telephone'] = 'Телефон:';
$_['text_fax']       = 'Факс:';
$_['text_open']      = 'Отваря в';
$_['text_comment']   = 'Мнения';
$_['text_success']   = '<p>Вашето запитване беше успешно изпратено до собственика на магазина!</p>';

// Entry 
$_['entry_name']     = 'Име:';
$_['entry_email']    = 'E-Mail адрес:';
$_['entry_enquiry']  = 'Запитване:';

// Email
$_['email_subject']  = 'Запитване %s';

// Errors
$_['error_name']     = 'Името трябва да съдържа между 3 и 32 символа!';
$_['error_email']    = 'Невалиден E-Mail адрес';
$_['error_enquiry']  = 'Запитването трябва да съдържа между 10 и 3000 символа!';
