<?php
// Heading
$_['heading_title'] = 'Периодични плащания';
// Text
$_['text_account'] = 'Профил';
$_['text_recurring'] = 'Периодични плащания';
$_['text_recurring_detail'] = 'Детайли за поръчките';
$_['text_order_recurring_id'] = 'Номер на профил: ';
$_['text_date_added'] = 'Създаден: ';
$_['text_status'] = 'Статус: ';
$_['text_payment_method'] = 'Метод за плащане: ';
$_['text_order_id'] = 'Номер на поръчка:';
$_['text_product'] = 'Продукт: ';
$_['text_quantity'] = 'Количество: ';
$_['text_description']                     = 'Описание';
$_['text_reference']                       = 'Справка';
$_['text_transaction']                     = 'Транзакция';
$_['text_status_1'] = 'Активни';
$_['text_status_2'] = 'Неактивни';
$_['text_status_3'] = 'Отмениени';
$_['text_status_4'] = 'Прекратени';
$_['text_status_5'] = 'Изтекли';
$_['text_status_6'] = 'Изчакващи';
$_['text_transaction_date_added'] = 'Създаден';
$_['text_transaction_payment'] = 'Плащане';
$_['text_transaction_outstanding_payment'] = 'Изключително плащане';
$_['text_transaction_skipped'] = 'Плащанете е пропусанто';
$_['text_transaction_failed'] = 'Плаането е неуспешно';
$_['text_transaction_cancelled'] = 'Отменен';
$_['text_transaction_suspended'] = 'Прекратен';
$_['text_transaction_suspended_failed'] = 'Премахни от неуспешни плащания';
$_['text_transaction_outstanding_failed'] = 'Изкючителото плащане е неуспешно';
$_['text_transaction_expired'] = 'Изтекъл';
$_['text_empty'] = 'Нямате периодични поръчки';
$_['text_error'] = 'Поръчката, която търсите не може да бъде намерена!';
$_['text_cancelled'] = 'Периодичното плащане беше отменено';
// Column
$_['column_date_added'] = 'Създаден';
$_['column_type'] = 'Вид';
$_['column_amount'] = 'Сума';
$_['column_status'] = 'Статус';
$_['column_product'] = 'Продукт';
$_['column_order_recurring_id'] = 'Номер на профил';
// Error
$_['error_not_cancelled'] = 'Грешка: %s';
$_['error_not_found'] = 'Неуспешно отменяне на периодично плащане';
// Button
$_['button_return']                        = 'Назад';