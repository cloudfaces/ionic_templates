<?php
// Heading
$_['heading_title']             = 'Вашата Афилиат информация';

// Text
$_['text_account'] = 'Профил';
$_['text_affiliate']            = 'Афилиат';
$_['text_my_affiliate']         = 'Моят акаунт';
$_['text_payment']              = 'Информация за плащане';
$_['text_cheque']               = 'Чек';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Банков превод';
$_['text_success']              = 'Вие успешно променихте настройките.';
$_['text_agree']                = 'Аз прочетохи се съгласих с <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_company']             = 'Фирма';
$_['entry_website']             = 'Уебсайт';
$_['entry_tax']                 = 'Данъчен номер';
$_['entry_payment']             = 'Метод за плащане';
$_['entry_cheque']              = 'Име на платеца';
$_['entry_paypal']              = 'PayPal Ел. поща';
$_['entry_bank_name']           = 'Име на банка';
$_['entry_bank_branch_number']  = 'ABA/BSB номер (номер на клона)';
$_['entry_bank_swift_code']     = 'СУИФТ код';
$_['entry_bank_account_name']   = 'Име на собственика';
$_['entry_bank_account_number'] = 'Номер на акаунта';

// Error
$_['error_agree']               = 'Вие трябва да се съглсите с %s!';
$_['error_cheque']              = 'Изисква се попълнено име на платеца!';
$_['error_paypal']              = 'PayPal Ел. поща е невалидна!';
$_['error_bank_account_name']   = 'Моля въведете име!';
$_['error_bank_account_number'] = 'Моля въведете номер!';
$_['error_custom_field']        = '%s задължително!';