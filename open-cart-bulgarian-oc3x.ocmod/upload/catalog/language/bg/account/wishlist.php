<?php
// Heading 
$_['heading_title'] = 'Любими продукти';

// Text
$_['text_account']  = 'Профил';
$_['text_instock']  = 'В наличност';
$_['text_wishlist'] = 'Любими продукти (%s)';
$_['text_login']    = 'Трябва да <a href="%s">се впишете</a> или <a href="%s">да създадете профил</a> за да запазите <a href="%s">%s</a> към <a href="%s"> Любими</a>!';
$_['text_success']  = 'Успех: Добавихте <a href="%s">%s</a> към <a href="%s">Любими продукти</a>!';
$_['text_remove']   = 'Успех: Вие променихте настройките!';
$_['text_empty']    = 'Нямате любими продукти.';

// Column
$_['column_image']  = 'Снимка';
$_['column_name']   = 'Име на продукт';
$_['column_model']  = 'Модел';
$_['column_stock']  = 'Наличност';
$_['column_price']  = 'Еденична цена';
$_['column_action'] = 'Действие';