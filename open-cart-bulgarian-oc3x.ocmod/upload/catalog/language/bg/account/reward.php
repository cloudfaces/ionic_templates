<?php
// Heading 
$_['heading_title']      = 'Вашите наградни точки';

// Column
$_['column_date_added']  = 'Дата на добавяне';
$_['column_description'] = 'Описание';
$_['column_points']      = 'Точки';

// Text
$_['text_account']       = 'Профил';
$_['text_reward']        = 'Наградни точки';
$_['text_total']         = 'Общият брой на вашите наградни точки е:';
$_['text_empty']         = 'Вие нямате натрупани наградни точки!';