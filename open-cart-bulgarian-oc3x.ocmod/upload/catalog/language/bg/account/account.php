<?php
// Heading
$_['heading_title'] = 'Моят профил';
// Text
$_['text_account'] = 'Профил';
$_['text_my_account'] = 'Моят профил';
$_['text_my_orders'] = 'Моите поръчки';
$_['text_my_affiliate']   = 'Моят акаунт';
$_['text_my_newsletter'] = 'Информационен бюлетин';
$_['text_edit'] = 'Редактиране на профила';
$_['text_password'] = 'Смяна на парола';
$_['text_address'] = 'Редактирайте въведените адреси';
$_['text_credit_card'] = 'Редактирай запазените кредитни карти';
$_['text_wishlist'] = 'Редактирайте списъка с любими';
$_['text_order'] = 'Хронологи на поръчките';
$_['text_download'] = 'Свалени файлове';
$_['text_reward'] = 'Вашите бонус точки';
$_['text_return'] = 'Върната стока';
$_['text_transaction'] = 'Вашите транзакции';
$_['text_newsletter'] = 'Абониране / отписване от информационния бюлетин';
$_['text_recurring'] = 'Повтарящи поръчки';
$_['text_transactions'] = 'Транзакции';
$_['text_affiliate_add']  = 'Регистриране на Афилиат';
$_['text_affiliate_edit'] = 'Редакция на информация';
$_['text_tracking']       = 'Допълнителен проследявващ код';