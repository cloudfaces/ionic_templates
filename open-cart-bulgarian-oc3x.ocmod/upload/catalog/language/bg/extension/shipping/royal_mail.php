<?php
// Text
$_['text_title']                = 'Ройал поща';
$_['text_weight']               = 'Тегло:';
$_['text_insurance']            = 'Застраховка до:';
$_['text_special_delivery']             = 'Специална доставка на следващия ден';
$_['text_1st_class_signed']             = 'Първа класа';
$_['text_2nd_class_signed']             = 'Втора класа';
$_['text_1st_class_standard']   = 'Първа класа стандартна';
$_['text_2nd_class_standard']   = 'Втора класа стандартна';
$_['text_international_standard']       = 'Международна стандартна';
$_['text_international_tracked_signed'] = 'Международна проследяваща и с разпис';
$_['text_international_tracked']        = 'Международна проследима';
$_['text_international_signed']  = 'Международна с разписка';
$_['text_international_economy']        = 'Междуанродна икономична';