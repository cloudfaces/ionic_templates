<?php
// Text
$_['text_title']                               = 'Федекс';
$_['text_weight']                              = 'Тегло:';
$_['text_eta']                                 = 'Очаквано време:';
$_['text_europe_first_international_priority'] = 'Европа първи международен приоритет';
$_['text_fedex_1_day_freight']                 = 'Федекс 1 ден';
$_['text_fedex_2_day']                         = 'Федекс 2 ден';
$_['text_fedex_2_day_am']                      = 'Федекс 2 дена сутрешна';
$_['text_fedex_2_day_freight']                 = 'Федекс 2 дена бърза';
$_['text_fedex_3_day_freight']                 = 'Федекс 3 дена бърза';
$_['text_fedex_express_saver']                 = 'Федекс експрес';
$_['text_fedex_first_freight']                 = 'Федекс първо товарене';
$_['text_fedex_freight_economy']               = 'Федекс икономично товарене';
$_['text_fedex_freight_priority']              = 'Федекс приоритетно товарене';
$_['text_fedex_ground']                        = 'Федекс земя';
$_['text_first_overnight']                     = 'Първа класа за ендо денонощие';
$_['text_ground_home_delivery']                = 'Доставяне по домовете';
$_['text_international_economy']               = 'Международно икономично';
$_['text_international_economy_freight']       = 'Международно товарене';
$_['text_international_first']                 = 'Международно първи ';
$_['text_international_priority']              = 'Международно приоритетно';
$_['text_international_priority_freight']      = 'Международно приоритетно товарене';
$_['text_priority_overnight']                  = 'Приоритет за едно денонощие';
$_['text_smart_post']                          = 'Умна поща';
$_['text_standard_overnight']                  = 'Стандартно за едно денонощие';