<?php
// Heading 
$_['heading_title']    = 'Профил';

// Text
$_['text_register']    = 'Регистрация';
$_['text_login']       = 'Вход';
$_['text_logout']      = 'Изход';
$_['text_forgotten']   = 'Забравена парола';
$_['text_account']     = 'Моят профил';
$_['text_edit']        = 'Редактиране на профил';
$_['text_password']    = 'Промяна на парола';
$_['text_address']     = 'Списък с адреси';
$_['text_wishlist']    = 'Любими продукти';
$_['text_order']       = 'Хронология на поръчки';
$_['text_download']    = 'Сваляне на файл';
$_['text_reward']      = 'Наградни точки';
$_['text_return']      = 'Връщане на стока';
$_['text_transaction'] = 'Вашите транзакции';
$_['text_newsletter']  = 'Информационен бюлетин';
$_['text_recurring']   = 'Периодични поръчки';