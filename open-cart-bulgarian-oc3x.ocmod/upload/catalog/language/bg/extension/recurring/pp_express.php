<?php
// Text
$_['text_title'] = 'PayPal Express Checkout';
$_['text_canceled'] = 'Успех: Вие променихте настройките!';
// Button
$_['button_cancel']       = 'Отмени периодично плащане';
// Error
$_['error_not_cancelled'] = 'Грешка: %s';
$_['error_not_found'] = 'Не може да отмени периодичният профил!';
?>