<?php
// Text
$_['text_title']           = 'Кредитна карта / Дебитна карта (SagePay)';
$_['text_credit_card']     = 'Кредитна карта Детайли';
$_['text_card_type']			= 'Вид карта: ';
$_['text_card_name']			= 'Картодържател: ';
$_['text_card_digits']			= 'Последни цифри: ';
$_['text_card_expiry']			= 'Валидност: ';
$_['text_trial']				= '%s всеки %s %s от %s плащанията ';
$_['text_recurring']			= '%s всеки %s %s';
$_['text_length']				= ' за %s пащания';

// Entry
$_['entry_card']				= 'Нова или съществуваща карта: ';
$_['entry_card_existing']		= 'Съществуваща';
$_['entry_card_new']			= 'Нова';
$_['entry_card_save']			= 'Запомни данните за картата';
$_['entry_cc_owner']       = 'Собственик на картата:';
$_['entry_cc_type']        = 'Вид карта:';
$_['entry_cc_number']      = 'Номер на картата:';
$_['entry_cc_expire_date'] = 'Изтича на:';
$_['entry_cc_cvv2']        = 'Код за сигурност (CVV2):';
$_['entry_cc_choice']			= 'Избери съществуваща карта';

// Button
$_['button_delete_card']		= 'Изтрий избраната карта';