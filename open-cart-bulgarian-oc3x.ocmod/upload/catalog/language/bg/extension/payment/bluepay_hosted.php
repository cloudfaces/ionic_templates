<?php
// Text
$_['text_title']				= 'Кредитна / Дебитна карта (BluePay)';
$_['text_credit_card']			= 'Информация за картата';
$_['text_description']			= 'Продукти на %s Поръчка No: %s';
$_['text_card_type']			= 'Вид карта:';
$_['text_card_name']			= 'Име върху картата:';
$_['text_card_digits']			= 'Последни цифри:';
$_['text_card_expiry']			= 'Валидност:';
$_['text_transaction_error']	= 'Имаше грешка при извършване на транзакцията';

// Entry
$_['entry_card']				= 'Нова или съществуваща карта: ';
$_['entry_card_existing']		= 'Съществуваща';
$_['entry_card_new']			= 'Нова';
$_['entry_card_save']			= 'Запомни данните на картата';
$_['entry_cc_owner']			= 'Собственик на картата';
$_['entry_cc_number']			= 'Номер на картата';
$_['entry_cc_start_date']		= 'Картата е валидна от дата';
$_['entry_cc_expire_date']		= 'Картата е валидна до дата';
$_['entry_cc_cvv2']				= 'Код за сигурност (CVV2)';
$_['entry_cc_address']			= 'Адрес';
$_['entry_cc_city']				= 'Град';
$_['entry_cc_state']			= 'Област';
$_['entry_cc_zipcode']			= 'Пощенски код';
$_['entry_cc_phone']			= 'Телефон';
$_['entry_cc_email']			= 'Имейл';
$_['entry_cc_choice']			= 'Избери съществуваща карта';