<?php
// Text
$_['text_title']				= 'Кредитна или Дебитна Карта';
$_['text_credit_card']			= 'Детайли за Карта';
$_['text_wait']					= 'Моля изчакайте!';
$_['text_result']				= 'Резултат';
$_['text_message']				= 'Съобщение';
$_['text_cvn_result']			= 'CVN резултат';
$_['text_avs_postcode']			= 'AVS пощенски код';
$_['text_avs_address']			= 'AVS адрес';
$_['text_eci']					= 'ECI (3D secure) резултат';
$_['text_tss']					= 'TSS резултат';
$_['text_card_bank']			= 'Банка издател на картата';
$_['text_card_country']			= 'Страна на картата';
$_['text_card_region']			= 'Регион на картата';
$_['text_last_digits']			= 'Последни 4 цифри';
$_['text_order_ref']			= 'Реф. номер на поръчка';
$_['text_timestamp']			= 'Време на поръчка';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Switch';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';
$_['text_auth_code']			= 'Код за вписване';
$_['text_3d_s1']				= 'Картодържателят не съществува.';
$_['text_3d_s2']				= 'Неможе да удостовери отговор от сървъра';
$_['text_3d_s3']				= 'Невалиден отговор от сървъра';
$_['text_3d_s4']				= 'Записано, но невалиден отговор от ACS (Access Control Server),';
$_['text_3d_s5']				= 'Успешно вписване';
$_['text_3d_s6']				= 'Опита за вписване е признат';
$_['text_3d_s7']				= 'Неправилно въведена парола';
$_['text_3d_s8']				= 'Вписването е непознато';
$_['text_3d_s9']				= 'Невалиден отговор от ACS';
$_['text_3d_s10']				= 'RealMPI Fatal Error, no liability shift';

// Entry
$_['entry_cc_type']				= 'Вид карта';
$_['entry_cc_number']			= 'Номер на карта';
$_['entry_cc_name']				= 'Картодържател';
$_['entry_cc_expire_date']		= 'Валидност до';
$_['entry_cc_cvv2']				= 'Код за сигурност (CVV2)';
$_['entry_cc_issue']			= 'Номер на издаване';

// Help
$_['help_start_date']			= '(ако има)';
$_['help_issue']				= '(за Maestro и Solo карти само)';

// Error
$_['error_card_number']			= 'Моля проверете номера на картата си';
$_['error_card_name']			= 'Моля проверете името на картодържателя';
$_['error_card_cvv']			= 'Моля проверете CVV2 дали е въведено правилно';
$_['error_3d_unable']			= 'Търговеца изисква 3D secure, но неможа да го удостовери с вашата банка, моля опитайте по-късно';
$_['error_3d_500_response_no_payment'] = 'Невалиден отговор се получи при процеса на обработка, не е таксувано плащане';
$_['error_3d_unsuccessful']		= '3D secure авторизацията е неуспешна';
