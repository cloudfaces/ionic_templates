<?php
// Heading
$_['express_text_title']		= 'Потвърди поръча';

// Text
$_['text_title']				= 'PayPal Express Checkout';
$_['text_cart']					= 'Кошница';
$_['text_shipping_updated']		= 'Доставка обновена';
$_['text_trial']				= '%s всеки %s %s от %s плащанета';
$_['text_recurring']			= '%s всеки %s %s';
$_['text_recurring_item']		= 'Переодичен продукт';
$_['text_length']				= ' за %s плащания';

// Entry
$_['express_entry_coupon']		= 'Въведете купон кода тук:';

// Button
$_['button_express_coupon']		= 'Добави';
$_['button_express_confirm']	= 'Потвърди';
$_['button_express_login']		= 'Напред към PayPal';
$_['button_express_shipping']	= 'Обнови доставка';

// Error
$_['error_heading_title']		= 'Имаше грешка';
$_['error_too_many_failures']	= 'Вашето плащане беше неуспешно твърде много пъти';