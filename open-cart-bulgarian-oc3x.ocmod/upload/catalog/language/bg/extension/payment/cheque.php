<?php
// Text
$_['text_title']       = 'Инструкции за поръчка с Чек';
$_['text_instruction'] = 'Чек поръчка';
$_['text_payable']     = 'Направи поръчка до: ';
$_['text_address']     = 'Изпрати на адрес: ';
$_['text_payment']     = 'Вашата поръчка няма да бъде изпратена докато не получиме плащането.';