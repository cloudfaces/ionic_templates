<?php
// Heading
$_['text_title']				= 'Кредитна / Дебитна Карта (Globalpay)';

// Button
$_['button_confirm']			= 'Потвърди';

// Entry
$_['entry_cc_type']				= 'Вид карта';

// Text
$_['text_success']				= 'Плащането е успешно.';
$_['text_decline']				= 'Плащането е неуспешно';
$_['text_bank_error']			= 'Имаше проблем с обработката на поръчката с банката.';
$_['text_generic_error']		= 'Имаше проблем с обработката на поръчката.';
$_['text_hash_failed']			= 'Hash проверка неуспешна. Моля не се опитвайте да плащате отново, защото статута е непознат. Моля свържете се с търговеца.';
$_['text_link']					= 'Моля натиснете <a href="%s">тук</a>, за да продължите';
$_['text_select_card']			= 'Моля изберете вида карта';
$_['text_result']				= 'Резултат от вписване';
$_['text_message']				= 'Съобщение';
$_['text_cvn_result']			= 'CVN резултат';
$_['text_avs_postcode']			= 'AVS пощенски код';
$_['text_avs_address']			= 'AVS адрес';
$_['text_eci']					= 'ECI (3D secure) резултат';
$_['text_tss']					= 'TSS резултат';
$_['text_order_ref']			= 'Реф. номер на поръчка';
$_['text_timestamp']			= 'Време на поръчка';
$_['text_card_type']			= 'Вид карта';
$_['text_card_digits']			= 'Номер на карта';
$_['text_card_exp']				= 'Валидна до';
$_['text_card_name']			= 'Картодържател';
$_['text_3d_s1']				= 'Картодържателят не съществува.';
$_['text_3d_s2']				= 'Неможе да удостовери отговор от сървъра';
$_['text_3d_s3']				= 'Невалиден отговор от сървъра';
$_['text_3d_s4']				= 'Записано, но невалиден отговор от ACS (Access Control Server),';
$_['text_3d_s5']				= 'Успешно вписване';
$_['text_3d_s6']				= 'Опита за вписване е признат';
$_['text_3d_s7']				= 'Неправилно въведена парола';
$_['text_3d_s8']				= 'Вписването е непознато';
$_['text_3d_s9']				= 'Невалиден отговор от ACS';
$_['text_3d_s10']				= 'RealMPI Fatal Error, no liability shift';
$_['text_3d_liability']     	= 'No Liability Shift';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Switch';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';
