<?php
// Text
$_['text_title']       = 'Кларна фактура';
$_['text_fee']            = 'Klarna фактури - Платете до 14 дена <span id="klarna_invoice_toc_link"></span> (+%s)<script text="javascript\">$.getScript(\'http://cdn.klarna.com/public/kitt/toc/v1.0/js/klarna.terms.min.js\', function(){ var terms = new Klarna.Terms.Invoice({ el: \'klarna_invoice_toc_link\', eid: \'%s\', country: \'%s\', charge: %s});})</script>';
$_['text_no_fee']         = 'Klarna фактури - Платете до 14 дена <span id="klarna_invoice_toc_link"></span><script text="javascript">$.getScript(\'http://cdn.klarna.com/public/kitt/toc/v1.0/js/klarna.terms.min.js\', function(){ var terms = new Klarna.Terms.Invoice({ el: \'klarna_invoice_toc_link\', eid: \'%s\', country: \'%s\'});})</script>';
$_['text_additional']  = 'Кларна изисква допълнителна информация преди да продължите вашата поръчка.';
$_['text_male']        = 'Мъж';
$_['text_female']      = 'Жена';
$_['text_year']           = 'Година';
$_['text_month']          = 'Месец';
$_['text_day']            = 'Ден';
$_['text_comment']        = 'Номер на фактура: %s\n%s/%s: %.4f';

// Entry
$_['entry_gender']     = 'Пол:';
$_['entry_pno']        = 'Рождена дата:';
$_['entry_dob']            = 'Дата на раждане:';
$_['entry_phone_no']    = 'Мобилен телефон:';
$_['entry_street']         = 'Улица:';
$_['entry_house_no']   = 'Адрес:';
$_['entry_house_ext']  = 'Апартамент:';
$_['entry_company']        = 'Регистрационен ноемр на компанията:';
// Help
$_['help_pno']					= 'Моля въведете осигурителния си номер тук.';
$_['help_phone_no']				= 'Моля въведете телефонен номер.';
$_['help_street']				= 'Моля забележете, че доставката е възможна само за регистрирани адреси, когато плащате с Кларна.';
$_['help_house_no']				= 'Моля въведете номера на къщата.';
$_['help_house_ext']			= 'Моля въведете допълнение към адреса на къщата. Например вход А, Б или Син, червен и т.н.';
$_['help_company']				= 'Моля въведете регистрационния код на вашатафирма';

// Error
$_['error_deu_terms']     = 'Трябва да се съгласите с условията на Klarna парава за ползване';
$_['error_address_match'] = 'Адресът за плащане и доставка трябва да съвпадат';
$_['error_network']       = 'Възникна грешка при опит за свързване с Klarna, моля опитайте по-късно.';