<?php
// Text
$_['text_title']           = 'Кларна Профил';
$_['text_terms']				= '<span id="klarna_account_toc"></span><script type="text/javascript">var terms = new Klarna.Terms.Account({el: \'klarna_account_toc\', eid: \'%s\', country: \'%s\'});</script>';
$_['text_information']     = 'Кларна Профил Информация';
$_['text_additional']      = 'Кларна Профил има нужда от малко допълнителна информация, преди да завършите поръчката.';
$_['text_male']            = 'Мъж';
$_['text_female']          = 'Жена';
$_['text_year']            = 'Година';
$_['text_month']           = 'Месец';
$_['text_day']             = 'Ден';
$_['text_payment_option']  = 'Опции за плащане';
$_['text_single_payment']  = 'Еднократно плащане';
$_['text_monthly_payment'] = '%s - %s за месец';
$_['text_comment']         = 'Номер на фактура: %s\n%s/%s: %.4f';

// Entry
$_['entry_gender']         = 'Пол:';
$_['entry_pno']            = 'Личен номер:';
$_['entry_dob']            = 'Дата на раждане:';
$_['entry_phone_no']       = 'Телефонен номер:';
$_['entry_street']				= 'Улица';
$_['entry_house_no']			= 'Номер на къща.';
$_['entry_house_ext']      = 'Апартамент:';
$_['entry_company']				= 'Регистрационен номер на компанията';

// Help
$_['help_pno']					= 'Моля въведете осигурителния си номер тук.';
$_['help_phone_no']				= 'Моля въведете телефонен номер.';
$_['help_street']				= 'Моля забележете, че доставката е възможна само за регистрирани адреси, когато плащате с Кларна.';
$_['help_house_no']				= 'Моля въведете номера на къщата.';
$_['help_house_ext']			= 'Моля въведете допълнение към адреса на къщата. Например вход А, Б или Син, червен и т.н.';
$_['help_company']				= 'Моля въведете регистрационния код на вашатафирма';

// Error
$_['error_deu_terms']     = 'Трябва да се съгласите с условията на Klarna парава за ползване';
$_['error_address_match'] = 'Адресът за плащане и доставка трябва да съвпадат';
$_['error_network']       = 'Възникна грешка при опит за свързване с Klarna, моля опитайте по-късно.';
