<?php

$_['text_title']           = 'Кредитна / Дебитна карта (SagePay)';
$_['button_confirm'] = 'Потвърди';

$_['text_postcode_check'] = 'Проверка на пощенският код: %s';
$_['text_security_code_check'] = 'CVV2 проверка: %s';
$_['text_address_check'] = 'Проверка на адреса: %s';
$_['text_not_given'] = 'Не е приложен';
$_['text_not_checked'] = 'Не е проверен';
$_['text_match'] = 'Съвпадна';
$_['text_not_match'] = 'Не съвпадна';
$_['text_payment_details'] = 'Детайли за плащане';

$_['entry_card_type'] = 'Вид карта';