<?php
// Text
$_['text_title']           = 'Кредитна или дебитна карта (Сигурен превод чрез PayPal)';
$_['text_wait']            = 'Моля изчакайте!';
$_['text_credit_card']     = 'Данни за кредитната карта';
// Entry
$_['entry_cc_type']        = 'Тип карта:';
$_['entry_cc_number']      = 'Номер на карта:';
$_['entry_cc_start_date']  = 'Валидна от:';
$_['entry_cc_expire_date'] = 'Изтича на:';
$_['entry_cc_cvv2']        = 'Код за сигурност на картата (CVV2):';
$_['entry_cc_issue']       = 'Номер на издаване:';
// Help
$_['help_start_date']			= '(ако е достъпна)';
$_['help_issue']				= '(Само за Maestro и Solo карти)';