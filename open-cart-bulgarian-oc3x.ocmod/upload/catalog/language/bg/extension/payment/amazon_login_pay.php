<?php
// Heading
$_['heading_title'] = 'Влез и плати с Амазон';
$_['heading_address'] = 'Моля изберете адрес за доставка';
$_['heading_payment'] = 'Моля изберете метод за плащане';
$_['heading_confirm'] = 'Преглед на поръчката';
// Text
$_['text_back'] = 'Назад';
$_['text_cart'] = 'Кошница';
$_['text_confirm'] = 'Потвърди';
$_['text_continue'] = 'Продължи';
$_['text_lpa'] = 'Влез и плати с Амазон';
$_['text_enter_coupon'] = 'Въведете вашият код за отстъка. Ако нямате оставете полето празно.';
$_['text_coupon'] = 'Код за отстъпка';
$_['text_tax_other'] = 'Такса за опаковане и други такси';
$_['text_success_title'] = 'Вашата поръчка е приета!';
$_['text_payment_success'] = 'Вашата поръчка е приета успешно, по-надолу за детайлите за нея';
// Error
$_['error_payment_method'] = 'Моля изберете метод за плащане';
$_['error_shipping'] = 'Моля изберете метод за доставка';
$_['error_shipping_address'] = 'Моля изберете адрес за доставка';
$_['error_shipping_methods'] = 'Имаше проблем с разчитането на адреса ви от Амазон. Моля сържете се с администратора на сайта за помощ.';
$_['error_no_shipping_methods'] = 'Няма опции за доставка на посочения от вас адрес. Моля изберете друг адрес за доставка.';
$_['error_process_order'] = 'Имаше проблем с вашата поръчка. Моля сържете се с администратора на сайта за помощ.';
$_['error_login'] = 'Неуспешно вписване';
$_['error_login_email'] = 'Неуспешно вписване: %s ел. поща на акаунта не съответства с този от Амазон';
$_['error_minimum'] = 'Минимална поръчка за "Влез и плати с Амазон" е %s!';
