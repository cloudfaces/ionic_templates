<?php
// Text
$_['text_title']				= 'Кредитна / Дебитна карта';
$_['text_credit_card']			= 'Детайли за картата';
$_['text_wait']					= 'Моля изчакайте!';
$_['text_result']				= 'Резултат';
$_['text_message']				= 'Съобщение';
$_['text_cvn_result']			= 'CVN резултат';
$_['text_avs_postcode']			= 'AVS пощенски код';
$_['text_avs_address']			= 'AVS адрес';
$_['text_eci']					= 'ECI (3D secure) резултат';
$_['text_tss']					= 'TSS резултат';
$_['text_card_bank']			= 'Банка издател';
$_['text_card_country']			= 'Страна на картата';
$_['text_card_region']			= 'Регион на картата';
$_['text_last_digits']			= 'Последни 4 цифри';
$_['text_order_ref']			= 'Справка за поръчка';
$_['text_timestamp']			= 'Клеймо';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Switch';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';
$_['text_auth_code']			= 'Код за удостоверяване';
$_['text_3d_s1']				= 'Картодържателят не е записан, сменете името';
$_['text_3d_s2']				= 'Неуспешно потвърждение на записа';
$_['text_3d_s3']				= 'Неуспешен отговор от сървъра.';
$_['text_3d_s4']				= 'Записано, но има невалиден отговор от ACS (Access Control Server)';
$_['text_3d_s5']				= 'Успешно удостоверяване';
$_['text_3d_s6']				= 'Опита за авторизация е признат';
$_['text_3d_s7']				= 'Грешна парола';
$_['text_3d_s8']				= 'Удостоверяването е недостъпно';
$_['text_3d_s9']				= 'Невалиден отговор от ACS';
$_['text_3d_s10']				= 'RealMPI фатална грешка';

// Entry
$_['entry_cc_type']				= 'Вид карта';
$_['entry_cc_number']			= 'Номер на карта';
$_['entry_cc_name']				= 'Картодържател';
$_['entry_cc_expire_date']		= 'Валидност на картата';
$_['entry_cc_cvv2']				= 'Код за сигурност (CVV2)';
$_['entry_cc_issue']			= 'Номер на издаване';

// Help
$_['help_start_date']			= '(ако е достъпна)';
$_['help_issue']				= '(Само за Maestro и Solo карти)';

// Error
$_['error_card_number']			= 'Моля проверете дали картата ви е валидна';
$_['error_card_name']			= 'Моля проверете дали името на картодържателя';
$_['error_card_cvv']			= 'Моля проверете CVV2 дали е валиден';
$_['error_3d_unable']			= 'Търговеца изисква 3D сигурност, но неуспя да потвърди банката, моля опитайте отново';
$_['error_3d_500_response_no_payment'] = 'Невалиден отговор беше върнат, не бяха взети средства от картата';
$_['error_3d_unsuccessful']		= '3D secure авторизацията е неуспешна';