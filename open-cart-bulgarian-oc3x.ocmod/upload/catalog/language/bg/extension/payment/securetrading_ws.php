<?php
$_['text_title'] = 'Кредитна / Дебитна карта (Realex)';
$_['text_card_details'] = 'Детайли за картата';
$_['text_wait'] = 'Обработва вашето плащане';
$_['text_auth_code'] = 'Код за потвърждение: %s';
$_['text_postcode_check'] = 'Проверка на пощенски код: %s';
$_['text_security_code_check'] = 'CVV2 проверка: %s';
$_['text_address_check'] = 'Проверка на адреса: %s';
$_['text_3d_secure_check'] = '3D сигурност: %s';
$_['text_not_given'] = 'Не е приложен';
$_['text_not_checked'] = 'Не е проверен';
$_['text_match'] = 'Съвпадна';
$_['text_not_match'] = 'Не съвпадна';
$_['text_authenticated'] = 'Потвърден';
$_['text_not_authenticated'] = 'Не е потвърден';
$_['text_authentication_not_completed'] = 'Направи опит, но не е успешен';
$_['text_unable_to_perform'] = 'Неуспешно изпълнен';
$_['text_transaction_declined'] = 'Вашата банка отхвърли транзакцията. Моля използвайте друг меод за плащане.';
$_['text_transaction_failed'] = 'Плащането е неуспешно. Моля проверете въведената информация.';
$_['text_connection_error'] = 'Моля опитайте по-късно или пробвайте друг метод за плащане.';

$_['entry_type'] = "Вид карта";
$_['entry_number'] = "Номер на карта";
$_['entry_expire_date'] = "Валидност";
$_['entry_cvv2'] = "Код за сигурност (CVV2)";

$_['button_confirm'] = 'Потвърди';

$_['error_failure'] = 'Неуспешно завършена транзакция. Моля опитайте по-късно или използвайтедруг начин за плащане.';