<?php
// Text
$_['text_title']           = 'Кредитна карта / Дебитна карта (SagePay)';
$_['text_credit_card']     = 'Кредитна карта Детайли';

// Entry
$_['entry_cc_owner']       = 'Собственик на картата:';
$_['entry_cc_number']      = 'Номер на картата:';
$_['entry_cc_expire_date'] = 'Изтича на:';
$_['entry_cc_cvv2']        = 'Код за сигурност (CVV2):';