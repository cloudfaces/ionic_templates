<?php
// Text
$_['text_title']				= 'Кредитна / Дебитна карта';
$_['text_secure_connection']	= 'Създаване на сигурна връзка...';

// Error
$_['error_connection']			= 'Неуспешно свързване с PayPal.';