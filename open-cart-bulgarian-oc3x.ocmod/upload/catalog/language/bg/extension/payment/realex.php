<?php
// Heading
$_['text_title']				= 'Кредитна / Дебитна карта (Realex)';

// Button
$_['button_confirm']			= 'Потвърждаване';

// Entry
$_['entry_cc_type']				= 'Вид карта';

// Text
$_['text_success']				= 'Вашето плащане е потвърдено.';
$_['text_decline']				= 'Вашето плащане е неуспешно';
$_['text_bank_error']			= 'Имаше грешка, обработвайки заявката към банката.';
$_['text_generic_error']		= 'Имаше грешка, обработвайки заявката.';
$_['text_hash_failed']			= 'Hash проверката е неуспешна. Не опитвайте да плащате отново, статуса на поръчката е непознат. Моля свържете се с търговеца.';
$_['text_link']					= 'Моля натиснете <a href="%s">тук,</a> за да продължите ';
$_['text_select_card']			= 'Моля изберете вашата карта';
$_['text_result']				= 'Резултат от удостоверяването';
$_['text_message']				= 'Съобщение';
$_['text_cvn_result']			= 'CVN резултат';
$_['text_avs_postcode']			= 'AVS Пощенски код';
$_['text_avs_address']			= 'AVS адрес';
$_['text_eci']					= 'ECI (3D secure) резултат';
$_['text_tss']					= 'TSS резултат';
$_['text_order_ref']			= 'Справка за поръчка';
$_['text_timestamp']			= 'Клеймо';
$_['text_card_type']			= 'Вид карта';
$_['text_card_digits']			= 'Номер на карта';
$_['text_card_exp']				= 'Валидност на картата';
$_['text_card_name']			= 'Име на картата';
$_['text_3d_s1']				= 'Картодържателят не е записан, сменете името';
$_['text_3d_s2']				= 'Неуспешно потвърждение на записа';
$_['text_3d_s3']				= 'Неуспешен отговор от сървъра.';
$_['text_3d_s4']				= 'Записано, но има невалиден отговор от ACS (Access Control Server)';
$_['text_3d_s5']				= 'Успешно удостоверяване';
$_['text_3d_s6']				= 'Опита за авторизация е признат';
$_['text_3d_s7']				= 'Грешна парола';
$_['text_3d_s8']				= 'Удостоверяването е недостъпно';
$_['text_3d_s9']				= 'Невалиден отговор от ACS';
$_['text_3d_s10']				= 'RealMPI фатална грешка';
$_['text_3d_liability']     	= 'No Liability Shift';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Switch';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';