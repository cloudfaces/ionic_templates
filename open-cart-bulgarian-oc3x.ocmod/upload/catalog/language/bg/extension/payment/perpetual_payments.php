<?php
// Text
$_['text_title']           = 'Кредитна / Дебитна карта (Processed securely by Perpetual Payments)';
$_['text_credit_card']			= 'Данни за картата';
$_['text_transaction']     = 'Номер на транзакция:';
$_['text_avs']             = 'AVS/CVV:';
$_['text_avs_full_match']  = 'Пълно съвпадение';
$_['text_avs_not_match']   = 'Няма съвпадение';
$_['text_authorisation']   = 'Код за авторизация:';

// Entry
$_['entry_cc_number']      = 'Номер на картата:';
$_['entry_cc_start_date']  = 'Валидна от:';
$_['entry_cc_expire_date'] = 'Изтича на:';
$_['entry_cc_cvv2']        = 'Код за сигурност (CVV2):';
$_['entry_cc_issue']       = 'Номер на карта:';
// Help
$_['help_start_date']			= '(ако е достъпно)';
$_['help_issue']				= '(Само за Maestro и Solo карти)';