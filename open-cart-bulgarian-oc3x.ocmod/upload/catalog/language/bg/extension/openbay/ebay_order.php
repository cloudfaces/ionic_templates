<?php
// Text
$_['text_total_shipping']		= 'Доставка';
$_['text_total_discount']		= 'Отстъпка';
$_['text_total_tax']			= 'Данък';
$_['text_total_sub']			= 'Междинна сума';
$_['text_total']				= 'Общо';
$_['text_smp_id']				= 'Продажбения номер на мениджъра: ';
$_['text_buyer']				= 'Име на клиента: ';