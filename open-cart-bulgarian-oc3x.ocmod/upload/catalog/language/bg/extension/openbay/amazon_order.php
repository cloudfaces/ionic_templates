<?php
// Text
$_['text_paid_amazon'] 			= 'Платено s Амазон';
$_['text_total_shipping'] 		= 'Доставка';
$_['text_total_shipping_tax'] 	= 'Такса за доставка';
$_['text_total_giftwrap'] 		= 'Подаръчна опаковка';
$_['text_total_giftwrap_tax'] 	= 'Такса за опаковане';
$_['text_total_sub'] 			= 'Междинна сума';
$_['text_tax'] 					= 'Данък';
$_['text_total'] 				= 'Общо';
$_['text_gift_message'] 		= 'Съобщение';
