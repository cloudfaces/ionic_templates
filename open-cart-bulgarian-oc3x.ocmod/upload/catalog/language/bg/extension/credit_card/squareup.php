<?php

$_['heading_title'] = 'Square Кредитни карти';

$_['text_account'] = 'Акаунт';
$_['text_back'] = 'Назад';
$_['text_delete'] = 'Изтрий';
$_['text_no_cards'] = 'Няма запазени карти.';
$_['text_card_ends_in'] = '%s изтича след &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; %s';
$_['text_warning_card'] = 'Искате ли да премахнете картата? Може да я добавите пак при следващо плащане.';
$_['text_success_card_delete'] = 'Вие успешно премахнахте картата.';