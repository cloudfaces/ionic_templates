<?php
// Heading
$_['heading_title']         = 'Sagepay Direct Cards';

// Text
$_['text_empty']		    = 'Нямате запазени карти';
$_['text_account']          = 'Акаунт';
$_['text_card']			    = 'SagePay Direct Card Management';
$_['text_fail_card']	    = 'Имаше проблем при премахването на картата, моля свържете се издателят й.';
$_['text_success_card']     = 'SagePay неуспешно премахната карта';
$_['text_success_add_card'] = 'SagePay успешно добавена карта';

// Column
$_['column_type']		    = 'Вид карта';
$_['column_digits']	        = 'Последни цифри';
$_['column_expiry']		    = 'Валидност';

// Entry
$_['entry_cc_owner']        = 'Картодържател';
$_['entry_cc_type']         = 'Вид карта';
$_['entry_cc_number']       = 'Номер на карта';
$_['entry_cc_expire_date']  = 'Валидна до';
$_['entry_cc_cvv2']         = 'Код за сигурност (CVV2)';
$_['entry_cc_choice']       = 'Изберете съществуваща карта';

// Button
$_['button_add_card']       = 'Добави карта';
$_['button_new_card']       = 'Добави нова карта';



