<?php
// Heading
$_['heading_title']     = 'Намери любим производител';

// Text
$_['text_brand']        = 'Производител';
$_['text_index']        = 'Индекс на Производителя:';
$_['text_error']        = 'Производителя не е открит!';
$_['text_empty']        = 'Няма производители в списъка.';
$_['text_quantity']     = 'Количество:';
$_['text_manufacturer'] = 'Производител:';
$_['text_model']        = 'Код на продукт:'; 
$_['text_points']       = 'Бонус точки:'; 
$_['text_price']        = 'Цена:'; 
$_['text_tax']          = 'без ДДС:'; 
$_['text_compare']      = 'Съпоставка на продукт (%s)'; 
$_['text_sort']         = 'Подреждане:';
$_['text_default']      = 'По подразбиране';
$_['text_name_asc']     = 'Име (А - Я)';
$_['text_name_desc']    = 'Име (Я - А)';
$_['text_price_asc']    = 'Цена (най-ниска &gt; най-висока)';
$_['text_price_desc']   = 'Цена (най-висока &gt; най-ниска)';
$_['text_rating_asc']   = 'Оценка (най-ниска)';
$_['text_rating_desc']  = 'Оценка (най-висока)';
$_['text_model_asc']    = 'Модел (А - Я)';
$_['text_model_desc']   = 'Модел (Я - А)';
$_['text_limit']        = 'Показване:';