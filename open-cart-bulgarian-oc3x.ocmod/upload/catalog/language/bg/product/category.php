<?php
// Text
$_['text_refine']       = 'Подробно ';
$_['text_product']      = 'Продукти';
$_['text_error']        = 'Категорията не е намерена!';
$_['text_empty']        = 'Няма добавени продукти в тази категория.';
$_['text_quantity']     = 'Количество:';
$_['text_manufacturer'] = 'Производител:';
$_['text_model']        = 'Код на продукт:'; 
$_['text_points']       = 'Бонус точки:'; 
$_['text_price']        = 'Цена:'; 
$_['text_tax']          = 'без ДДС:'; 
$_['text_compare']      = 'Сравнение на продукт (%s)'; 
$_['text_sort']         = 'Подреждане по:';
$_['text_default']      = 'По подразбиране';
$_['text_name_asc']     = 'Име (А - Я)';
$_['text_name_desc']    = 'Име (Я - А)';
$_['text_price_asc']    = 'Цена (най-ниска &gt; най-висока)';
$_['text_price_desc']   = 'Цена (най-висока &gt; най-ниска)';
$_['text_rating_asc']   = 'Оценка (най-ниска)';
$_['text_rating_desc']  = 'Оценка (най-висока)';
$_['text_model_asc']    = 'Модел (А - Я)';
$_['text_model_desc']   = 'Модел (Я - А)';
$_['text_limit']        = 'Показване:';