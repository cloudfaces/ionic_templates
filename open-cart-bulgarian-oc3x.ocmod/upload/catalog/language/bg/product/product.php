<?php
// Text
$_['text_search'] = 'Търсене';
$_['text_brand'] = 'Марка:';
$_['text_manufacturer'] = 'Производител:';
$_['text_model'] = 'Модел:';
$_['text_reward'] = 'Бонус точки:';
$_['text_points'] = 'Цена в Бонус точки:';
$_['text_stock'] = 'Наличност:';
$_['text_instock'] = 'В наличност';
$_['text_tax'] = 'без ДДС:';
$_['text_discount'] = ' или повече ';
$_['text_option'] = 'Опции';
$_['text_minimum'] = 'Този продукт има минимално количество на поръчка %s';
$_['text_reviews'] = '%s мнения';
$_['text_write'] = 'Напишете мнение';
$_['text_login'] = 'Моля <a href="%s">впишете се</a> или <a href="%s">се регистрирайте,</a> за да напишете мнение';
$_['text_no_reviews'] = 'Няма мнения за този продукт.';
$_['text_note'] = '<span style="color: #FF0000;">Забележка:</span> HTML не се потдържа!';
$_['text_success'] = 'Благодарим за вашето мнение.';
$_['text_related'] = 'Подобни продукти';
$_['text_tags'] = 'Етикети:';
$_['text_error'] = 'Продуктът не беше открит!';
$_['text_payment_recurring'] = 'Профили за плащане';
$_['text_trial_description'] = '%s всеки %d %s(s) за %d плащане после';
$_['text_payment_description'] = '%s всеки %d %s(s) за %d плащане';
$_['text_payment_cancel'] = '%s всеки %d %s(s) до отмяна';
$_['text_day'] = 'ден';
$_['text_week'] = 'седмица';
$_['text_semi_month'] = 'половин месец';
$_['text_month'] = 'месец';
$_['text_year'] = 'година';
// Entry
$_['entry_qty'] = 'Количество';
$_['entry_name'] = 'Вашето име:';
$_['entry_review'] = 'Вашият коментар:';
$_['entry_rating'] = 'Оценка:';
$_['entry_good'] = 'Добър';
$_['entry_bad'] = 'Лош';
// Tabs
$_['tab_description'] = 'Описание';
$_['tab_attribute'] = 'Спецификация';
$_['tab_review'] = 'Коментари (%s)';

// Error
$_['error_name'] = 'Грешка: Името на коментара трябва да бъде между 3 и 25 символа!';
$_['error_text'] = 'Грешка: Коментарът трябва съдържа между 25 и 1000 символа!';
$_['error_rating'] = 'Грешка: Моля изберете оценка!';
?>