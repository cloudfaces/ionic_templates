<?php
// Text
$_['text_subject']      = '%s - Поръча %s';
$_['text_received']     = 'Получихте поръчка.';
$_['text_order_id']     = 'Номер поръчка:';
$_['text_date_added']   = 'Дата:';
$_['text_order_status'] = 'Статут на поръчка:';
$_['text_product']      = 'Продукти';
$_['text_total']        = 'Общо';
$_['text_comment']      = 'Коментар към поръчката:';
