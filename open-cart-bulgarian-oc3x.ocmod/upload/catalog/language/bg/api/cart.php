<?php
// Text
$_['text_success']     = 'Вие обновихте вашата кошница!';

// Error
$_['error_permission'] = 'Вие нямате права за достъп до API връзката!';
$_['error_stock']      = 'Продуктите маркирани с *** не са налични в желаното количество или не са налични на склад!';
$_['error_minimum']    = 'Минималната сума за поръчката %s е %s!';
$_['error_store']      = 'Продуктите не могат да бъдат закупени от магазина, който сте избрали!';
$_['error_required']   = '%s се изисква!';