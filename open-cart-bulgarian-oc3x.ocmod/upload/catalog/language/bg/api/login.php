<?php
// Text
$_['text_success'] = 'Успешно започнахте API сесия!';

// Error
$_['error_key']  = 'Внимание: въведохте грешен ключ!';
$_['error_ip']   = 'Внимание: вашето АйПи %s не е разрешено за достъп до API!';
