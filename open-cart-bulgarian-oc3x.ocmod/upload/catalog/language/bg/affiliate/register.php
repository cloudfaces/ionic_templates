<?php
// Heading 
$_['heading_title']             = 'Партньорска програма';

// Text
$_['text_account']              = 'Профил';
$_['text_register']             = 'Регистрация на партньор';
$_['text_account_already']      = 'Ако вече имате профил при нас, моля влезте оттук <a href="%s">Влизане</a>.';
$_['text_signup']               = 'За да създадете партньорски профил, попълнете формата отдолу като попълните всички задължителни полета:';
$_['text_your_details']         = 'Вашите лични данни';
$_['text_your_address']         = 'Вашият адрес';
$_['text_your_affiliate']       = 'Вашата информация';
$_['text_your_password']        = 'Вашата парола';
$_['text_cheque']               = 'Чек';
$_['text_paypal']               = 'ПейПал';
$_['text_bank']                 = 'Банков превод';
$_['text_agree']                = 'Прочетох и съм съгласен с <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_firstname']           = 'Име:';
$_['entry_lastname']            = 'Фамилия:';
$_['entry_email']               = 'Ел. поща:';
$_['entry_telephone']           = 'Телефон:';
$_['entry_company']             = 'Фирма:';
$_['entry_website']             = 'Домейн:';
$_['entry_tax']                 = 'Данъчен номер:';
$_['entry_payment']             = 'Метод на плащане:';
$_['entry_cheque']              = 'Име на полуучателя на чека:';
$_['entry_paypal']              = 'ПейПал ел. поща:';
$_['entry_bank_name']           = 'Име на банката:';
$_['entry_bank_branch_number']  = 'ABA/BSB Номер (номер на клон):';
$_['entry_bank_swift_code']     = 'SWIFT код:';
$_['entry_bank_account_name']   = 'Име на сметка:';
$_['entry_bank_account_number'] = 'Номер на сметка:';
$_['entry_password']            = 'Парола:';
$_['entry_confirm']             = 'Потвърждение на паролата:';

// Error
$_['error_exists']              = 'Грешка: електронната поща вече съществува в базата!';
$_['error_firstname']           = 'Името трябва да бъде между 1 и 32 символа!';
$_['error_lastname']            = 'Фамилията трябва да бъде между 1 и 32 символа!';
$_['error_email']               = 'Невалиден адрес на ел. поща';
$_['error_telephone']           = 'Телефонът трябва да съдържа между 3 и 32 символа!';
$_['error_custom_field']        = '%s задължително!';
$_['error_cheque']              = 'Попълнете име за чека!';
$_['error_paypal']              = 'PayPal Ел. поща е невалидна!';
$_['error_bank_account_name']   = 'Въведете име!';
$_['error_bank_account_number'] = 'Въведете номер на акунта!';
$_['error_password']            = 'Паролата трябва да съдържа между 4 и 20 символа!';
$_['error_confirm']             = 'Потвърждението на паролата не съвпада с паролата!';
$_['error_agree']               = 'Грешка: Трябва да се съгласите с %s!';
