<?php
// Text
$_['text_home']           = 'Начало';
$_['text_wishlist']       = 'Любими (%s)';
$_['text_shopping_cart']  = 'Кошница';
$_['text_category']      = 'Категории';
$_['text_account']        = 'Моят профил';
$_['text_register']      = 'Регистриране';
$_['text_login']         = 'Вход';
$_['text_order']         = 'История на поръчките';
$_['text_transaction']   = 'Транзакции';
$_['text_download']      = 'За сваляне';
$_['text_logout']        = 'Изход';
$_['text_checkout']       = 'Плащане';
$_['text_search']        = 'Търсене';
$_['text_all']           = 'Виж всички';