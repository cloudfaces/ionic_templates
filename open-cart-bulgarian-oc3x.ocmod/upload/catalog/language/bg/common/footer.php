<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Обслужване на клиенти';
$_['text_extra']        = 'Допълнения';
$_['text_contact']      = 'Свържете се с нас';
$_['text_return']       = 'Връщане на стока';
$_['text_sitemap']      = 'Карта на сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Ваучери';
$_['text_affiliate']    = 'Партньори';
$_['text_special']      = 'Промоции';
$_['text_account']      = 'Моят профил';
$_['text_order']        = 'История на поръчките';
$_['text_wishlist']     = 'Любими продукти';
$_['text_newsletter']   = 'Информационен бюлетин';
$_['text_powered']      = 'Powered By <a href="http://www.opencart-bg.com">OpenCart</a><br /> %s &copy; %s';
