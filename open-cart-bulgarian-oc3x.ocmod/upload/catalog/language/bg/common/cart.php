<?php
// Text
$_['text_items']    = '%s продукт(и) - %s';
$_['text_empty']    = 'Вашата кошница е празна!';
$_['text_cart']     = 'Към кошницата';
$_['text_checkout'] = 'Плащане';
$_['text_recurring']  = 'Профил за плащане';