<?php
// Heading  
$_['heading_title']          = 'Кошница';

// Text
$_['text_success']           = 'Добавихте <a href="%s">%s</a> към вашата <a href="%s">кошница</a>!';
$_['text_remove']            = 'Вие променихте кошницата!';
$_['text_login']             = 'Внимание: Трябва да се <a href="%s">логнете</a> или да<a href="%s">създатете профил</a> за да виждате цените!';
$_['text_items']             = '%s артикул(а) - %s';
$_['text_points']            = 'Бонус точки: %s';
$_['text_next']              = 'Каква следваща стъпка искате да направите?';
$_['text_next_choice']       = 'Изберете, ако имате код за отстъпка или наградни точки, който искате да използвате, или биха искали да се изчисли разходите си за доставка.';
$_['text_empty']             = 'Вашата кошница е празна!';
$_['text_day']         = 'ден';
$_['text_week']        = 'седмица';
$_['text_semi_month']  = 'половин месец';
$_['text_month']       = 'месец';
$_['text_year']        = 'година';
$_['text_trial']       = '%s всяко %s %s от %s плащанията ';
$_['text_recurring']   = '%s всяко %s %s';
$_['text_payment_cancel']   	= 'до отмяна';
$_['text_recurring_item']    	              = 'Повтарящ се продукт';
$_['text_payment_recurring']                    = 'Профил за плащане';
$_['text_trial_description'] 	              = '%s всяко %d %s(и) от %d плащане тогава';
$_['text_payment_description'] 	              = '%s всяко %d %s(и) за %d плащане';
$_['text_payment_cancel']      = '%s всеки %d %s(и) до отмяна';
// Column
$_['column_image']           = 'Снимка';
$_['column_name']            = 'Име на продукт';
$_['column_model']           = 'Модел';
$_['column_quantity']        = 'Количество';
$_['column_price']           = 'Ед. цена';
$_['column_total']           = 'Всичко';


// Error
$_['error_stock']            = 'Продуктите маркирани с  *** не са налични!';
$_['error_minimum']          = 'Минималната сума за % е %s!';	
$_['error_required']         = '%s е нужно!';	
$_['error_product']          = 'Внимание: Няма продукти в кошницата!';	
$_['error_recurring_required'] = 'Моля изберете периодично плащане!';