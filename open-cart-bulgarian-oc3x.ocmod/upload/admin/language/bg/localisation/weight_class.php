<?php
// Heading$_['heading_title']    = 'Мерни единици за тегло';
// Text$_['text_success']     = 'Успешно променихте параметрите на Мерни единици за тегло!';$_['text_list']        = 'Списък с мерни единици за тегло';$_['text_add']         = 'Добави мерна единица';$_['text_edit']        = 'Редактирай мерна единица';
// Column$_['column_title']     = 'Име на мерната единица';$_['column_unit']      = 'Съкращение на името на мерната единица';
$_['column_value']     = 'Стойност';
$_['column_action']    = 'Действие';
// Entry$_['entry_title']      = 'Име на мерната единица:';$_['entry_unit']       = 'Съкращение на името на мерната единица:';
$_['entry_value']      = 'Стойност:';
// Help$_['help_value']       = 'Настрой на 1.00000 ако това е твоята мерна еденица по подразбиране.';
// Error$_['error_permission'] = 'Внимание: Нямате права да променяте параметрите на Мерни единици за тегло!';$_['error_title']      = 'Името на мерната единица трябва да е с дължина между 3 и 32 символа!';$_['error_unit']       = 'Съкращението на името трябва да е с дължина между 1 и 4 символа!';$_['error_default']    = 'Внимание: Тази мерна единица за тегло не може да бъде изтрита, тъй като е зададена като основна мерна единица за този магазин!';$_['error_product']    = 'Внимание: Тази мерна единица за тегло не може да бъде изтрита, тъй като е асоциирана към %s продукта!';