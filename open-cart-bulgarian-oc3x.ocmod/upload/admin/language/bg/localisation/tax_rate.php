<?php
// Heading$_['heading_title']        = 'Данъчни ставки';
// Text$_['text_success']         = 'Списъкът с данъци е променен!';$_['text_list']            = 'Списък с данъчни ставки';$_['text_add']             = 'Добавяне на данъчна ставка';$_['text_edit']            = 'Редактиране';
$_['text_percent']         = 'Процент';$_['text_amount']          = 'Фиксирана величина';
// Column$_['column_name']          = 'Наименование на данъка';
$_['column_rate']          = 'Размер';
$_['column_type']          = 'Тип';$_['column_geo_zone']      = 'Географска зона';
$_['column_date_added']    = 'Добавено';
$_['column_date_modified'] = 'Променено';
$_['column_action']        = 'Действие';
// Entry$_['entry_name']           = 'Наименование:';
$_['entry_rate']           = 'Размер:';
$_['entry_type']           = 'Тип:';
$_['entry_customer_group'] = 'Група клиенти:';$_['entry_geo_zone']       = 'Географска зона:';
// Error$_['error_permission']  = 'Вие нямате права да променяте данъчните ставки!';$_['error_tax_rule']    = 'Данъкът не може да бъде изключен, докато е свързан с %s класа!';$_['error_name']        = 'Името трябва да бъде между 3 и 32 символа!';$_['error_rate']        = 'Изисква да се въведе размер!';
