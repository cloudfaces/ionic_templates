<?php
// Heading$_['heading_title']      = 'Географски зони';
// Text$_['text_success']       = 'Успешно променихте параметрите на Географски зони!';$_['text_list']          = 'Списък с географски зони';$_['text_add']           = 'Добавяне на зона';$_['text_edit']          = 'Редактиране на зона';
$_['text_geo_zone']      = 'Гео зони';// Column$_['column_name']        = 'Име на географски зони';
$_['column_description'] = 'Описание';
$_['column_action']      = 'Действие';
// Entry$_['entry_name']         = 'Име на географски зони:';
$_['entry_description']  = 'Описание:';
$_['entry_country']      = 'Държава:';
$_['entry_zone']         = 'Регион:';
// Error$_['error_permission']   = 'Внимание: Нямате права да променяте параметрите на Географски зони!';$_['error_name']         = 'Името на географски зони трябва да е с дължина между 3 и 32 символа!';$_['error_description']  = 'Описанието трябва да е с дължина между 3 и 255 символа!';$_['error_tax_rate']     = 'Внимание: Тази географска зона не може да бъде изтрита, тъй като е асоциирана с един или повече данъчни класове!';