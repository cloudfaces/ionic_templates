<?php
// Heading
$_['heading_title'] = 'Потребител';
// Text
$_['text_success'] = 'Успешно променихте параметрите на Потребител!';
$_['text_list'] = 'Списък с потребители';
$_['text_add'] = 'Добави потребител';
$_['text_edit'] = 'Редкатиране';
// Column
$_['column_username'] = 'Потребителско име';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата на добавяне';
$_['column_action'] = 'Действие';
// Entry
$_['entry_username'] = 'Потребителско име:';
$_['entry_user_group'] = 'Потребителска група';
$_['entry_password'] = 'Парола:';
$_['entry_confirm'] = 'Потвърждение на паролата:';
$_['entry_firstname'] = 'Име:';
$_['entry_lastname'] = 'Фамилия:';
$_['entry_email'] = 'E-Mail:';
$_['entry_image'] = 'Изображение';
$_['entry_status'] = 'Статус:';

// Error
$_['error_permission'] = 'Внимание: Нямате права да променяте параметрите на Потребител!';
$_['error_account'] = 'Внимание: Не можете да изтриете собствения си профил!';
$_['error_exists_username'] = 'Внимание: Потребителското име вече е регистрирано!';
$_['error_username'] = 'Потребителското име трябва да е с дължина между 3 и 20 символа!';
$_['error_password'] = 'Паролата трябва да е с дължина между 4 и 20 символа!';
$_['error_confirm'] = 'Въведените парола и потвърждение на паролата не съвпадат!';
$_['error_firstname'] = 'Името трябва да е с дължина между 1 и 32 символа!';
$_['error_lastname'] = 'Фамилията трябва да е с дължина между 1 и 32 символа!';
$_['error_email'] = 'Невалиден адрес на електронната поща!';
$_['error_exists_email'] = 'Внимание: електронната поща вече е регистрирана!';
?>