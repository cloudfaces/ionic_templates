<?php
// Heading
$_['heading_title'] = 'API';
// Text
$_['text_success'] = 'Успешно редактирахте APIs!';
$_['text_list'] = 'Списък';
$_['text_add'] = 'Добавяне';
$_['text_edit'] = 'Редкатиране';
$_['text_ip'] = 'По-долу можете да създадете списък от IP адреси позволени да имат достъп до вашето API. Текущия ви IP адрес е %s';

// Column
$_['column_username'] = 'Потребителско име';
$_['column_status'] = 'Статус';
$_['column_token'] = 'Символ';
$_['column_ip'] = 'Адрес';
$_['column_date_added'] = 'Дата на добавяне';
$_['column_date_modified'] = 'Дата на редактиране';
$_['column_action'] = 'Действие';
// Entry
$_['entry_username'] = 'Потребителско име';
$_['entry_key'] = 'Ключ';
$_['entry_status'] = 'Статус';
$_['entry_ip'] = 'Адрес';
// Error
$_['error_permission'] = 'Нямате права да редактирате модула!';
$_['error_username'] = 'Потребителското име трябва да е между 3 и 20 символа!';
$_['error_key'] = 'API ключа трябва да е между 3 и 256 символа!';
$_['error_ip']             = 'You must have atleast one IP added to the allowed list!';
?>