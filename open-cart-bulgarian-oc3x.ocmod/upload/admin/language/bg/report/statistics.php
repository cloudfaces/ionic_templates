<?php
// Heading
$_['heading_title']         = 'Статистика';

// Text
$_['text_success']          = 'Вие успешно променихте настройките!';
$_['text_list']             = 'Списък статистики';
$_['text_order_sale']       = 'Статут на поръчка';
$_['text_order_processing'] = 'Обработвани поръчки';
$_['text_order_complete']   = 'Завършени поръчки';
$_['text_order_other']      = 'Други поръчки';
$_['text_returns']          = 'Върната стока';
$_['text_customer']         = 'Киенти чакащи одобрение';
$_['text_affiliate']        = 'Афилиати чакащи одобрение';
$_['text_product']          = 'Изчерпани продукти';
$_['text_review']           = 'Чакащи мнения от клиенти';

// Column
$_['column_name']           = 'Име на статистика';
$_['column_value']	        = 'Стойност';
$_['column_action']         = 'Действие';

// Error
$_['error_permission']      = 'Внимание вие нямате права да променяте настройките!';