<?php
// Heading
$_['heading_title']     = 'Отчет кой е онлайн';

// Text
$_['text_extension']    = 'Допълнения';
$_['text_success']      = 'Вие успешно променихте настройките!';
$_['text_list']         = 'Списък';
$_['text_filter']       = 'Филтър';
$_['text_guest']        = 'Гост';

// Column
$_['column_ip']         = 'Адрес';
$_['column_customer']   = 'Клиент';
$_['column_url']        = 'Последно посетена страница';
$_['column_referer']    = 'Относно';
$_['column_date_added'] = 'Последен клик';
$_['column_action']     = 'Действие';

// Entry
$_['entry_ip']          = 'Адрес';
$_['entry_customer']    = 'Клиент';
$_['entry_status']      = 'Статут';
$_['entry_sort_order']  = 'Подреждане';

// Error
$_['error_permission']  = 'Вие нямате права да променяте настройките!';