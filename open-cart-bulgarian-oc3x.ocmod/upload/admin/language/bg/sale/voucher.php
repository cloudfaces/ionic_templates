<?php
// Heading  
$_['heading_title']     = 'Ваучери';
// Text$_['text_success']      = 'Вие променихте настройките!';$_['text_list']         = 'Списък с ваучери';$_['text_add']          = 'Добавяне';$_['text_edit']         = 'Редактиране';$_['text_sent']         = 'Ваучера е изпратен по E-Mail!';
// Column$_['column_name']       = 'Име на ваучера';
$_['column_code']       = 'Код';
$_['column_from']       = 'От';
$_['column_to']         = 'До';
$_['column_theme']      = 'Тема';
$_['column_amount']     = 'Сума';
$_['column_status']     = 'Статус';
$_['column_order_id']   = 'Поръчка No.';
$_['column_customer']   = 'Клиент';$_['column_date_added'] = 'Дата на добавяне';
$_['column_action']     = 'Напред';
// Entry
$_['entry_code']        = 'Код';$_['entry_from_name']   = 'Име на изпращача:';
$_['entry_from_email']  = 'E-Mail на изпращача:';$_['entry_to_name']     = 'Име на получателя:';
$_['entry_to_email']    = 'E-Mail на получателя:';
$_['entry_theme']       = 'Тема:';
$_['entry_message']     = 'Собщение:';
$_['entry_amount']      = 'Сума:';
$_['entry_status']      = 'Статус:';
// Help$_['help_code']         = 'Кодът, който клиента трябва да въведе да активира ваучера.';
// Error$_['error_permission']  = 'Вие нямате права да променяте настройките!';$_['error_exists']      = 'Ваучер кода вече е използван!';$_['error_code']        = 'Пощенският код трябва да бъде между 3 и 10 символа!';$_['error_to_name']     = 'Името трябва да бъде между 1 и 64 символа!';$_['error_from_name']   = 'Името на изпращача трябва да бъде между 1 и 64 символа!';$_['error_email']       = 'Невалиден E-Mail адрес!';$_['error_amount']      = 'Сумата трябва да не е по-малко от 1!';$_['error_order']       = 'Внимание: Този ваучер неможе да бъде изтрит, защото е свързан с <a href="%s">Поръчка </a>!';