<?php
$_['heading_title'] = 'Върната стока';
$_['text_success'] = 'Промените за върнатата стока са успешно въведени!';
$_['text_list'] = 'Списък с върната стока';
$_['text_add'] = 'Добавяне';
$_['text_edit'] = 'Редактиране';
$_['text_opened'] = 'Отворена опаковка';
$_['text_unopened'] = 'Неразопакован';
$_['text_order'] = 'Информация за поръчка';
$_['text_product'] = 'Продукт:';
$_['text_history'] = 'История върнати';
$_['text_history_add']     = 'Добави история';
// Column
$_['column_return_id'] = 'Номер на върната стока';
$_['column_order_id'] = 'Номер на поръчка';
$_['column_customer'] = 'Клиент';
$_['column_product'] = 'Продукт';
$_['column_model'] = 'Модел';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата на добавяне';
$_['column_date_modified'] = 'Дата на промяна';
$_['column_comment'] = 'Коментари';
$_['column_notify'] = 'Клиента е уведомен';
$_['column_action'] = 'Действие';
// Entry
$_['entry_customer'] = 'Клиент:';
$_['entry_order_id'] = 'Поръчка No.:';
$_['entry_date_ordered'] = 'Дата поръчка:';
$_['entry_firstname'] = 'Име, Презиме:';
$_['entry_lastname'] = 'Фамилия:';
$_['entry_email'] = 'E-Mail:';
$_['entry_telephone'] = 'Телефон:';
$_['entry_product'] = 'Поръчка:';
$_['entry_model'] = 'Модел:';
$_['entry_quantity'] = 'Количество:';
$_['entry_opened'] = 'Отворен:';
$_['entry_comment'] = 'Коментари:';
$_['entry_return_reason'] = 'Причина на връщането:';
$_['entry_return_action'] = 'Връщане';
$_['entry_return_status'] = 'Статус за връщане';
$_['entry_notify'] = 'Информирай клиента';
$_['entry_return_id'] = 'Номер на връщане';
$_['entry_date_added'] = 'Дата на добавяне';
$_['entry_date_modified'] = 'Дата на редактиране';
// Help
$_['help_product'] = '(Авто-допълване)';

// Error
$_['error_warning'] = 'Внимателно проверете формуляра за грешки!';
$_['error_permission'] = 'Нямате права да променяте връщаната стока!';
$_['error_order_id'] = 'Поръчка ID задължително!';
$_['error_firstname'] = 'Името трябва да бъде между 1 и 32 символа!';
$_['error_lastname'] = 'Фамилията трябва да бъде между 1 и 32 символа!';
$_['error_email'] = 'Невалиден E-Mail адрес';
$_['error_telephone'] = 'Телефонът трябва да бъде между 1 и 32 символа!';
$_['error_product'] = 'Името на продукта трябва да бъде повече от 3 и по-малко от 255 символа!';
$_['error_model'] = 'Името на продуктовия модел да бъде повече от 3 и по-малко от 64 символа!';
?>