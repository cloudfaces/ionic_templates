<?php
// Heading
$_['heading_title']        = 'Електронна поща';
// Text$_['text_success']         = 'Вашето съобщение беше успешно изпратено!';$_['text_sent']            = 'Вашето съобщение беше успешно изпратено до %s от %s получатели!';$_['text_list']            = 'Списък с електронни пощи';$_['text_default']         = 'По подразбиране';$_['text_newsletter']      = 'Всички абонати на бюлетина';$_['text_customer_all']    = 'Всички клиенти';
$_['text_customer_group']  = 'Групи клиенти';
$_['text_customer']        = 'Клиенти';$_['text_affiliate_all']   = 'Всички афилиейти';
$_['text_affiliate']       = 'Афилиейти';
$_['text_product']         = 'Продукти';
// Entry
$_['entry_store']          = 'От';
$_['entry_to']             = 'До';
$_['entry_customer_group'] = 'Група клиенти';
$_['entry_customer']       = 'Клиент';
$_['entry_affiliate']      = 'Афилиат';
$_['entry_product']        = 'Продукти';
$_['entry_subject']        = 'Тема';
$_['entry_message']        = 'Съобщение';
// Help$_['help_customer']       = 'Авто-допълване';$_['help_affiliate']      = 'Авто-допълване';$_['help_product']        = 'Изпрати само до клиентите, които са поръчали от списъска. (Авто-допълване)';
// Error$_['error_permission']     = 'Внимание: Вие нямате право да изпращате електронни съобщения!';$_['error_subject']        = 'Изисква се тема на съобщението!';$_['error_message']        = 'Изисква се да попълните полето за съобщение!';