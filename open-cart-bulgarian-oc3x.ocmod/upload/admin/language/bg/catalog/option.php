<?php
// Heading
$_['heading_title']       = 'Опции';
// Text$_['text_success']        = 'Промените са запазени!';$_['text_list']           = 'Списък с опции';$_['text_add']            = 'Добавяне на опция';$_['text_edit']           = 'Редактиране на опция';$_['text_choose']         = 'Избор';$_['text_select']         = 'Списък';$_['text_radio']          = 'Радиобутон';$_['text_checkbox']       = 'Отметка';
$_['text_input']          = 'Въведи текст';
$_['text_text']           = 'Текстово поле';$_['text_textarea']       = 'Текстова област';
$_['text_file']           = 'Файл';
$_['text_date']           = 'Дата';$_['text_datetime']       = 'Дата и време';
$_['text_time']           = 'Време';$_['text_option']        = 'Опции';
$_['text_value']         = 'Стойност на опция';
// Column$_['column_name']         = 'Наименование';
$_['column_sort_order']   = 'Подреждане';
$_['column_action']       = 'Действие';
// Entry$_['entry_name']         = 'Име на опция:';
$_['entry_type']         = 'Вид:';$_['entry_option_value'] = 'Въведете стойност:';
$_['entry_image']        = 'Изображение:';
$_['entry_sort_order']   = 'Подреждане:';
// Error$_['error_permission']   = 'Вие нямате рпава да променяте опциите!';$_['error_name']         = 'Името трябва да бъде между 1 и 128 символа!';$_['error_type']         = 'Не е указана стойност!';$_['error_option_value'] = 'Стойността на опцията трябва да бъде между 1 и 128!';$_['error_product']      = 'Тази опция неможе да бъде изтрита, докато е свързана %s продукта!';