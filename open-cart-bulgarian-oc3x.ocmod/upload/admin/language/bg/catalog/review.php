<?php
// Heading$_['heading_title']       = 'Мнения на потребители';
// Text$_['text_success']      = 'Успешно променихте параметрите на модула!';$_['text_list']         = 'Списък с мнения';$_['text_add']          = 'Добавяне на мнение';$_['text_edit']         = 'Редактиране на мнение';$_['text_filter']       = 'Филтър';// Column
$_['column_product']    = 'Продукт';
$_['column_author']     = 'Автор';
$_['column_rating']     = 'Рейтинг';
$_['column_status']     = 'Статус';$_['column_date_added'] = 'Дата на добавяне';
$_['column_action']     = 'Действие';
// Entry
$_['entry_product']     = 'Продукт:';
$_['entry_author']      = 'Автор:';
$_['entry_rating']      = 'Оценка:';
$_['entry_status']      = 'Статус:';
$_['entry_text']        = 'Текст:';$_['entry_date_added']  = 'Дата на добавяне';
// Help$_['help_product']      = '(Авто-допълване)';
// Error$_['error_permission']  = 'Внимание: Нямате права да променяте параметрите!';$_['error_product']     = 'Необходимо е да въведете продукт!';$_['error_author']      = 'Името на автора трябва да е с дължина между 3 и 64 символа!';$_['error_text']        = 'Текстът на мнението трябва да е с поне 1 символ!';$_['error_rating']      = 'Необходимо е да се посочи оценка на продукта!';