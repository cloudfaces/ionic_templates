<?php
// Heading
$_['heading_title']          = 'Атрибути';

// Text$_['text_success']           = 'Атрибутите са успешно обновени!';$_['text_list']              = 'Списък с атрибути';$_['text_add']               = 'Добавяне атрибут';$_['text_edit']              = 'Редактиране на атрибут';

// Column$_['column_name']            = 'Име на атрибута';$_['column_attribute_group'] = 'Група на атрибута';$_['column_sort_order']      = 'Подреждане';$_['column_action']          = 'Действие';

// Entry$_['entry_name']            = 'Име на атрибута:';$_['entry_attribute_group'] = 'Група на атрибута:';$_['entry_sort_order']      = 'Подреждане:';

// Error$_['error_permission']      = 'Вие нямате права да променяте атрибутите!';$_['error_attribute_group'] = 'Изисква се групов атрибут!';$_['error_name']            = 'Името на атрибута трябва да бъде между 3 и 64 символа!';$_['error_product']         = 'Този атрибут неможе да бъде изтрит, свързан е с %s !';