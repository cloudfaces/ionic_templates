<?php
// Heading
$_['heading_title'] = 'Продукти';
// Text
$_['text_success'] = 'Успешно променихте параметрите на Продукти!';
$_['text_list'] = 'Списък с продукти';
$_['text_add'] = 'Добавяне на продукт';
$_['text_edit'] = 'Редактиране на продукт';
$_['text_filter']            = 'Filter';
$_['text_plus'] = '+';
$_['text_minus'] = '-';
$_['text_default'] = 'Основен';
$_['text_option'] = 'Опция';
$_['text_option_value'] = 'Стойност на опцията';
$_['text_percent'] = 'Процент';
$_['text_amount'] = 'Фиксирана сума';

// Column

$_['column_name'] = 'Име на продукта';
$_['column_model'] = 'Модел';
$_['column_image'] = 'Изображение';
$_['column_price'] = 'Цена:';
$_['column_quantity'] = 'Наличност';
$_['column_status'] = 'Статус';
$_['column_action'] = 'Действие';

// Entry
$_['entry_name'] = 'Име на продукта:';
$_['entry_description'] = 'Описание:';
$_['entry_meta_title'] = 'Мета таг заглавие';
$_['entry_meta_keyword'] = 'Мета таг ключови думи:';
$_['entry_meta_description'] = 'Мета таг описание:';
$_['entry_store'] = 'Магазин';
$_['entry_keyword'] = 'SEO ключови думи';
$_['entry_model'] = 'Модел:';
$_['entry_sku'] = 'SKU:';
$_['entry_upc'] = 'UPC:';
$_['entry_ean'] = 'EAN:';
$_['entry_jan'] = 'JAN:';
$_['entry_isbn'] = 'ISBN:';
$_['entry_mpn'] = 'MPN:';
$_['entry_location'] = 'Различно местоположение:';
$_['entry_shipping'] = 'Изисква доставка';
$_['entry_manufacturer'] = 'Производител:';
$_['entry_date_available'] = 'Налично от:';
$_['entry_quantity'] = 'Количество:';
$_['entry_minimum'] = 'Минимално количество:';
$_['entry_stock_status'] = 'Изчерпано:';
$_['entry_price'] = 'Цена:';
$_['entry_tax_class'] = 'Вид на данъка:';
$_['entry_points'] = 'Въведете точки:';
$_['entry_option_points'] = 'Tочки:';
$_['entry_subtract'] = 'Изваждане на стоката:';
$_['entry_weight_class'] = 'Мярка за тегло:';
$_['entry_weight'] = 'Тегло:';
$_['entry_dimension'] = 'Размери (Д x Ш x В)';
$_['entry_length_class'] = 'Клас дължина';
$_['entry_length'] = 'Линейна мярка';
$_['entry_width'] = 'Ширина';
$_['entry_height'] = 'Височина';
$_['entry_image'] = 'Изображение:';
$_['entry_additional_image'] = 'Допълнителни изображения';
$_['entry_customer_group']   = 'Клиентска група';
$_['entry_date_start'] = 'Начална дата:';
$_['entry_date_end'] = 'Крайна дата:';
$_['entry_priority'] = 'Приоритет:';
$_['entry_attribute'] = 'Име на атрибута:';
$_['entry_attribute_group'] = 'Групов атрибут:';
$_['entry_text'] = 'Текст:';
$_['entry_option'] = 'Опция:';
$_['entry_option_value'] = 'Стойност:';
$_['entry_required'] = 'Задължително:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
$_['entry_category'] = 'Категория:';
$_['entry_filter'] = 'Филтри:';
$_['entry_download'] = 'Сваляне:';
$_['entry_related'] = 'Подобни продукти:';
$_['entry_tag'] = 'Етикети за продукта:';
$_['entry_reward'] = 'Наградни точки:';
$_['entry_layout'] = 'Оформление:';
$_['entry_recurring'] = 'Повтарящ се профил';
// Help
$_['help_sku'] = 'Stock Keeping Unit (SKU)';
$_['help_upc'] = 'Universal Product Code (UPC)';
$_['help_ean'] = 'European Article Number (EAN)';
$_['help_jan'] = 'Japanese Article Number';
$_['help_isbn'] = 'International Standard Book Number';
$_['help_mpn'] = 'Manufacturer Part Number';
$_['help_manufacturer'] = '(Авто-допълване)';
$_['help_minimum'] = 'Изискай минимална сума за покупка';
$_['help_stock_status'] = 'Статуса се покзазва, когато продуктът е изчерпан';
$_['help_points'] = 'Брой точки необходими за закупуване на тои продукт. Ако не желаете тази да се дават точки, попълнете с "0" полето.';
$_['help_category'] = '(Авто-допълване)';
$_['help_filter'] = '(Авто-допълване)';
$_['help_download'] = '(Авто-допълване)';
$_['help_related'] = '(Авто-допълване)';
$_['help_tag'] = 'разделени със запетая';
// Error
$_['error_warning'] = 'Не е въведена необходимата информация. Проверете полетата за грешки!';
$_['error_permission'] = 'Внимание: Нямате права да променяте параметрите на Продукти!';
$_['error_name'] = 'Името на продукта трябва да е с дължина между 3 и 255 символа!';
$_['error_meta_title'] = 'Мета заглаието трябва да е между3 и по-малко от 255 символа!';
$_['error_model'] = 'Името на модела на продукта трябва да е с дължина между 3 и 64 символа!';
$_['error_keyword'] = 'SEO ключова дума вече се използва!';
$_['error_unique']           = 'SEO URL трябва да е уникален!';
?>