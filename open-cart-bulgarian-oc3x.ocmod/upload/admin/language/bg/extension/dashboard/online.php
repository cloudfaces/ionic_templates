<?php
// Heading
$_['heading_title'] = 'Потребители на линия';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успех: Вие успешно променихте настройките!';
$_['text_edit'] = 'Редакция на настройките';
$_['text_view'] = 'Още...';
// Entry
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_width'] = 'Ширина';

// Error
$_['error_permission'] = 'Вие нямате права да променяте настройките!';
?>