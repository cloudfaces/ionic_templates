<?php
// Heading
$_['heading_title'] = 'Анализ на продажби';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успех: Вие успешно променихте настройките!';
$_['text_edit'] = 'Редакция на графиката на таблото';
$_['text_order'] = 'Поръчки';
$_['text_customer'] = 'Клиенти';
$_['text_day'] = 'Днес';
$_['text_week'] = 'Седмица';
$_['text_month'] = 'Месец';
$_['text_year'] = 'Година';
// Entry
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_width'] = 'Ширина';
// Error
$_['error_permission'] = 'Внимание: Вие нямате права да редактирате настройките!';

?>