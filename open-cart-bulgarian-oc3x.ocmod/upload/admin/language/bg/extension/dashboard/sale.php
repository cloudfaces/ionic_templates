<?php
// Heading
$_['heading_title'] = 'Общо продажби';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Вие успешно променихте настройките!';
$_['text_edit'] = 'Редактирайте продажбите от таблото';
$_['text_view'] = 'Още...';
// Entry

$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_width'] = 'Ширина';
// Error
$_['error_permission'] = 'Вие нямате права да редактирате модула!';
?>