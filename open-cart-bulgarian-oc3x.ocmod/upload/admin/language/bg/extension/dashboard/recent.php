<?php
// Heading
$_['heading_title'] = 'Последни поръчки';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успех: Вие успешно променихте настройките!';
$_['text_edit'] = 'Редакция на настройките';
// Column
$_['column_order_id'] = 'Номер на поръчка';
$_['column_customer'] = 'Клиент';
$_['column_status'] = 'Статус';
$_['column_total'] = 'Общо';
$_['column_date_added'] = 'Дата на добавяне';
$_['column_action'] = 'Действие';
// Entry
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_width'] = 'Ширина';
// Error
$_['error_permission'] = 'Внимание: Вие нямате права да редактирате настройките!';
?>