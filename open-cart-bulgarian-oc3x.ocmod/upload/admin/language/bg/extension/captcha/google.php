<?php// Heading$_['heading_title']    = 'Гугъл тест за сигурност';
// Text$_['text_extension']   = 'Допълнения';$_['text_success']	   = 'Успешно променихте настройките!';$_['text_edit']        = 'Редактирай настройките';$_['text_signup']      = 'Отидете на <a href="https://www.google.com/recaptcha/intro/index.html" target="_blank"><u>страницата на Гугъл reCAPTCHA</u></a> и регистрирайте уеб страницата Ви.';
// Entry$_['entry_key']        = 'Ключ на сайта';
$_['entry_secret']     = 'Таен ключ';
$_['entry_status']     = 'Състояние';
// Error$_['error_permission'] = 'Внимание Вие нямате права да редактирате настройките!';$_['error_key']	       = 'Въведете ключ!';$_['error_secret']	   = 'Въведете таен ключ!';
