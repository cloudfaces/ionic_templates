<?php
// Heading
$_['heading_title'] = 'FraudLabs Pro';

// Text
$_['text_extension']             = 'Допълнения';
$_['text_success'] = 'Успех: Вие променихте настройките!';
$_['text_edit'] = 'Настройки';
$_['text_signup'] = 'FraudLabsPro е услуга за залавяне на измами. Ако нямате API ключ Вие можете да <a href="http://www.fraudlabspro.com/plan?ref=1730" target="_blank"><u>се регистрирате тук</u></a>.';
$_['text_id'] = 'FraudLabs Pro ID';
$_['text_ip_address'] = 'IP адрес';
$_['text_ip_net_speed'] = 'IP скорост на мрежа';
$_['text_ip_isp_name'] = 'IP име на доставчик';
$_['text_ip_usage_type'] = 'IP Вид ползване';
$_['text_ip_domain'] = 'IP Домейн';
$_['text_ip_time_zone'] = 'IP Часова зона';
$_['text_ip_location'] = 'IP Локация';
$_['text_ip_distance'] = 'IP Разстояние';
$_['text_ip_latitude'] = 'IP ширина';
$_['text_ip_longitude'] = 'IP дължина';
$_['text_risk_country'] = 'Страна с висок рисков потенциал';
$_['text_free_email'] = 'Безплатна ел. поща';
$_['text_ship_forward'] = 'Доставяне';
$_['text_using_proxy'] = 'Използване на Proxy';
$_['text_bin_found'] = 'BIN Намерен';
$_['text_email_blacklist'] = 'Черен списък с ел. пощи';
$_['text_credit_card_blacklist'] = 'Черен списък с кредитни карти';
$_['text_score'] = 'FraudLabsPro Резултат';
$_['text_status'] = 'FraudLabs Pro Статут';
$_['text_message'] = 'Съобщение';
$_['text_transaction_id'] = 'ID на транзакцията';
$_['text_credits'] = 'Баланс';
$_['text_error'] = 'Грешка:';
$_['text_flp_upgrade'] = '<a href="http://www.fraudlabspro.com/plan" target="_blank">[Обнови]</a>';
$_['text_flp_merchant_area'] = 'Моля влезте в <a href="http://www.fraudlabspro.com/login" target="_blank">FraudLabs Pro секция на продавача</a> за повече информация относно тази поръчка.';
// Entry
$_['entry_status'] = 'Статут';
$_['entry_key'] = 'API Ключ';
$_['entry_score'] = 'Риск';
$_['entry_order_status'] = 'Статут на поръчката';
$_['entry_review_status'] = 'Статут на побликувано мнение';
$_['entry_approve_status'] = 'Статут на одобрени';
$_['entry_reject_status'] = 'Статут на отхвърлени';
$_['entry_simulate_ip'] = 'Симулация на IP';
// Help
$_['help_order_status'] = 'Поръки с рисков резултат над определения от вас, няма да бъдат допуснати да достигнат крайния статут на поръчката автоматично.';
$_['help_review_status'] = 'Поръчки маркирани за преглед от FraudLabs Pro ще бъдат с такъв статут.';
$_['help_approve_status'] = 'Поръчки маркирани като одобрени от FraudLabs Pro ще бъдат с такъв статут.';
$_['help_reject_status'] = 'Поръчки маркирани като отхвърлени от FraudLabs Pro ще бъдат с такъв статут.';
$_['help_simulate_ip'] = 'Симулиране на посетителски IP address с цел тестване.';
$_['help_fraudlabspro_id'] = 'Уникален индификатор на транзакции сканирани от FraudLabs Pro.';
$_['help_ip_address'] = 'IP Адрес.';
$_['help_ip_net_speed'] = 'Скорост на връзката.';
$_['help_ip_isp_name'] = 'Очаквано ISP (интенет доставчик) на IP адреса.';
$_['help_ip_usage_type'] = 'Очакван  вид потребление на IP адреса.';
$_['help_ip_domain'] = 'Очаквано домейн име на IP адреса.';
$_['help_ip_time_zone'] = 'Очаквана часова зона на IP адреса.';
$_['help_ip_location'] = 'Очаквана локация на IP адреса.';
$_['help_ip_distance'] = 'Разстояние от IP до локацията на плащане.';
$_['help_ip_latitude'] = 'Очаквана географска ширина на IP адреса.';
$_['help_ip_longitude'] = 'Очаквана географска дължина на IP адреса.';
$_['help_risk_country'] = 'Дали IP адреса или адреса на плащане е в държава с потенциален риск.';
$_['help_free_email'] = 'Дали ел.поща е от безплатен тип доставчик.';
$_['help_ship_forward'] = 'Дали адреса за доставка е в базата данни на познатите пощенски адреси.';
$_['help_using_proxy'] = 'Дали IP адреса е от Anonymous Proxy Server.';
$_['help_bin_found'] = 'Дали BIN информация съвпада с BIN списъка.';
$_['help_email_blacklist'] = 'Дали ел. поща е в чрния списък на ел. пощи.';
$_['help_credit_card_blacklist'] = 'Дали кредитната карта е в чрния списък на кредитните карти.';
$_['help_score'] = 'Рисков резултат, 0 (малък риск) - 100 (висок риск).';
$_['help_status'] = 'FraudLabs Pro статут.';
$_['help_message'] = 'FraudLabs Pro грешка в описанието на съобщението.';
$_['help_transaction_id'] = 'Натиснете линка за да видите подробен анализ на измамата.';
$_['help_credits'] = 'Баланс на вашият акаунт, след извършване на транзакцията.';
// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте настройките!';
$_['error_key'] = 'Изискава се лиценз ключ!';
?>