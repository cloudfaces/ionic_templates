<?php
// Heading
$_['heading_title'] = 'Anti-Fraud IP';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Вие успешно променихте настройките!';
$_['text_edit'] = 'Редактиране на Anti-Fraud IP';
$_['text_ip_add'] = 'Добави IP адрес';
$_['text_ip_list'] = 'Fraud IP Списък с адреси';
// Column
$_['column_ip'] = 'IP адрес';
$_['column_total'] = 'Общо акаунти';
$_['column_date_added'] = 'Дата на добавяне';
$_['column_action'] = 'Действие';
// Entry
$_['entry_ip'] = 'IP адрес';
$_['entry_status'] = 'Състояние';
$_['entry_order_status'] = 'Подреждане';
// Help
$_['help_order_status'] = 'Клиенти, които имат блокирано IP в акаунта си ще бъдат зачислени към този статут на поръчка и няма да достигнат автоматично до завършване на поръчката.';
// Error
$_['error_permission'] = 'Внимание Вие нямате права да променяте настройките!';
?>