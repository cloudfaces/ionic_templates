<?php
// Text
$_['text_capture_status'] = 'Статус';
$_['text_amount_auth'] = 'Сумата е удостоверена';
$_['text_amount_captured'] = 'Сумата е таксувана';
$_['text_amount_refunded'] = 'Сумата е върната';
$_['text_transaction'] = 'Транзакция';
$_['text_complete'] = 'Завършен';
$_['text_confirm_void'] = 'Ако анулирате неможете за напред да таксувате други суми';
$_['text_view'] = 'Преглед';
$_['text_refund'] = 'Връщане';
$_['text_resend'] = 'Изпращане отново';
$_['text_success'] = 'Транзакцията е успешно изпратена';
$_['text_full_refund']		 = 'Пълен рефунд';
$_['text_partial_refund']	 = 'Частичен рефунд';
$_['text_payment']		 	 = 'Плащане';
$_['text_current_refunds'] = 'Връщането вече е осъществено за тази транзакция. максималната сума за рефунд е';
// Column
$_['column_transaction'] = 'Номер на транзакция';
$_['column_amount'] = 'Сума';
$_['column_type'] = 'Вид плащане';
$_['column_status'] = 'Статус';
$_['column_pending_reason'] = 'Причина за изчакване';
$_['column_date_added'] = 'Добавена';
$_['column_action'] = 'Действие';
// Entry
$_['entry_capture_amount']	 = 'Таксуване на сума';
$_['entry_capture_complete'] = 'Пълно таксуване';
$_['entry_full_refund']		 = 'Пълен рефунд';
$_['entry_amount']			 = 'Сума';
$_['entry_note']             = 'Бележка';
// Help
$_['help_capture_complete']  = 'Ако това е последното таксуване.';
// Tab
$_['tab_capture']		     = 'Таксуване';
$_['tab_refund']             = 'Рефунд';
// Button
$_['button_void'] = 'Анулиране';
$_['button_capture'] = 'Таксуване';

// Error
$_['error_capture'] = 'Въведи сума за таксуване';
$_['error_transaction'] = 'Транзакция не може да се извърши!';
$_['error_not_found'] = 'Транзакция не може да се намери!';
$_['error_partial_amt']		 = 'Трябва да въведете сума';
?>