<?php
// Heading
$_['heading_title']		 = 'Sage Payment Solutions (US)';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките на модула!';
$_['text_edit'] = 'Редкатиране';
// Entry
$_['entry_merchant_id'] = 'ID на търговеца:';
$_['entry_merchant_key'] = 'Търговец Ключ:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус поръчка:';
$_['entry_geo_zone'] = 'Географска зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте payment SagePay!';
$_['error_merchant_id'] = 'ID на търговеца се изисква!';
$_['error_merchant_key'] = 'Търговец Ключ се изисква!';
?>