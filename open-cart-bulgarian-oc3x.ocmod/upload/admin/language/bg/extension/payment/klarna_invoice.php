<?php
// Heading
$_['heading_title'] = 'Klarna Invoice';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успех: Вие променихте Klarna Invoice модул!';
$_['text_edit'] = 'Редактиране';
$_['text_klarna_invoice'] = '<a href="https://merchants.klarna.com/signup?locale=en&partner_id=d5c87110cebc383a826364769047042e777da5e8&utm_campaign=Platform&utm_medium=Partners&utm_source=Opencart" target="_blank"><img src="https://cdn.klarna.com/public/images/global/logos/v1/basic/global_basic_logo_std_blue-black.png?width=60&eid=opencart" alt="Klarna Invoice" title="Klarna Invoice" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live'] = 'Работещ режим';
$_['text_beta'] = 'Бета';
$_['text_sweden'] = 'Швеция';
$_['text_norway'] = 'Норвегия';
$_['text_finland'] = 'Финландия';
$_['text_denmark'] = 'Дания';
$_['text_germany'] = 'Германия';
$_['text_netherlands'] = 'Холандия';
// Entry
$_['entry_merchant'] = 'Klarna търговско ID:';
$_['entry_secret'] = 'Klarna тайна фраза:';
$_['entry_server'] = 'Сървър:';
$_['entry_total'] = 'Общо:';
$_['entry_pending_status'] = 'Очакващ одобрение:';
$_['entry_accepted_status'] = 'Одобрен:';
$_['entry_geo_zone'] = 'Географска зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_merchant'] = '(estore id) използва се за услугата (издава се от Klarna).';
$_['help_secret'] = 'Споделена тайна използва се за услугата (издава се от Klarna).';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';

// Error
$_['error_permission'] = 'Внимание: Вие нямате разрешение да променяте Klarna Фактури!';
?>