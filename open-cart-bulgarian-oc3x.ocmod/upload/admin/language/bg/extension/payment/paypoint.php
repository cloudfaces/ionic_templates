<?php
// Heading
$_['heading_title'] = 'PayPoint';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките на модула!';
$_['text_edit'] = 'Редкатиране';
$_['text_paypoint'] = '<a onclick="window.open(\'https://www.paypoint.net/partners/opencart\');"><img src="view/image/payment/paypoint.png" alt="PayPoint" title="PayPoint" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live'] = 'В продукция';
$_['text_successful'] = 'Винаги е успешно';
$_['text_fail'] = 'винаги е неуспешно';
// Entry
$_['entry_merchant'] = 'ID на търговеца:';
$_['entry_password'] = 'Парола:';
$_['entry_test'] = 'Тестов режим:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус поръчка:';
$_['entry_geo_zone'] = 'Географска зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_password'] = 'Оставете празно ако нямате "Digest Key Authentication" разрешен във вашият акаунт.';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';

// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте payment PayPoint!';
$_['error_merchant'] = 'ID на търговеца се изисква!';
?>