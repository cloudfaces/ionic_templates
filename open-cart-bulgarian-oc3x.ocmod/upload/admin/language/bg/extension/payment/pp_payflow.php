<?php
// Heading
$_['heading_title']					= 'PayPal Payflow Pro';
// Text$_['text_extension']     = 'Extensions';$_['text_success']					= 'Успешно променихте настройките!';$_['text_edit']                     = 'Редактиране';$_['text_pp_payflow']				= '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';$_['text_authorization']			= 'Удостоверяване';$_['text_sale']						= 'Продажби';
// Entry
$_['entry_vendor']					= 'Издател';
$_['entry_user']					= 'Потребител';
$_['entry_password']				= 'Парола';
$_['entry_partner']					= 'Партньор';
$_['entry_test']					= 'Тестов мод';$_['entry_transaction']				= 'Транзакционен мод';
$_['entry_total']					= 'Общо';$_['entry_order_status']			= 'Статус на поръчка';
$_['entry_geo_zone']				= 'Гео зона';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Подреждане';
// Help$_['help_vendor']					= 'Вашият търковси номер, който получихте при регистрацията';$_['help_user']						= 'Ако сте настроили един или повече допълнителни акуната, тази стойност е номерът на потребителя, който е упълномощен да изпълни транзакцията. Ако не сте настройвали допълнителни акаунти, то потребителя има същият номер като издетелят';$_['help_password']					= 'Пароалта която сте задали при създаването на акунта';$_['help_partner']					= 'Номерът предостаен от упълномощените PayPal реселъри, които са се регистрирали за Payflow SDK.';$_['help_test']						= 'Използване на директиня или тестовия (sandbox) сървър за изпълнение на поръчката?';$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
// Error$_['error_permission']				= 'Внимание вие нямате права да редактирате този модул!';$_['error_vendor']					= 'изисква се издател!';$_['error_user']					= 'Изисква се потребител!';$_['error_password']				= 'Изисква се парола!';$_['error_partner']					= 'Изисква се партньор!';
