<?php
// Heading
$_['heading_title'] = 'First Data EMEA Connect (3DSecure enabled)';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте модула!';
$_['text_edit'] = 'Редактиране на модула';
$_['text_notification_url'] = 'URL за известяване';
$_['text_live'] = 'Жива връзка';
$_['text_demo'] = 'Демо';
$_['text_enabled'] = 'Разрешен';
$_['text_merchant_id'] = 'ID на магазина';
$_['text_secret'] = 'Споделена тайна';
$_['text_capture_ok'] = 'Плащането е успешно';
$_['text_capture_ok_order'] = 'Плащането е успешно, статуса на поръчката беше променен на - установен';
$_['text_void_ok'] = 'Анулирането беше успешно, поръчката беше променена на статус - анулиран';
$_['text_settle_auto'] = 'Продажба';
$_['text_settle_delayed'] = 'Предварително удостверяване';
$_['text_success_void'] = 'Транзакцията беше анулирана';
$_['text_success_capture'] = 'Транзакцията беше успешна';
$_['text_firstdata'] = '<img src="view/image/payment/firstdata.png" alt="First Data" title="First Data" style="border: 1px solid #EEEEEE;" />';
$_['text_payment_info'] = 'Информация за плащане';
$_['text_capture_status'] = 'Успешно плащане';
$_['text_void_status'] = 'Анулирано плащане';
$_['text_order_ref'] = 'Поръчка Реф.';
$_['text_order_total'] = 'Общо удостоверени';
$_['text_total_captured'] = 'Общо усешни';
$_['text_transactions'] = 'Транзакции';
$_['text_column_amount'] = 'Сума';
$_['text_column_type'] = 'Вид';
$_['text_column_date_added'] = 'Добавени';
$_['text_confirm_void'] = 'Сигурни ли сте, че искате да анулирате това плащане?';
$_['text_confirm_capture'] = 'Сигурни ли сте, че искате да изпълните това плащане?';
// Entry
$_['entry_merchant_id'] = 'ID на магазина';
$_['entry_secret'] = 'Споделена тайна';
$_['entry_total'] = 'Общо';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_geo_zone'] = 'Гео зона';
$_['entry_status'] = 'Статус';
$_['entry_debug'] = 'Отстраняане на грешки';
$_['entry_live_demo'] = 'На живо / Деом';
$_['entry_auto_settle'] = 'Вид връзка';
$_['entry_card_select'] = 'Избери карта';
$_['entry_tss_check'] = 'TSS проверка';
$_['entry_live_url'] = 'URL за жива връзка';
$_['entry_demo_url'] = 'URL за демовръзка';
$_['entry_status_success_settled'] = 'Успешно - изплатен';
$_['entry_status_success_unsettled'] = 'Успешно - не е изплатен';
$_['entry_status_decline'] = 'Отхвърляне';
$_['entry_status_void'] = 'Анулирани';
$_['entry_enable_card_store'] = 'Разреши отговори от картите';
// Help
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
$_['help_notification'] = 'Трябва да доставите този URL в First Data, за да получавате известия за плащанията';
$_['help_debug'] = 'Активиране отстраняване на грешки ще запише голямо количество данни в лог файла. Винаги трябва да се деактивирано, освен ако не е указано друго';
$_['help_settle'] = 'Ако използвате пре-авторизиране трябва да завършите след-авторизиране в рамките на 3-5 дена в противен случай транзакцията ще отпадне';
// Tab
$_['tab_account'] = 'API инфо';
$_['tab_order_status'] = 'Статус на поръчки';
$_['tab_payment'] = 'Настройки плащане';
$_['tab_advanced'] = 'За напреднали';
// Button
$_['button_capture'] = 'Улавяне';
$_['button_void'] = 'Анулиране';

// Error
$_['error_merchant_id'] = 'Изисква се ноемр на магазина';
$_['error_secret'] = 'Изисква се споделена тайна';
$_['error_live_url'] = 'Изисква се URL за жива връзка';
$_['error_demo_url'] = 'Изисква се URL за демо връзка';
$_['error_data_missing'] = 'Липсват данни';
$_['error_void_error'] = 'Неуспешно анулиране';
$_['error_capture_error'] = 'Неуспешно улавяне';
?>