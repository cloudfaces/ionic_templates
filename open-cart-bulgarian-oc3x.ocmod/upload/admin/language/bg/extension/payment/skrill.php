<?php
// Heading
$_['heading_title'] = 'Skrill';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките.';
$_['text_edit'] = 'Редактиране';
$_['text_Skrill'] = '<a href="https://www.moneybookers.com/partners/?p=OpenCart" target="_blank"><img src="view/image/payment/moneybookers.png" alt="Skrill" title="Skrill" style="border: 1px solid #EEEEEE;" /></a>';
// Entry
$_['entry_email'] = 'E-Mail';
$_['entry_secret'] = 'Таен';
$_['entry_total'] = 'Общо';
$_['entry_order_status'] = 'Статутси на поръчка';
$_['entry_pending_status'] = 'Статус изчакваща';
$_['entry_canceled_status'] = 'Статус отменена';
$_['entry_failed_status'] = 'Статус неуспешна';
$_['entry_chargeback_status'] = 'Статус повторно таксуване';
$_['entry_geo_zone'] = 'Гео зона';
$_['entry_status'] = 'Статутси';
$_['entry_sort_order'] = 'Подреждане';
// Help
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
// Error
$_['error_permission'] = 'Нямате права да променяте настройките!';
$_['error_email'] = 'Изисква се E-Mail!';
?>