<?php
// Heading
$_['heading_title'] = '2Checkout';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките на модула!';
$_['text_edit'] = 'Редактиране';
$_['text_twocheckout'] = '<a onclick="window.open(\'https://www.2checkout.com/2co/affiliate?affiliate=1596408\');"><img src="view/image/payment/2checkout.png" alt="2Checkout" title="2Checkout" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_account'] = 'Номер на профил:';
$_['entry_secret'] = 'Тайна дума:';
$_['entry_display'] = 'Директно плащане';
$_['entry_test'] = 'Тест режим:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус поръчка:';
$_['entry_geo_zone'] = 'Географска зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_secret'] = 'Тайната дума, с която да потвърдите плащането, трябва да е същата като определната в акаунта на търговеца.';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
$_['error_permission'] = 'Внимание: Вие нямате права да променяте настройките!';
$_['error_account'] = 'Account No. се изисква!';
$_['error_secret'] = 'Тайна дума се изисква!';
?>