<?php
// Heading
$_['heading_title'] = 'Безплатна поръчка';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте параметрите на Безплатна поръчка!';
$_['text_edit'] = 'Редактиране';
// Entry
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Error
$_['error_permission'] = 'Внимание: Нямате права да променяте параметрите на Безплатна поръчка!';
?>