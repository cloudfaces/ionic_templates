<?php
$_['heading_title'] = 'Payza';
$_['text_payment'] = 'Плащане';
$_['text_success'] = 'Успешно променихте настройките!';
$_['text_edit'] = 'Редактиране';
$_['entry_merchant'] = 'Търговско ID:';
$_['entry_security'] = 'Код за сигурност:';
$_['entry_callback'] = 'Аларма URL:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус на поръкча:';
$_['entry_geo_zone'] = 'Гео зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
$_['help_callback'] = 'Това трябва да е настроено в контролния панел на Payza. Трябва да е отбелязано и "IPN Status" за да се разреши.';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
$_['error_permission'] = 'Нямате права да променяте модула!';
$_['error_merchant'] = 'Изисква се търговско ID!';
$_['error_security'] = 'Изисква се код за сигурност!';
$_['text_extension'] = 'Допълнения';
?>