<?php
// Heading$_['heading_title']					= 'Връщане на транзакция';
// Text
$_['text_pp_express']				= 'PayPal Express Checkout';$_['text_current_refunds']			= 'Връщането за тази транзакция вече беше извършено.';
$_['text_refund']					= 'Refund';
// Entry$_['entry_transaction_id']			= 'Номер на транзакция';
$_['entry_full_refund']				= 'Пълно връщане';
$_['entry_amount']					= 'Сума';
$_['entry_message']					= 'Съобщение';
// Button$_['button_refund']					= 'Разпореди връщане';
// Error$_['error_partial_amt']				= 'Вие трябва да въведете сума за връщане';$_['error_data']					= 'В завката липсва информация';