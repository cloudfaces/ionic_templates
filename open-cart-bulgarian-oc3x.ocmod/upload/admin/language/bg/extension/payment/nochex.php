<?php
// Heading
$_['heading_title'] = 'NOCHEX';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките на модула!';
$_['text_edit'] = 'Редкатиране';
$_['text_nochex'] = '<a onclick="window.open(\'https://secure.nochex.com/apply/merchant_info.aspx?partner_id=172198798\');"><img src="view/image/payment/nochex.png" alt="NOCHEX" title="NOCHEX" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_seller'] = 'Продавач / Лични данни';
$_['text_merchant'] = 'Търговски профил';
// Entry
$_['entry_email'] = 'E-Mail:';
$_['entry_account'] = 'Тип профил:';
$_['entry_merchant'] = 'ID на търговеца:';
$_['entry_template'] = 'Сложи шаблон:';
$_['entry_test'] = 'Тест:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус поръчка:';
$_['entry_geo_zone'] = 'Географска зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';

// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте payment NOCHEX!';
$_['error_email'] = 'Невалиден E-Mail адрес';
$_['error_merchant'] = 'ID на търговеца се изисква!';
?>