<?php
// Heading
$_['heading_title']      = 'PayPal Pro';
// Text $_['text_extension']     = 'Допълнения';
$_['text_success']       = 'Успешно променихте настройките на модула!';
$_['text_edit']                     = 'Редкатиране';
$_['text_pp_pro']        = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Оторизация';
$_['text_sale']          = 'Продажба';
// Entry
$_['entry_username']     = 'API потребител:';
$_['entry_password']     = 'API парола:';
$_['entry_signature']    = 'API подпис:';
$_['entry_test']         = 'Тестов модел:';
$_['entry_transaction']  = 'Транзакция:';
$_['entry_total']        = 'Общо:';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone']     = 'Гео зона:';
$_['entry_status']       = 'Статус:'; 
$_['entry_sort_order']   = 'Подреждане:';
// Help
$_['help_test']		= 'Използване на директиня или тестовия (sandbox) сървър за изпълнение на поръчката?';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
// Error
$_['error_permission']   = 'Внимание: Нямате права да променяте параметрите на PayPal Website Payment Pro!';
$_['error_username']     = 'Необходимо е да въведете API потребител!'; 
$_['error_password']     = 'Необходимо е да въведете API парола!'; 
$_['error_signature']    = 'Необходимо е да въведете API подпис!';
