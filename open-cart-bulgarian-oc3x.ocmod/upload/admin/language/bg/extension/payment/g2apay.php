<?php
// Heading
$_['heading_title'] = 'G2APay';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Вие успешно променихте настройките.';
$_['text_edit'] = 'Редактирай G2APay';
$_['text_g2apay'] = '<a href="https://pay.g2a.com/" target="_blank"><img src="view/image/payment/g2apay.png" alt="G2APay" title="G2APay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_payment_info'] = 'Информация за плащане';
$_['text_refund_status'] = 'Връщане на плащане';
$_['text_order_ref'] = 'Референтен номер на поръчка';
$_['text_order_total'] = 'Общо упълномощени поръчки';
$_['text_total_released'] = 'Общо освободени поръчки';
$_['text_transactions'] = 'Транзакции';
$_['text_column_amount'] = 'Сума';
$_['text_column_type'] = 'Вид';
$_['text_column_date_added'] = 'Добавени на';
$_['text_refund_ok'] = 'Успешно е изискано връщане на средства';
$_['text_refund_ok_order'] = 'Успешно е изискано връщане на средства, сумата е върната';
// Entry
$_['entry_username'] = 'Потрбителско име';
$_['entry_secret'] = 'Таен';
$_['entry_api_hash'] = 'API Hash';
$_['entry_environment'] = 'Среда';
$_['entry_secret_token'] = 'Таен ключ';
$_['entry_ipn_url'] = 'IPN URL:';
$_['entry_total'] = 'Общо';
$_['entry_geo_zone'] = 'Гео зона';
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_debug'] = 'За разработчици';
$_['entry_order_status'] = 'Статус на поръчка';
$_['entry_complete_status'] = 'Завършен статус:';
$_['entry_rejected_status'] = 'Отхвърлен статус:';
$_['entry_cancelled_status'] = 'Отменен статус:';
$_['entry_pending_status']            = 'Pending Status:';
$_['entry_refunded_status'] = 'Връщане статус:';
$_['entry_partially_refunded_status'] = 'Частично връщане статус:';
// Help
$_['help_username'] = 'Ел. поща на вашият профил';
$_['help_total'] = 'Минимална сума за поръчката.';
$_['help_debug'] = 'Разрешаването на тази опция ще запише информация в лог файл';
// Tab
$_['tab_settings'] = 'Настройки';
$_['tab_order_status'] = 'Статус на поръчка';
// Error
$_['error_permission'] = 'Внимание Вие нямате права да променяте настройките!';
$_['error_email'] = 'Липсва ел. поща!';
$_['error_secret'] = 'Липсва таен въпрос!';
$_['error_api_hash'] = 'API Hash Required!';
//Button
$_['btn_refund'] = 'връщане';
$_['g2apay_environment_live'] = 'На живо';
$_['g2apay_environment_test'] = 'Тест';
?>