<?php
// Heading
$_['heading_title'] = 'Paymate';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките на модула!';
$_['text_edit'] = 'Редкатиране';
$_['text_paymate'] = '<img src="view/image/payment/paymate.png" alt="Paymate" title="Paymate" style="border: 1px solid #EEEEEE;" />';
// Entry
$_['entry_username'] = 'Потребителско име:';
$_['entry_password'] = 'Парола:';
$_['entry_test'] = 'Тестов режим:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус поръчка:';
$_['entry_geo_zone'] = 'Географска зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_password'] = 'Използвайте случайна парола.';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';

// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте payment Paymate!';
$_['error_username'] = 'Изисква се потребителско име!';
$_['error_password'] = 'Изисква се парола!';
?>