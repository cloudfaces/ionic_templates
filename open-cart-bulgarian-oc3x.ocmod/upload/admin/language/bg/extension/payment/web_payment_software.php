<?php
// Heading
$_['heading_title'] = 'Web Payment Software';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките на модула!';
$_['text_edit'] = 'Edit AWeb Payment Software';
$_['text_web_payment_software'] = '<a onclick="window.open(\'http://www.web-payment-software.com/\');"><img src="view/image/payment/wps-logo.jpg" alt="Web Payment Software" title="Web Payment Software" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_test'] = 'Тест';
$_['text_live'] = 'На живо';
$_['text_authorization'] = 'Авторизация';
$_['text_capture'] = 'Залавяне';
// Entry
$_['entry_login'] = 'ID на търговеца:';
$_['entry_key'] = 'Търговец Ключ:';
$_['entry_mode'] = 'Транзакция режим:';
$_['entry_method'] = 'Транзакция метод:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус поръчка:';
$_['entry_geo_zone'] = 'Географска зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
$_['error_permission'] = 'Нямате права да редактирате настройките!';
$_['error_login'] = 'Изисква се ID за вход!';
$_['error_key'] = 'Изисква се транзакционен ключ!';
?>