<?php
// Heading
$_['heading_title'] = 'Authorize.Net (AIM)';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките на модула!';
$_['text_edit'] = 'Редактиране';
$_['text_authorizenet_sim'] = '<a onclick="window.open(\'http://reseller.authorize.net/application/?id=5561142\');"><img src="view/image/payment/authorizenet.png" alt="Authorize.Net" title="Authorize.Net" style="border: 1px solid #EEEEEE;" /></a>';
// Entry
$_['entry_merchant'] = 'ID на търговеца :';
$_['entry_key'] = 'Транзакция Ключ:';
$_['entry_callback'] = 'Обратен отговор URL:';
$_['entry_md5'] = 'MD5 Хаш стойност';
$_['entry_test'] = 'Тест режим:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус поръчка:';
$_['entry_geo_zone'] = 'Географска зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_callback'] = 'Моля влезте и настройте това на <a href="https://secure.authorize.net" target="_blank" class="txtLink">https://secure.authorize.net</a>.';
$_['help_md5'] = 'MD5 хаш опцията Ви позволява да разпознаете, че транзакционият отговор е защитено получен от Authorize.Net. Моля влезнете в системата и настройте това тук <a href="https://secure.authorize.net" target="_blank" class="txtLink">https://secure.authorize.net</a>.(Optional)';
$_['help_total'] = 'Общакрайна сума, която трябва да се достигне, за да стане плащането активно.';
// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте плащания Authorize.Net (AIM)!';
$_['error_merchant'] = 'ID на търговеца се изисква!';
$_['error_key'] = 'Транзакция Ключ се изисква!';
?>