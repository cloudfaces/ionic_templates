<?php
// Heading
$_['heading_title'] = 'Perpetual Payments';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките на модула!';
$_['text_edit'] = 'Редактиране';
// Entry
$_['entry_auth_id'] = 'ID на профила:';
$_['entry_auth_pass'] = 'Парола:';
$_['entry_test'] = 'Тест режим:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус поръчка:';
$_['entry_geo_zone'] = 'Географска зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_test'] = 'Използвай този модул в тестов режим (YES) или производствен режим (NO)?';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';

// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте настройките на модула!';
$_['error_auth_id'] = 'Изисква се ID на профила!';
$_['error_auth_pass'] = 'Изисква се парола!';
?>