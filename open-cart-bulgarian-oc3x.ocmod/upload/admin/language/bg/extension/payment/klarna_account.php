<?php
// Heading
$_['heading_title'] = 'Klarna Профил';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките!';
$_['text_edit'] = 'Редактиране';
$_['text_klarna_account'] = '<a href="https://merchants.klarna.com/signup?locale=en&partner_id=d5c87110cebc383a826364769047042e777da5e8&utm_campaign=Platform&utm_medium=Partners&utm_source=Opencart" target="_blank"><img src="https://cdn.klarna.com/public/images/global/logos/v1/basic/global_basic_logo_std_blue-black.png?width=60&eid=opencart" alt="Klarna Account" title="Klarna Account" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live'] = 'Жива връзка';
$_['text_beta'] = 'Бета връзка';
$_['text_sweden'] = 'Швеция';
$_['text_norway'] = 'Норвегия';
$_['text_finland'] = 'Финландия';
$_['text_denmark'] = 'Дания';
$_['text_germany'] = 'Германия';
$_['text_netherlands'] = 'Холандия';
// Entry
$_['entry_merchant'] = 'Klarna номер на търговец:';
$_['entry_secret'] = 'Klarna таен:';
$_['entry_server'] = 'Сървър';
$_['entry_total'] = 'Общо:';
$_['entry_pending_status'] = 'Изчакващ статус:';
$_['entry_accepted_status'] = 'Приет статус:';
$_['entry_geo_zone'] = 'Гео Зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_merchant'] = '(estore id) за използване услугите на Кларна (издаден от Klarna).';
$_['help_secret'] = 'Споделен таен за използване услугите на Кларна (издаден от Klarna).';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';

// Error
$_['error_permission'] = 'Нямате права да редактирате модула!';
$_['error_pclass'] = 'Неуспешно извличане pClass за %s. Грешка: %s; Съобщение за грешка: %s';
$_['error_curl'] = 'Curl грешка - Код: %d; Съобщение: %s';
$_['error_log'] = 'Възникна грешка по време на обновяването. Моля прегледайте лог файла';
?>