<?php
// Heading
$_['heading_title'] = 'Realex пренасочване';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките!';
$_['text_edit'] = 'Редактиране';
$_['text_live'] = 'Директни продажби';
$_['text_demo'] = 'Демо';
$_['text_card_type'] = 'Вид карта';
$_['text_enabled'] = 'Разрешено';
$_['text_use_default'] = 'Използвай по подразбране';
$_['text_merchant_id'] = 'Номер на търговеца';
$_['text_subaccount'] = 'Подакаунт';
$_['text_secret'] = 'Таен ключ';
$_['text_card_visa'] = 'Visa';
$_['text_card_master'] = 'Mastercard';
$_['text_card_amex'] = 'American Express';
$_['text_card_switch'] = 'Switch/Maestro';
$_['text_card_laser'] = 'Laser';
$_['text_card_diners'] = 'Diners';
$_['text_capture_ok'] = 'Таксуването беше успешно';
$_['text_capture_ok_order'] = 'Таксуването беше успешно, статута на поръчката се обнови';
$_['text_rebate_ok'] = 'Отстъпката е успешна';
$_['text_rebate_ok_order'] = 'Отстъпката беше успешна, статута на поръчката се обнови';
$_['text_void_ok'] = 'Анулирането беше успешно, статута на поръчката се обнови';
$_['text_settle_auto'] = 'Автоматично';
$_['text_settle_delayed'] = 'Закъсняло';
$_['text_settle_multi'] = 'Мулти';
$_['text_url_message'] = 'Вие трябва да поставите URL на магазина преди вашият акаунт да бъде активиран';
$_['text_payment_info'] = 'Платежна информация';
$_['text_capture_status'] = 'Плащането е успешно';
$_['text_void_status'] = 'Плащането е анулирано';
$_['text_rebate_status'] = 'Плащането е с отстъпка';
$_['text_order_ref'] = 'Поръчка реф.';
$_['text_order_total'] = 'Общо удостоверени';
$_['text_total_captured'] = 'Общоо успешни';
$_['text_transactions'] = 'Транзакции';
$_['text_column_amount'] = 'Сума';
$_['text_column_type'] = 'Вид';
$_['text_column_date_added'] = 'Създадена';
$_['text_confirm_void'] = 'Сигурни ли сте, че искате да анулирате плащането?';
$_['text_confirm_capture'] = 'Сигурни ли сте, че искате да извършите плащането?';
$_['text_confirm_rebate'] = 'Сигурни ли сте, че искате да направите отстъпка с плащането?';
$_['text_realex'] = '<a target="_blank" href="http://www.realexpayments.co.uk/partner-refer?id=opencart"><img src="view/image/payment/realex.png" alt="Realex" title="Realex" style="border: 1px solid #EEEEEE;" /></a>';
// Entry
$_['entry_merchant_id'] = 'Номер на търговеца';
$_['entry_secret'] = 'Таен ключ';
$_['entry_rebate_password'] = 'Парола';
$_['entry_total'] = 'Общо';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_geo_zone'] = 'Гео зона';
$_['entry_status'] = 'Статус';
$_['entry_debug'] = 'Отстраняване на грешки';
$_['entry_live_demo'] = 'Жив /Демо';
$_['entry_auto_settle'] = 'Вид споразумение';
$_['entry_card_select'] = 'Избери карта';
$_['entry_tss_check'] = 'TSS проверки';
$_['entry_live_url'] = 'URL за жива връзка';
$_['entry_demo_url'] = 'URL за демо връзка';
$_['entry_status_success_settled'] = 'Успешно - изплатен';
$_['entry_status_success_unsettled'] = 'Успешно - не изплатен';
$_['entry_status_decline'] = 'Отхвърлен';
$_['entry_status_decline_pending'] = 'Отхвърлен - удостоверяване извън линия';
$_['entry_status_decline_stolen'] = 'Отхвърлен - изгубена или открадната карта';
$_['entry_status_decline_bank'] = 'Отхвърлен - грешка на банката';
$_['entry_status_void'] = 'Анулирани';
$_['entry_status_rebate'] = 'Отстъпени';
$_['entry_notification_url'] = 'URL за известия';
// Help
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
$_['help_card_select'] = 'Помоли потребителят да посочи вида карта, преди да е пренасочен';
$_['help_notification'] = 'Трябва да попълните този URL ако искате да получавате известия';
$_['help_debug'] = 'Допълнителна информация';
$_['help_dcc_settle'] = 'Ако вашият акаунт е DCC разрешен, вие трябва да използвате Autosettle';
// Tab
$_['tab_api'] = 'API детайли';
$_['tab_account'] = 'Акаунти';
$_['tab_order_status'] = 'Статутси на поръчка';
$_['tab_payment'] = 'Настройки за плащане';
$_['tab_advanced'] = 'За напреднали';
// Button
$_['button_capture'] = 'Таксуване';
$_['button_rebate'] = 'Отстъпка / Връщане';
$_['button_void'] = 'Анулиране';
// Error
$_['error_merchant_id'] = 'Изисква се номер на търговеца';
$_['error_secret'] = 'Изисква се таен ключ';
$_['error_live_url'] = 'Изисква се адрес за живата връзка';
$_['error_demo_url'] = 'Изисква се адрес за демо връзка';
$_['error_data_missing'] = 'Липсваща информация';
$_['error_use_select_card'] = 'Вие трябва да сте разрешили "Избор на карта" за вашият субакаунт, за да работи';
?>