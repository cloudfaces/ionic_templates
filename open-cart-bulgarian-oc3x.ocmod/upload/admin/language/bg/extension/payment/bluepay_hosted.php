<?php
// Heading
$_['heading_title'] = 'BluePay заявка за хостинг';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успех!: Вие запазихте настройките успешно!';
$_['text_edit'] = 'Редактиране на модула';
$_['text_bluepay_hosted'] = '<a href="http://www.bluepay.com/preferred-partner/opencart" target="_blank"><img src="view/image/payment/bluepay.jpg" alt="BluePay хостинг" title="BluePay хостинг" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_test'] = 'Тест';
$_['text_live'] = 'Жива връзка';
$_['text_sale'] = 'Продажба';
$_['text_authenticate'] = 'Удостоверяване';
$_['text_release_ok'] = 'Освобождаването бе успешно';
$_['text_release_ok_order'] = 'Освобождаването бе успешно';
$_['text_rebate_ok'] = 'Отстъпката беше успешна';
$_['text_rebate_ok_order'] = 'Отстъпка е била успешна, статуса на поръчката актуализиран, на отстъпен';
$_['text_void_ok'] = 'Анулирането е било успешено, състоянието на поръчката актуализиран до анулиран';
$_['text_payment_info'] = 'Информация за разплащане';
$_['text_release_status'] = 'Плащане освобождаване';
$_['text_void_status'] = 'Плащане анулиране';
$_['text_rebate_status'] = 'Плащане отстъпка';
$_['text_order_ref'] = 'Поръчка Реф.';
$_['text_order_total'] = 'Общо разрешен';
$_['text_total_released'] = 'Общо освободен';
$_['text_transactions'] = 'Сделки';
$_['text_column_amount'] = 'Сума';
$_['text_column_type'] = 'Вид';
$_['text_column_date_added'] = 'Добавено';
$_['text_confirm_void'] = 'Сигурни ли сте, искате да се анулира плащането?';
$_['text_confirm_release'] = 'Сигурни ли сте, искате да се освободите плащането?';
$_['text_confirm_rebate'] = 'Сигурни ли сте, искате да направите отстъпката в плащането?';
// Entry
$_['entry_account_name'] = 'Името на профил';
$_['entry_account_id'] = 'ID профил';
$_['entry_secret_key'] = 'Secret Key';
$_['entry_test'] = 'Тест';
$_['entry_transaction'] = 'Метод на сделка';
$_['entry_card_amex'] = 'Amex';
$_['entry_card_discover'] = 'Discover';
$_['entry_total'] = 'Общо';
$_['entry_order_status'] = 'Състояние на поръчките';
$_['entry_geo_zone'] = 'Гео Зона';
$_['entry_status'] = 'Състояние';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_debug'] = 'Отстраняване на грешки';
// Help
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
$_['help_debug'] = 'Активиране отстраняване на грешки ще запише голямо количество данни в лог файла. Винаги трябва да се деактивирано, освен ако не е указано друго';
$_['help_transaction'] = 'Метода за сделките трябва да се настрои на "плащане", за да се даде възможност за абонаментни  плащания';
$_['help_cron_job_token'] = 'Направи това дълго и трудно за отгатване';
$_['help_cron_job_url'] = 'Настройте Cron job, за да извика този URL';
// Button
$_['button_release'] = 'Освобождаване';
$_['button_rebate'] = 'Отстъпка / Възстановяване';
$_['button_void'] = 'Анулиране';
// Error
$_['error_permission'] = 'Внимание: Вие нямате право да променяте плащане BluePay';
$_['error_account_name'] = 'Име задължително!';
$_['error_account_id'] = 'Номер на акаунт задължителен!';
$_['error_secret_key'] = 'Secret Key Задължително!';
?>