<?php
// Heading
$_['heading_title'] = 'LIQPAY';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките на модула!';
$_['text_edit'] = 'Редактиране';
$_['text_pay'] = 'LIQPAY';
$_['text_card'] = 'Кредитна карта';
$_['text_liqpay'] = '<img src="view/image/payment/liqpay.png" alt="LIQPAY" title="LIQPAY" style="border: 1px solid #EEEEEE;" />';
// Entry
$_['entry_merchant'] = 'ID на търговеца:';
$_['entry_signature'] = 'Подпис:';
$_['entry_type'] = 'Вид:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус поръчка:';
$_['entry_geo_zone'] = 'Географска зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';

// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте настройките!';
$_['error_merchant'] = 'ID на търговеца се изисква!';
$_['error_signature'] = 'Изисква се подпис!';
?>