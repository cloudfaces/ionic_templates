<?php
// Heading
$_['heading_title'] = 'Банков превод';
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте параметрите на Банков превод!';
$_['text_edit'] = 'Редактиране';
// Entry
$_['entry_bank'] = 'Инструкции за банков превод:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone'] = 'Гео зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Help
$_['help_total'] = 'Общатата сума на поръчката трябва да достигне преди този начин на плащане да стане активно.';

// Error 
$_['error_permission'] = 'Внимание: Нямате права да променяте параметрите на Банков превод!';
$_['error_bank'] = 'Необходимо е да въведете инструкции за банков превод!';
?>