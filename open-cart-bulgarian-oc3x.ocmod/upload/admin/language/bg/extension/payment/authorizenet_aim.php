<?php
// Heading
$_['heading_title'] = 'Authorize.Net (AIM)';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките на модула!';
$_['text_edit'] = 'Редактиране Authorize.Net (AIM)';
$_['text_test'] = 'Тест';
$_['text_live'] = 'На живо';
$_['text_authorization'] = 'Авторизация';
$_['text_capture'] = 'Улавяне';
$_['text_authorizenet_aim'] = '<a onclick="window.open(\'http://reseller.authorize.net/application/?id=5561142\');"><img src="view/image/payment/authorizenet.png" alt="Authorize.Net" title="Authorize.Net" style="border: 1px solid #EEEEEE;" /></a>';
// Entry
$_['entry_login'] = 'Вход ID';
$_['entry_key'] = 'Ключ за транзакция';
$_['entry_hash'] = 'MD5 Хаш';
$_['entry_server'] = 'Сървър за транзакции';
$_['entry_mode'] = 'Модул за транзакции';
$_['entry_method'] = 'Метод на транзакции';
$_['entry_total'] = 'Общо';
$_['entry_order_status'] = 'Статус на поръчките';
$_['entry_geo_zone'] = 'Географска зона';
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Подреждане';
// Help
$_['help_total'] = 'Общакрайна сума, която трябва да се достигне, за да стане плащането активно.';
// Error
$_['error_permission'] = 'Внимание: Вие нямате право да редактирате модула!';
$_['error_login'] = 'Изисква се ID!';
$_['error_key'] = 'Изисква се ключ за транзакция!';
?>