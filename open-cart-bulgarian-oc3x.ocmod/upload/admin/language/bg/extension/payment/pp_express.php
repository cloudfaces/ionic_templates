<?php
// Heading
$_['heading_title'] = 'PayPal Express';
// Text
$_['text_extension']				 = 'Допълнения';
$_['text_success'] = 'Успешно променихте параметрите на PayPal Express Checkout!';
$_['text_edit'] = 'Редкатиране';
$_['text_pp_express'] = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Аторизация';
$_['text_sale'] = 'Продажба';
$_['text_signup'] = 'Регистрирай се в PayPal  - запази настройките се първо, защото страницата ще се презареди';
$_['text_sandbox'] = 'Регистрирай се в PayPal Sandbox - запази настройките се първо, защото страницата ще се презареди';
$_['text_configure_live']            = 'Configure Live';
$_['text_configure_sandbox']         = 'Configure Sandbox';
$_['text_show_advanced']             = 'Show Advanced';
$_['text_show_quick_setup']          = 'Show Quick Setup';
$_['text_quick_setup']             	 = 'Quick setup - Link an existing or create a new PayPal account to start accepting payments in minutes';
$_['text_paypal_consent']		 	 = 'By using the quick setup tool you allow PayPal to receive information about your store';
$_['text_success_connect']			 = 'Success: You have connected your PayPal account!';
$_['text_preferred_main']		 	 = 'Gives your buyers a simplified checkout experience on multiple devices that keeps them local to your website throughout the payment authorization process';
$_['text_learn_more']			 	 = '(Learn more)';
$_['text_preferred_li_1']			 = 'Start accepting PayPal in three clicks';
$_['text_preferred_li_2']			 = 'Accept payments from around the world';
$_['text_preferred_li_3']			 = 'Offer Express Checkout Shortcut, letting buyers checkout directly from your basket page';
$_['text_preferred_li_4']			 = 'Improve conversion with PayPal One Touch and In-Context checkout';
$_['text_connect_paypal']			 = 'Connect with PayPal';
$_['text_incontext_not_supported']	 = '* Not supported with In-Context Checkout';
$_['text_retrieve']	 				 = 'Your details have been entered from PayPal';
$_['text_enable_button']			 = 'We recommend offering PayPal Express Shortcut to maximise checkout conversion, this allows customers to use their PayPal address book and <strong>checkout is as little as three taps</strong> from the basket page. Click enable to install the extension and access the layout manager, you will ned to add "PayPal Express Checkout Button" to the checkout layout';
// Entry
$_['entry_username'] = 'API Username';
$_['entry_password'] = 'API Password';
$_['entry_signature'] = 'API Signature';
$_['entry_sandbox_username'] = 'API Sandbox Username';
$_['entry_sandbox_password'] = 'API Sandbox Password';
$_['entry_sandbox_signature'] = 'API Sandbox Signature';
$_['entry_ipn'] = 'IPN URL';
$_['entry_test'] = 'Тестов модел:';
$_['entry_debug'] = 'Отстраняване на грекшки';
$_['entry_currency'] = 'Валутапо подразбиране';
$_['entry_recurring_cancel'] = 'Разреш на клиентите да отменят периодичните плащания';
$_['entry_transaction'] = 'Метод на транзакция';
$_['entry_total'] = 'Общо';
$_['entry_geo_zone'] = 'Гео зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
$_['entry_canceled_reversal_status'] = 'Статус обратно връщане';
$_['entry_completed_status'] = 'Завършен статус';
$_['entry_denied_status'] = 'Отхвърлен статус';
$_['entry_expired_status'] = 'Изтекъл статус';
$_['entry_failed_status'] = 'Неуспешен статус';
$_['entry_pending_status'] = 'Изчакващ статус';
$_['entry_processed_status'] = 'В процес на обработка статус';
$_['entry_refunded_status'] = 'Върнат статус';
$_['entry_reversed_status'] = 'Обърнат статус';
$_['entry_voided_status'] = 'Отменен статус';
$_['entry_allow_notes'] = 'Разреши бележки';
$_['entry_colour'] = 'Цвят за фон на страницата';
$_['entry_logo'] = 'Лого';
$_['entry_incontext']				 = 'Disable In-Context Checkout';
// Tab
$_['tab_api'] = 'API детайли';
$_['tab_order_status'] = 'Статус на поръчка';
$_['tab_checkout'] = 'Плащане';
// Help
$_['help_ipn'] = 'Изисква се за абонаменти';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
$_['help_logo'] = 'Максимално 750px(ш) x 90px(в) Вие може да използвате лого, само ако сте настроили SSL.';
$_['help_colour'] = '6 знаков HTML цветен код';
$_['help_currency'] = 'Използва се за търсене на транзакции';
// Error
$_['error_permission'] = 'Внимание: Нямате права да променяте параметрите на PayPal Express Checkout!';
$_['error_username'] = 'Необходимо е да въведете API потребител!';
$_['error_password'] = 'Необходимо е да въведете API парола!';
$_['error_signature'] = 'Необходимо е да въведете API подпис!';
$_['error_sandbox_username'] = 'API Sandbox Username Required!';
$_['error_sandbox_password'] = 'API Sandbox Password Required!';
$_['error_sandbox_signature'] = 'API Sandbox Signature Required!';
$_['error_api'] = 'Paypal Authorization Error';
$_['error_api_sandbox'] = 'Paypal Sandbox Authorization Error';
$_['error_consent']				 	 = 'To use quick setup you need to permit PayPal to use your store information';
?>