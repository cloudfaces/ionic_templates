<?php
// Heading
$_['heading_title']	 = 'PayPal Payments Standard';
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно модифицирахте PayPal модула!';
$_['text_edit'] = 'Редактиране на PayPal';
$_['text_pp_standard'] = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Удостоверяване';
$_['text_sale'] = 'Продажба';
// Entry
$_['entry_email'] = 'E-Mail:';
$_['entry_test'] = 'Тестов режим:';
$_['entry_transaction'] = 'Метод на транзакция:';
$_['entry_debug'] = 'Режим за отстраняване на грешки:';
$_['entry_total'] = 'Общо:';
$_['entry_canceled_reversal_status'] = 'Статус на поръчката при връщане на задържани от Paypal средства (Reversal):';
$_['entry_completed_status'] = 'Статус при завършена поръчка:';
$_['entry_denied_status'] = 'Статус при отказана поръчка:';
$_['entry_expired_status'] = 'Статус при изтекла поръчка:';
$_['entry_failed_status'] = 'Статус на поръчката при неуспешно плащане:';
$_['entry_pending_status'] = 'Статус на поръчката при изчакване на плащането:';
$_['entry_processed_status'] = 'Статус при изпълнена поръчка:';
$_['entry_refunded_status'] = 'Статус на поръчката при връщане на парите:';
$_['entry_reversed_status'] = 'Статус на поръчката при върнати средства (Reversed):';
$_['entry_voided_status'] = 'Статус при анулирана поръчка:';
$_['entry_geo_zone'] = 'Гео зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Tab
$_['tab_general'] = 'Главен';
$_['tab_order_status'] = 'Статус на поръчки';
// Help
$_['help_test'] = 'Използване на директиня или тестовия (sandbox) сървър за изпълнение на поръчката?';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
$_['help_debug'] = 'Записва допълнителна информация в системиния лог файл';
// Error
$_['error_permission'] = 'Нямате права да променяте модула на PayPal!';
$_['error_email'] = 'Невалиден E-Mail адрес';
?>