<?php
// Heading
$_['heading_title'] = 'PayPal Payflow Pro iFrame';
$_['heading_refund'] = 'Връщане';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките!';
$_['text_edit'] = 'Редактиране';
$_['text_pp_payflow_iframe'] = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Удостоверяване';
$_['text_sale'] = 'Продажби';
$_['text_authorise'] = 'Удостоверение';
$_['text_capture'] = 'Късно таксуване';
$_['text_void'] = 'Анулиране';
$_['text_payment_info'] = 'Информация за плащане';
$_['text_complete'] = 'Завършено';
$_['text_incomplete'] = 'Незавършено';
$_['text_transaction'] = 'Транзакция';
$_['text_confirm_void'] = 'Ако анулирате неможе да таксувате други суми';
$_['text_refund'] = 'Връщане';
$_['text_refund_issued'] = 'Връщането беше успешно';
$_['text_redirect'] = 'Пренасочване';
$_['text_iframe'] = 'Iframe';
$_['help_checkout_method'] = 'моля използвайте метода на пренасочване ако нямате инсталиран SSL или ако нямате Pay with PayPal опция забранена на вашата страница.';
// Column
$_['column_transaction_id'] = 'Номер на транзакция';
$_['column_transaction_type'] = 'Вид транзакция';
$_['column_amount'] = 'Сума';
$_['column_time'] = 'Време';
$_['column_actions'] = 'Действие';
// Tab
$_['tab_settings'] = 'Настройки';
$_['tab_order_status'] = 'Статус на поръчка';
$_['tab_checkout_customisation'] = 'Настройки на плащане';
// Entry
$_['entry_vendor'] = 'Издател';
$_['entry_user'] = 'Потребител';
$_['entry_password'] = 'Парола';
$_['entry_partner'] = 'Партньор';
$_['entry_test'] = 'Тестов мод';
$_['entry_transaction'] = 'Транзакционен мод';
$_['entry_total'] = 'Общо';
$_['entry_order_status'] = 'Статус на поръчка';
$_['entry_geo_zone'] = 'Гео зона';
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_transaction_id'] = 'Номер на транзакция';
$_['entry_full_refund'] = 'Пълно връщане';
$_['entry_amount'] = 'Сума';
$_['entry_message'] = 'Съобщение';
$_['entry_ipn_url'] = 'IPN URL';
$_['entry_checkout_method'] = 'Метод за плащане';
$_['entry_debug'] = 'Отстаняване на грешки';
$_['entry_transaction_reference'] = 'Транзакционен Реф.';
$_['entry_transaction_amount'] = 'Транзакционна сума';
$_['entry_refund_amount'] = 'Сума за връщане';
$_['entry_capture_status'] = 'Статус на таксуване';
$_['entry_void'] = 'Анулиране';
$_['entry_capture'] = 'Таксуване';
$_['entry_transactions'] = 'Транзакция';
$_['entry_complete_capture'] = 'Завършено таксуване';
$_['entry_canceled_reversal_status'] = 'Статус на отменени:';
$_['entry_completed_status'] = 'Завършен Статус:';
$_['entry_denied_status'] = 'Отхвърлен Статус:';
$_['entry_expired_status'] = 'Изтекъл Статус:';
$_['entry_failed_status'] = 'Неуспешен Статус:';
$_['entry_pending_status'] = 'Изчакващ Статус:';
$_['entry_processed_status'] = 'В процес на обрабока Статус:';
$_['entry_refunded_status'] = 'Върнат Статус:';
$_['entry_reversed_status'] = 'Обърнат Статус:';
$_['entry_voided_status'] = 'Анулиран Статус:';
$_['entry_cancel_url'] = 'URL за отмяна:';
$_['entry_error_url'] = 'URL за грешка:';
$_['entry_return_url'] = 'URL за връщане:';
$_['entry_post_url'] = 'Silent POST URL:';
// Help
$_['help_vendor'] = 'Вашият търковси номер, който получихте при регистрацията';
$_['help_user'] = 'Ако сте настроили един или повече допълнителни акуната, тази стойност е номерът на потребителя, който е упълномощен да изпълни транзакцията. Ако не сте настройвали допълнителни акаунти, то потребителя има същият номер като издетелят';
$_['help_password'] = 'Пароалта която сте задали при създаването на акунта';
$_['help_partner'] = 'Номерът предостаен от упълномощените PayPal реселъри, които са се регистрирали за Payflow SDK.';
$_['help_test'] = 'Използване на директиня или тестовия (sandbox) сървър за изпълнение на поръчката?';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
$_['help_debug'] = 'Допълнителна информация';
// Button
$_['button_refund'] = 'Връшане';
$_['button_void'] = 'Анулиране';
$_['button_capture'] = 'Таксуване';
// Error
$_['error_permission'] = 'Внимание вие нямате права да редактирате този модул!';
$_['error_vendor'] = 'изисква се издател!';
$_['error_user'] = 'Изисква се потребител!';
$_['error_password'] = 'Изисква се парола!';
$_['error_partner'] = 'Изисква се партньор!';
$_['error_missing_data'] = 'Лисва информация';
$_['error_missing_order'] = 'Не открива поръчката';
$_['error_general'] = 'Има грешка';
$_['error_capture'] = 'Въведете сума за таксуване';
?>