<?php
// Heading
$_['heading_title'] = 'BluePay Пренасочване (Изисква SSL)';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките!';
$_['text_edit'] = 'Редактирай модула (Изисква SSL)';
$_['text_bluepay_redirect'] = '<a href="http://www.bluepay.com/preferred-partner/opencart" target="_blank"><img src="view/image/payment/bluepay.jpg" alt="BluePay пренасочване" title="BluePay Пренасочване" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_sim'] = 'Симулатор';
$_['text_test'] = 'Тест';
$_['text_live'] = 'На живо';
$_['text_sale'] = 'Продажби';
$_['text_authenticate'] = 'Удостоверяване';
$_['text_release_ok'] = 'Освобождаването бе успешно';
$_['text_release_ok_order'] = 'Освобождаването бе успешно';
$_['text_rebate_ok'] = 'Отстъпката беше успешна';
$_['text_rebate_ok_order'] = 'Отстъпка е била успешна, статуса на поръчката актуализиран, на отстъпен';
$_['text_void_ok'] = 'Анулирането е било успешено, състоянието на поръчката актуализиран до анулиран';
$_['text_payment_info'] = 'Информация за разплащане';
$_['text_release_status'] = 'Плащане освобождаване';
$_['text_void_status'] = 'Плащане анулиране';
$_['text_rebate_status'] = 'Плащане отстъпка';
$_['text_order_ref'] = 'Поръчка Реф.';
$_['text_order_total'] = 'Общо разрешен';
$_['text_total_released'] = 'Общо освободен';
$_['text_transactions'] = 'Сделки';
$_['text_column_amount'] = 'Сума';
$_['text_column_type'] = 'Вид';
$_['text_column_date_added'] = 'Добавено';
$_['text_confirm_void'] = 'Сигурни ли сте, искате да се анулира плащането?';
$_['text_confirm_release'] = 'Сигурни ли сте, искате да се освободите плащането?';
$_['text_confirm_rebate'] = 'Сигурни ли сте, искате да направите отстъпката в плащането?';
// Entry
$_['entry_vendor'] = 'Акаунт ID';
$_['entry_secret_key'] = 'Таен ключ';
$_['entry_test'] = 'Тест';
$_['entry_transaction'] = 'Метод за транзакция';
$_['entry_total'] = 'Общо';
$_['entry_order_status'] = 'Статс на поръчка';
$_['entry_geo_zone'] = 'Гео Зона';
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_debug'] = 'Отстраняване на грешки';
$_['entry_card'] = 'Карти за магазина';
// Help
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
$_['help_debug'] = 'Активиране отстраняване на грешки ще запише голямо количество данни в лог файла. Винаги трябва да се деактивирано, освен ако не е указано друго';
$_['help_transaction'] = 'Метода за сделките трябва да се настрои на "плащане", за да се даде възможност за абонаментни  плащания';
$_['help_cron_job_token'] = 'Направи това дълго и трудно за отгатване';
$_['help_cron_job_url'] = 'Настройте Cron job, за да извика този URL';
// Button
$_['button_release'] = 'Освобождаване';
$_['button_rebate'] = 'Отстъпка / Възстановяване';
$_['button_void'] = 'Анулиране';
// Error
$_['error_permission'] = 'Внимание: Вие нямате право да променяте плащане BluePay';
$_['error_account_id'] = 'Име задължително!';
$_['error_secret_key'] = 'Secret Key Задължително!';
?>