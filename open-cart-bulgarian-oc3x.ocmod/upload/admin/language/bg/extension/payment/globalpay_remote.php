<?php
// Heading
$_['heading_title'] = 'Globalpay Remote';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките!';
$_['text_edit'] = 'Редактиране на настройките';
$_['text_card_type'] = 'Вид карта';
$_['text_enabled'] = 'Разрешен';
$_['text_use_default'] = 'Използвай по подразбиране';
$_['text_merchant_id'] = 'ID на търговца';
$_['text_subaccount'] = 'Допълнителен акаунт';
$_['text_secret'] = 'Сопделен таен';
$_['text_card_visa'] = 'Visa';
$_['text_card_master'] = 'Mastercard';
$_['text_card_amex'] = 'American Express';
$_['text_card_switch'] = 'Switch/Maestro';
$_['text_card_laser'] = 'Laser';
$_['text_card_diners'] = 'Diners';
$_['text_capture_ok'] = 'Успешно плащане';
$_['text_capture_ok_order'] = 'Плащането е успешно, статута на поръчката е - платен';
$_['text_rebate_ok'] = 'Връщането е успешно';
$_['text_rebate_ok_order'] = 'Връщането е успешно, статута на поръчката е - върнат';
$_['text_void_ok'] = 'Отмяната е успешна, статута на поръчката е - отменен';
$_['text_settle_auto'] = 'Автоматично';
$_['text_settle_delayed'] = 'Забавен';
$_['text_settle_multi'] = 'Мулти';
$_['text_ip_message'] = 'Трябва да предоствите адреса на магазина в настройките на акаунта, преди да се активира услугата';
$_['text_payment_info'] = 'Информация за плащане';
$_['text_capture_status'] = 'Плащането е успешно';
$_['text_void_status'] = 'Плащането е отменено';
$_['text_rebate_status'] = 'Плащането е с отстъпка';
$_['text_order_ref'] = 'Реф. номер на поръчка';
$_['text_order_total'] = 'Общо удостоверени';
$_['text_total_captured'] = 'Общо успешни';
$_['text_transactions'] = 'Транзакции';
$_['text_confirm_void'] = 'Сигурни ли сте, че искате да отмените плашането?';
$_['text_confirm_capture'] = 'Сигурни ли сте, че искате да довършите плащането?';
$_['text_confirm_rebate'] = 'Сигурни ли сте, че искате да направите отстъпка?';
$_['text_globalpay_remote'] = '<a target="_blank" href="https://resourcecentre.globaliris.com/getting-started.php?id=OpenCart"><img src="view/image/payment/globalpay.png" alt="Globalpay" title="Globalpay" style="border: 1px solid #EEEEEE;" /></a>';
// Column
$_['text_column_amount'] = 'Сума';
$_['text_column_type'] = 'Вид';
$_['text_column_date_added'] = 'Създаден';
// Entry
$_['entry_merchant_id'] = 'ID на търговеца';
$_['entry_secret'] = 'Споделен таен';
$_['entry_rebate_password'] = 'Парола';
$_['entry_total'] = 'Общо';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_geo_zone'] = 'Гео зона';
$_['entry_status'] = 'Статут';
$_['entry_debug'] = 'Разработчици';
$_['entry_auto_settle'] = 'Вид споразумение';
$_['entry_tss_check'] = 'TSS проверки';
$_['entry_card_data_status'] = 'Card info logging';
$_['entry_3d'] = 'Enable 3D secure';
$_['entry_liability_shift'] = 'Accept non-liability shifting scenarios';
$_['entry_status_success_settled'] = 'Успех - установено';
$_['entry_status_success_unsettled'] = 'Успех - не е установено';
$_['entry_status_decline'] = 'Отмяна';
$_['entry_status_decline_pending'] = 'Отмяна - авторизация извън линия';
$_['entry_status_decline_stolen'] = 'Отмяна - изгубена или открадната карта';
$_['entry_status_decline_bank'] = 'Отмяна - банкова грешка';
$_['entry_status_void'] = 'Отменени';
$_['entry_status_rebate'] = 'Върнати';
// Help
$_['help_total'] = 'Общата сума, която трябва да се достигне преди този метод за плащане ада стане активен';
$_['help_card_select'] = 'Помолете потребителя да избере вида карта, преди да бъдепренасочен.';
$_['help_notification'] = 'Вие трябва да посочите този URL в Globalpay, за да получавате известия';
$_['help_debug'] = 'Разрешавайки това вие ще запазвате данни в лог файл.';
$_['help_liability'] = 'Това означава, че пак ще приемате поръчки, дори и плащането да не отговаря на 3D secure.';
$_['help_card_data_status'] = 'Запазва в лог файлпоследните 4 цифри на картата, валидноста й, името и вида.';
$_['tab_api'] = 'API Детайли';
$_['tab_account'] = 'Акаунти';
$_['tab_order_status'] = 'Статуси на поръчки';
$_['tab_payment'] = 'Настройки на плащане';
// Button
$_['button_capture'] = 'Плащане';
$_['button_rebate'] = 'Връщане';
$_['button_void'] = 'Отмяна';
// Error
$_['error_merchant_id'] = 'Изисква се номер на търговеца';
$_['error_secret'] = 'Изисква се споделен таен ключ';
?>