<?php
// Heading
$_['heading_title'] = 'PayPal Pro iFrame';
// Text
$_['text_extension']                 = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките!';
$_['text_edit'] = 'Редактиране';
$_['text_pp_pro_iframe'] = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Удостоверяване';
$_['text_sale'] = 'Продажби';
$_['text_payment_info'] = 'Информация за плащане';
$_['text_capture_status'] = 'Късно таксуване';
$_['text_amount_auth'] = 'Сумата е удостоверена';
$_['text_amount_captured'] = 'Сумата е таксувана';
$_['text_amount_refunded'] = 'Сумата е върната';
$_['text_capture_amount'] = 'Таксуване на сума';
$_['text_complete_capture'] = 'Пълно таксуване';
$_['text_transactions'] = 'Транзакции';
$_['text_complete'] = 'Завършено';
$_['text_confirm_void'] = 'Ако анулирате неможе да таксувате други суми';
$_['text_view'] = 'Преглед';
$_['text_refund'] = 'Връщане';
$_['text_resend'] = 'Изпращане отново';
$_['text_reauthorise'] = 'Повторно удостоверяване';
$_['text_reauthorised'] = 'Транзакцията беше повторно удостоверена';
$_['text_transaction'] = 'Транзакция';
$_['text_product_lines'] = 'Продуктови линии';
$_['text_ebay_txn_id'] = 'eBay номер на транзакция';
$_['text_name'] = 'Име';
$_['text_qty'] = 'Количество';
$_['text_price'] = 'Цена';
$_['text_number'] = 'Номер';
$_['text_coupon_id'] = 'Номер купон';
$_['text_coupon_amount'] = 'Сума купон';
$_['text_coupon_currency'] = 'Валута купон';
$_['text_loyalty_disc_amt'] = 'Клиентска карта сума на отстъпка';
$_['text_loyalty_currency'] = 'Валута на клиентската карта';
$_['text_options_name'] = 'Име на опции';
$_['text_tax_amt'] = 'Сума на данък';
$_['text_currency_code'] = 'Код валута';
$_['text_amount'] = 'Сума';
$_['text_gift_msg'] = 'Съобщение (Gift)';
$_['text_gift_receipt'] = 'Получател';
$_['text_gift_wrap_name'] = 'Име върху опаковката';
$_['text_gift_wrap_amt'] = 'Сума за опаковане';
$_['text_buyer_email_market'] = 'Поща на купувача';
$_['text_survey_question'] = 'Анкетен върпос';
$_['text_survey_chosen'] = 'Анкетния въпрос е избран';
$_['text_receiver_business'] = 'Бизнес на получателя';
$_['text_receiver_email'] = 'Поща на получателя';
$_['text_receiver_id'] = 'Номер на получателя';
$_['text_buyer_email'] = 'Еmail на купувача';
$_['text_payer_id'] = 'Номер на платеца';
$_['text_payer_status'] = 'Статус на платеца';
$_['text_country_code'] = 'Код на държавата';
$_['text_payer_business'] = 'Бизнес на платеца';
$_['text_payer_salute'] = 'Поздрав на платеца';
$_['text_payer_firstname'] = 'Платец собствено';
$_['text_payer_middlename'] = 'Платец бащино';
$_['text_payer_lastname'] = 'Платец фамилия';
$_['text_payer_suffix'] = 'Платец суфикс';
$_['text_address_owner '] = 'Собственик - адрес';
$_['text_address_status'] = 'Адрес Статус';
$_['text_ship_sec_name '] = 'Достави до друго име';
$_['text_ship_name '] = 'Достави до име';
$_['text_ship_street1 '] = 'Адрес за доставка 1';
$_['text_ship_street2 '] = 'Адрес за доставка 2';
$_['text_ship_city '] = 'Град за доставка';
$_['text_ship_state '] = 'Държава за доставка';
$_['text_ship_zip '] = 'Достави до ZIP номер';
$_['text_ship_country '] = 'Достави до - код на страната';
$_['text_ship_phone '] = 'Доставка до телефонен номер';
$_['text_ship_sec_add1 '] = 'Втори адрес за доставка 1';
$_['text_ship_sec_add2 '] = 'Втори адрес за доставка 2';
$_['text_ship_sec_city '] = 'Втори град за доставка';
$_['text_ship_sec_state '] = 'Втора област за доставка';
$_['text_ship_sec_zip '] = 'Доставка до втори ZIP адрес';
$_['text_ship_sec_country '] = 'Достави до втори код на страната';
$_['text_ship_sec_phone '] = 'Достави до втори телефон';
$_['text_trans_id'] = 'Номер на транзакция';
$_['text_receipt_id'] = 'Номер на извлечение';
$_['text_parent_trans_id'] = 'Номер на транзакция';
$_['text_trans_type'] = 'Вид транзакция';
$_['text_payment_type'] = 'Вид плащане';
$_['text_order_time'] = 'Поръчка време';
$_['text_fee_amount'] = 'Такса сума';
$_['text_settle_amount'] = 'Сума';
$_['text_tax_amount'] = 'Сума на данъка';
$_['text_exchange'] = 'Валутен курс';
$_['text_payment_status'] = 'Статус на плащане';
$_['text_pending_reason'] = 'Изчаване причина';
$_['text_reason_code'] = 'Код причина';
$_['text_protect_elig'] = 'Защита допустимост';
$_['text_protect_elig_type '] = 'Тип допустимост Защита';
$_['text_store_id'] = 'Номер на магазина';
$_['text_terminal_id'] = 'Номер на терминала';
$_['text_invoice_number '] = 'Номер на фактура';
$_['text_custom'] = 'По-поръчка';
$_['text_note'] = 'Забележка';
$_['text_sales_tax '] = 'Данък върху продажбите';
$_['text_buyer_id'] = 'Номер на купувач';
$_['text_close_date '] = 'Краен срок';
$_['text_multi_item '] = 'Мулти-прдукт';
$_['text_sub_amt'] = 'Мейл сума';
$_['text_sub_period'] = 'абонаментен период';
$_['text_redirect'] = 'Пренасочване';
$_['text_iframe'] = 'Iframe';
$_['help_checkout_method'] = 'Please use Redirect method if do not have SSL installed or if you do not have Pay with PayPal option disabled on your hosted payment page.';
$_['help_debug'] = 'Logs additional information.';

// Column
$_['column_trans_id'] = 'Номер на транзакция';
$_['column_amount'] = 'Сума';
$_['column_type'] = 'Вид плащане';
$_['column_status'] = 'Статус';
$_['column_pend_reason'] = 'Причина за изчакване';
$_['column_date_added'] = 'Добавена';
$_['column_action'] = 'Действие';
// Tab
$_['tab_settings'] = 'Настройки';
$_['tab_order_status'] = 'Статус на поръчка';
$_['tab_checkout_customisation'] = 'Настройки на плащане';

// Entry
$_['entry_username'] = 'API потребителско име';
$_['entry_password'] = 'Парола';
$_['entry_signature'] = 'API подпис';
$_['entry_test'] = 'Тестов мод';
$_['entry_total'] = 'Общо';
$_['entry_order_status'] = 'Статус на поръчка';
$_['entry_geo_zone'] = 'Гео зона';
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_transaction_method'] = 'Метод транзакция';
$_['entry_transaction_id'] = 'Номер на транзакция';
$_['entry_full_refund'] = 'Пълно връщане';
$_['entry_amount'] = 'Сума';
$_['entry_message'] = 'Съобщение';
$_['entry_ipn_url'] = 'IPN URL';
$_['entry_checkout_method'] = 'Метод за плащане';
$_['entry_debug'] = 'Отстаняване на грешки';
$_['entry_canceled_reversal_status'] = 'Статус на отменени:';
$_['entry_completed_status'] = 'Статус на завършени:';
$_['entry_denied_status'] = 'Отказан Статус:';
$_['entry_expired_status'] = 'Изтекъл Статус:';
$_['entry_failed_status'] = 'Неуспешен Статус:';
$_['entry_pending_status'] = 'Изчакващ Статус:';
$_['entry_processed_status'] = 'В процес на обрабока Статус:';
$_['entry_refunded_status'] = 'Върнат Статус:';
$_['entry_reversed_status'] = 'Обърнат Статус:';
$_['entry_voided_status'] = 'Анулиран Статус:';
// Help
$_['help_test'] = 'Използване на директиня или тестовия (sandbox) сървър за изпълнение на поръчката?';
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
// Button
$_['button_refund'] = 'Връшане';
$_['button_void'] = 'Анулиране';
$_['button_capture'] = 'Таксуване';
$_['button_reauthorise'] = 'Ре-усдостоверяване';
// Error
$_['error_permission'] = 'Внимание вие нямате права да редактирате този модул!';
$_['error_sig'] = 'Изисква се подпис!';
$_['error_user'] = 'Изисква се потребител!';
$_['error_password'] = 'Изисква се парола!';
$_['error_timeout'] = 'Времето за отговор изтече';
$_['error_transaction_missing'] = 'Не намери транзакцията';
$_['error_missing_data'] = 'Липсваща информация';
$_['error_general'] = 'Има грешка';
$_['error_capture'] = 'Въведете сума за таксуване';
?>