<?php
// Heading
$_['heading_title']                  = 'Alipay Pay';

// Text
$_['text_extension']                 = 'Допълнения';
$_['text_success']                   = 'Вие успешно променихте настройките!';
$_['text_edit']                      = 'Редакция на модула';
$_['text_alipay']                    = '<a target="_BLANK" href="https://open.alipay.com"><img src="view/image/payment/alipay.png" alt="Alipay Pay Website" title="Alipay Pay Website" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']                      = 'Жива среда';
$_['text_sandbox']                   = 'Тестова среда';

// Entry
$_['entry_app_id']                   = 'Номер на апликацията';
$_['entry_merchant_private_key']     = 'Частен ключ на търговеца';
$_['entry_alipay_public_key']        = 'Alipay публичен ключ';
$_['entry_test']                     = 'Тестова среда';
$_['entry_total']                    = 'Общо';
$_['entry_order_status']             = 'Завършен статус';
$_['entry_geo_zone']                 = 'Гео зона';
$_['entry_status']                   = 'Статут';
$_['entry_sort_order']               = 'Подреждане';

// Help
$_['help_total']                     = 'Минимална сума, за да бъде този метод активен при плащане';
$_['help_alipay_setup']              = '<a target="_blank" href="http://www.opencart.cn/docs/alipay">Натисни тук</a> ,за да научите как се настройва Alipay акаунт.';

// Error
$_['error_permission']               = 'Вие нямате права да редактирате настройките!';
$_['error_app_id']                   = 'изисква се номер на апликацията!';
$_['error_merchant_private_key']     = 'Изисква се частен ключ на търговеца!';
$_['error_alipay_public_key']        = 'Изисква се публичен ключ на Alipay!';
