<?php
//Headings
$_['heading_title'] = 'Влез и плати с Amazon';

// Text
$_['text_success'] = 'Успешно променихте настройките';
$_['text_ipn_url'] = 'Cron Job\'s URL';
$_['text_ipn_token'] = 'Secret Token';
$_['text_us'] = 'US';
$_['text_de'] = 'Germany';
$_['text_uk'] = 'United Kingdom';
$_['text_fr']                      = 'French';
$_['text_it']                      = 'Italian';
$_['text_es']                      = 'Spanish';
$_['text_us_region']			   = 'United States';
$_['text_eu_region']               = 'Euro region';
$_['text_uk_region']               = 'United Kingdom';
$_['text_live'] = 'Работен режим';
$_['text_sandbox'] = 'Тестов режим';
$_['text_auth'] = 'Упълномощаване';
$_['text_payment'] = 'Плащане';
$_['text_extension']               = 'Допълнения';
$_['text_account']                 = 'Акаунт';
$_['text_guest']	   = 'Гост';
$_['text_no_capture'] = '--- Без автоматично залавяне ---';
$_['text_all_geo_zones'] = 'Всички географски зони';
$_['text_button_settings'] = 'Настройки на бутона за плащане';
$_['text_colour'] = 'Цвят';
$_['text_orange'] = 'Оранж';
$_['text_tan'] = 'Бежов';
$_['text_white'] = 'Бял';
$_['text_light'] = 'Светъл';
$_['text_dark'] = 'Тъмен';
$_['text_size'] = 'Размер';
$_['text_medium'] = 'Среден';
$_['text_large'] = 'Голям';
$_['text_x_large'] = 'Екстра голям';
$_['text_background'] = 'Фон';
$_['text_edit'] = 'Редактиране на настройките';
$_['text_amazon_login_pay'] = '<a href="http://go.amazonservices.com/opencart.html" target="_blank" title="Регистрирай се и влез да платиш"><img src="view/image/payment/amazon.png" alt="Регистрирай се и влез да платиш" title="Регистрирай се и влез да платиш" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_amazon_join'] = '<a href="http://go.amazonservices.com/opencart.html" target="_blank" title="Регистрирай се и влез да платиш"><u>Регистрирай се и влез да платиш с Amazon</u></a>';
$_['text_payment_info'] = 'Информация за плащане';
$_['text_capture_ok'] = 'Таксуването е успешно';
$_['text_capture_ok_order'] = 'Таксуването е успешно, упълнамощяването беше осъществено напълно.';
$_['text_refund_ok'] = 'Изискване за връщане на сумата';
$_['text_refund_ok_order'] = 'Изискване за връщане на сумата, сумата напълно възстановена';
$_['text_cancel_ok'] = 'Отменянето е успешно, състоянието на поръчката е променено на отменена';
$_['text_capture_status'] = 'Плащането е прихванато';
$_['text_cancel_status'] = 'Плащането е отменено';
$_['text_refund_status'] = 'Плащането е върнато';
$_['text_order_ref'] = 'Референтен номер на поръчка';
$_['text_order_total'] = 'Общо упълномощени';
$_['text_total_captured'] = 'Общо прихванати';
$_['text_transactions'] = 'Транзакции';
$_['text_column_authorization_id'] = 'Амазон упълномощяващо ID';
$_['text_column_capture_id'] = 'Амазон прихванати ID';
$_['text_column_refund_id'] = 'Амазон върнати суми ID';
$_['text_column_amount'] = 'Суми';
$_['text_column_type'] = 'Вид';
$_['text_column_status'] = 'Състояние';
$_['text_column_date_added'] = 'Дата на добавяне';
$_['text_confirm_cancel'] = 'Сигурни ли сте, че искате да отмените плащането?';
$_['text_confirm_capture'] = 'Сигурни ли сте, че искате да прихванете плащането?';
$_['text_confirm_refund'] = 'Сигурни ли сте, че искате да върнете плащането?';
$_['text_minimum_total'] = 'Минимална сума за поръчка';
$_['text_geo_zone'] = 'Гео зона';
$_['text_status'] = 'Състояние';
$_['text_declined_codes'] = 'Тестови код за отмяна';
$_['text_sort_order'] = 'Подреждане';
$_['text_amazon_invalid'] = 'Невалиден метод за плащане';
$_['text_amazon_rejected'] = 'Амазон отхвърлен';
$_['text_amazon_timeout'] = 'Изтекло време за транзакция';
$_['text_amazon_no_declined'] = '--- Няма автоматично отхвърлени упълнамощявания ---';
$_['text_amazon_signup']		   = 'Sign-up to Amazon Pay';
$_['text_credentials']			   = 'Please paste your keys here (in JSON format)';
$_['text_validate_credentials']	   = 'Validate and Use Credentials';
// Columns
$_['column_status'] = 'Състояние';
// Еntry

$_['entry_merchant_id'] = 'Търговец ID';
$_['entry_access_key'] = 'Ключ за достъп';
$_['entry_access_secret'] = 'Таен ключ';
$_['entry_client_id'] = 'Клиентско ID';
$_['entry_client_secret'] = 'Клиснтски таен ключ';
$_['entry_language']			   = 'Език';
$_['entry_login_pay_test']         = 'Тестова среда';
$_['entry_login_pay_mode']         = 'Жива среда';
$_['entry_checkout']			   = 'Чекаут среда';
$_['entry_payment_region']		   = 'Регион на плащане';
$_['entry_capture_status'] = 'Статут за автоматично прихващане';
$_['entry_pending_status'] = 'Статут за изчакващи поръчки';
$_['entry_ipn_url'] = 'IPN\'s URL';
$_['entry_ipn_token'] = 'Таен токън';
$_['entry_debug'] = 'За разработчици';
// Help
$_['help_pay_mode'] = 'Плащането е възможно само за US пазарите';
$_['help_checkout']				   = 'Should payment button also login customer';
$_['help_capture_status'] = 'Избери статут за поръчките, които ще правят автоматично прихващане при упълномощено плащане';
$_['help_ipn_url'] = 'Посочете URL в Amazon Настройките';
$_['help_ipn_token'] = 'Направете това трудно за досещане';
$_['help_minimum_total']		   = 'This must be above zero';
$_['help_debug'] = 'Разрешаване на тази опция, ще записва данни в лог файла.';
$_['help_declined_codes'] = 'Това е само с тестова цел';
// Order Info
$_['tab_order_adjustment'] = 'Настройки на поръчките';
// Errors
$_['error_permission'] = 'Вие нямате права да променяте настройките';
$_['error_merchant_id'] = 'Изисква се номер на търговеца';
$_['error_access_key'] = 'Изисква се ключ за достъп';
$_['error_access_secret'] = 'Изисква се таен ключ';
$_['error_client_id'] = 'Изисква се номер на клиента';
$_['error_client_secret'] = 'Изисква се номер на клиента';
$_['error_pay_mode'] = 'Важи само на територията на САЩ';
$_['error_minimum_total']		   = 'Minimum order total must be above zero';
$_['error_curreny'] = 'Вашият магазин трябва да има %s инсталирани и разрешени валути';
$_['error_upload'] = 'Неуспешно качване';
$_['error_data_missing'] = 'Липсват попълнени данни';
$_['error_credentials'] 		   = 'Please enter the keys in a valid JSON format';
// Buttons
$_['button_capture'] = 'Залавяне';
$_['button_refund'] = 'Връщане';
$_['button_cancel'] = 'Отмяна';
?>