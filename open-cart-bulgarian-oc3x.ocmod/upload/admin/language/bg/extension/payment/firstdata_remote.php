<?php
// Heading
$_['heading_title'] = 'First Data EMEA Web Service API';
// Text
$_['text_firstdata_remote'] = '<img src="view/image/payment/firstdata.png" alt="First Data" title="First Data" style="border: 1px solid #EEEEEE;" />';
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките!';
$_['text_edit'] = 'Редакторане на модула';
$_['text_card_type'] = 'Вид карта';
$_['text_enabled'] = 'Разрешени';
$_['text_merchant_id'] = 'ID на магазина';
$_['text_subaccount'] = 'Подакаунт';
$_['text_user_id'] = 'ID на потребителя';
$_['text_capture_ok'] = 'Улавянето беше успешно';
$_['text_capture_ok_order'] = 'Плащането е успешно, статуса на поръчката беше променен на - установен';
$_['text_refund_ok'] = 'Връщането беше успешно';
$_['text_refund_ok_order'] = 'Връщането беше успешно, поръчката беше променена на статус - върнат';
$_['text_void_ok'] = 'Анулирането беше успешно, поръчката беше променена на статус - анулиран';
$_['text_settle_auto'] = 'Продажби';
$_['text_settle_delayed'] = 'Пре-удостоверяване';
$_['text_mastercard'] = 'Mastercard';
$_['text_visa'] = 'Visa';
$_['text_diners'] = 'Diners';
$_['text_amex'] = 'American Express';
$_['text_maestro'] = 'Maestro';
$_['text_payment_info'] = 'Информация за плащане';
$_['text_capture_status'] = 'Улавяне на плащане';
$_['text_void_status'] = 'Анулиране на плащане';
$_['text_refund_status'] = 'Върнатоплащане';
$_['text_order_ref'] = 'Поръчка Реф.';
$_['text_order_total'] = 'Общо удостоверени';
$_['text_total_captured'] = 'Общо уловени';
$_['text_transactions'] = 'Транзакции';
$_['text_column_amount'] = 'Сума';
$_['text_column_type'] = 'Вид';
$_['text_column_date_added'] = 'Добавено';
$_['text_confirm_void'] = 'Сигурни ли сте, че искате да анулирате плащането?';
$_['text_confirm_capture'] = 'Сигурни ли сте, че искате да уловите плащането?';
$_['text_confirm_refund'] = 'Сигурни ли сте, че искате да върнете плащането?';
// Entry
$_['entry_certificate_path'] = 'Пътечка до сертификата';
$_['entry_certificate_key_path'] = 'Пътечка до ключа';
$_['entry_certificate_key_pw'] = 'Парола за ключа';
$_['entry_certificate_ca_path'] = 'CA пътечка';
$_['entry_merchant_id'] = 'ID на магазина';
$_['entry_user_id'] = 'ID на потребител';
$_['entry_password'] = 'Парола';
$_['entry_total'] = 'Общо';
$_['entry_sort_order'] = 'Подреждане';
$_['entry_geo_zone'] = 'Гео Зона';
$_['entry_status'] = 'Статус';
$_['entry_debug'] = 'Отстраняване на грешки';
$_['entry_auto_settle'] = 'Вид разпореждане';
$_['entry_status_success_settled'] = 'Успешно - изплатен';
$_['entry_status_success_unsettled'] = 'Успешно - не е изплатен';
$_['entry_status_decline'] = 'Отхвърлен';
$_['entry_status_void'] = 'Анулиран';
$_['entry_status_refund'] = 'Върнат';
$_['entry_enable_card_store'] = 'Разрешете отговори за картите';
$_['entry_cards_accepted'] = 'Разрешени видове карти';
// Help
$_['help_total'] = 'Общатата сума на поръчката трябва да достигне преди този начин на плащане да стане активно.';
$_['help_certificate'] = 'Сертификата и ключа трябва да се съхраняват извън публичната папка на вашият магазин';
$_['help_card_select'] = 'Помоли потребителят да посочи вида карта, преди да бъде пренасочен';
$_['help_notification'] = 'Вие трябва да доставите този URL до First Data, за да получавате известия за плащания';
$_['help_debug'] = 'Активиране отстраняване на грешки ще запише голямо количество данни в лог файла. Винаги трябва да се деактивирано, освен ако не е указано друго';
$_['help_settle'] = 'Ако използвате пре-авторизиране трябва да завършите след-авторизиране в рамките на 3-5 дена в противен случай транзакцията ще отпадне';
// Tab
$_['tab_account'] = 'API инфо';
$_['tab_order_status'] = 'Статус на поръчките';
$_['tab_payment'] = 'Настройки за плащане';
// Button
$_['button_capture'] = 'Улавяне';
$_['button_refund'] = 'Връщане';
$_['button_void'] = 'Анулиране';
// Error
$_['error_merchant_id'] = 'Изисква се номер на магазина';
$_['error_user_id'] = 'Изисква се номер на потребителя';
$_['error_password'] = 'Изисква се парола';
$_['error_certificate'] = 'Изисква се пътека до сертификата';
$_['error_key'] = 'Изисква се ключ за сертификата';
$_['error_key_pw'] = 'Изисква се парола за сертификата';
$_['error_ca'] = 'Изисква се авторизация на сертификата (CA)';
?>