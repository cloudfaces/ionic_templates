<?php
// Heading
$_['heading_title']			  = 'SagePay Direct';
// Text
$_['text_extension']		  = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките на модула!';
$_['text_edit'] = 'Редкатиране';
$_['text_sagepay_direct'] = '<a href="https://support.sagepay.com/apply/default.aspx?PartnerID=E511AF91-E4A0-42DE-80B0-09C981A3FB61" target="_blank"><img src="view/image/payment/sagepay.png" alt="SagePay" title="SagePay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_sim'] = 'Симулатор';
$_['text_test'] = 'Тест';
$_['text_live'] = 'На живо';
$_['text_defered'] = 'Отсрочен';
$_['text_authenticate'] = 'Авторизация';
$_['text_payment']		  = 'Плащане';
$_['text_release_ok'] = 'Пускането беше успешно';
$_['text_release_ok_order'] = 'Пускането беше успешно, статута на поръчката се обнови';
$_['text_rebate_ok'] = 'Отстъпката е успешна';
$_['text_rebate_ok_order'] = 'Отстъпката беше успешна, статута на поръчката се обнови';
$_['text_void_ok'] = 'Анулирането беше успешно, статута на поръчката се обнови';
$_['text_payment_info'] = 'Платежна информация';
$_['text_release_status'] = 'Пускането е успешно';
$_['text_void_status'] = 'Плащането е анулирано';
$_['text_rebate_status'] = 'Плащането е с отстъпка';
$_['text_order_ref'] = 'Поръчка реф.';
$_['text_order_total'] = 'Общо удостоверени';
$_['text_total_released'] = 'Общо пуснати';
$_['text_transactions'] = 'Транзакции';
$_['text_column_amount'] = 'Сума';
$_['text_column_type'] = 'Вид';
$_['text_column_date_added'] = 'Добавено';
$_['text_confirm_void'] = 'Сигурни ли сте, че искате да анулирате плащането?';
$_['text_confirm_release'] = 'Сигурни ли сте, че искате да извършите пускането?';
$_['text_confirm_rebate'] = 'Сигурни ли сте, че искате да направите отстъпка с плащането?';
// Entry
$_['entry_vendor'] = 'Издател:';
$_['entry_test'] = 'Тест режим:';
$_['entry_transaction'] = 'Транзакция метод:';
$_['entry_total'] = 'Общо:';
$_['entry_order_status'] = 'Статус поръчка:';
$_['entry_geo_zone'] = 'Географска зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
$_['entry_debug'] = 'Отстранване на грешки';
$_['entry_card'] = 'Карти на магазина';
$_['entry_cron_job_token'] = 'Таен отговор';
$_['entry_cron_job_url'] = 'Cron Job\'s URL';
$_['entry_last_cron_job_run'] = 'Последно изпълнен cron job\'s време:';
// Help
$_['help_total'] = 'Общата сума  на поръчката, която трябва да се достигне преди този метод на плащане да стане активен.';
$_['help_debug'] = 'Допълнителна информация';
$_['help_transaction'] = 'Транзакционният метод трябва да бъде настроен и разрешен,за да приемате абонаментни плащания';
$_['help_cron_job_token'] = 'Направете това дълго и трудно за отгатване';
$_['help_cron_job_url'] = 'Настройте cron job, за да повикате този URL';
// Button
$_['button_release'] = 'Пускане';
$_['button_rebate'] = 'Отстъпки / Връщтане';
$_['button_void'] = 'Анулиране';
// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте payment SagePay!';
$_['error_vendor'] = 'Издател ID се изисква!';
?>