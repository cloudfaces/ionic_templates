<?php
// Heading
$_['heading_title']                            = 'Fedex';
// Text
$_['text_shipping']                            = 'Доставка';$_['text_success']                             = 'Успешно променихте настройките!';$_['text_edit']                                = 'Редактиране';$_['text_europe_first_international_priority'] = 'Европа първи международен приоритет';
$_['text_fedex_1_day_freight']                 = 'Fedex 1 Day Freight';
$_['text_fedex_2_day']                         = 'Fedex 2 Day';
$_['text_fedex_2_day_am']                      = 'Fedex 2 Day AM';
$_['text_fedex_2_day_freight']                 = 'Fedex 2 Day Freight';
$_['text_fedex_3_day_freight']                 = 'Fedex 3 Day Freight';
$_['text_fedex_express_saver']                 = 'Fedex експрес';
$_['text_fedex_first_freight']                 = 'Fedex първо товарене';
$_['text_fedex_freight_economy']               = 'Fedex икономично товарене';$_['text_fedex_freight_priority']              = 'Fedex приоритетно товарене';
$_['text_fedex_ground']                        = 'Fedex земя';
$_['text_first_overnight']                     = 'First за ендо денонощие';$_['text_ground_home_delivery']                = 'Доставяне по домовете';$_['text_international_economy']               = 'Международно икономично';$_['text_international_economy_freight']       = 'Международно товарене';$_['text_international_first']                 = 'Международно първи ';$_['text_international_priority']              = 'Международно приоритетно';$_['text_international_priority_freight']      = 'Международно приоритетно товарене';$_['text_priority_overnight']                  = 'Приоритет за едно денонощие';
$_['text_smart_post']                          = 'Умна поща';$_['text_standard_overnight']                  = 'Стандартно за едно денонощие';
$_['text_regular_pickup']                      = 'Взимане в:';$_['text_request_courier']                     = 'Поискай куриер';
$_['text_drop_box']                            = 'Кутия';
$_['text_business_service_center']             = 'Бизнес център';
$_['text_station']                             = 'Станция';
$_['text_fedex_envelope']                      = 'FedEx Envelope';
$_['text_fedex_pak']                           = 'FedEx Pak';
$_['text_fedex_box']                           = 'FedEx Box';
$_['text_fedex_tube']                          = 'FedEx Tube';
$_['text_fedex_10kg_box']                      = 'FedEx 10kg Box';
$_['text_fedex_25kg_box']                      = 'FedEx 25kg Box';
$_['text_your_packaging']                      = 'Вашите пакети';$_['text_list_rate']                           = 'Покажи курсове';$_['text_account_rate']                        = 'Курсове на акаунта';
// Entry
$_['entry_key']                                = 'Ключ';
$_['entry_password']                           = 'Парола';$_['entry_account']                            = 'Номер на акаунт';
$_['entry_meter']                              = 'Номер';
$_['entry_postcode']                           = 'Пощенски код';
$_['entry_test']                               = 'Тестов мод';
$_['entry_service']                            = 'Услуги';$_['entry_dimension']                          = 'Размери на кутия (д x щ x в)';
$_['entry_length_class']                       = 'Клас дължина';
$_['entry_length']                             = 'Дължина';
$_['entry_width']                              = 'Ширина';
$_['entry_height']                             = 'Височина';$_['entry_dropoff_type']                       = 'Вид за доставяне';
$_['entry_packaging_type']                     = 'Вид опаковка';
$_['entry_rate_type']                          = 'Вид цена на курс';$_['entry_display_time']                       = 'Покажи времето за доставка';$_['entry_display_weight']                     = 'Покажи теглото на доставка';
$_['entry_weight_class']                       = 'Клас тежина';
$_['entry_tax_class']                          = 'Клас данъци';
$_['entry_geo_zone']                           = 'Гео зона';
$_['entry_status']                             = 'Статус';
$_['entry_sort_order']                         = 'Подреждане';
// Help$_['help_length_class']                        = 'Насторй на инчове или сантиметри.';$_['help_display_time']                        = 'Искате ли да покзавате времето за изпращане? Например от 3 до 5 дена';$_['help_display_weight']                      = 'Искате ли да покажете теглото на доставката? Например теглото е: 2.7674 kg';$_['help_weight_class']                        = 'Настрой на килограми или паундове.';
// Error$_['error_permission']                         = 'Вие неможе да редакирате настройките!';$_['error_key']                                = 'Изисква се ключ!';$_['error_password']                           = 'Изисква се парола!';$_['error_account']                            = 'Изисква се акаунт!';$_['error_meter']                              = 'Лиспват данни!';$_['error_postcode']                           = 'Изисква се пощенски код!';$_['error_dimension']                          = 'Въведете Ширина &amp; Височина!';