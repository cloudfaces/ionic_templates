<?php
// Heading
$_['heading_title'] = 'Доставка базирана на тегло';
// Text
$_['text_extension']   = 'Допълнения';
$_['text_success'] = 'Успешно променихте параметрите на Доставка базирана на тегло!';
$_['text_edit'] = 'Редактиране';
// Entry
$_['entry_rate'] = 'Тарифа:';
$_['entry_tax_class'] = 'Вид на данъка:';
$_['entry_geo_zone'] = 'Гео зона:';
$_['entry_status'] = 'Статаус:';
$_['entry_sort_order'] = 'Подреждане';
// Help
$_['help_rate'] = 'Пример: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..';
// Error
$_['error_permission'] = 'Внимание: Нямате права да променяте параметрите на Доставка, базирана на тегло!';
?>