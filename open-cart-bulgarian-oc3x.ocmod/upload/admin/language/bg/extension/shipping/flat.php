<?php
// Heading
$_['heading_title'] = 'Фиксирана сума';
// Text
$_['text_extension']   = 'Допълнения';
$_['text_success'] = 'Успешно променихте параметрите на Фиксирана сума за доставка!';
$_['text_edit'] = 'Редкатиране';
// Entry
$_['entry_cost'] = 'Цена:';
$_['entry_tax_class'] = 'Данъчна основа:';
$_['entry_geo_zone'] = 'Гео зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Подреждане';
// Error
$_['error_permission'] = 'Внимание: Нямате права да променяте параметрите на Фиксирана сума!';
?>