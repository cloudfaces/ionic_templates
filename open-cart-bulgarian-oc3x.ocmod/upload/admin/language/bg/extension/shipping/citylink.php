<?php
// Heading
$_['heading_title']    = 'Citylink';
// Text$_['text_extension']   = 'Доъплнения';$_['text_success']     = 'Успех: Вие променихте настройките на Citylink доставка!';$_['text_edit']        = 'Редактиране';
// Entry
$_['entry_rate']       = 'Citylink Такси:';$_['entry_tax_class']  = 'Данъчна основа:';$_['entry_geo_zone']   = 'Географска зона:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Пореден:'; 
// Help$_['help_rate']        = 'Въведи стойности до 5,2 десетични места. ';
// Error$_['error_permission'] = 'Внимание: Вие нямате право да променяте настройките на Citylink доставка!';