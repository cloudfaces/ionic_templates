<?php
// Heading
$_['heading_title']                = 'UPS';
// Text$_['text_extension']               = 'Extensions';$_['text_success']                 = 'Успешно промеихте настройките!';$_['text_edit']                    = 'Редактиране';$_['text_regular_daily_pickup']    = 'Редовни дневни взимания';$_['text_daily_pickup']            = 'Дневни взимания';
$_['text_customer_counter']        = 'Брояч клиенти';$_['text_one_time_pickup']         = 'Еднократно взиме';$_['text_on_call_air_pickup']      = 'Взимане по телефона';
$_['text_letter_center']           = 'Център писма';$_['text_air_service_center']      = 'Въздушен център';
$_['text_suggested_retail_rates']  = 'Предложени тарифи (UPS Store)';
$_['text_package']                 = 'Пакет';
$_['text_ups_letter']              = 'UPS Letter';
$_['text_ups_tube']                = 'UPS Tube';
$_['text_ups_pak']                 = 'UPS Pak';
$_['text_ups_express_box']         = 'UPS Express Box';
$_['text_ups_25kg_box']            = 'UPS 25kg box';
$_['text_ups_10kg_box']            = 'UPS 10kg box';
$_['text_us']                      = 'US Origin';
$_['text_ca']                      = 'Canada Origin';
$_['text_eu']                      = 'European Union Origin';
$_['text_pr']                      = 'Puerto Rico Origin';
$_['text_mx']                      = 'Mexico Origin';$_['text_other']                   = 'Всички останали';
$_['text_test']                    = 'Тест';
$_['text_production']              = 'Продукция';$_['text_residential']             = 'Населено място';
$_['text_commercial']              = 'Рекламен';
$_['text_next_day_air']            = 'UPS Next Day Air';
$_['text_2nd_day_air']             = 'UPS Second Day Air';
$_['text_ground']                  = 'UPS Ground';
$_['text_3_day_select']            = 'UPS Three-Day Select';
$_['text_next_day_air_saver']      = 'UPS Next Day Air Saver';
$_['text_next_day_air_early_am']   = 'UPS Next Day Air Early A.M.';
$_['text_2nd_day_air_am']          = 'UPS Second Day Air A.M.';
$_['text_saver']                   = 'UPS Saver';
$_['text_worldwide_express']       = 'UPS Worldwide Express';
$_['text_worldwide_expedited']     = 'UPS Worldwide Expedited';
$_['text_standard']                = 'UPS Standard';
$_['text_worldwide_express_plus']  = 'UPS Worldwide Express Plus';
$_['text_express']                 = 'UPS Express';
$_['text_expedited']               = 'UPS Expedited';
$_['text_express_early_am']        = 'UPS Express Early A.M.';
$_['text_express_plus']            = 'UPS Express Plus';
$_['text_today_standard']          = 'UPS Today Standard';
$_['text_today_dedicated_courier'] = 'UPS Today Dedicated Courier';
$_['text_today_intercity']         = 'UPS Today Intercity';
$_['text_today_express']           = 'UPS Today Express';
$_['text_today_express_saver']     = 'UPS Today Express Saver';

// Entry$_['entry_key']                    = 'Ключ за достъп';$_['entry_username']               = 'Потребителско име';
$_['entry_password']               = 'Парола';$_['entry_pickup']                 = 'Метод на взимане';$_['entry_packaging']              = 'Вид опаковка';$_['entry_classification']         = 'Код за класификация на клиентите';$_['entry_origin']                 = 'Кодна изпащане';
$_['entry_city']                   = 'Град';$_['entry_state']                  = 'Област';
$_['entry_country']                = 'Държава';$_['entry_postcode']               = 'Пощенски код';
$_['entry_test']                   = 'Тестов мод';
$_['entry_quote_type']             = 'Вид запитване';
$_['entry_service']                = 'Услуги';$_['entry_insurance']              = 'Разреши застраховане';$_['entry_display_weight']         = 'Покажи тегло';
$_['entry_weight_class']           = 'Клас тежина';
$_['entry_length_class']           = 'Клас дължина';
$_['entry_dimension']			   = 'Размери (д x ш x в)';
$_['entry_length']                 = 'Дължина';
$_['entry_height']                 = 'Височина';
$_['entry_width']                  = 'Ширина';
$_['entry_tax_class']              = 'Данъчен клас';
$_['entry_geo_zone']               = 'Гео зона';
$_['entry_status']                 = 'Статус';
$_['entry_sort_order']             = 'Подреждане';$_['entry_debug']      			   = 'Разработчици';

// Help
$_['help_key']                     = 'Enter the XML rates access key assigned to you by UPS.';
$_['help_username']                = 'Enter your UPS Services account username.';
$_['help_password']                = 'Enter your UPS Services account password.';
$_['help_pickup']                  = 'How do you give packages to UPS (only used when origin is US)?';
$_['help_packaging']               = 'What kind of packaging do you use?';
$_['help_classification']          = '01 - If you are billing to a UPS account and have a daily UPS pickup, 03 - If you do not have a UPS account or you are billing to a UPS account but do not have a daily pickup, 04 - If you are shipping from a retail outlet (only used when origin is US)';
$_['help_origin']                  = 'What origin point should be used (this setting affects only what UPS product names are shown to the user)';
$_['help_city']                    = 'Enter the name of the origin city.';
$_['help_state']                   = 'Enter the two-letter code for your origin state/province.';
$_['help_country']                 = 'Enter the two-letter code for your origin country.';
$_['help_postcode']                = 'Enter your origin zip/postalcode.';
$_['help_test']                    = 'Use this module in Test (YES) or Production mode (NO)?';
$_['help_quote_type']              = 'Quote for Residential or Commercial Delivery.';
$_['help_service']                 = 'Select the UPS services to be offered.';
$_['help_insurance']               = 'Enables insurance with product total as the value';
$_['help_display_weight']          = 'Do you want to display the shipping weight? (e.g. Delivery Weight : 2.7674 kg)';
$_['help_weight_class']            = 'Set to kilograms or pounds.';
$_['help_length_class']            = 'Set to centimeters or inches.';
$_['help_dimension']			   = 'This is assumed to be your average packing box size. Individual item dimensions are not supported at this time so you must enter average dimensions like 5x5x5.';
$_['help_debug']      			   = 'Saves send/recv data to the system log';

// Error$_['error_permission']             = 'Внимание нямате права да променяте модула!';$_['error_key']                    = 'Изисква се ключ за достъп!';$_['error_username']               = 'Изисква се потребителско име!';$_['error_password']               = 'Изисква се парола!';
$_['error_city']                   = 'Град!';$_['error_state']                  = 'Изисква се област!';$_['error_country']                = 'Изисква се страна!';$_['error_dimension']              = 'Изискват се размери!';