<?php
// Heading
$_['heading_title']           = 'Parcelforce 48';
// Text$_['text_extension']          = 'Extensions';
$_['text_success']            = 'Успех: Вие променихте настройките на Parcelforce 48 доставка!';
$_['text_edit']               = 'Редкатиране';
// Entry
$_['entry_rate']              = 'Parcelforce 48 такси:';$_['entry_insurance']         = 'Parcelforce48 компесационни такси:';$_['entry_display_weight']    = 'Покажи теглото на доставката:';$_['entry_display_insurance'] = 'Покажи застраховка:';$_['entry_display_time']      = 'Покажи време за доставка:';
$_['entry_tax_class']         = 'Вид данък:';$_['entry_geo_zone']          = 'Географска зона:';
$_['entry_status']            = 'Статус:';
$_['entry_sort_order']        = 'Пореден:';
// Help
$_['help_rate']              = 'Enter values upto 5,2 decimal places. (12345.67) Example: .1:1,.25:1.27 - Weights less than or equal to 0.1Kg would cost 1.00, Weights less than or equal to 0.25g but more than 0.1Kg will cost 1.27. Do not enter KG or symbols.';
$_['help_insurance']         = 'Enter values upto 5,2 decimal places. (12345.67) Example: 34:0,100:1,250:2.25 - Insurance cover for cart values upto 34 would cost 0.00 extra, those values more than 100 and upto 250 will cost 2.25 extra. Do not enter currency symbols.';
$_['help_display_weight']    = 'Do you want to display the shipping weight? (e.g. Delivery Weight : 2.7674 kg)';
$_['help_display_insurance'] = 'Do you want to display the shipping insurance? (e.g. Insured upto 500)';
$_['help_display_time']      = 'Do you want to display the shipping time? (e.g. Ships within 3 to 5 days)';

// Error
$_['error_permission']        = 'Внимание: Вие нямате право да променяте настройките на Parcelforce 48 доставка!';