<?php
// Heading$_['heading_title']      = 'Австралийски пощи';
// Text
$_['text_extension']     = 'Допълнения';$_['text_success']       = 'Успешно редактирахте настройктие!';$_['text_edit']          = 'Редкатиране';
// Entry
$_['entry_api']          = 'API Key';$_['entry_postcode']     = 'Пощенскикод:';
$_['entry_weight_class'] = 'Клас тежина:';
$_['entry_tax_class']    = 'Данъчен клас:';
$_['entry_geo_zone']     = 'Гео зона:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Подреждане:';

// Help$_['help_weight_class']  = 'Настрой в грамове.';
// Error$_['error_permission']   = 'Нямате право да променяте настройките!';$_['error_postcode']     = 'Пощенският код тряба да е 4 символа!';