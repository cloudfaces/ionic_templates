<?php
// Heading
$_['heading_title']    = 'Кларна (Klarna) такса';
// Text$_['text_extension']   = 'Допълнения';$_['text_success']     = 'Успех: Вие променихте настройките на Кларна такси!';$_['text_edit']        = 'Редакция';
$_['text_sweden']      = 'Швеция';
$_['text_norway']      = 'Норвегия';
$_['text_finland']     = 'Финландия';
$_['text_denmark']     = 'Дания';
$_['text_germany']     = 'Германия';
$_['text_netherlands'] = 'Холандия';
// Entry$_['entry_total']      = 'Общо поръчки:';
$_['entry_fee']        = 'Такса:';
$_['entry_tax_class']  = 'Данъчен клас:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Подреждане:';
// Error$_['error_permission'] = 'Внимание: Вие нямате разрешение да променяте настройките на Klarna такси!';