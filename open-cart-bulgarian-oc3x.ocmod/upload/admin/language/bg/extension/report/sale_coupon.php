<?php
// Heading
$_['heading_title']    = 'Отчет Купони';

// Text
$_['text_extension']        = 'Допълнния';
$_['text_edit']             = 'Редакция на настройките';
$_['text_success']          = 'Успешно променихте настройките!';
$_['text_filter']           = 'Филтър';
// Column
$_['column_name']      = 'Име на купона';
$_['column_code']      = 'Код';
$_['column_orders']         = 'Брой поръчки';
$_['column_total']          = 'Общо';
$_['column_action']         = 'Действие';

// Entry
$_['entry_date_start']      = 'Начална дата';
$_['entry_date_end']        = 'Крайна дата';
$_['entry_status']          = 'Статут';
$_['entry_sort_order']      = 'Подреждане';

// Error
$_['error_permission']  = 'Вие нямате права да променте настройките!';
