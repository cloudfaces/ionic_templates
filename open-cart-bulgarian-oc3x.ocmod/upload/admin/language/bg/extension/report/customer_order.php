<?php
// Heading
$_['heading_title']         = 'Отчет клиентски поръчки';

// Text
$_['text_extension']        = 'Допълнния';
$_['text_edit']             = 'Редакция на настройките';
$_['text_success']          = 'Успешно променихте настройките!';
$_['text_filter']           = 'Филтър';
$_['text_all_status']       = 'Всички статути';

// Column
$_['column_customer']       = 'Име';
$_['column_email']          = 'Ел. поща';
$_['column_customer_group'] = 'Клиентска група';
$_['column_status']         = 'Статут';
$_['column_orders']         = 'Брой поръчки';
$_['column_products']       = 'Брой продукти';
$_['column_total']          = 'Общо';
$_['column_action']         = 'Действие';

// Entry
$_['entry_date_start']      = 'Начална дата';
$_['entry_date_end']        = 'Крайна дата';
$_['entry_customer']        = 'Клиент';
$_['entry_status']          = 'Статут на поръчка';
$_['entry_status']          = 'Статут';
$_['entry_sort_order']      = 'Подреждане';

// Error
$_['error_permission']  = 'Вие нямате права да променте настройките!';