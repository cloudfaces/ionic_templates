<?php
// Heading
$_['heading_title']    = 'Отчет разгеждани продукти';

// Text
$_['text_extension']        = 'Допълнния';
$_['text_edit']             = 'Редакция на настройките';
$_['text_success']          = 'Успешно променихте настройките!';

// Column
$_['column_name']      = 'Име на продукт';
$_['column_model']     = 'Модел';
$_['column_viewed']    = 'Разгледан';
$_['column_percent']   = 'Процент';

// Entry
$_['entry_status']     = 'Статут';
$_['entry_sort_order'] = 'Подреждане';

// Error
$_['error_permission'] = 'Вие нямате права да променяте настройките!';