<?php
// Heading
$_['heading_title']     = 'Отчет продажби';

// Text
$_['text_extension']    = 'Допълнения';
$_['text_edit']         = 'Редакция на настройките';
$_['text_success']      = 'Вие успешно променихте настройките!';
$_['text_filter']       = 'Филтър';
$_['text_year']         = 'Години';
$_['text_month']        = 'Месеци';
$_['text_week']         = 'Седмици';
$_['text_day']          = 'Дни';
$_['text_all_status']   = 'Всички статути';

// Column
$_['column_date_start'] = 'Начална дата';
$_['column_date_end']   = 'Крайна дата';
$_['column_orders']     = 'Брой поръчки';
$_['column_products']   = 'Брой продукти';
$_['column_tax']        = 'Данък';
$_['column_total']      = 'Общо';

// Entry
$_['entry_date_start']  = 'Начална дата';
$_['entry_date_end']    = 'Крайна дата';
$_['entry_group']       = 'Сортирай по';
$_['entry_status']      = 'Статут на поръчка';
$_['entry_status']      = 'Статут';
$_['entry_sort_order']  = 'Подреждане';

// Error
$_['error_permission']  = 'Вие нямате права да редактирате настройките!';