<?php
// Heading
$_['heading_title']     = 'Отчет търсения от клиенти';

// Text
$_['text_extension']        = 'Допълнния';
$_['text_edit']             = 'Редакция на настройките';
$_['text_success']          = 'Успешно променихте настройките!';
$_['text_filter']           = 'Филтър';
$_['text_guest']        = 'Гост';
$_['text_customer']     = '<a href="%s">%s</a>';
// Column
$_['column_keyword']    = 'Ключова дума';
$_['column_products']       = 'Брой продукти';
$_['column_category']   = 'Категория';
$_['column_customer']       = 'Име';
$_['column_ip']         = 'Адрес';
$_['column_date_added'] = 'Дата на добавяне';

// Entry
$_['entry_date_start']      = 'Начална дата';
$_['entry_date_end']        = 'Крайна дата';
$_['entry_keyword']     = 'Ключова дума';
$_['entry_customer']        = 'Клиент';
$_['entry_ip']          = 'Адрес';
$_['entry_status']          = 'Статут';
$_['entry_sort_order']      = 'Подреждане';

// Error
$_['error_permission']  = 'Вие нямате права да променте настройките!';
