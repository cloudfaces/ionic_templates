<?php
// Heading
$_['heading_title']    = 'Отчет на пазарите';

// Text
$_['text_extension']        = 'Допълнния';
$_['text_edit']             = 'Редакция на настройките';
$_['text_success']          = 'Успешно променихте настройките!';
$_['text_filter']           = 'Филтър';
$_['text_all_status']       = 'Всички статути';

// Column
$_['column_campaign']  = 'Име на кампания';
$_['column_code']      = 'Код';
$_['column_clicks']    = 'Кликове';
$_['column_orders']         = 'Брой поръчки';
$_['column_total']          = 'Общо';
// Entry
$_['entry_date_start']      = 'Начална дата';
$_['entry_date_end']        = 'Крайна дата';
$_['entry_status']          = 'Статут на поръчка';
$_['entry_status']          = 'Статут';
$_['entry_sort_order']      = 'Подреждане';

// Error
$_['error_permission']  = 'Вие нямате права да променте настройките!';
