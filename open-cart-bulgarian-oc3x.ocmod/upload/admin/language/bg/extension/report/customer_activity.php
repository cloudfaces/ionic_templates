<?php
// Heading
$_['heading_title']                = 'Доклад за активност на потребителите';

// Text
$_['text_extension']               = 'Допълнения';
$_['text_edit']                    = 'Редакция на опциите';
$_['text_success']                 = 'Вие успешно променихте настройките!';
$_['text_filter']                  = 'Филтър';
$_['text_activity_register']       = '<a href="customer_id=%d">%s</a> регистрира нов акаунт.';
$_['text_activity_edit']           = '<a href="customer_id=%d">%s</a> промени данните на акаунта.';
$_['text_activity_password']       = '<a href="customer_id=%d">%s</a> обнови паролата на акаунта.';
$_['text_activity_reset']          = '<a href="customer_id=%d">%s</a> възобнови паролата на акаунта.';
$_['text_activity_login']          = '<a href="customer_id=%d">%s</a> влезна.';
$_['text_activity_forgotten']      = '<a href="customer_id=%d">%s</a> изиска забравена парола.';
$_['text_activity_address_add']    = '<a href="customer_id=%d">%s</a> добави нов адрес.';
$_['text_activity_address_edit']   = '<a href="customer_id=%d">%s</a> обнови своите адреси.';
$_['text_activity_address_delete'] = '<a href="customer_id=%d">%s</a> изтри единия от адресите.';
$_['text_activity_return_account'] = '<a href="customer_id=%d">%s</a> добави продукт <a href="return_id=%d">return</a>.';
$_['text_activity_return_guest']   = '%s изиска връщане на <a href="return_id=%d">продукт</a>.';
$_['text_activity_order_account']  = '<a href="customer_id=%d">%s</a> добави <a href="order_id=%d">нова поръчка</a>.';
$_['text_activity_order_guest']    = '%s създаде <a href="order_id=%d">нова поръчка</a>.';
$_['text_activity_affiliate_add']  = '<a href="customer_id=%d">%s</a> се регистрира за Афилиатски акаунт.';
$_['text_activity_affiliate_edit'] = '<a href="customer_id=%d">%s</a> обнови Афилиатските си данни.';
$_['text_activity_transaction']    = '<a href="customer_id=%d">%s</a> получи комисионна от <a href="order_id=%d">поръчка</a>.';

// Column
$_['column_customer']              = 'Клиент';
$_['column_comment']               = 'Бележка';
$_['column_ip']                    = 'Адрес';
$_['column_date_added']            = 'Дата на добавяне';

// Entry
$_['entry_customer']               = 'Клиент';
$_['entry_ip']                     = 'Адрес';
$_['entry_date_start']             = 'Начална дата';
$_['entry_date_end']               = 'Крайна дата';
$_['entry_status']                 = 'Статут';
$_['entry_sort_order']             = 'Подреждане';

// Error
$_['error_permission']             = 'Внимание: Вие нямате права да променяте настройките!';