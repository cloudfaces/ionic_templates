<?php
// Heading
$_['heading_title']         		= 'Амазон USA';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_dashboard']				= 'Amazon US Табло';

// Text
$_['text_heading_settings'] 		= 'Настройки';
$_['text_heading_account'] 			= 'Смяна на плана';
$_['text_heading_links'] 			= 'Линкове към продукт';
$_['text_heading_register'] 		= 'Регистране';
$_['text_heading_bulk_listing'] 	= 'Масови списъци';
$_['text_heading_stock_updates'] 	= 'Обновяване на стока';
$_['text_heading_saved_listings'] 	= 'Запаени списъци';
$_['text_heading_bulk_linking'] 	= 'Масови списъци';
