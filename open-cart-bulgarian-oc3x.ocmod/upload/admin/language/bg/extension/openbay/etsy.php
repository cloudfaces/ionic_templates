<?php
// Heading
$_['heading_title']         		= 'Etsy';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_dashboard']				= 'Etsy Табло';

// Messages
$_['text_success']         			= 'Успешно запазихте настройките';
$_['text_heading_settings']         = 'Настройки';
$_['text_heading_sync']             = 'Синхронизиране';
$_['text_heading_register']         = 'Регистрация';
$_['text_heading_products']        	= 'Продуктови линкове';
$_['text_heading_listings']        	= 'Etsy обяви';

// Errors
$_['error_generic_fail']			= 'Непозната грешка!';
$_['error_permission']				= 'нямате права да променяте настройките';
