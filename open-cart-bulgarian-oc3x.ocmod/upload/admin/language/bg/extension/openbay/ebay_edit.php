<?php
// Heading
$_['heading_title']					= 'Поправяне на списъци eBay';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_ebay']						= 'eBay';

// Tab
$_['tab_recommendations']			= 'Recommendations';

// Text
$_['text_revise']               	= 'Поправяне';
$_['text_loading']                  = 'Изтегляне на информацията от eBay';
$_['text_error_loading']            = 'Възникна грешка при изтегляне на информацията от eBay';
$_['text_saved']                    = 'Списъците бяха запазени';
$_['text_alert_removed']            = 'Връзките към списъците бяха премахнати';
$_['text_alert_ended']              = 'Списъжите бяха добавени в eBay';
$_['text_listing_info']             = 'Информация за списъците';
$_['text_check_recommendations']    = 'Checking for eBay listing recommendations';
$_['text_success_recommendations']  = 'There are no listing improvement recommendations for this item!';

// Buttons
$_['button_view']					= 'Преглед на списъци';
$_['button_remove']					= 'Премахни връзка';
$_['button_end']                    = 'Спри списък';
$_['button_retry']					= 'Опитай отново';

// Entry
$_['entry_title']					= 'Заглавие';
$_['entry_price']					= 'Продажна цена (вклюва данъци)';
$_['entry_stock_store']				= 'Местна наличност';
$_['entry_stock_listed']			= 'eBay наличност';
$_['entry_stock_reserve']			= 'Ново на резерв';
$_['entry_stock_matrix_active']		= 'Наличност матрица (активна)';
$_['entry_stock_matrix_inactive']	= 'Наличност матрица (неактивна)';

// Column
$_['column_sku']					= 'Var code / SKU';
$_['column_stock_listed']			= 'Записани';
$_['column_stock_reserve']			= 'Резерв';
$_['column_stock_total']			= 'В наличност';
$_['column_price']					= 'Цена';
$_['column_status']					= 'Активни';
$_['column_add']					= 'Добави';
$_['column_combination']			= 'Комбинации';

// Help
$_['help_stock_store']				= 'Това е наличността на вашият OpenCart';
$_['help_stock_listed']				= 'Това е наличността в момента в eBay';
$_['help_stock_reserve']			= 'Това е максималното количество наличност за eBay (0 = няма лимит на резерв)';

// Error
$_['error_ended']					= 'Посоченият списък с продукти завърши, вие неможе да го редактирате. Трябва да премахнете връзката.';
$_['error_reserve']					= 'Вие неможе да посочите резерв с по-голямо количество от наличната стока в магазина';
$_['error_no_sku']          		= 'No SKU found!';
$_['error_no_item_id']              = 'Item ID is missing from the request';
$_['error_recommendations_load']    = 'Unable to load item recommendations';