<?php
// Heading
$_['heading_title'] 				= 'Масово качване';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon US';

// Text
$_['text_searching'] 				= 'Търси';
$_['text_finished'] 				= 'Завъши';
$_['text_dont_list'] 				= 'Не качвай';
$_['text_listing_values'] 			= 'Стойности на списъка';
$_['text_new'] 						= 'Ново';
$_['text_used_like_new'] 			= 'Използвано - Като ново';
$_['text_used_very_good'] 			= 'Използвано - Много добро';
$_['text_used_good'] 				= 'Използвано - Добро';
$_['text_used_acceptable'] 			= 'Използвано - Приемливо';
$_['text_collectible_like_new'] 	= 'Колекционерско - Като ново';
$_['text_collectible_very_good'] 	= 'Колекционерско - Много добро';
$_['text_collectible_good'] 		= 'Колекционерско - Добро';
$_['text_collectible_acceptable'] 	= 'Колекционерско - Приемливо';
$_['text_refurbished'] 				= 'Поправено';


// Entry
$_['entry_condition'] 				= 'Състояние';
$_['entry_condition_note'] 			= 'Бележка';
$_['entry_start_selling'] 			= 'Започни да продаваш';

// Column
$_['column_name'] 					= 'Име';
$_['column_image'] 					= 'Изображение';
$_['column_model'] 					= 'Модел';
$_['column_status'] 				= 'Статус';
$_['column_matches'] 				= 'Съвпадения';
$_['column_result'] 				= 'Резултат';

// Button
$_['button_list'] 					= 'Списък';

// Error
$_['error_product_sku'] 			= 'Посочете SKU номер';
$_['error_searchable_fields'] 		= 'продукта трябва да има ISBN, EAN, UPC или JAN неуспешно обновяване';
$_['error_bulk_listing_permission'] = 'Масовото качване не е разрешено с вашият план, моля сменете плана';
$_['error_select_items'] 			= 'Трябва да изберете поне 1 продукт за търсенето';