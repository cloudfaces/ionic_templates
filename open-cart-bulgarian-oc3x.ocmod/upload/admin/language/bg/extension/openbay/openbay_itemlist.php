<?php
// Heading
$_['heading_title'] 					= 'Управление на обяви';

// Text
$_['text_markets']                  	= 'Пазари';
$_['text_openbay']                  	= 'OpenBay Pro';
$_['text_ebay'] 						= 'eBay';
$_['text_amazon'] 						= 'Amazon EU';
$_['text_amazonus'] 					= 'Amazon US';
$_['text_etsy'] 						= 'Etsy';
$_['text_status_all'] 					= 'Всички';
$_['text_status_ebay_active'] 			= 'eBay активен';
$_['text_status_ebay_inactive'] 		= 'eBay неактивен';
$_['text_status_amazoneu_saved'] 		= 'Amazon EU съхранен';
$_['text_status_amazoneu_processing'] 	= 'Amazon EU в процес на обработка';
$_['text_status_amazoneu_active'] 		= 'Amazon EU активен';
$_['text_status_amazoneu_notlisted'] 	= 'Amazon EU не е в обявата';
$_['text_status_amazoneu_failed'] 		= 'Amazon EU неуспешно';
$_['text_status_amazoneu_linked'] 		= 'Amazon EU посочени';
$_['text_status_amazoneu_notlinked'] 	= 'Amazon EU непосочени';
$_['text_status_amazonus_saved'] 		= 'Amazon US запазени';
$_['text_status_amazonus_processing'] 	= 'Amazon US в процес на обработка';
$_['text_status_amazonus_active'] 		= 'Amazon US активен';
$_['text_status_amazonus_notlisted'] 	= 'Amazon US не е в обявата';
$_['text_status_amazonus_failed'] 		= 'Amazon US неуспешно';
$_['text_status_amazonus_linked'] 		= 'Amazon US посочени';
$_['text_status_amazonus_notlinked'] 	= 'Amazon US непосочени';
$_['text_processing']       			= 'В процес на обработка';
$_['text_category_missing'] 			= 'Липсващи катгории';
$_['text_variations'] 					= 'вариации';
$_['text_variations_stock'] 			= 'наличност';
$_['text_min']                      	= 'Мин';
$_['text_max']                      	= 'Макс';
$_['text_option']                   	= 'Опция';
$_['text_list']              			= 'Списък с продукти';

// Entry
$_['entry_title'] 						= 'Заглавие';
$_['entry_model'] 						= 'Модел';
$_['entry_manufacturer'] 				= 'Производител';
$_['entry_status'] 						= 'Статус';
$_['entry_status_marketplace'] 			= 'Статус на пазар';
$_['entry_stock_range'] 				= 'Обхват на наличността';
$_['entry_category'] 					= 'Категория';
$_['entry_populated'] 					= 'Обновени';
$_['entry_sku'] 						= 'SKU';
$_['entry_description'] 				= 'Описание';

// Button
$_['button_error_fix']              	= 'Попрвяне на грешки';
$_['button_amazon_eu_bulk']         	= 'Amazon EU масово действие';
$_['button_amazon_us_bulk']         	= 'Amazon US масово действие';
$_['button_ebay_bulk']              	= 'eBay масово действие';

// Error
$_['error_select_items']            	= 'Вие трябва да изберете поне 1 продукт';