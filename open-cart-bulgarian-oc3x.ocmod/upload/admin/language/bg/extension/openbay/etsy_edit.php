<?php
// Headings
$_['heading_title']      		= 'Редкатиране на обява';
$_['text_openbay']          	= 'OpenBay Pro';
$_['text_etsy']             	= 'Etsy';

// Tabs

// Text
$_['text_option']      			= 'Избери опция';
$_['text_listing_id']  			= 'Номер на обява';
$_['text_updated']  			= 'Успешно обновихте настройките';
$_['text_edit']  				= 'Обновете вашите настройки';

// Entry
$_['entry_title']      			= 'Име  на продукт';
$_['entry_description']     	= 'Описание';
$_['entry_price']      			= 'Цена';
$_['entry_state']      			= 'Област';

// Errors
$_['error_price_missing']  		= 'Ценат липсва';
$_['error_title_length']  		= 'Заглавието е твърде дълго';
$_['error_title_missing']  		= 'Заглавието липсва';
$_['error_desc_missing']  		= 'Описанието е празно';
$_['error_price_missing']  		= 'Цената липсва';
$_['error_state_missing']  		= 'Областта липсва';
