<?php
// Heading
$_['heading_title']					= 'Посочване на продукти';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_amazon']					= 'Amazon EU';

// Text
$_['text_desc1']                    = 'Посочването (свързване) на вашите продукти ще разреши контрола на стока във списъка на Amazon.';
$_['text_desc2'] 					= 'За всички продукти,за които е обновен местното количество стока (което съдържа вашият магазин) Ще обнови Amazon listing';
$_['text_desc3']                    = 'Вие можете да обновите списъка на ръчно, посочвайки Amazon SKU и име на продукти или заредете всички непосочени продукти и после въведете amazon SKUs. (Зареждане на продукти от OpenCart to Amazon ще прибави линкове автоматично.)';
$_['text_new_link']                 = 'Нов линк';
$_['text_autocomplete_product']     = 'Продукт (Автоматично се допълва от името на продукт)';
$_['text_amazon_sku']               = 'Amazon продуктово SKU';
$_['text_action']                   = 'Действие';
$_['text_linked_items']             = 'Посочени (свързани) продукти';
$_['text_unlinked_items']           = 'Непосочени продукти';
$_['text_name']                     = 'Име';
$_['text_model']                    = 'Модел';
$_['text_combination']              = 'Комбинации';
$_['text_sku']                      = 'SKU';
$_['text_sku_variant']              = 'Variant SKU';
$_['text_amazon_sku']               = 'Amazon продукт SKU номер';

// Button
$_['button_load']                 	= 'Зареждане';

// Error
$_['error_empty_sku']        		= 'Amazon SKU неможе да е празно!';
$_['error_empty_name']       		= 'Името на продукта неможе да бъде празно!';
$_['error_no_product_exists']       = 'Продуктът не съществува. Моля използвайте стойности за автоматично допъване';