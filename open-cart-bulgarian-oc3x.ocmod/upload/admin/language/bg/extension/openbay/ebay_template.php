<?php
// Headings
$_['heading_title']                 = 'Шаблон за обява';
$_['text_ebay']                     = 'eBay';
$_['text_openbay']                  = 'OpenBay Pro';

// Columns
$_['column_name']            		= 'Име на шаблона';
$_['column_action']            		= 'Действие';

// Entry
$_['entry_template_name']           = 'Име';
$_['entry_template_html']           = 'HTML';

// Text
$_['text_added']                    = 'Нов шаблон беше прибавен';
$_['text_updated']                  = 'Шаблона беше обновен';
$_['text_deleted']                  = 'Шаблона беше изтрит';
$_['text_confirm_delete']           = 'Сигурни ли сте, че искате да изтриете шаблона?';
$_['text_list']           			= 'Шаблони';
$_['text_add']      				= 'Добави профил';
$_['text_edit']      				= 'Редактирай профил';

// Error
$_['error_name']               		= 'Вие трябва да въведете име на шаблона';
$_['error_permission']            	= 'Нямате права да редактирате настройките';
$_['error_no_template']             = 'Номерът на шаблона не съществува!';
