<?php
// Heading
$_['heading_title']         		= 'eBay';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_dashboard']				= 'eBay Табло';

// Text
$_['text_success']         			= 'Вие запазихте успешно промените';
$_['text_heading_settings']         = 'Настройки';
$_['text_heading_sync']             = 'Синхронизиране';
$_['text_heading_subscription']     = 'Смяна на план';
$_['text_heading_usage']          	= 'Потребление';
$_['text_heading_links']            = 'Продуктови връзки';
$_['text_heading_item_import']      = 'Внасяне на продукти';
$_['text_heading_order_import']     = 'Внасяне на поръчки';
$_['text_heading_adds']             = 'Инсталирани допълнения';
$_['text_heading_summary']          = 'eBay отчет';
$_['text_heading_profile']          = 'Профили';
$_['text_heading_template']         = 'Готови теми';
$_['text_heading_ebayacc']          = 'eBay акаунт';
$_['text_heading_register']         = 'Регистрирай се';

// Error
$_['error_category_nosuggestions']  = 'Неможе да зареди предложените категории';
$_['error_loading_catalog']         = 'eBay неуспешно търсене в каталога';
$_['error_generic_fail']         	= 'Непозната грешка!';