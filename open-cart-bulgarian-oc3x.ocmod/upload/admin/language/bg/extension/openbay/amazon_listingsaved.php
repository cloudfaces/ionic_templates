<?php
// Heading
$_['heading_title'] 				= 'Запазени списъци';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon EU';

// Text
$_['text_description']              = 'Това са списъците съхранени и готови за качване в Амазон.';
$_['text_uploaded_alert']           = 'Съхранените списъци са качени!';
$_['text_delete_confirm']           = 'Сигурен ли сте?';
$_['text_complete']           		= 'Списъците са качени';

// Column
$_['column_name']              		= 'Име';
$_['column_model']             		= 'Модел';
$_['column_sku']               		= 'SKU номер';
$_['column_amazon_sku']        		= 'Amazon продуктов SKU номер';
$_['column_action']           		= 'Действие';
