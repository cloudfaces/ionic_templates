<?php
// Heading
$_['heading_title']         		= 'Амазон Европа';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_dashboard']				= 'Амазон Европа Табло';

// Text
$_['text_heading_settings'] 		= 'Настройки';
$_['text_heading_account'] 			= 'Смени плана';
$_['text_heading_links'] 			= 'Линкове към продукти';
$_['text_heading_register'] 		= 'Регистриране';
$_['text_heading_bulk_listing'] 	= 'Масови списъци';
$_['text_heading_stock_updates'] 	= 'Обновяване на стока';
$_['text_heading_saved_listings'] 	= 'Записани списъци';
$_['text_heading_bulk_linking'] 	= 'Масово линкване';