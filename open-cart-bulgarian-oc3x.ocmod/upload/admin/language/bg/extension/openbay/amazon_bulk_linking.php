<?php
// Heading
$_['heading_title'] 				= 'Масово линкване';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Амазон Европа';

// Button
$_['button_load'] 					= 'Зареждане';
$_['button_link'] 					= 'Линк';

// Text
$_['text_local'] 					= 'Местен';
$_['text_load_listings'] 			= "Зареждане на настройките от вашият Амазон, може да отнеме известно време (до 2 часа в някои случаи). Ако линквате вашите продукти, количествово стока ще бъде синхронизирана и обновена с количествово във вашия магаизн.";
$_['text_report_requested'] 		= 'Успешно изискахте доклад на списъците от Амазон!';
$_['text_report_request_failed'] 	= 'Неуспешно зареждане на списъците от Амазон!';
$_['text_loading'] 					= 'Зарежда продукти...';
$_['text_choose_marketplace'] 		= 'Избери пазар';
$_['text_uk'] 						= 'Великобритания';
$_['text_de'] 						= 'Германия';
$_['text_fr'] 						= 'Франция';
$_['text_it'] 						= 'Италия';
$_['text_es'] 						= 'Испания';

// Column
$_['column_asin'] 					= "ASIN номер";
$_['column_price'] 					= "Цена";
$_['column_name'] 					= "Име";
$_['column_sku'] 					= "SKU";
$_['column_quantity'] 				= "Количество";
$_['column_combination'] 			= "Комбинация";

// Error
$_['error_bulk_link_permission'] 	= 'Масовото линкване не е разрешено за вашия профил, моля надстройте плана!';
