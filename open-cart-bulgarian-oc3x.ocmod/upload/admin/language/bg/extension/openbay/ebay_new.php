<?php
// Heading
$_['heading_title']               	= 'New listing';
$_['text_ebay']               		= 'eBay';
$_['text_openbay']               	= 'Openbay Pro';

// Text
$_['text_listing_1day'] = 'един ден ';
$_['text_listing_3day'] = 'три дни ';
$_['text_listing_5day'] = 'пет дни ';
$_['text_listing_7day'] = 'седем дни ';
$_['text_listing_10day'] = '10 дни ';
$_['text_listing_30day'] = '30 дни ';
$_['text_listing_gtc'] = 'GTC- до отмяна ';
$_['text_none'] = 'Няма';
$_['text_pixels'] = 'Пиксела';
$_['text_add'] = 'Добавяне';
$_['text_other'] = 'Други';
$_['text_loading'] = 'Зареждане';
$_['text_confirm_action'] = 'Сигурни ли сте? ';
$_['text_paypal'] = 'PayPal имейл адрес ';
$_['text_return'] = 'Върни се към продукти ';
$_['text_category_suggested_help'] = 'На базата на заглавието си ';
$_['text_category_popular_help'] = 'На база на историята ';
$_['text_category_checking'] = 'Проверка на изисквания категория иБей, моля изчакайте ';
$_['text_features_help'] = 'Въвеждане на конкретни препоръки. Вашата позиция ще помогне на купувача да стесни точната позиция, от която се нуждае. Тя може също така да подобри ефективността на продукта и иБей може да вкара най-добрата съответстваща стойност. ';
$_['text_images_text_1'] = 'иБей снимки ще бъдат качени в иБей. Свръхголям и Галерия плюс, са в списъка на подобренията, които могат да отнемат повече време да се качат. ';
$_['text_images_text_2'] = 'Изображенията в шаблона ще бъде прибавени към  описанието на Вашата обява и хоствани от вашия уеб сайт, те са свободни. (Вашата обява трябва да има {gallery} таг) ';
$_['text_image_ebay'] = 'иБей изображение ';
$_['text_images_none'] = 'Вие нямате снимки за този продукт, трябва да добавите снимки, преди да можете да го добавите ';
$_['text_width'] = 'Ширина ';
$_['text_height'] = 'Височина ';
$_['text_px'] = 'Пиксели';
$_['text_item_postcode_help'] = 'Пощенският код ще помогне на иБей да избере правилното място за вашата оферта ';
$_['text_item_location_help'] = 'Въвеждане на град е по-малко надеждно от пощенски код ';
$_['text_despatch_country_help'] = 'Това е страната от, която продукта ще бъде изпратен ';
$_['text_shipping_zones'] = 'Доставка до зони ';
$_['text_shipping_worldwide'] = 'Целият свят ';
$_['text_shipping_flat'] = 'Плосък курс ';
$_['text_shipping_calculated'] = 'Изчислено ';
$_['text_shipping_freight'] = 'Товарен ';
$_['text_unit'] = 'Единица';
$_['text_unit_english'] = 'Английски език';
$_['text_unit_metric'] = 'Метричен ';
$_['text_weight_major'] = 'Максимално тегло ';
$_['text_weight_minor'] = 'Минимално тегло';
$_['text_package'] = 'Пакет тип ';
$_['text_shape'] = 'неправилна форма ';
$_['text_width'] = 'Ширина ';
$_['text_length'] = 'Дължина ';
$_['text_depth'] = 'Дълбочина / Височина ';
$_['text_return_accepted'] = 'Рекламация приета? ';
$_['text_return_type'] = 'Вид рекламация';
$_['text_return_policy'] = 'Политика на връщане';
$_['text_return_days'] = 'Дни за рекламация';
$_['text_return_scosts'] = 'Разходи за доставка';
$_['text_return_restock'] = 'Такса ново зареждане';
$_['text_return_scosts_1'] = 'Купувача заплаща цялата обратна доставка ';
$_['text_return_scosts_2'] = 'Продавача заплаща цялата обратна доставка ';
$_['text_review_costs'] = 'Разходи за Обявата';
$_['text_review_costs_total'] = 'Общо иБей такси ';
$_['text_review_edit'] = 'Редактирай';
$_['text_preview'] = 'Преглед на описание ';
$_['text_verify'] = 'Проверка';
$_['text_created_msg'] = 'Обявата е създадена в иБей. Номерът на иБей е';
$_['text_option_images'] = 'Промяна на снимки';
$_['text_option_images_grp'] = 'Изберете опция за групата';
$_['text_option_images_choice'] = 'Изображения';
$_['text_option_description'] = 'Промяна на изображения могат да бъдат използвани за показване на определен образ, когато потребителят прави избор на опция. Можете да използвате само една промяна, определена за снимките, но може да има до 12 изображения на вариация. Изображенията по подразбиране се зареждат от опцията си стойности (набор в Catalog> Options) ';
$_['text_catalog_help'] = 'Това ще промени основното ви изображение и ще бъде настроено да използвате иБей изображение';
$_['text_failed_title'] = 'Качването навашата обява е неуспешно ';
$_['text_failed_msg1'] = 'Може да има няколко причини за това. ';
$_['text_failed_li1'] = 'Ако сте нов иБей продавач (или не сте продадавали много в миналото) - ще трябва да се свържете с иБей за отстраняване на ограниченията ';
$_['text_failed_li2'] = 'Може да не сте се абонирали за Selling Manager Pro от иБей - това е изискване. ';
$_['text_failed_li3'] = 'Вашият OpenBay Pro акаунт е спрян. Моля, проверете вашето административното пространство в модула под "My Account" бутона ';
$_['text_failed_contact'] = 'Ако тази грешка продължи, моля свържете се с поддръжката, след като сте се уверили, че въпросът не е нито едно от горните. ';
$_['text_template_images'] = 'Изображенията в шаблона';
$_['text_ebay_images'] = 'иБей снимки';
$_['text_shipping_first'] = 'Първи елемент ';
$_['text_shipping_add'] = 'Допълнителни елементи';
$_['text_shipping_service'] = 'Обслужване ';
$_['text_stock_reserved'] = 'ще бъде резервиран ';
$_['text_insert'] = 'Добавяне на нова иБей регистрация ';
$_['text_price_ex_tax'] = 'без данък ';
$_['text_price_inc_tax'] = 'включително данък ';
$_['text_ebay_imagesize_ok'] = 'Размерът на изображението е добро, то може да се използва от иБей ';
$_['text_compatible']        		= 'Compatible options';
$_['text_loading_compatibility']    = 'Loading compatibility options';
$_['text_product_identifiers']    	= 'Product identifiers';
$_['text_ean']    					= 'EAN';
$_['text_upc']    					= 'UPC';
$_['text_isbn']    					= 'ISBN';
$_['text_identifier_not_required']  = 'Not required';

// Column
$_['column_stock_total'] = 'В наличност ';
$_['column_stock_col_qty'] = 'За да покажете списък ';
$_['column_stock_col_qty_reserve'] = 'Запазени';
$_['column_price_ex_tax'] = 'без данък ';
$_['column_price_inc_tax'] = 'включително данък ';
$_['column_stock_col_comb'] = 'комбинация';
$_['column_price'] = 'Цена';
$_['column_stock_col_enabled'] = 'Активиран';
$_['column_thumb'] = 'Изобажение';
$_['column_img_size'] = 'Размер';
$_['column_template_image'] = 'Изображение за шаблона';
$_['column_ebay_image'] = 'иБей изображение';
$_['column_main_ebay_image'] = 'Главно иБей изображение ';
$_['column_sku']        			= 'SKU';

// Entry
$_['entry_compatibility']           = 'Parts compatibility';
$_['entry_shop_category'] = 'Категория на магазина';
$_['entry_category_popular'] = 'Популярни категории ';
$_['entry_category_suggested'] = 'иБей предложени категории ';
$_['entry_category'] = 'Категория';
$_['entry_listing_condition'] = 'Състояние на продукт';
$_['entry_listing_duration'] = 'Продължителност на обявата ';
$_['entry_search_catalog'] = 'Търсене в иБей каталога: ';
$_['entry_catalog'] = 'Използване на стандартното изображение';
$_['entry_title'] = 'Заглавие';
$_['entry_subtitle'] = 'Под заглавието';
$_['entry_description'] = 'Описание';
$_['entry_profile_load'] = 'Профил зареждане ';
$_['entry_template'] = 'Тема';
$_['entry_image_gallery'] = 'Размер на изображението в галерията';
$_['entry_image_thumb'] = 'Размер на Thumb изображението';
$_['entry_images_supersize'] = 'Свръх големи снимки ';
$_['entry_images_gallery_plus'] = 'Галерия плюс ';
$_['text_stock_matrix'] = 'Стокова матрица ';
$_['entry_qty'] = 'Количество за обявата';
$_['entry_price'] = 'Цена';
$_['entry_tax_inc'] = 'Включен данък';
$_['entry_offers'] = 'Разреши купувачи да правят оферти';
$_['entry_private'] = 'Частна обява';
$_['entry_imediate_payment'] = 'Изисква незабавно плащане?';
$_['entry_payment'] = 'Разрешени плащания';
$_['entry_payment_instruction'] = 'Инструкции за плащане ';
$_['entry_item_postcode'] = 'Пощенски код / Zip на населено място ';
$_['entry_item_location'] = 'Град или област на местопложението';
$_['entry_despatch_country'] = 'Страна на изпращане';
$_['entry_despatch_time'] = 'Време за изпращане';
$_['entry_shipping_getitfast'] = 'Вземи сега!';
$_['entry_shipping_cod'] = 'Такса за наложен платеж';
$_['entry_shipping_type_nat'] = 'Доставка вид';
$_['entry_shipping_nat'] = 'Национални спедиторски услуги';
$_['entry_shipping_handling_nat'] = 'Опаковъне такса (национална)';
$_['entry_shipping_in_desc'] = 'Информация за товарителница в описанието ';
$_['entry_shipping_type_int'] = 'Международни доставки';
$_['entry_shipping_intnat'] = 'Международни спедиторски услуги';
$_['entry_shipping_handling_int'] = 'Опаковане такса (международна) ';
$_['entry_shipping_pickupdropoff']  = 'Click and Collect';
$_['entry_shipping_pickupinstore']  = 'Available for In-Store Pickup';
$_['entry_shipping_global_shipping']= 'Use Global shipping service';
$_['entry_shipping_promotion_discount'] = 'Combined shipping discounts (national)';
$_['entry_shipping_promotion_discount_international'] = 'Combined shipping discounts (international)';
$_['entry_vrm']   					= 'Vehicle Registration Mark';
$_['entry_vin']   					= 'Vehicle Identification Number';

// Tab
$_['tab_feature'] = 'Характеристики';
$_['tab_ebay_catalog'] = 'иБей каталог';
$_['tab_description'] = 'Описание';
$_['tab_price'] = 'Цена &amp; подробни данни ';
$_['tab_payment'] = 'Плащане';
$_['tab_returns'] = 'Връщане';

// Help
$_['help_quantity_reserve'] = 'Въведете по-ниска сума, ако искате да се поддържа по-ниско ниво наличност на иБей ';
$_['help_price_ex_tax'] = 'Вашият стандартен продукт Цена без ДДС. Тази стойност не се изпраща на иБей. ';
$_['help_price_inc_tax'] = 'Тази стойност се изпраща към иБей и е цената, която потребителите ще платят. ';
$_['help_private'] = 'Скрий имената на купувача';
$_['help_category_suggested']       = 'The list of categories eBay has suggested based of your item title';
$_['help_category_popular']       	= 'A list of your recently used categories';
$_['help_shop_category']       		= 'The category where the product will be added in your eBay shop';
$_['help_shipping_promotion_discount'] = 'Offer national buyers a discount on shipping if they buy multiple items. Discounts must have been setup in eBay to take effect.';
$_['help_shipping_promotion_discount_international'] = 'Offer international buyers a discount on shipping if they buy multiple items. Discounts must have been setup in eBay to take effect.';

// Error
$_['error_choose_category'] = 'Трябва да изберете категория ';
$_['error_search_text'] = 'Въведете текст за търсене ';
$_['error_no_stock'] = 'Не може да се обявите артикул с нулева наличност ';
$_['error_catalog_data'] = 'Не беше открита информация за продукта ви в иБей';
$_['error_missing_settings'] = 'Не може да се качат продукти, докато не се синхронизират настройките на иБей ';
$_['error_category_load'] = 'Не може да зареди категории ';
$_['error_features'] = 'Грешка при зареждането на функции';
$_['error_catalog_load'] = 'Грешка при зареждане на стоките ';
$_['error_category_sync'] = 'Трябва да се поправи вашия проблем с категорията, преди да можете да се качат. Опитайте повторно синхронизиране в настройките на модул админ. ';
$_['error_choose_category'] = 'Моля, изберете иБей категория ';
$_['error_sku'] = 'Не може да се приеме продукт без SKU ';
$_['error_name'] = 'Не може да се представи продукт без име ';
$_['error_name_length'] = 'Име на продукта трябва да е под 80 знака ';
$_['error_item_location'] = 'Въведете пощенски код за продукта ';
$_['error_dispatch_time'] = 'Въведете час изпращане';
$_['error_shipping_national'] = 'Добавяне на поне една национална спедиторска услуга';
$_['error_stock'] = 'Трябва да имате в наличност даден продукт, за да го добавите в списък ';
$_['error_duration'] = 'Изберете продължителност на обявата';
$_['error_listing_duration'] = 'Изберете продължителност на обява, изберете категория, за да заредите тези опции ';
$_['error_image_size'] = 'Уверете се, че сте задали размер за галерията и за thumb на изображенята ';
$_['error_no_images'] = 'Обявата трябва да има поне една снимка, качена в иБей ';
$_['error_main_image'] = 'Трябва да изберете основно иБей изображение от вашия набор на иБей снимки ';
$_['error_ebay_imagesize'] = 'Снимката трябва да бъде поне 500px от 1 страна, за да се използва от иБей ';
$_['error_no_sku']          		= 'No SKU found!';