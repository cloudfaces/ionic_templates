<?php
// Heading
$_['heading_title'] 				= 'Нов списък за Амазон';
$_['text_title_advanced'] 			= 'Подробен списък';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon US';

// Buttons
$_['button_new'] 					= 'Създай нов продукт';
$_['button_amazon_price'] 			= 'Вземи цената от Амазон';
$_['button_list'] 					= 'Качи в Амазон';
$_['button_remove_error'] 			= 'Премахни съобщението за грешка';
$_['button_save_upload'] 			= 'Запази и качи';
$_['button_browse'] 				= 'Отвори';
$_['button_saved_listings'] 		= 'Преглед на запазените списъци';
$_['button_remove_links'] 			= "Премахни линковете";
$_['button_create_new_listing'] 	= "Създай нов списък";

// Help
$_['help_sku'] 						= "Уникални продуктови номера попълнени от търговеца";
$_['help_restock_date'] 			= "Това е датата, на която ще бъдат изпратени изчакващите поръчки. Тази дата не трябва да превишава 30 дена, след това е възможно поръчката да бъде отменена автоматично.";
$_['help_sale_price'] 				= "Разпродажбата трябва да има начална и крайна дата";

//Text
$_['text_products_sent'] 			= 'Продуктите се обработват';
$_['button_view_on_amazon'] 		= 'Прегледай в Амазон';
$_['text_list'] 					= 'Покажи в Амазон';
$_['text_new'] 						= 'Нов';
$_['text_used_like_new'] 			= 'Използвано - Като ново';
$_['text_used_very_good'] 			= 'Използвано - Много добро';
$_['text_used_good'] 				= 'Използвано - Добро';
$_['text_used_acceptable'] 			= 'Използвано - Приемливо';
$_['text_collectible_like_new'] 	= 'Колекционерско - Като ново';
$_['text_collectible_very_good'] 	= 'Колекционерско - Много добро';
$_['text_collectible_good'] 		= 'Колекционерско - Добро';
$_['text_collectible_acceptable'] 	= 'Колекционерско - Приемливо';
$_['text_refurbished'] 				= 'Поправено';

$_['text_product_not_sent'] 		= 'Продукта не беше изпратендо Амазон. Причина: %s';
$_['text_not_in_catalog'] 			= 'Или ако не е в каталога&nbsp;&nbsp;&nbsp;';
$_['text_placeholder_search'] 		= 'Въведете име на продукта, UPC, EAN, ISBN or ASIN';
$_['text_placeholder_condition'] 	= 'Използвайте това поле, за да опишете състоянието на продукта.';
$_['text_characters'] 				= 'символи';
$_['text_uploaded'] 				= 'Съхранените списъци се качиха!';
$_['text_saved_local'] 				= 'Списъците са съхранени, но не са качени все още';
$_['text_product_sent'] 			= 'Продукта беше успешно публикуван в Амазон.';
$_['text_links_removed'] 			= 'Продуктовите линкове са изтрити';
$_['text_product_links'] 			= 'Продуктови линкове';
$_['text_has_saved_listings'] 		= 'Този продукт има един или повече списъци, в които е посочен, но не е качен';
$_['text_edit_heading'] 			= 'Редактирай списък';

// Columns
$_['column_image'] 					= 'Изображение';
$_['column_asin'] 					= 'ASIN';
$_['column_price'] 					= 'Цена';
$_['column_action'] 				= 'Действие';
$_['column_name'] 					= 'Име на продукт';
$_['column_model'] 					= 'Модел';
$_['column_combination'] 			= 'Комбинации';
$_['column_sku_variant'] 			= 'Variant SKU';
$_['column_sku'] 					= 'SKU';
$_['column_amazon_sku'] 			= 'Amazon SKU';

// Entry
$_['entry_sku'] 					= 'SKU';
$_['entry_condition'] 				= 'Състояние';
$_['entry_condition_note'] 			= 'Бележка за състоянието';
$_['entry_price'] 					= 'Цена';
$_['entry_sale_price'] 				= 'Разпродажна цена';
$_['entry_sale_date'] 				= 'Обхват на разпродажбата';
$_['entry_quantity'] 				= 'Количество';
$_['entry_start_selling'] 			= 'Достъпно от дата';
$_['entry_restock_date'] 			= 'Дата на презареждане';
$_['entry_country_of_origin'] 		= 'Страна производител';
$_['entry_release_date'] 			= 'Дата на пускане в продажба';
$_['entry_from'] 					= 'Начална дата';
$_['entry_to'] 						= 'Крайна дата';
$_['entry_product'] 				= 'Списък за продукт';
$_['entry_category'] 				= 'Amazon категория';

//Tabs
$_['tab_main'] 						= 'Главно';
$_['tab_required'] 					= 'Задължителна информация';
$_['tab_additional'] 				= 'Допълнителна информация';

//Errors
$_['error_text_missing'] 			= 'Вие трябва да добавите детайли за търсенето';
$_['error_data_missing'] 			= 'Търсената информация липсва';
$_['error_missing_asin'] 			= 'ASIN липсва';
$_['error_marketplace_missing'] 	= 'Моля изберете пазар';
$_['error_condition_missing'] 		= "Моля изберете състояние";
$_['error_fetch'] 					= 'Неуспешно изтегляне на информация';
$_['error_amazonus_price'] 			= 'Неуспешно взимане на цената от Amazon US';
$_['error_stock'] 					= 'Неможе да качвате продукт, който е с по-малко 1 брой';
$_['error_sku'] 					= 'Въведете SKU номер';
$_['error_price'] 					= 'Въведете цена';
$_['error_connecting'] 				= 'Неуспешно свързване с API. моля проверете настройките на OpenBay Pro Amazon. Ако проблема продължава свържете се с поддръжка.';
$_['error_required'] 				= 'Това поле е задължително!';
$_['error_not_saved'] 				= 'Списъкът не беше запазен. Проверете полетата.';
$_['error_char_limit'] 				= 'символи над лимита.';
$_['error_length'] 					= 'Минималната дължина е';
$_['error_upload_failed'] 			= 'Неуспешно качване на продукт с SKU: "%s". Причина: "%s" .';
$_['error_load_nodes'] 				= 'Невъзможно търсене';
$_['error_not_searched'] 			= 'Потърсете съвпадащи продукти преди да качвате.';