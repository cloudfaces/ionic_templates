<?php
// Heading
$_['heading_title'] 				= 'Масови посочване';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon US';

// Button
$_['button_load'] 					= 'Зареждане';
$_['button_link'] 					= 'Линк';

// Text
$_['text_local'] 					= 'Местнен';
$_['text_load_listings'] 			= "Зареди всички ваши настройки от Амазон, може да отнеме време (до 2 часа в някои случаи). Ако свържете вашите продукти, количеството на стока в Амазон ще се изравни онлайн магазина.";
$_['text_report_requested'] 		= 'Успешно изискахте докалад за списъците от Амазон';
$_['text_report_request_failed'] 	= 'Неуспешно изискване на докалд';
$_['text_loading'] 					= 'Зарежда продукти';

// Column
$_['column_asin'] 					= "ASIN номер";
$_['column_price'] 					= "Цена";
$_['column_name'] 					= "Име";
$_['column_sku'] 					= "SKU номер";
$_['column_quantity'] 				= "Количество";
$_['column_combination'] 			= "Комбинация";

// Error
$_['error_bulk_link_permission'] 	= 'Масовото посочване не е разрешено с вашият план, моля сменете плана';
