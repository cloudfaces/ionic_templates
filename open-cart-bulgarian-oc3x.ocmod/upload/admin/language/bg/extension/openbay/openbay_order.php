<?php
// Heading
$_['heading_title']   			= 'Масово обновяване на поръчки';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_confirm_title']        = 'Прегледай масово обновяване на поръчки';

// Button
$_['button_status']           	= 'Промяна на статус';
$_['button_update']           	= 'Обновяване';

// Column
$_['column_channel']        	= 'Канал на поръчка';
$_['column_additional']     	= 'Допълнителна информация';
$_['column_comments']      		= 'Коментари';
$_['column_notify']        		= 'Извести';

// Text
$_['text_confirmed']            = '%s бяха маркирани %t';
$_['text_no_orders']            = 'Не бяха избрани поръчки за обновяване';
$_['text_confirm_change_text']  = 'Сменяне на статуса на поръчките към';
$_['text_other']                = 'Друг';
$_['text_error_carrier_other']  = 'На поръчката липсва "Други доставчици" поле!';
$_['text_web']                  = 'Уеб';
$_['text_ebay']                 = 'eBay';
$_['text_amazon']               = 'Amazon EU';
$_['text_amazonus']             = 'Amazon US';
$_['text_etsy']             	= 'Etsy';
$_['text_list']					= 'Списък с поръчки';

// Entry
$_['entry_carrier']             = 'Доставчик';
$_['entry_tracking_no']         = 'Проследяване';
$_['entry_other']               = 'Друго';
$_['entry_date_added']          = 'Дата на добавяне';
$_['entry_order_channel']       = 'Канал за поръчки';
