<?php
// Heading
$_['heading_title']					= 'Посочване на продукти';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_amazon']					= 'Amazon EU';

// Text
$_['text_desc1']                    = 'Посочването на вашите продукти, ще разреши стоков контрол в списъците на Амазон.';
$_['text_desc2'] 					= 'За всеки продукт обновен в местният списък (количестата от вашият онлайн магазин) ще бъде обновено и количеството в Амазон';
$_['text_desc3']                    = 'Може да добавяте продукти ръчно, вкарвайки Amazon SKU номер и име на продукт или заредете всички не посочени продукти и после въведете Амазон SKUs. (Uploading products from OpenCart to Amazon will automatically add links)';
$_['text_new_link']                 = 'Нов линк';
$_['text_autocomplete_product']     = 'Продукт (Автоматично допълване)';
$_['text_amazon_sku']               = 'Amazon SKU номер';
$_['text_action']                   = 'Действие';
$_['text_linked_items']             = 'Посочени продукти';
$_['text_unlinked_items']           = 'Непосочени продукти';
$_['text_name']                     = 'Име';
$_['text_model']                    = 'Модел';
$_['text_combination']              = 'Комбинация';
$_['text_sku']                      = 'SKU номер';
$_['text_sku_variant']              = 'Variant SKU';
$_['text_amazon_sku']               = 'Amazon SKU номер';

// Button
$_['button_load']                 	= 'Зареди';

// Error
$_['error_empty_sku']        		= 'Попълни полето Amazon SKU!';
$_['error_empty_name']       		= 'Попълни име на продукта!';
$_['error_no_product_exists']       = 'Продуктът не съществува.';
