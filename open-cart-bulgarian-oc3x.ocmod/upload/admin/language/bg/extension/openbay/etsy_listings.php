<?php
// Headings
$_['heading_title']        		= 'Etsy обяви';
$_['text_openbay'] = 'OpenBay Pro';
$_['text_etsy'] = 'Etsy';

// Text
$_['text_link_saved'] = 'Елемент се свързва';
$_['text_activate'] = 'Активиране';
$_['text_deactivate'] = 'Дезактивиране';
$_['text_add_link'] = 'Добавяне на връзка';
$_['text_delete_link'] = 'Изтриване на връзка';
$_['text_delete'] = 'Изтриване на обявата';
$_['text_status_stock'] = 'Наличността не е синхронизирана';
$_['text_status_ok'] = 'ОК';
$_['text_status_nolink'] = 'Не е свързан';
$_['text_link_added'] = 'Продукт е бил свързан с обявата';
$_['text_link_deleted'] = 'Продуктът е прекратен от обявата';
$_['text_item_ended'] = 'Продуктът е бил отстранен от Etsy';
$_['text_item_deactivated'] = 'Продуктът е деактивирана в Etsy';
$_['text_item_activated'] = 'Продуктът е бил активиран в Etsy';
$_['text_confirm_end'] = 'Сигурни ли сте, че искате да премахнете обявата?';
$_['text_confirm_deactivate'] = 'Сигурни ли сте, искате да деактивирате обявата?';
$_['text_confirm_activate'] = 'Сигурни ли сте, искате да активирате обявата?';
$_['text_listings'] = 'Управление на вашите Etsy обяви';

// Columns
$_['column_listing_id'] = 'Etsy ID';
$_['column_title'] = 'Заглавие';
$_['column_listing_qty'] = 'Количество на обявата';
$_['column_store_qty'] = 'Количество в магазина';
$_['column_status'] = 'Съобщение статус';
$_['column_link_status'] = 'Статус на връзката';
$_['column_action'] = 'Действие';

// Entry
$_['entry_limit'] = 'Ограничение на страницата';
$_['entry_status'] = 'Състояние';
$_['entry_keywords'] = 'Ключови думи';
$_['entry_name'] = 'Име на продукта';
$_['entry_etsy_id'] = 'Etsy ID';

// Help
$_['help_keywords'] = 'Ключовите думи важат само с активни обяви';

// Error
$_['error_etsy'] = 'Грешка! Etsy API отговор:';
$_['error_product_id'] = 'Изисква продуктов номер';
$_['error_etsy_id'] = 'Изисква Etsy продуктов номер';
