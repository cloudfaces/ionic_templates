<?php
// Heading
$_['heading_title']             = 'Абонамент';
$_['text_openbay'] = 'OpenBay Pro';
$_['text_ebay'] = 'иБей';

// Buttons
$_['button_plan_change'] = 'Промяна на план';

// Columns
$_['column_plan'] = 'Име на план';
$_['column_call_limit'] = 'Ограничение';
$_['column_price'] = 'Цена (на месец)';
$_['column_description'] = 'Описание';
$_['column_current'] = 'Сегашен план';

// Text
$_['text_subscription_current'] = 'Сегашен план';
$_['text_subscription_avail'] = 'Налични планове';
$_['text_subscription_avail1'] = 'Промяната на плановете ще бъдат извършени незабавно.';
$_['text_ajax_acc_load_plan'] = 'PayPal абонамент ID:';
$_['text_ajax_acc_load_plan2'] = ', трябва да отмените всички останали абонаменти от нас';
$_['text_load_my_plan'] = 'Зареждане на вашия план';
$_['text_load_plans'] = 'Зареждане на наличните планове';
$_['text_subscription'] = 'Промяна на OpenBay Pro абонамент';

// Errors
$_['error_ajax_load'] = 'За съжаление, не може да се получи отговор. Опитайте по-късно.';