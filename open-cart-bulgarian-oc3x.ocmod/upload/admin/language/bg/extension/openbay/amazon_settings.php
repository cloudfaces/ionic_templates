<?php
// Heading
$_['heading_title']        				= 'Настройки на пазар';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon EU';

// Text
$_['text_api_status']               	= 'API статус на връзка';
$_['text_api_ok']                   	= 'Връзката OK, Вписване OK';
$_['text_api_auth_error']           	= 'Връзка OK, Вписване неуспешно';
$_['text_api_error']                	= 'Грешка при свързване';
$_['text_order_statuses']           	= 'Статуси на поръчките';
$_['text_unshipped']                	= 'Непубликувано';
$_['text_partially_shipped']        	= 'Частично изпратено';
$_['text_shipped']                  	= 'Изпратено';
$_['text_canceled']                 	= 'Отменено';
$_['text_other']                    	= 'Друго';
$_['text_marketplaces']             	= 'Пазари';
$_['text_markets']                  	= 'Изберете пазари, от които може да вмъкнете вашите поръчки';
$_['text_de']                       	= 'Германия';
$_['text_fr']                       	= 'Франция';
$_['text_it']                       	= 'Италия';
$_['text_es']                       	= 'Испания';
$_['text_uk']                       	= 'Великобритания';
$_['text_settings_updated']        		= 'Настройките бяха обновени успешно.';
$_['text_new'] 							= 'Ново';
$_['text_used_like_new'] 				= 'Използвано - Като ново';
$_['text_used_very_good'] 				= 'Използвано - Много добро';
$_['text_used_good'] 					= 'Използвано - Добро';
$_['text_used_acceptable'] 				= 'Използвано - Приемливо';
$_['text_collectible_like_new'] 		= 'Колекционерско - Като ново';
$_['text_collectible_very_good'] 		= 'Колекционерско - Много добро';
$_['text_collectible_good'] 			= 'Колекционерско - Добро';
$_['text_collectible_acceptable'] 		= 'Колекционерско - Приемливо';
$_['text_refurbished'] 					= 'Поправено';

// Error
$_['error_permission']         			= 'Вие нямате права да променяте настройките';

// Entry
$_['entry_status']                 		= 'Статус';
$_['entry_token']                    	= 'Token';
$_['entry_string1']              		= 'Шифрирана линия 1';
$_['entry_string2']              		= 'Шифрирана линия 2';
$_['entry_import_tax']               	= 'Данък за импортираните продукти';
$_['entry_customer_group']           	= 'Група клиенти';
$_['entry_tax_percentage']           	= 'Промени цена';
$_['entry_default_condition']        	= 'Състояние на продукта по подразбиране';
$_['entry_marketplace_default']			= 'Пазар по подразбиране';
$_['entry_notify_admin']             	= 'Информирай админа за новипоръчки';
$_['entry_default_shipping']         	= 'Доставка по подразбиране';

// Tabs
$_['tab_settings']            			= 'API детайли';
$_['tab_listing']                  		= 'Списъци';
$_['tab_orders']                   		= 'Поръчки';

// Help
$_['help_import_tax']          			= 'Използва се ако Амазон не даде информация за данъка';
$_['help_customer_group']      			= 'Изберете група клиенти, която да използва импортираните поръчки';
$_['help_default_shipping']    			= 'Използва се като предварителна опция, преди масово обновяване на поръчка';
$_['help_entry_marketplace_default']	= 'Пазар по подразбиране за списъците с продукти';
$_['help_tax_percentage']           	= 'процент добавен към цената по подразбиране';