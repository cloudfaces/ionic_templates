<?php
// Heading
$_['heading_title'] 				= 'Масово качване на списъци';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Амазон Европа';

// Text
$_['text_searching'] 				= 'Търсене';
$_['text_finished'] 				= 'Завърши';
$_['text_marketplace'] 				= 'Пазар';
$_['text_de'] 						= 'Германия';
$_['text_fr'] 						= 'Франция';
$_['text_es'] 						= 'Испания';
$_['text_it'] 						= 'Италия';
$_['text_uk'] 						= 'Великобритания';
$_['text_dont_list'] 				= 'Не покзавай в списъка';
$_['text_listing_values'] 			= 'Стойностина списъка';
$_['text_new'] 						= 'Нов';
$_['text_used_like_new'] 			= 'Използвано - Като ново';
$_['text_used_very_good'] 			= 'Използвано - Много добро';
$_['text_used_good'] 				= 'Използвано - Добро';
$_['text_used_acceptable'] 			= 'Използвано - Приемливо';
$_['text_collectible_like_new'] 	= 'Колекционерско - Като ново';
$_['text_collectible_very_good'] 	= 'Колекционерско - Много добро';
$_['text_collectible_good'] 		= 'Колекционерско - Добро';
$_['text_collectible_acceptable'] 	= 'Колекционерско - Приемливо';
$_['text_refurbished'] 				= 'Поправено';

// Entry
$_['entry_condition'] 				= 'Състояние';
$_['entry_condition_note'] 			= 'Бележка за състоянието';
$_['entry_start_selling'] 			= 'Започни да продаваш';

// Column
$_['column_name'] 					= 'Име';
$_['column_image'] 					= 'Изображение';
$_['column_model'] 					= 'Модел';
$_['column_status'] 				= 'Статус';
$_['column_matches']				= 'Съвпадения';
$_['column_result'] 				= 'Резултат';

// Button
$_['button_list'] 					= 'Списък';

// Error
$_['error_product_sku'] 			= 'Продукта трябва да има SKU';
$_['error_searchable_fields'] 		= 'Продукта трябва да съдържа ISBN, EAN, UPC или JAN неуспешно обновяване';
$_['error_bulk_listing_permission'] = 'Масовото качване на списъци не е разрешено за вашия профил, моля надстройте плана!';
$_['error_select_items'] 			= 'Вие тряба да изберете поне 1 продукт за търсенето';
