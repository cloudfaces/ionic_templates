<?php
// Heading
$_['heading_title'] 				= 'Нов списък продукти за Амазон';
$_['text_title_advanced'] 			= 'Подробни списъци';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Амазон Европа';

// Buttons
$_['button_new'] 					= 'Създаване на нов продукт';
$_['button_amazon_price'] 			= 'Вземи цената от Амазон';
$_['button_list'] 					= 'Публикувай в Амазон';
$_['button_remove_error'] 			= 'Премахване на съобщенията за грешки!';
$_['button_save_upload'] 			= 'Запази и качи';
$_['button_browse'] 				= 'Отвори';
$_['button_saved_listings'] 		= 'Покажи запазените списъци';
$_['button_remove_links'] 			= 'Премахни линкове';
$_['button_create_new_listing'] 	= 'Създай нов списък за публикуване в Амазон';

// Help
$_['help_sku'] 						= "Уникално продуктово ID вписано от търговеца";
$_['help_restock_date'] 			= "Това е дата, която ще е възможно да доставите поръчки със статус 'изчакваща поръчка' на клиента (back-ordered). Тази дата не трябва да е повече от 30 дена, възможно е след това автоматично поръчката да бъде отменена.";
$_['help_sale_price'] 				= "Цената за намалените продукти трябва да има начална и крайна дата";

//Text
$_['text_products_sent'] 			= 'Продуктите бяха изпратени за обработка';
$_['button_view_on_amazon'] 		= 'Покажи в Амазон';
$_['text_list'] 					= 'Качи в Амаон';
$_['text_new'] 						= 'Нов';
$_['text_used_like_new'] 			= 'Използвано - Като ново';
$_['text_used_very_good'] 			= 'Използвано - Много добро';
$_['text_used_good'] 				= 'Използвано - Добро';
$_['text_used_acceptable'] 			= 'Използвано - Приемливо';
$_['text_collectible_like_new'] 	= 'Колекционерско - Като ново';
$_['text_collectible_very_good'] 	= 'Колекционерско - Много добро';
$_['text_collectible_good'] 		= 'Колекционерско - Добро';
$_['text_collectible_acceptable'] 	= 'Колекционерско - Приемливо';
$_['text_refurbished'] 				= 'Поправено';
$_['text_product_not_sent'] 		= 'Продуктът не беше качен в Амазон. Причина: %s';
$_['text_not_in_catalog'] 			= "Или ако не е посочен в каталога&nbsp;&nbsp;&nbsp;";
$_['text_placeholder_search'] 		= 'Въведи име на продукта, UPC, EAN, ISBN или ASIN';
$_['text_placeholder_condition'] 	= 'Използвайте полето, за да опишете състоянието на вашия продукт.';
$_['text_characters'] 				= 'знаци';
$_['text_uploaded'] 				= 'Съхранените списъци качени!';
$_['text_saved_local'] 				= 'Списъкците са запазени, но все още не са качени';
$_['text_product_sent'] 			= 'Продукта беше успешно изпратен до Amazon.';
$_['text_links_removed'] 			= 'Амазон продуктови линкове бяха изтрити';
$_['text_product_links'] 			= 'Линкове към продукт';
$_['text_has_saved_listings'] 		= 'Този продукт има един или повече съхранени списъци, които не са качени';
$_['text_edit_heading'] 			= 'Редактиране на списък';
$_['text_germany'] 					= 'Германия';
$_['text_france'] 					= 'Франция';
$_['text_italy'] 					= 'Италия';
$_['text_spain'] 					= 'Испания';
$_['text_united_kingdom'] 			= 'Великобритания';

// Columns
$_['column_image'] 					= 'Изображение';
$_['column_asin'] 					= 'ASIN номер';
$_['column_price'] 					= 'Цена';
$_['column_action'] 				= 'Действие';
$_['column_name'] 					= 'Име на продукт';
$_['column_model'] 					= 'Модел';
$_['column_combination'] 			= 'Комбинации';
$_['column_sku_variant'] 			= 'Variant SKU';
$_['column_sku'] 					= 'SKU номер';
$_['column_amazon_sku'] 			= 'Amazon продуктов SKU номер';

// Entry
$_['entry_sku'] 					= 'SKU номер';
$_['entry_condition'] 				= 'Състояние';
$_['entry_condition_note'] 			= 'Бележка за състояние';
$_['entry_price'] 					= 'Цена';
$_['entry_sale_price'] 				= 'Разпродажна цена';
$_['entry_sale_date'] 				= 'Обхват на разпродажбата (дата)';
$_['entry_quantity'] 				= 'Количество';
$_['entry_start_selling'] 			= 'Налично от дата';
$_['entry_restock_date'] 			= 'Дата за получаване на нова стока';
$_['entry_country_of_origin'] 		= 'Страна производител';
$_['entry_release_date'] 			= 'Дата за пускане в продажба';
$_['entry_from'] 					= 'Дата от';
$_['entry_to'] 						= 'Дата до';
$_['entry_product'] 				= 'Списъци за продукти';
$_['entry_category'] 				= 'Амазон категории';
$_['entry_browse_node'] 			= 'Избери точка за отваряне (browse node)';
$_['entry_marketplace'] 			= 'Пазар';

//Tabs
$_['tab_main'] 						= 'Главен';
$_['tab_required'] 					= 'Задължителна информация';
$_['tab_additional'] 				= 'Допълнителни опции';

// Error
$_['error_required'] 				= 'Това поле е задължително!';
$_['error_not_saved'] 				= 'Списъкът не беше запазен. Проверете всички полета';
$_['error_char_limit'] 				= 'Символите са над разрешените';
$_['error_length'] 					= 'Минималната дължина е';
$_['error_upload_failed'] 			= 'Неуспешно качване на продукт с SKU номер: "%s". Причина: "%s" Процесът на качване е отменен.';
$_['error_load_nodes'] 				= 'Неуспешно зареждане на точки за отваряне (browse nodes)';
$_['error_connecting'] 				= 'Имаше проблем със свързването на API. Моля проверете OpenBay Pro Amazon настройки. Ако проблема продължава, моля свържете се с поддръжка.';
$_['error_text_missing'] 			= 'Вие трябва да въведете детайли за търсенето';
$_['error_missing_asin'] 			= 'ASIN номера липсва';
$_['error_marketplace_missing'] 	= 'Моля изберете пазар';
$_['error_condition_missing'] 		= "моля изберете състояние";
$_['error_amazon_price'] 			= 'Неуспешно изтегляне на цената от Амазон';
$_['error_stock'] 					= 'Неможе да качвате продукт, на който количеството е под 1 ';
$_['error_sku'] 					= 'Вие трябва да въведете SKU номер за продукта';
$_['error_price'] 					= 'Вие трябва да въведете цена на продукта';
$_['error_sending_products'] 		= 'Неуспешно качване на продукти в списъка. Моля свържете се с поддръжка';
$_['error_no_products_selected'] 	= 'Няма избрани продукти за качване в списъка';
$_['error_not_searched'] 			= 'Потърсете за съществуващи продукти, преди да качвате в списъка. Продуктите трябва да съвпадат с тези от каталога на Амазон';
