<?php
// Headings
$_['heading_title']        		= 'Настройки на пазар';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_etsy']                 = 'Etsy';

// Text
$_['text_success']     			= 'Вашите настройки бяха запазени';
$_['text_status']         		= 'Статус';
$_['text_account_ok']  			= 'Връзка до Etsy OK';
$_['text_api_other']            = 'Линкове';
$_['text_token_register']       = 'Регистриране';
$_['text_api_ok']       		= 'API връзка OK';
$_['text_pull_orders']    		= 'Изтегли поръчки';
$_['text_sync_settings']    	= 'Настройки засинхронизация';
$_['text_complete']    			= 'Завършена';
$_['text_failed']    			= 'Неуспешна';
$_['text_orders_imported']    	= 'Беше заявено изтелгяне на поръчките';
$_['text_api_status']           = 'API връзка';
$_['text_edit']           		= 'Редактирай настройките';

// Entry
$_['entry_import_def_id']       = 'Въведи настройки по подразбиране:';
$_['entry_import_paid_id']      = 'Платен статус:';
$_['entry_import_shipped_id']   = 'Доставка статус:';
$_['entry_enc1']            	= 'API криптиране 1';
$_['entry_enc2']            	= 'API криптиране 2';
$_['entry_token']            	= 'API отговор';
$_['entry_address_format']      = 'Формат на адреса по подразбиране';

// Error
$_['error_api_connect']         = 'Неуспешно свързване с API';
$_['error_account_info']    	= 'Неуспешно индифициране на API връзка до Etsy ';

// Tabs
$_['tab_api_info']            	= 'API детайли';

// Help
$_['help_address_format']  		= 'Използва се само, ако посочената страна няма зададен адерес по подаразбиране.';
$_['help_sync_settings']  		= 'Това ще обнови вашата база данни с последните настройки, като дати, вид състояния и др..';
$_['help_pull_orders']  		= 'Това ще предизвика ръчно вкарване на нови и обновени поръчки.';