<?php
// Heading
$_['heading_title']        				= 'Обновяване на стока';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon EU';

// Text
$_['text_empty']                    	= 'Няма резултат!';

// Entry
$_['entry_date_start']               	= 'Начална дата';
$_['entry_date_end']                 	= 'Крайна дата';

// Column
$_['column_ref']                      	= 'Реф';
$_['column_date_requested']           	= 'Изискано на дата';
$_['column_date_updated']             	= 'Дата на обновяване';
$_['column_status']                   	= 'Статус';
$_['column_sku']                      	= 'Amazon SKU';
$_['column_stock']                    	= 'Стока';