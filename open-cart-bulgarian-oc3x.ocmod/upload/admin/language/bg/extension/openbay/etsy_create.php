<?php
// Headings
$_['heading_title']      		= 'Нова обява';
$_['text_openbay']          	= 'OpenBay Pro';
$_['text_etsy']             	= 'Etsy';

// Tabs
$_['tab_additional']      		= 'Допълнителна информация';

// Text
$_['text_option']      			= 'Избери опция';
$_['text_category_selected']	= 'Избрана категория';
$_['text_material_add']  		= 'Добави материал';
$_['text_material_remove']  	= 'Премахни материал';
$_['text_tag_add']  			= 'Добави таг';
$_['text_tag_remove']  			= 'Премахни таг';
$_['text_created']  			= 'Вашата обява беше съсздадена';
$_['text_listing_id']  			= 'Номер на обява';
$_['text_img_upload']  			= 'Качване на изображение';
$_['text_img_upload_done']  	= 'Качено изображение';
$_['text_create']  				= 'Създай нова Etsy обява';

// Entry
$_['entry_title']      			= 'Име на продукта';
$_['entry_description']     	= 'Описание';
$_['entry_price']      			= 'Цена';
$_['entry_non_taxable']     	= 'не се таксува';
$_['entry_category']     		= 'Главна категория';
$_['entry_sub_category']    	= 'Под-категория';
$_['entry_sub_sub_category']	= 'Под-под-категория';
$_['entry_who_made']			= 'Кой го създаде?';
$_['entry_when_made']			= 'Кога се създаде?';
$_['entry_recipient']			= 'За кога е?';
$_['entry_occasion']			= 'За какъв повод е?';
$_['entry_is_supply']			= 'Това доставка ли е?';
$_['entry_state']      			= 'Статус на обявата';
$_['entry_style']      			= 'Таг 1';
$_['entry_style_2']      		= 'Таг 2';
$_['entry_processing_min']  	= 'Минимално време за обработка';
$_['entry_processing_max']  	= 'Максимално време за обработка';
$_['entry_materials']  			= 'Материали';
$_['entry_tags']  				= 'Тагове';
$_['entry_shipping']  			= 'Профил за доставка';
$_['entry_shop']  				= 'Секция на магазина';
$_['entry_is_custom']  			= 'Може ли да бъде променено?';
$_['entry_image']  				= 'Главно изображение';
$_['entry_image_other']			= 'Други изображения';

// Help
$_['help_description']			= 'Всичкият HTML код беше премахнат от описанието, тъй като не се поддържа';

// Errors
$_['error_no_shipping']  		= 'Не сте настроили профил за доставка!';
$_['error_no_shop_secton']  	= 'Вие не сте настроили секции в магазина!';
$_['error_no_img_url']  		= 'Не сте избрали изображение за качване';
$_['error_no_listing_id']  		= 'Няма ноемр на обява';
$_['error_title_length']  		= 'Името е твърде дълго';
$_['error_title_missing']  		= 'Името липсва';
$_['error_desc_missing']  		= 'Описанието липсва или е празно';
$_['error_price_missing']  		= 'Цената е празна или липсва';
$_['error_category']  			= 'Не е избрана категория';
$_['error_style_1_tag']  		= 'Таг 1 не е валиден';
$_['error_style_2_tag']  		= 'Таг 2 е невалиден';
$_['error_materials']  			= 'Вие можете да добавите само 13 материала';
$_['error_tags']  				= 'Вие можете да добавите сам 13 тага';
$_['error_stock_max']  			= 'Максималната наличност, която меже да обявите с Etsy е 999, вие имате %s наличност';
$_['error_image_max']  			= 'Максималният брой изображения, разрешени за използване с Etsy е 5, вие сте избрали %s';
$_['error_variant']				= 'Variant items on Etsy.com are not yet supported by OpenBay Pro';