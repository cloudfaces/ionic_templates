<?php
// Heading
$_['heading_title']                     = 'Profiles';
$_['text_openbay']                      = 'OpenBay Pro';
$_['text_ebay']                         = 'eBay';

// Tabs
$_['tab_returns'] = 'Рекламация';
$_['tab_template'] = 'Шаблони';
$_['tab_gallery'] = 'Галерия';
$_['tab_settings'] = 'Насройки';

//Shipping Profile
$_['text_shipping_dispatch_country'] = 'Цена за доставка от страна';
$_['text_shipping_postcode'] = 'Пощенска / Zip код на населено място';
$_['text_shipping_location '] =' Град или държава на населено място';
$_['text_shipping_despatch'] = 'Времето за изпращане';
$_['text_shipping_despatch_help'] = 'Това е максималният брой на дни, за да изпратите продукта';
$_['text_shipping_nat'] = 'Национални спедиторски услуги';
$_['text_shipping_intnat'] = 'Международни спедиторски услуги';
$_['text_shipping_first'] = 'Първи елемент';
$_['text_shipping_add'] = 'Допълнителни елементи';
$_['text_shipping_service'] = 'Услуги';
$_['text_shipping_in_desc'] = 'Информация за товарителница в описанието';
$_['text_shipping_getitfast'] = 'Вземи сега!';
$_['text_shipping_zones'] ='Зони за доставка';
$_['text_shipping_worldwide'] = 'Целият свят';
$_['text_shipping_type_nat'] = 'Национални видове спедитори';
$_['text_shipping_type_int'] = 'Междуанродни видове спедитори';
$_['text_shipping_flat'] = 'Такса доставка';
$_['text_shipping_calculated'] = 'Изчислено';
$_['text_shipping_freight'] = 'Товарене';
$_['text_shipping_handling'] = 'Опаковъчна такса';
$_['text_shipping_cod'] = 'Такса за наложен платеж';
$_['text_shipping_handling_nat'] = 'Опаковъчна такса (национална)';
$_['entry_shipping_handling_int'] = 'Опаковъчна такса (международна)';
$_['entry_shipping_pickupdropoff']  	= 'Click and Collect';
$_['entry_shipping_pickupinstore']  	= 'Available for In-Store Pickup';
$_['entry_shipping_global_shipping']  	= 'Use eBay global shipping service';
$_['entry_shipping_promotion_discount'] = 'Combined shipping discounts (national)';
$_['entry_shipping_promotion_discount_international'] = 'Combined shipping discounts (international)';

//Returns profile
$_['text_returns_accept'] = 'Рекламацията е приета';
$_['text_returns_inst'] = 'Политика на връщане';
$_['text_returns_days'] = 'Дни за връщане';
$_['text_returns_days10'] = '10 Дни';
$_['text_returns_days14'] = '14 Дни';
$_['text_returns_days30'] = '30 Дни';
$_['text_returns_days60'] = '60 Дни';
$_['text_returns_type'] = 'Вид връщае';
$_['text_returns_type_money'] = 'Пари обратно';
$_['text_returns_type_exch'] = 'Пари обратно или замяна';
$_['text_returns_costs'] = 'Връщане на доставката';
$_['text_returns_costs_b'] = 'Купувач плаща';
$_['text_returns_costs_s'] = 'Продавач плаща';
$_['text_returns_restock'] = 'Такса за ново зареждане';
$_['text_list'] = 'Списък на профил';

//Template profile
$_['text_template_choose'] = 'Шаблон по подразбиране';
$_['text_template_choose_help'] = 'Шаблон по подразбиране, автоматично ще се зареди, за да се спести време';
$_['text_image_gallery'] = 'Размер на изображението в галерията';
$_['text_image_gallery_help'] = 'Размер на пикселите в галерията с изображения, които се добавят към вашия шаблон.';
$_['text_image_thumb'] = 'Thumbnail размер на изображението';
$_['text_image_thumb_help'] = 'Размер на пикселите на миниятурите, които се добавят към вашия шаблон.';
$_['text_image_super'] = 'Големи снимки';
$_['text_image_gallery_plus'] = 'Галерия плюс';
$_['text_image_all_ebay'] = 'Добавяне на всички изображения в иБей';
$_['text_image_all_template'] = 'Добавяне на всички изображения към Шаблон';
$_['text_image_exclude_default'] = 'Изключване на изображението по подразбиране';
$_['text_image_exclude_default_help'] = 'Само за опцията за масово качване на продукти! Няма да включва стандартното изображение на продукта в списъка с изображения на темата';
$_['text_confirm_delete'] = 'Сигурни ли сте, че искате да изтриете профила?';
$_['text_width'] = 'Ширина';
$_['text_height'] = 'Височина';
$_['text_px'] = 'Пиксел';
$_['text_add'] = 'Добавяне на профил';
$_['text_edit'] = 'Редакция на профил';

//General profile
$_['text_general_private'] = 'Обяви продуктите като частен търг';
$_['text_general_price'] = 'Цена % промяна';
$_['text_general_price_help'] = '0 е по подразбиране, -10, ще намали с 10%, 10 ще увеличи с 10% (използва се само за масови качвания)';

//General profile options
$_['text_profile_name'] = 'Име';
$_['text_profile_default'] = 'По подразбиране';
$_['text_profile_type'] = 'Вид';
$_['text_profile_desc'] = 'Описание';
$_['text_profile_action'] = 'Действие';

// Profile types
$_['text_type_shipping'] = 'Доставка';
$_['text_type_returns'] = 'Връщане';
$_['text_type_template'] = 'Шаблон &amp; галерия';
$_['text_type_general'] = 'Общи настройки';

//Success messages
$_['text_added'] = 'Добавен е нов профил';
$_['text_updated'] = 'Профилът е бил актуализиран';

//Errors
$_['error_permission'] = 'Не е нужно разрешение за редактиране на профили';
$_['error_name'] = 'Трябва да въведете име профил';
$_['error_no_template'] = 'Номер на шаблона не съществува';
$_['error_missing_settings'] = 'Не можете да добавяте, променяте или изтривате профили докато не синхронизирате настройките си с иБей';

//Help
$_['help_shipping_promotion_discount']  = 'Offer national buyers a discount on shipping if they buy multiple items. Discounts must have been setup in eBay to take effect.';
$_['help_shipping_promotion_discount_international']  = 'Offer international buyers a discount on shipping if they buy multiple items. Discounts must have been setup in eBay to take effect.';