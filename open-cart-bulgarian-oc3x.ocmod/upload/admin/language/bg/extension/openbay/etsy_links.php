<?php
// Headings
$_['heading_title']        		= 'Etsy линкове';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_etsy']                 = 'Etsy';

// Text
$_['text_loading']              = 'Зареждане на продукти';
$_['text_new_link']             = 'Създай нов линк';
$_['text_current_links']        = 'Сегашни линкове';
$_['text_link_saved']           = 'Продукта беше посочен';
$_['text_no_links']           	= 'Нямате продукти псочени към Etsy продукти';

// Columns
$_['column_product']			= 'Име на продукта';
$_['column_item_id']			= 'Etsy номер';
$_['column_store_stock']		= 'Наличност';
$_['column_etsy_stock']			= 'Etsy наличност';
$_['column_status']				= 'Статус';
$_['column_action']				= 'Действие';

// Entry
$_['entry_name']				= 'Име на продукта';
$_['entry_etsy_id']				= 'Etsy номер на продукт';

// Error
$_['error_product']				= 'Продуктът не съществува във вашият магаизн';
$_['error_stock']				= 'Неможе да посочите продукт без наличност';
$_['error_product_id']			= 'Изисква се номер на продукт';
$_['error_etsy_id']				= 'Изисква се Etsy номер на продукт';
$_['error_link_id']				= 'Изисква се номер на линк';
$_['error_link_exists']			= 'Има активен линк към този елемент';
$_['error_etsy']				= 'Неуспешно посочване, Etsy API отговор: ';
$_['error_status']				= 'Изисква се статус филтър';
