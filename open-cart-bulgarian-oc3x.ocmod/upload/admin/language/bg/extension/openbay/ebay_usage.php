<?php
// Headings
$_['heading_title']             = 'Потребление';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_ebay']                 = 'eBay';

// Text
$_['text_usage']                = 'Потребление на вашият профил';

// Errors
$_['error_ajax_load']      		= 'Съжаляваме няма отговор, моля опитайте пак по-късно.';
