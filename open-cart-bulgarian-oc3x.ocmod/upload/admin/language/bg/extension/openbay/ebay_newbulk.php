<?php
//Heading
$_['heading_title']               	= 'New listings';
$_['text_ebay']               		= 'eBay';
$_['text_openbay']               	= 'Openbay Pro';

// Buttons
$_['text_none'] = 'Няма';
$_['text_preview'] = 'Преглед';
$_['text_add'] = 'Добавяне';
$_['text_preview_all'] = 'Потвърди всички';
$_['text_submit'] = 'Приеми';
$_['text_features'] = 'функции';
$_['text_catalog'] = 'Изберете каталог';
$_['text_catalog_search'] = 'Търсене в каталог';
$_['text_search_term'] = 'Търсене в условия за ползвяне';
$_['text_close'] = 'Затвори';
$_['text_bulk'] = 'Създаване на масови нови обяви';

//Form options / text
$_['text_pixels'] = 'Пиксела';
$_['text_other'] = 'Други';

//Profile names
$_['text_profile'] = 'Профили';
$_['text_profile_theme'] = 'Тема на профила';
$_['text_profile_shipping'] = 'Профил доставка';
$_['text_profile_returns'] = 'Профил рекламации';
$_['text_profile_generic'] = 'Профил главен';

//Text
$_['text_title'] = 'Заглавие';
$_['text_price'] = 'Цена';
$_['text_stock'] = 'Наличност';
$_['text_search'] = 'Търсене';
$_['text_loading'] = 'Зареждане на детайли';
$_['text_preparing0'] = 'Подготовка';
$_['text_preparing1'] = 'на';
$_['text_preparing2'] = 'елементи';
$_['entry_condition'] = 'Състояние';
$_['text_duration'] = 'Продължителност';
$_['text_category'] = 'Категория';
$_['text_exists'] = 'Някои продукти са вече регистрирани в иБей, така че бяха отстранени';
$_['text_error_count'] = 'Вие сте избрали %s продукта, може да отнеме известно време, за да обработим данните';
$_['text_verifying'] = 'Проверка на елементи';
$_['text_processing'] = 'Обработка на <span id="activeItems"></span> елементите';
$_['text_listed'] = 'Продуктите са добавени! ID:';
$_['text_ajax_confirm_listing'] = 'Сигурни ли сте, искате да масово качване за тези продукти?';
$_['text_bulk_plan_error'] = 'Текущият ви план не дава възможност за масови качвания, обновите своя план <a href="%s"> тук </a>';
$_['text_item_limit'] = 'Не можете да качвате повече продукти, надвишавате лимита си за вашият план, обновете своя план <a href="%s"> тук </a>';
$_['text_search_text'] = 'Въведете текст за търсене';
$_['text_catalog_no_products'] = 'Не бяха намерени елементи';
$_['text_search_failed'] = 'Търсенето не успя';
$_['text_esc_key'] = 'Страницата беше скрита, но може да не приключи зареждането си';
$_['text_loading_categories'] = 'Зареждане на категориите';
$_['text_loading_condition'] = 'Зареждане състоянието на продукта';
$_['text_loading_duration'] = 'Зарежда продължителност на обявата';
$_['text_total_fee'] = 'Общо такси';
$_['text_category_choose'] = 'Намери категория';
$_['text_suggested'] = 'Предложени категории';
$_['text_product_identifiers']    	= 'Product identifiers';
$_['text_ean']    					= 'EAN';
$_['text_upc']    					= 'UPC';
$_['text_isbn']    					= 'ISBN';
$_['text_identifier_not_required']  = 'Not required';

//Errors
$_['text_error_ship_profile'] = 'Трябва да имате настроен профил за доствка по подразбиране';
$_['text_error_generic_profile'] = 'Трябва да има общ профил по подразбиране създаден';
$_['text_error_return_profile'] = 'Трябва да имате създаден профил за рекаламация';
$_['text_error_theme_profile'] = 'Трябва да имате създаден профил по подразбиране за тема';
$_['text_error_variants'] = 'Продукти с вариации не могат да бъдат масово качени и бяха деселектирани';
$_['text_error_stock'] = 'Някои елементи не са в наличност и са отстранени';
$_['text_error_no_product'] = 'Няма допустими продукти, избрани да използвате функцията за качване на веднъж';
$_['text_error_reverify'] = 'Имаше грешка, трябва да редактирате и отново проверите елементите';
$_['error_missing_settings'] = 'Не можете да качвате, докато не syncronise вашите настройки иБей';
$_['text_error_no_selection'] = 'Трябва да изберете поне 1 продукт, за да се обяви';
