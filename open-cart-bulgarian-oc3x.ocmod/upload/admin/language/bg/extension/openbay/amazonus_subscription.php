<?php
// Heading
$_['heading_title']        				= 'Абонамент';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon EU';

// Text
$_['text_current_plan']             	= 'Сегашен план';
$_['text_register_invite']          	= "Още нямате недайли за API връзка?";
$_['text_available_plans']          	= 'Налични планове';
$_['text_listings_remaining']       	= 'Оставащи в списъка';
$_['text_listings_reserved']        	= 'Продуктите се обработват';
$_['text_account_status']           	= 'Статус на акунта';
$_['text_merchantid']               	= 'Номер на търговеца';
$_['text_change_merchantid']        	= 'Промени';
$_['text_allowed']                  	= 'Разрешен';
$_['text_not_allowed']              	= 'Забранен';
$_['text_price']              			= 'Цена';
$_['text_name']              			= 'Име';
$_['text_description']              	= 'Описание';
$_['text_order_frequency']          	= 'Честота на импортиране на поръчките';
$_['text_bulk_listing']             	= 'Масови списъци';
$_['text_product_listings']         	= 'Списъци за месец';

// Columns
$_['column_name']                     	= 'Име';
$_['column_description']              	= 'Опсание';
$_['column_order_frequency']          	= 'История на импортираните поръчки';
$_['column_bulk_listing']             	= 'Масови списъци';
$_['column_product_listings']         	= 'Списъци за месец';
$_['column_price']                    	= 'Цена';

// Buttons
$_['button_change_plan']              	= 'Смяна на плана';
$_['button_register']                 	= 'Регистрация';
