<?php
// Heading
$_['heading_title'] = 'Табло';
// Text
$_['text_success'] = 'Успех: Вие променихте успешно настройките!';
$_['text_list'] = 'Табло списък с допълнения';
// Column
$_['column_name'] = 'Име на допълнение за таблото';
$_['column_width'] = 'Ширина';
$_['column_status'] = 'Статус';
$_['column_sort_order'] = 'Подреждане';
$_['column_action'] = 'Действие';
// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте настройките!';
?>