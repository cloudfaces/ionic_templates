<?php
// Heading
$_['heading_title'] = 'Теми';
// Text
$_['text_success'] = 'Успех: Вие успешно променихте настройките!';
$_['text_list'] = 'Списък с теми';
// Column
$_['column_name'] = 'Име на темата';
$_['column_status'] = 'Статус';
$_['column_action'] = 'Действие';
// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте настройките!';
?>