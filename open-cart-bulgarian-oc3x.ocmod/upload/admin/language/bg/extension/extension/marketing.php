<?php
// Heading
$_['heading_title']    = 'Търговия';

// Text
$_['text_success']     = 'Вие успешно променихте нстройките!';
$_['text_list']        = 'Аналитичен списък';

// Column
$_['column_name']      = 'Име на тържището';
$_['column_status']    = 'Статут';
$_['column_action']    = 'Действие';

// Error
$_['error_permission'] = 'Вие нямате права да променяте нстройките!';