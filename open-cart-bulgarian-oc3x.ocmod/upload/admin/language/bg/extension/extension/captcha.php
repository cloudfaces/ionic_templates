<?php
// Heading
$_['heading_title'] = 'Тест за сигурност';
// Text
$_['text_success'] = 'Вие успешно променихте настройките!';
$_['text_list'] = 'Списък';
// Column
$_['column_name'] = 'Име';
$_['column_status'] = 'Състояние';
$_['column_action'] = 'Действие';
// Error
$_['error_permission'] = 'Внимание Вие нямате права да променяте настройките!';
?>