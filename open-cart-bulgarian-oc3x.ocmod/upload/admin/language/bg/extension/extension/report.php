<?php
// Heading
$_['heading_title']     = 'Отчети';

// Text
$_['text_success']      = 'Вие успешно променихте настройките!';
$_['text_list']         = 'Списък отчети';

// Column
$_['column_name']       = 'Име на отчет';
$_['column_status']     = 'Статут';
$_['column_sort_order'] = 'Подреждане';
$_['column_action']     = 'Действие';

// Error
$_['error_permission']  = 'Вие нямате права да променяте настройките!';