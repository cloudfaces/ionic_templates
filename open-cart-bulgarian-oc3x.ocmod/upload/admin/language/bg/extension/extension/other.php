<?php
// Heading
$_['heading_title']     = 'Други';

// Text
$_['text_success']      = 'Вие успешно променихте настройките!';
$_['text_list']         = 'Списък';

// Column
$_['column_name']       = 'Други';
$_['column_status']     = 'Статут';
$_['column_action']     = 'Действие';

// Error
$_['error_permission']  = 'Вие нямате права да променяте настройките!';