<?php
// Heading
$_['heading_title']    = 'Меню';

// Text
$_['text_success']     = 'Вие успешно променихте нстройките!';
$_['text_list']        = 'Менюта списък';

// Column
$_['column_name']      = 'Име на меню';
$_['column_status']    = 'Статут';
$_['column_action']    = 'Действие';

// Error
$_['error_permission'] = 'Вие нямате права да промените настройките!';