<?php
// Heading
$_['heading_title']     = 'Обща сума';
// Text$_['text_success']      = 'Успешно променихте настройките!';$_['text_list']         = 'Списък с Обща сума';
// Column
$_['column_name']       = 'Обща сума';
$_['column_status']     = 'Статус';
$_['column_sort_order'] = 'Подреждане';
$_['column_action']     = 'Действие';
// Error$_['error_permission']  = 'Внимание: Нямате права да променяте параметрите на добавки към поръчките!';