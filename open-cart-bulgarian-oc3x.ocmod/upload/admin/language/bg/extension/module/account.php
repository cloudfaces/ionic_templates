<?php
// Heading
$_['heading_title'] = 'Акаунт';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Модулът успешно е обновен!';
$_['text_edit'] = 'Редактиране';
// Entry
$_['entry_status'] = 'Статус:';

// Error
$_['error_permission'] = 'Вие нямате право да променяте настройките!';
?>