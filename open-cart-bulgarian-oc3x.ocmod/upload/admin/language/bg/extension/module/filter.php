<?php
// Heading
$_['heading_title'] = 'Филтър';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките!';
$_['text_edit'] = 'Редактиране';
// Entry
$_['entry_status'] = 'Статус:';
// Error
$_['error_permission'] = 'Внимание нямате права да променяте настройките!';
?>