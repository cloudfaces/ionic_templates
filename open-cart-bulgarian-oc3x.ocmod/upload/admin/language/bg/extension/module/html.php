<?php
// Heading
$_['heading_title'] = 'HTML Съдържание';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихта настройките!';
$_['text_edit'] = 'Редактиране';
// Entry
$_['entry_name'] = 'Име на модела';
$_['entry_title'] = 'Име на страницата';
$_['entry_description'] = 'Описание';
$_['entry_status'] = 'Статус';

// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте настройките!';
$_['error_name'] = 'Името трябва да е от 3 до 64 символа!';
?>