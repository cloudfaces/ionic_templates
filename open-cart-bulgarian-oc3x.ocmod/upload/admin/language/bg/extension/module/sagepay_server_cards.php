<?php
// Heading
$_['heading_title'] = 'Sagepay Server Card Management';
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Вие успешно променихте настройките!';
$_['text_edit'] = 'Редакция на модула';
// Entry
$_['entry_status'] = 'Статус';
// Error
$_['error_permission'] = 'Вие нямате права да редактирате настройките!';
?>