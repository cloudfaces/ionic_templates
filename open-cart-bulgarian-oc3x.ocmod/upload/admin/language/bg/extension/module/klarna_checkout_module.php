<?php
// Heading
$_['heading_title'] = 'Klarna Checkout';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Вие успешно променихте настройките!';
//Entry
$_['entry_status'] = 'Статус';
//Error
$_['error_permission'] = 'Вие нямате права да променяте настойките!';
?>