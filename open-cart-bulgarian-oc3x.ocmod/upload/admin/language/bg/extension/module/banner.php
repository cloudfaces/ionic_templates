<?php
// Heading
$_['heading_title'] = 'Банери';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Модул Банери успешно е обновен!';
$_['text_edit'] = 'Редактиране';
// Entry
$_['entry_name'] = 'Име на модула';
$_['entry_banner'] = 'Банер';
$_['entry_dimension'] = 'Размери (Ш x В)';
$_['entry_width'] = 'Ширина';
$_['entry_height'] = 'Височина';
$_['entry_status'] = 'Статус';
// Error
$_['error_permission'] = 'Вие нямате права да променяте модулът "Банери"!';
$_['error_name'] = 'Името на модула трябва да бъде между 3 и 64 символа!';
$_['error_width'] = 'Посочете широчина!';
$_['error_height'] = 'Посоччете височина!';
?>