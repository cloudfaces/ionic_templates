<?php
// Heading
$_['heading_title'] = 'Слайдъри';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте параметрите на модула Слайдшоу!';
$_['text_edit'] = 'Редактиране';
// Entry
$_['entry_name'] = 'Име на модула';
$_['entry_banner'] = 'Лимит';
$_['entry_width'] = 'Широчина';
$_['entry_height'] = 'Височина';
$_['entry_status'] = 'Статус';

// Error
$_['error_permission'] = 'Нямате права да променяте този модул!';
$_['error_name'] = 'Името на модула трябва да е между 3 и 64 символа!';
$_['error_width'] = 'Изисква се широчина!';
$_['error_height'] = 'Изисква се височина!';
?>