<?php
// Heading
$_['heading_title'] = 'Магазин';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Настройките са обновени!';
$_['text_edit'] = 'Редактиране';
// Entry
$_['entry_admin'] = 'Само администратори:';
$_['entry_status'] = 'Статус:';

// Error
$_['error_permission'] = 'Вие нямате права да променяте този модул!';
?>