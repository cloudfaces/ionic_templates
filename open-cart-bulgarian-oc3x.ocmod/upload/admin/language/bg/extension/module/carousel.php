<?php
// Heading
$_['heading_title'] = 'Въртележка снимки на предната страница';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Модулът е обновен!';
$_['text_edit'] = 'Редактиране';
// Entry
$_['entry_name'] = 'Име на модула';
$_['entry_banner'] = 'Банер';
$_['entry_width'] = 'Ширина';
$_['entry_height'] = 'Височина';
$_['entry_status'] = 'Статуси';
// Error
$_['error_permission'] = 'Нямате права да променяте този модул!';
$_['error_name'] = 'Името трябва да е между 3 и 64 символа!';
$_['error_width'] = 'Въведете ширина!';
$_['error_height'] = 'Въведете височина!';
?>