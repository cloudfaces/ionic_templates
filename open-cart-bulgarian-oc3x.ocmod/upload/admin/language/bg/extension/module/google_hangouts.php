<?php
// Heading
$_['heading_title'] = 'Гугъл чат';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешно променихте настройките!';
$_['text_edit'] = 'Редактиране на настройки';
// Entry
$_['entry_code'] = 'Google Talk Код';
$_['entry_status'] = 'Статус';
// Help
$_['help_code'] = 'Отиди на <a href="https://developers.google.com/+/hangouts/button" target="_blank">Стздайте Google Hangout значка</a> и копирайте &amp; поставете генерирания код в полето.';
// Error
$_['error_permission'] = 'Вие няматеправа да променяте настройките на модула!';
$_['error_code'] = 'Изисква се код';
?>