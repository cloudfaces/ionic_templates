<?php
// Heading
$_['heading_title'] = 'Влез с Амазон';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Вие успешно променихте настройките!';
$_['text_content_top'] = 'Съдържание горе';
$_['text_content_bottom'] = 'Съдържание долу';
$_['text_column_left'] = 'Лява колона';
$_['text_column_right'] = 'Дясна колона';
$_['text_lwa_button'] = 'Влез с Амазон';
$_['text_login_button'] = 'Вход';
$_['text_a_button'] = 'А';
$_['text_gold_button'] = 'Златно';
$_['text_darkgray_button'] = 'Тъмно сиво';
$_['text_lightgray_button'] = 'Светло сиво';
$_['text_small_button'] = 'Малък';
$_['text_medium_button'] = 'Среден';
$_['text_large_button'] = 'Голям';
$_['text_x_large_button'] = 'Екстра голям';
//Entry
$_['entry_button_type'] = 'Вида на бутона';
$_['entry_button_colour'] = 'Цвят на бутона';
$_['entry_button_size'] = 'Размер на бутона';
$_['entry_layout'] = 'Изглед';
$_['entry_position'] = 'Позиция';
$_['entry_status'] = 'Състояние';
$_['entry_sort_order'] = 'Подреждане';
//Error
$_['error_permission'] = 'Внимание Вие нямате права да променяте настройките!';
?>