<?php
$_['heading_title'] = 'BG Помощник преводач';
$_['text_module'] = 'Модули';
$_['text_success'] = 'Успех: Промените са запазени в езиковите файлове: %s';
$_['text_edit'] = 'Редактиране на Помощник преовдача';
$_['text_main_lang_file'] = 'Главен езиков файл';
$_['text_page'] = 'Страница';
$_['text_key'] = 'Код';
$_['text_all_pages'] = 'Всички страници';
$_['text_search_keys'] = 'Търси код';
$_['text_search_text'] = 'Търси текст';
$_['text_not_translated_only'] = 'Покажи само непреведени';
$_['text_not_translated'] = 'Непреведени';
$_['text_help_errors'] = '<strong><i class="fa fa-exclamation-circle"></i> Грешки:</strong> Проверете <strong>README</strong> ПЪРВО</a>.';
$_['text_help_hotkeys'] = '<strong><i class="fa fa-info-circle"></i> Клавиши:</strong> <code>Cntl+Enter</code> за запис <code>Esc</code> to cancel.';
$_['text_help_caution'] = '<strong><i class="fa fa-exclamation-triangle"></i> Внимание!</strong> <u>Не</u> превежда HTML code (всъщност всичко в скоби от вида <code>< ></code>) нито специални знаци (неща като <code>%s</code>, <code>%d</code>, и др.).';
$_['text_no_texts_found'] = 'Няма намерен текст.';
$_['text_loading_please_wait'] = 'Зарежда текст, моля изчакайте.';
$_['text_load_more'] = 'Покажи още';
$_['text_save_translation'] = 'Запази превод';
$_['text_cancel'] = 'Откажи';
$_['tab_frontend'] = 'Предна част';
$_['tab_backend'] = 'Админ панел';
$_['entry_status'] = 'Статус';
$_['error_permission'] = 'Внимание: Вие нямате права да променяте настройките на модула!';
$_['error_missing'] = 'Неуспешно записване на настройките. Следните задължителни полета липсват:';
$_['error_post'] = 'Неуспешно записване на превода.';
$_['error_write_permission'] = 'Неуспешно записване на файл \'%s\'. Бъдете сигурни, че имате необходимите файлови разрешения и езиковият пакет се намира в правилната директория (for example: try 750, 755, or 775).';
$_['error_unexpected'] = 'Неочаквана грешка (Моля уведомете разработчика)';
$_['error_error'] = 'Грешка';