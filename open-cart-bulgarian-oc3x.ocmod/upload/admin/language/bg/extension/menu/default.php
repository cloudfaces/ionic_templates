<?php
// Heading
$_['heading_title']          = 'Меню по подразбиране';

// Text
$_['text_extension']         = 'Допълнения';
$_['text_success']           = 'Вие успешно променихте настройките!';
$_['text_edit']              = 'Редактирай Гугъл емисии';
$_['text_import']            = 'За да свалите последния списък с категории на гугъл <a href="https://support.google.com/merchants/answer/160081?hl=en" target="_blank" class="alert-link">натиснете тук</a> и изберете таксономия с цифрово ID в обикновен текстови (.txt) файл. Импортирайте посредством зеления бутон.';

// Column
$_['column_google_category'] = 'Гугъл категория';
$_['column_category']        = 'Категория';
$_['column_action']          = 'Действие';

// Entry
$_['entry_google_category']  = 'Гугъл категория';
$_['entry_category']         = 'Категория';
$_['entry_data_feed']        = 'Url адрес за емисията';
$_['entry_status']           = 'Статут';

// Error
$_['error_permission']       = 'Вие нямате права да променяте настройките!';
$_['error_upload']           = 'Файлът е повреден!';
$_['error_filetype']         = 'Невалидно разширение на файлът!';