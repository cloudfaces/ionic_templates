<?php
// Heading
$_['heading_title'] = 'Гугъл карта на магазина';
// Text
$_['text_extension'] = 'Допълнения';
$_['text_success'] = 'Успешна променихте параметрите на база данни Google Sitemap!';
$_['text_edit'] = 'Редакция';
// Entry
$_['entry_status'] = 'Статус:';
$_['entry_data_feed'] = 'Интернет адрес на страницата с информация за продуктите:';
// Error
$_['error_permission'] = 'Внимание: Нямате права да променяте параметрите на база данни Google Sitemap!';
?>