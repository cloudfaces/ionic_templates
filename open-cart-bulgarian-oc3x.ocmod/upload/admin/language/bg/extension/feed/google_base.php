<?php
// Heading
$_['heading_title'] = 'Гугъл емисии / Google Base';
// Text
$_['text_extension']         = 'Допълнения';
$_['text_success'] = 'Успешна променихте параметрите на емисии!';
$_['text_edit'] = 'Редакция';
$_['text_import'] = 'За да свалите списъка с гугъл категориите <a href="https://support.google.com/merchants/answer/160081?hl=en" target="_blank" class="alert-link">натиснете тук</a> и изберете таксономии с номеричен индификационен номер като обикновен текстов (.txt) файл. Качете чрез зеления бутон.';
// Column
$_['column_google_category'] = 'Гугъл категории';
$_['column_category'] = 'Категории';
$_['column_action'] = 'Действие';
// Entry
$_['entry_google_category'] = 'Гугъл категории';
$_['entry_category'] = 'Категории';
$_['entry_data_feed'] = 'Адрес на ';
$_['entry_status'] = 'Статут';
// Error
$_['error_permission'] = 'Внимание: Нямате права да променяте параметрите на база данни Google!';
$_['error_upload'] = 'Файлът не може да бъде качен!';
$_['error_filetype'] = 'Невалиден вид файл!';
?>