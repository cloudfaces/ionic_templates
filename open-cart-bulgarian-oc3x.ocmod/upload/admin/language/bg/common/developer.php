<?php
// Heading
$_['heading_title']    = 'Настройки за разработчик';

// Text
$_['text_success']     = 'Успех: Вие успешно променихте настройките!';
$_['text_theme']       = 'тема';
$_['text_sass']        = 'SASS';
$_['text_cache']       = 'Успех: Вие изчистихте %s кеш!';

// Column
$_['column_component'] = 'Компонент';
$_['column_action']    = 'Действие';

// Entry
$_['entry_theme']      = 'Тема';
$_['entry_sass']       = 'SASS';
$_['entry_cache']      = 'Кеш';

// Button
$_['button_on']        = 'Вкл.';
$_['button_off']       = 'Изкл.';

// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте настройките!';