<?php


// Heading
$_['heading_title']  = 'Администрация';

// Text
$_['text_heading']   = 'Администрация';
$_['text_login']     = 'Въведете потребител и парола.';
$_['text_forgotten'] = 'Забравена парола?';

// Entry

$_['entry_username'] = 'Потребител';

$_['entry_password'] = 'Парола';

// Button

$_['button_login']   = 'Вход';

// Error
$_['error_login']    = 'Няма съвпадение на потребител и/или парола.';
$_['error_token']    = 'Невалидна сесия. Моля, влезте отново.';