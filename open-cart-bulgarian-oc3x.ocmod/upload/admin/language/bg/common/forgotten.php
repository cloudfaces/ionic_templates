<?php
// Heading
$_['heading_title'] = 'Забраена парола?';
// Text
$_['text_forgotten'] = 'Забравена парола';
$_['text_your_email'] = 'Вашата електронна поща';
$_['text_email'] = 'Въведете адреса на пощенската кутия, която сте въвели по време на регистрацията. Кликнете върху линка получен по електронната поща, променете паролата си.';
$_['text_success'] = 'Линк за потвърждение е изпратен на посоченият от вас пощенски адрес.';
// Entry
$_['entry_email'] = 'E-Mail адрес:';
$_['entry_password'] = 'Нова парола:';
$_['entry_confirm'] = 'Повтори паролата:';


// Error
$_['error_email'] = 'Внимание: адресът на електронната поща съществува в нашите записи, моля опитайте отново!';
$_['error_password'] = 'Паролата трябва да бъде между 4 и 20 символа!';
$_['error_confirm'] = 'Паролите не съвпадат!';
?>