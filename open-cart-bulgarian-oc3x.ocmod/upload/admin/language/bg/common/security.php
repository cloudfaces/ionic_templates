<?php
// Heading
$_['heading_title']    = 'Важни известия за сигурност!';

// Text
$_['text_success']     = 'Успех: Вие променихте настройките!';
$_['text_admin']       = 'Редактирай admin/config.php и промени';
$_['text_security']    = 'Много е важно да преместите съхраняващата папка извън главната директория на сайта (public_html, www или htdocs).';
$_['text_choose']      = 'Изберете как да преместите директорията';
$_['text_automatic']   = 'Автоматично преместване';
$_['text_manual']      = 'Ръчно преместване';
$_['text_move']        = 'Премести';
$_['text_to']          = 'към';
$_['text_config']      = 'Редакитрай config.php и промени';
$_['text_admin']       = 'Редактирай admin/config.php и промени';

// Button
$_['button_move']      = 'Премести';
$_['button_manual']    = 'Ръчно';

// Error
$_['error_permission'] = 'Вие нямате права да променяте настройките!';
$_['error_path']       = 'Невалидна пътека към директорията!';
$_['error_directory']  = 'Невалидна директория!';
$_['error_exists']     = 'Директорията вече съществува!';
$_['error_writable']   = 'Внимание config.php и admin/config.php трябва да могат да се презаписват!';
