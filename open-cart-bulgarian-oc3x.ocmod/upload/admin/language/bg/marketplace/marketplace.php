<?php
// Heading
$_['heading_title']      = 'Пазар за допълнения';

// Text
$_['text_success']       = 'Вие успешно променихте настройките!';
$_['text_list']          = 'Списък допълнения';
$_['text_filter']        = 'Филтър';
$_['text_search']        = 'Търси допълнения и теми';
$_['text_category']      = 'Категории';
$_['text_all']           = 'Всички';
$_['text_theme']         = 'Теми';
$_['text_marketplace']   = 'Пазар';
$_['text_language']      = 'Езици';
$_['text_payment']       = 'Плащане';
$_['text_shipping']      = 'Доставка';
$_['text_module']        = 'Модули';
$_['text_total']         = 'Общо поръчки';
$_['text_feed']          = 'Емисии (Фийдове)';
$_['text_report']        = 'Отчети';
$_['text_other']         = 'Друго';
$_['text_free']          = 'Безплатни';
$_['text_paid']          = 'Платени';
$_['text_purchased']     = 'Закупени';
$_['text_date_modified'] = 'Дата на промяна';
$_['text_date_added']    = 'Дата на добавяне';
$_['text_rating']        = 'Рейтинг';
$_['text_reviews']       = 'мнения';
$_['text_compatibility'] = 'Съвместимост';
$_['text_downloaded']    = 'Свалени';
$_['text_member_since']  = 'Членува от:';
$_['text_price']         = 'Цена';
$_['text_partner']       = 'Разработено от Опенкарт партньор';
$_['text_support']       = '12 Месеца безплатна поддръжка';
$_['text_documentation'] = 'Включена документация';
$_['text_sales']         = 'Продажби';
$_['text_comment']       = 'Коментари';
$_['text_download']      = 'Сваля';
$_['text_install']       = 'Инсталира';
$_['text_comment_add']   = 'Остави коментар';
$_['text_write']         = 'Напиши своя коментар тук..';
$_['text_purchase']      = 'Моля потвърди самоличност!';
$_['text_pin']           = 'моля въведи 4 цифровия PIN номер. Този номер защитава вашият акаунт.';
$_['text_secure']        = 'Не давайте своят ПИН номер на никой!.';
$_['text_name']          = 'Име на файл за сваляне';
$_['text_progress']      = 'Прогрес';
$_['text_available']     = 'Налични инсталации';
$_['text_action']        = 'Действие';

// Entry
$_['entry_pin']          = 'ПИН';

// Tab
$_['tab_description']    = 'Описание';
$_['tab_documentation']  = 'Документация';
$_['tab_download']       = 'Сваляне';
$_['tab_comment']        = 'Коментар';

// Button
$_['button_opencart']    = 'Marketplace API';
$_['button_purchase']    = 'Покупка';
$_['button_view_all']    = 'Виж всички';
$_['button_get_support'] = 'Получи помощ';
$_['button_comment']     = 'Коментар';
$_['button_reply']       = 'Отговор';

// Error
$_['error_permission']   = 'Вие нямате права да променяте настройките!';
$_['error_opencart']     = 'Моля въведете вашата OpenCart API информация преди да извършвате покупки!';
$_['error_install']      = 'Това ще отнеме време, моля изчакайте преди да инсталирате дуго допълнение!';
$_['error_purchase']     = 'Допълнението не може да бъде закупено!';
$_['error_download']     = 'Допълнението не може да бъде свалено!';
