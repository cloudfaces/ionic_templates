<?php
// Heading
$_['heading_title']     = 'Модификации';

// Text
$_['text_success']      = 'Вие успешно променихте настройките!';
$_['text_refresh']      = 'Когато и да инсталирате / премахвате модификации - не забрявайте след това да натиснете бутона обнови!';
$_['text_list']         = 'Списък модификации';

// Column
$_['column_name']       = 'Име';
$_['column_author']     = 'Автор';
$_['column_version']    = 'Версия';
$_['column_status']     = 'Статут';
$_['column_date_added'] = 'Дата на добавяне';
$_['column_action']     = 'Действие';

// Error
$_['error_permission']  = 'Вие нямате права да променяте настройките!';