<?php
// Heading
$_['heading_title']    = 'OpenCart Marketplace API';

// Text
$_['text_success']     = 'Вие успешно променихте настройките!';
$_['text_signup']      = 'Моля въведете вашата OpenCart API информация, която може да намерите на <a href="https://www.opencart.com/index.php?route=account/store" target="_blank" class="alert-link">тук</a>.';

// Entry
$_['entry_username']   = 'Потребителско име';
$_['entry_secret']     = 'Ключ';

// Error
$_['error_permission'] = 'Вие нямате права да променяте настройките!';
$_['error_username']   = 'Попълни потребителско име!';
$_['error_secret']     = 'Попълни таен ключ!';
