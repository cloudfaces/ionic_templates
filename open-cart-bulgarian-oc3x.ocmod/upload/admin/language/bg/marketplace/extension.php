<?php
// Heading
$_['heading_title'] = 'Допълнения';

// Text
$_['text_success']  = 'Вие успешно променихте настройките!';
$_['text_list']     = 'Списък с допълнения';
$_['text_type']     = 'Избери вид допълнение';
$_['text_filter']   = 'Филтър';