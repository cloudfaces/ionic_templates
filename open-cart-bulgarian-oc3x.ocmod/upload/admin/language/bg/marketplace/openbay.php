<?php
// Heading
$_['heading_title']        				= 'ОпенБей Про';

// Buttons
$_['button_retry']						= 'Опитай отново';
$_['button_update']						= 'Обнови';
$_['button_patch']						= 'Пач';
$_['button_faq']						= 'Виж чести въпроси';

// Tab
$_['tab_setting']						= 'Настройки';
$_['tab_update']						= 'Обновяване на софтуер';
$_['tab_developer']						= 'Създател';

// Text
$_['text_dashboard']         			= 'Табло';
$_['text_success']         				= 'Успешно променихте настройките';
$_['text_products']          			= 'Продукти';
$_['text_orders']          				= 'Поръчки';
$_['text_manage']          				= 'Управление';
$_['text_help']                     	= 'Помощ';
$_['text_tutorials']                    = 'Помагала';
$_['text_suggestions']                  = 'Предложения';
$_['text_version_latest']               = 'Вие имате последната версия';
$_['text_version_check']     			= 'Проверка на версията';
$_['text_version_installed']    		= 'Инсталирана версия на OpenBay Pro: v';
$_['text_version_current']        		= 'Вашата версия е';
$_['text_version_available']        	= 'последната е';
$_['text_language']             		= 'API език за отговор';
$_['text_getting_messages']     		= 'Изтегляне съобщенията на OpenBay Pro';
$_['text_complete']     				= 'Завърши';
$_['text_patch_complete']           	= 'Пачът беше приложен';
$_['text_updated']						= 'Модулът се обнови (v.%s)';
$_['text_update_description']			= "Инструментът за обновяване ще извърши промени в файловата система на вашият магазин. Бъдете сигурни, че имате резервно копие на магазина (файлове и база данни).";
$_['text_patch_description']			= 'Ако сте качили файловете на пача ръчно, моля изпълнете файлът с пача, за да завъши обновяването';
$_['text_clear_faq']                    = 'Изчисти FAQ прозорци';
$_['text_clear_faq_complete']           = 'Известията сега ще се покзават отново';
$_['text_install_success']              = 'Пазар беше инсталиран';
$_['text_uninstall_success']            = 'Пазар беше премахнат';
$_['text_title_messages']               = 'Съобщения &amp; известия';
$_['text_marketplace_shipped']			= 'Статусът на поръчката ще бъде променен на изпратен в пазара';
$_['text_action_warning']				= 'Това действие е опасно и защитено с парола.';
$_['text_check_new']					= 'Проверка за нова версия';
$_['text_downloading']					= 'Сваляне на новите файлове';
$_['text_extracting']					= 'Извличане на файловете';
$_['text_running_patch']				= 'Изпълнение на пач файловете';
$_['text_fail_patch']					= 'Неуспешно извличане на новите файлове';
$_['text_updated_ok']					= 'Обновяването приклучи, инсталираната версия сега е ';
$_['text_check_server']					= 'Проверете изискванията на сървъра';
$_['text_version_ok']					= 'Софтуера вече е обновен, моментната версия е ';
$_['text_remove_files']					= 'Премахване на ненужни файлове';
$_['text_confirm_backup']				= 'Бъдете сигурни, че имате резервно копие преди да продължите';
$_['text_software_update']				= 'OpenBay Pro software update';
$_['text_patch_option']				    = 'Manual patching';

// Column
$_['column_name']          				= 'Име на плъгин';
$_['column_status']        				= 'Статус';
$_['column_action']        				= 'Действие';

// Entry
$_['entry_patch']            			= 'Ръчно обновяване на пач';
$_['entry_courier']						= 'Доставчик';
$_['entry_courier_other']           	= 'Друг доставчик';
$_['entry_tracking']                	= 'Проследяване #';
$_['entry_empty_data']					= 'Празни данни на магазина?';
$_['entry_password_prompt']				= 'Моля въведете паролата за данните';
$_['entry_update']						= 'Лесно обновяване с 1 клик';
$_['entry_beta']						= 'Използва бета версия';

// Error
$_['error_admin']             			= 'Търсене на Админ директория';
$_['error_failed']						= 'Неуспешно зареждане, нов опит?';
$_['error_tracking_id_format']			= 'Вашето ID за проседяване нможе да съдържа символите > or <';
$_['error_tracking_courier']			= 'Вие трябва да изберете доставчик ако искате да добавите проследяващ ID';
$_['error_tracking_custom']				= 'Моля оставете полето доставчик празно, ако искате да добавите друг доствчик';
$_['error_permission']					= 'Вие нямате права да променяте OpenBay Pro';
$_['error_file_delete']					= 'Неуспешно изтриване на файлове, трябва да ги изтриете ръчно';
$_['error_mkdir']						= 'PHP mkdir функцията е забранена, всържете се с вашият хост';
$_['error_openssl_encrypt']            	= 'PHP function "openssl_encrypt" is not enabled. Contact your hosting provider.';
$_['error_openssl_decrypt']            	= 'PHP function "openssl_decrypt" is not enabled. Contact your hosting provider.';
$_['error_fopen']             			= 'PHP function "fopen" is not enabled. Contact your hosting provider.';
$_['error_url_fopen']             		= '"allow_url_fopen" directive is disabled by your host - you will be unable to import images when importing products from eBay';
$_['error_curl']                        = 'PHP library "CURL" is not enabled. Contact your hosting provider.';
$_['error_zip']                         = 'ZIP extension needs to be loaded. Contact your hosting provider.';
$_['error_mbstring']               		= 'PHP library "mb strings" is not enabled. Contact your hosting provider.';
$_['error_oc_version']             		= 'Your version of OpenCart is not tested to work with this module. You may experience problems.';

// Help
$_['help_clear_faq']					= 'Покажи всички помощни известия отново';
$_['help_empty_data']					= 'Това може да има серизни поражения, не го използвайте, ако незнаете за какво служи!';
$_['help_easy_update']					= 'Натиснете обновяване, за да инсталирате най-новата версия на OpenBay Pro автоматично';
$_['help_patch']						= 'Натисни да изпълнш скрипта на пача';
$_['help_beta']							= 'Внимание това е бета версия и може да съдържа бъгове.';
