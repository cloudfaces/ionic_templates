<?php
// Heading
$_['heading_title']        = 'Инсталатор на допълнения';

// Text
$_['text_progress']     = 'Инсталационен прогрес';
$_['text_upload']       = 'Качи своето допълнение';
$_['text_history']      = 'История';
$_['text_success']         = 'Инсталацията завърши успешно!';
$_['text_install']      = 'Инсталира';
// Column
$_['column_filename']   = 'Име на файла';
$_['column_date_added'] = 'Дата на добавяне';
$_['column_action']     = 'Действие';

// Entry
$_['entry_upload']         = 'Качи файл';
$_['entry_progress']       = 'Прогрес';

// Help
$_['help_upload']          = 'Изисква файлове от типа ".ocmod.zip" или ".ocmod.xml".';

// Error
$_['error_permission']     = 'Внимание: Вие нямате право да проемняте настройките!';
$_['error_install']     = 'В момента се инталира допълнение, моля изчакайте процеса да завърши преди да инсталирате друго!';
$_['error_upload']         = 'Файлът не може да бъде качен!';
$_['error_filetype']       = 'Невалиден формат!';
$_['error_file']           = 'Файлът неможе да бъде намерен!';
