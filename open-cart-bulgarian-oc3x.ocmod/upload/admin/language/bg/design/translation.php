<?php
// Heading
$_['heading_title'] = 'Езиков превод';

// Text
$_['text_success']     = 'Вие успешно променихте настройките!';
$_['text_list'] = 'Списък с преводи';
$_['text_edit'] = 'Редактиране на превод';
$_['text_add']         = 'Добави превод';
$_['text_default']     = 'Подразбиране';
$_['text_store']       = 'Магазин';
$_['text_language']    = 'Език';
// Column
$_['column_store']     = 'Магазин';
$_['column_language']  = 'Език';
$_['column_route']     = 'Пътечка';
$_['column_key']       = 'Ключ';
$_['column_value']     = 'Стойност';
$_['column_action'] = 'Действие';
// Entry
$_['entry_store']      = 'Магазин';
$_['entry_route']      = 'Пътечка';
$_['entry_key']        = 'Ключ';
$_['entry_default']    = 'Подразбиране';
$_['entry_value']      = 'Стойност';
// Error
$_['error_permission'] = 'Внимание: Вие нямате права да променяте настройките!';
$_['error_key']        = 'Ключът трябва да е между 3 и 64 символа!';
?>
