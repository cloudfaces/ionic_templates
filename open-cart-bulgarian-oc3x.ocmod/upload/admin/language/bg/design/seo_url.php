<?php
// Heading
$_['heading_title']    = 'SEO URL';

// Text
$_['text_success']     = 'Вие успешно променихте настройките!';
$_['text_list']        = 'Списък URL адрес за SEO';
$_['text_add']         = 'Добави URL адрес за SEO';
$_['text_edit']        = 'Редактирай URL адрес за SEO';
$_['text_filter']      = 'Филтър';
$_['text_default']     = 'Подразбиране';

// Column
$_['column_query']     = 'Запитване';
$_['column_keyword']   = 'Ключова дума';
$_['column_store']     = 'Магазин';
$_['column_language']  = 'Език';
$_['column_action']    = 'Действие';

// Entry
$_['entry_query']      = 'Запитване';
$_['entry_keyword']    = 'Ключова дума';
$_['entry_store']      = 'Магазин';
$_['entry_language']   = 'Език';

// Error
$_['error_permission'] = 'Вие нямате права да променяте настройките!';
$_['error_query']      = 'Запитването трябва да е между 3 и 64 символа!';
$_['error_keyword']    = 'Ключовата дума трябва да бъде между 3 и 64 символа!';
$_['error_exists']     = 'Ключовата дума вече се използва!';
