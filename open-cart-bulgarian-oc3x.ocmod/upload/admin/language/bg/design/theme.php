<?php
// Heading
$_['heading_title'] = 'Редактор за теми';
// Text
$_['text_success'] = 'Успех: Вие успешно променихте настройките!';
$_['text_edit'] = 'Редакция на тема';
$_['text_store'] = 'Избери магазин';
$_['text_template'] = 'Избери тема';
$_['text_default'] = 'По подразбиране';
$_['text_history']      = 'Тема - история';
$_['text_twig']         = 'Редактора на теми използва езика на темата. Прочетете повече <a href="http://twig.sensiolabs.org/documentation" target="_blank" class="alert-link">Twig синтаксис тук</a>.';
// Column
$_['column_store']      = 'Магазин';
$_['column_route']      = 'Пътечка';
$_['column_theme']      = 'Тема';
$_['column_date_added'] = 'Дата на добавяне';
$_['column_action']     = 'Действие';
// Error
$_['error_permission'] = 'Внимание: Вие нямате права да редактирате настройките!';
$_['error_twig']        = 'Внимание вие може да запазвате .twig файлове!';
?>