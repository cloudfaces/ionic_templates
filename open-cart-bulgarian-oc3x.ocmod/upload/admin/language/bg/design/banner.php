<?php
// Heading
$_['heading_title']      = 'Банери';
// Text$_['text_success']       = 'Банерът успешно е обновен!';$_['text_list']          = 'Списък с банери';$_['text_add']           = 'Добави банер';$_['text_edit']          = 'Редактирай банер';$_['text_default']       = 'По подразбиране';
// Column$_['column_name']        = 'Име на банера';
$_['column_status']      = 'Статус';
$_['column_action']      = 'Действие';
// Entry
$_['entry_name']         = 'Име банера:';
$_['entry_title']        = 'Заглавие:';
$_['entry_link']         = 'Линк:';
$_['entry_image']        = 'Изображение:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Подреждане';
// Error$_['error_permission']   = 'Вие нямате право да променяте изображения!';$_['error_name']         = 'Името на банера трябва да бъде между 3 и 64 символа!';$_['error_title']        = 'Заглавието трябва да бъде между 2 и 64 символа!';