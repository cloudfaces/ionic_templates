<?php


// Heading
$_['heading_title']         = 'Клиенти';

// Text
$_['text_success']          = 'Вие успешно променихте настройките!';
$_['text_list']             = 'Списък с клиенти';
$_['text_add']              = 'Добави клиент';
$_['text_edit']             = 'Редкатирай клиент';
$_['text_default']          = 'По подразбиране';
$_['text_account']          = 'Детайли за клиента';
$_['text_password']         = 'Парола';
$_['text_other']            = 'Други';
$_['text_affiliate']        = 'Детайли за Афилиата';
$_['text_payment']          = 'Плащане';
$_['text_balance']          = 'Баланс';
$_['text_cheque']           = 'Чек';

$_['text_paypal']           = 'PayPal';
$_['text_bank']             = 'Банков трансфер';
$_['text_history']          = 'История';
$_['text_history_add']      = 'Добави история';
$_['text_transaction']      = 'Транзакции';
$_['text_transaction_add']  = 'Добави транзакция';
$_['text_reward']           = 'Точки за награда';
$_['text_reward_add']       = 'Добави точки за награда';
$_['text_ip']               = 'Адрес';
$_['text_option']           = 'Опции';
$_['text_login']            = 'Влез в магазина';
$_['text_unlock']           = 'Отключи акаунта';


// Column
$_['column_name']           = 'Име';

$_['column_email']          = 'Електронна поща';
$_['column_customer_group'] = 'Група';

$_['column_status']         = 'Състояние';
$_['column_date_added']     = 'Дата на добавяне';
$_['column_comment']        = 'Бележка (коментар)';

$_['column_description']    = 'Описание';

$_['column_amount']         = 'Сума';

$_['column_points']         = 'Точки';

$_['column_ip']             = 'Адрес';
$_['column_total']          = 'Общо профили';

$_['column_action']         = 'Действие';

// Entry
$_['entry_customer_group']  = 'Клиентска група';

$_['entry_firstname']       = 'Първо име';

$_['entry_lastname']        = 'Фамилно име';

$_['entry_email']           = 'Ел. поща';

$_['entry_telephone']       = 'Телефон';
$_['entry_newsletter']      = 'Информационен бюлетин';

$_['entry_status']          = 'Състояние';

$_['entry_approved']        = 'Одобрен';

$_['entry_safe']            = 'Безопасен';

$_['entry_password']        = 'Парола';

$_['entry_confirm']         = 'Потвърди';

$_['entry_company']         = 'Фирма';

$_['entry_address_1']       = 'Адрес 1';

$_['entry_address_2']       = 'Адрес 2';

$_['entry_city']            = 'Град';

$_['entry_postcode']        = 'Пощенски код';

$_['entry_country']         = 'Страна';
$_['entry_zone']            = 'Регион / Област';
$_['entry_default']         = 'Адрес по подразбиране';
$_['entry_affiliate']           = 'Афилиат';
$_['entry_tracking']            = 'Проследяващ код';
$_['entry_website']             = 'Уеб страница';
$_['entry_commission']          = 'Комисионна (%)';
$_['entry_tax']                 = 'Данъчен номер';

$_['entry_payment']             = 'Плащане';
$_['entry_cheque']              = 'Име изписано на чека';
$_['entry_paypal']              = 'PayPal електронна поща';
$_['entry_bank_name']           = 'Име на банка';
$_['entry_bank_branch_number']  = 'ABA/BSB номер (Branch Number)';
$_['entry_bank_swift_code']     = 'СУИФТ Код';
$_['entry_bank_account_name']   = 'Име';
$_['entry_bank_account_number'] = 'Номер на акаунта';
$_['entry_comment']         = 'Белжка (коментар)';
$_['entry_description']     = 'Описание';

$_['entry_amount']          = 'Сума';

$_['entry_points']          = 'Точки';
$_['entry_name']            = 'Име на клиент';

$_['entry_ip']              = 'Адрес';
$_['entry_date_added']      = 'Дата на добавяне';

// Help
$_['help_safe']             = 'Посочете true, за да предпазите клиента да бъде прихванат от системата за защита от злонамерени действия';
$_['help_affiliate']            = 'Разреши / забрани клиента да изпозлва Афилиатската програма.';
$_['help_tracking']             = 'Проследяващият код, който ще се използва за проследяване на реферали.';
$_['help_commission']           = 'Проецнтът, който Афилиата получава при всяка транзакция.';
$_['help_points']           = 'Използвайте минус (-), за да премахнете точките';



// Error


$_['error_warning']         = 'Моля проверете полетата внимателно за грешки!';


$_['error_permission']      = 'Внимание Вие нямате права да променяте настройките!';


$_['error_exists']          = 'Внимание ел. поща вече е се използва от системата!';


$_['error_firstname']       = 'Първото име трябва да е мнежду 1 и 32 символа!';


$_['error_lastname']        = 'Фамилното име трябва да е мнежду 1 и 32 символа!';


$_['error_email']           = 'Електронната поща е невалидна!';


$_['error_telephone']       = 'Телефона трябва да е между  3 и 32 символа!';


$_['error_password']        = 'Паролата трябва да е между 4 и 20 символа!';


$_['error_confirm']         = 'Паролите не съвпадат, моля въведете отново!';


$_['error_address_1']       = 'Адрес 1 трябва да е между 3 и 128 символа!';


$_['error_city']            = 'Градът трябва да е между 2 и 128 символа!';


$_['error_postcode']        = 'Пощенският код трябва да е между 2 и 10 символа за тази страна!';


$_['error_country']         = 'Моля изберете страна!';


$_['error_zone']            = 'Моля изберете регион / област!';


$_['error_custom_field']    = '%s изисква се!';


