<?php
// Heading
$_['heading_title'] = 'Потребителски полета';
// Text
$_['text_success'] = 'Вие успешно променихте настройките!';
$_['text_list'] = 'Списък';
$_['text_add'] = 'Добави поле';
$_['text_edit'] = 'Редактирай поле';
$_['text_choose'] = 'Избери';
$_['text_select'] = 'Маркирай';
$_['text_radio'] = 'Радио бутон';
$_['text_checkbox'] = 'Отметка';
$_['text_input'] = 'Въвеждане';
$_['text_text'] = 'Текс';
$_['text_textarea'] = 'Текстово поле';
$_['text_file'] = 'Файл';
$_['text_date'] = 'Дата';
$_['text_datetime'] = 'Дата &amp; Час';
$_['text_time'] = 'Час';
$_['text_account'] = 'Профил';
$_['text_address'] = 'Адрес';
$_['text_affiliate']       = 'Афилиат';
$_['text_regex']           = 'Regex';
$_['text_custom_field']    = 'Потребителско поле';
$_['text_value']           = 'Стойности за потребителско поле';
// Column
$_['column_name'] = 'Име';
$_['column_location'] = 'Местоположение';
$_['column_type'] = 'Вид';
$_['column_sort_order'] = 'Подреждане';
$_['column_action'] = 'Действие';
// Entry
$_['entry_name'] = 'Име';
$_['entry_location'] = 'Местоположение';
$_['entry_type'] = 'Вид';
$_['entry_value'] = 'Стойност';
$_['entry_validation']     = 'Потвърждение';
$_['entry_custom_value'] = 'Име на стойността';
$_['entry_customer_group'] = 'Клиентска група';
$_['entry_required'] = 'Изисква се';
$_['entry_status'] = 'Състояние';
$_['entry_sort_order'] = 'Подреждане';
// Help
$_['help_regex'] = 'Use regex. E.g: /[a-zA-Z0-9_-]/';
$_['help_sort_order'] = 'Използвайте минус (-), за да отброявате назад от последното посочено поле.';
// Error
$_['error_permission'] = 'Внимание Вие нямате права да редактирате нстройките!';
$_['error_name'] = 'Потребителските полета трябва да са между 1 и 128 символа!';
$_['error_type'] = 'Внимание попълнете стойност за потребителско поле!';
$_['error_custom_value'] = 'Стойността на потребителското поле трябва да е между 1 и 128 символа!';
?>