<?php
// Heading
$_['heading_title']         = 'Одобрение на клиент';

// Text
$_['text_success']          = 'Вие успешно променихте настройките!';
$_['text_list']             = 'Списък с клиенти за одобряване';
$_['text_default']          = 'Подразбиране';
$_['text_customer']         = 'Клиент';
$_['text_affiliate']        = 'Афилиат';

// Column
$_['column_name']           = 'Име клиент';
$_['column_email']          = 'Електронна поща';
$_['column_customer_group'] = 'Клиентска група';
$_['column_type']           = 'Вид';
$_['column_date_added']     = 'Дата на доабвяне';
$_['column_action']         = 'Действие';

// Entry
$_['entry_name']            = 'Име клиент';
$_['entry_email']           = 'Ел. поща';
$_['entry_customer_group']  = 'Клиентска група';
$_['entry_type']            = 'Вид';
$_['entry_date_added']      = 'Дата на добавяне';

// Error
$_['error_permission']      = 'Вие нямате права да променяте настройките!';