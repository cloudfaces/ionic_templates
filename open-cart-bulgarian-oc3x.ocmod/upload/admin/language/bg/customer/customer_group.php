<?php


// Heading
$_['heading_title']     = 'Клиентски групи';

// Text
$_['text_success']      = 'Успешно променихте настройките!';
$_['text_list']         = 'Списък с клиентските групи';
$_['text_add']          = 'Добавете клиентска група';
$_['text_edit']         = 'Редактиране на клиентска група';

// Column
$_['column_name']       = 'Име';

$_['column_sort_order'] = 'Подреждане';

$_['column_action']     = 'Действие';

// Entry
$_['entry_name']        = 'Име на клиентска група';

$_['entry_description'] = 'Описание';
$_['entry_approval']    = 'Одобри нови клиенти';

$_['entry_sort_order']  = 'Подреждане';

// Help
$_['help_approval']     = 'Клиентите трябва да бъдат одобрени от администратор, преди те да могат да се впишат.';


// Error
$_['error_permission']   = 'Внимание Вие нямате права да променяте настройките!';
$_['error_name']         = 'Клиентската група трябва да е между 3 и 32 символа!';
$_['error_default']      = 'Вниманмие тази група неможе да бъде изтрита, защото е посочена като група по подразбиране!';
$_['error_store']        = 'Внимание тази клиентска група неможе да бъде изтрита, тъй като е свързана с %s магазина!';
$_['error_customer']     = 'Внимание тази клиентска група неможе да бъде изтрита, тъй като е свързана с %s клиенти!';