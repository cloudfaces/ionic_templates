<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('index', [CustomAuthController::class, 'dashboard']); 
Route::get('login', [CustomAuthController::class, 'index'])->name('login');
Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('signup', [CustomAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [CustomAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');

Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('/index', function () {
    return view('index');
})->name('index');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/listing-grid-sidebar', function () {
    return view('listing-grid-sidebar');
})->name('listing-grid-sidebar');

Route::get('/service-details', function () {
    return view('service-details');
})->name('service-details');

Route::get('/add-listing', function () {
    return view('add-listing');
})->name('add-listing');

Route::get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/profile', function () {
    return view('profile');
})->name('profile');

Route::get('/my-listing', function () {
    return view('my-listing');
})->name('my-listing');

Route::get('/bookmarks', function () {
    return view('bookmarks');
})->name('bookmarks');

Route::get('/messages', function () {
    return view('messages');
})->name('messages');

Route::get('/reviews', function () {
    return view('reviews');
})->name('reviews');

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::get('/index-2', function () {
    return view('index-2');
})->name('index-2');

Route::get('/index-3', function () {
    return view('index-3');
})->name('index-3');

Route::get('/index-4', function () {
    return view('index-4');
})->name('index-4');

Route::get('/index-5', function () {
    return view('index-5');
})->name('index-5');

Route::get('/index-6', function () {
    return view('index-6');
})->name('index-6');

Route::get('/index-7', function () {
    return view('index-7');
})->name('index-7');

Route::get('/index-8', function () {
    return view('index-8');
})->name('index-8');

Route::get('/index-9', function () {
    return view('index-9');
})->name('index-9');

Route::get('/signup', function () {
    return view('signup');
})->name('signup');

Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Route::get('/listing-grid', function () {
    return view('listing-grid');
})->name('listing-grid');

Route::get('/listing-list-sidebar', function () {
    return view('listing-list-sidebar');
})->name('listing-list-sidebar');

Route::get('/listingmap-list', function () {
    return view('listingmap-list');
})->name('listingmap-list');

Route::get('/listingmap-grid', function () {
    return view('listingmap-grid');
})->name('listingmap-grid');

Route::get('/pricing', function () {
    return view('pricing');
})->name('pricing');

Route::get('/faq', function () {
    return view('faq');
})->name('faq');

Route::get('/gallery', function () {
    return view('gallery');
})->name('gallery');

Route::get('/categories', function () {
    return view('categories');
})->name('categories');

Route::get('/howitworks', function () {
    return view('howitworks');
})->name('howitworks');

Route::get('/terms-condition', function () {
    return view('terms-condition');
})->name('terms-condition');

Route::get('/privacy-policy', function () {
    return view('privacy-policy');
})->name('privacy-policy');

Route::get('/error-404', function () {
    return view('error-404');
})->name('error-404');

Route::get('/error-500', function () {
    return view('error-500');
})->name('error-500');

Route::get('/signup', function () {
    return view('signup');
})->name('signup');

Route::get('/blog-list', function () {
    return view('blog-list');
})->name('blog-list');

Route::get('/blog-grid', function () {
    return view('blog-grid');
})->name('blog-grid');

Route::get('/blog-details', function () {
    return view('blog-details');
})->name('blog-details');

Route::get('/blog-list-sidebar', function () {
    return view('blog-list-sidebar');
})->name('blog-list-sidebar');

Route::get('/blog-grid-sidebar', function () {
    return view('blog-grid-sidebar');
})->name('blog-grid-sidebar');

Route::get('/forgot-password', function () {
    return view('forgot-password');
})->name('forgot-password');
