<?php $page = 'about'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Forgot Password
        @endslot
        @slot('li_1')
            Forgot Password
        @endslot
    @endcomponent
    <!-- Login Section -->
    <div class="login-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-5 mx-auto">
                    <div class="login-wrap password-form">

                        <div class="login-header">
                            <h3>Forgot Password</h3>
                            <p>Enter your email and we will send you a link to reset your password.</p>
                        </div>

                        <!-- Login Form -->
                        <form action="{{ url('login') }}">
                            <div class="form-group group-img">
                                <div class="group-img">
                                    <i class="feather-mail"></i>
                                    <input type="text" class="form-control" name="email" id="email"
                                        autocomplete="off" placeholder="Email Address">
                                </div>
                            </div>
                            <button class="btn btn-primary w-100 login-btn" type="submit">Submit</button>
                        </form>
                        <a href="{{ url('login') }}" class="back-home"><i class="fas fa-regular fa-arrow-left me-1"></i>
                            Back to Login</a>
                        <!-- /Login Form -->

                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /Login Section -->

    @component('components.scrolltotop')
    @endcomponent
@endsection
