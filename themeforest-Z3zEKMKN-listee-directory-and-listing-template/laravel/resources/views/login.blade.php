<?php $page = 'login'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Login
        @endslot
        @slot('li_1')
            Login
        @endslot
    @endcomponent

    <!-- Login Section -->
    <div class="login-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-5 mx-auto">
                    <div class="login-wrap">

                        <div class="login-header">
                            <h3>Welcome Back</h3>
                            <p>Please Enter your Details</p>
                        </div>

                        <!-- Login Form -->
                        <form method="post" action="{{ route('login.custom') }}">
                            @csrf
                            <div class="form-group group-img">
                                <div class="group-img">
                                    <i class="feather-mail"></i>
                                    <input type="text" class="form-control" value="admin@example.com" name="email"
                                        id="email" autocomplete="off">
                                        <div class="text-danger pt-2">
                                            @error('0')
                                                {{ $message }}
                                            @enderror
                                            @error('email')
                                                {{ $message }}
                                            @enderror
                                        </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="pass-group group-img">
                                    <i class="feather-lock"></i>
                                    <input type="password" class="form-control pass-input" value="123456"
                                        name="password" id="password" autocomplete="off">
                                    <span class="toggle-password feather-eye-off"></span>
                                </div>
                                <div class="text-danger pt-2">
                                    @error('0')
                                        {{ $message }}
                                    @enderror
                                    @error('password')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <label class="custom_check" for="rememberme">
                                        <input type="checkbox" name="rememberme" class="rememberme" id="rememberme"
                                            autocomplete="off">
                                        <span class="checkmark"></span>Remember Me
                                    </label>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="text-md-end">
                                        <a class="forgot-link" href="{{ url('forgot-password') }}">Forgot password?</a>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary w-100 login-btn" type="submit"
                                href="{{ url('dashboard') }}">Sign in</button>
                            <div class="register-link text-center">
                                <p>No account yet? <a class="forgot-link" href="{{ url('signup') }}">Signup</a></p>
                            </div>
                            <div class="login-or">
                                <span class="or-line"></span>
                                <span class="span-or">Sign in with Social Media Accounts</span>
                            </div>
                            <div class="social-login">
                                <a href="#" class="btn btn-apple w-100"><img
                                        src="{{ URL::asset('/assets/img/apple.svg') }}" class="me-1" alt="img">Sign
                                    in with Apple</a>
                            </div>
                            <div class="social-login">
                                <a href="#" class="btn btn-google w-100"><img
                                        src="{{ URL::asset('/assets/img/google.svg') }}" class="me-1" alt="img">Sign
                                    in with Google</a>
                            </div>
                            <div class="social-login">
                                <a href="#" class="btn btn-facebook w-100 mb-0"><img
                                        src="{{ URL::asset('/assets/img/facebook.svg') }}" class="me-2"
                                        alt="img">Continue with Facebook</a>
                            </div>
                        </form>
                        <!-- /Login Form -->

                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /Login Section -->

    @component('components.scrolltotop')
    @endcomponent
@endsection
