<?php $page = 'blog-list-sidebar'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Listings-Blog
        @endslot
        @slot('li_1')
            Blog
        @endslot
    @endcomponent

    <!-- Blog List -->
    <div class="bloglisting">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="bloglistleft-widget blog-listview">
                        <div class="card">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('blog-details') }}">
                                        <img src="{{ URL::asset('/assets/img/blog/bloglistview-1.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="blog-category">
                                        <a href="javascript:void(0)"><span>Health</span></a><a
                                            href="javascript:void(0)"><span>Care</span></a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <ul class="entry-meta meta-item">
                                            <li>
                                                <div class="post-author">
                                                    <div class="post-author-img">
                                                        <img src="{{ URL::asset('/assets/img/profiles/avatar-01.jpg') }}"
                                                            alt="author">
                                                    </div>
                                                    <a href="javascript:void(0)"><span> John Doe </span></a>
                                                </div>
                                            </li>
                                            <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> May 1,
                                                2023</li>
                                        </ul>
                                        <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best SPA Salons
                                                For Your Relaxation</a></h3>
                                        <p class="mb-0">Contrary to popular belief, Lorem Ipsum is not simply random
                                            text. It has roots in a piece of classical Latin literature from 45 BC,
                                            making it over 2000 years old. Richard</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('blog-details') }}">
                                        <img src="{{ URL::asset('/assets/img/blog/bloglistview-2.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="blog-category">
                                        <a href="javascript:void(0)"><span>Health</span></a><a
                                            href="javascript:void(0)"><span>Care</span></a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <ul class="entry-meta meta-item">
                                            <li>
                                                <div class="post-author">
                                                    <div class="post-author-img">
                                                        <img src="{{ URL::asset('/assets/img/profiles/avatar-07.jpg') }}"
                                                            alt="author">
                                                    </div>
                                                    <a href="javascript:void(0)"><span> Mary </span></a>
                                                </div>
                                            </li>
                                            <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> January 2,
                                                2023</li>
                                        </ul>
                                        <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best SPA Salons
                                                For Your Relaxation</a></h3>
                                        <p class="mb-0">Contrary to popular belief, Lorem Ipsum is not simply random
                                            text. It has roots in a piece of classical Latin literature from 45 BC,
                                            making it over 2000 years old. Richard</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('blog-details') }}">
                                        <img src="{{ URL::asset('/assets/img/blog/bloglistview-3.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="blog-category">
                                        <a href="javascript:void(0)"><span>Health</span></a><a
                                            href="javascript:void(0)"><span>Care</span></a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <ul class="entry-meta meta-item">
                                            <li>
                                                <div class="post-author">
                                                    <div class="post-author-img">
                                                        <img src="{{ URL::asset('/assets/img/profiles/avatar-04.jpg') }}"
                                                            alt="author">
                                                    </div>
                                                    <a href="javascript:void(0)"><span> Amara </span></a>
                                                </div>
                                            </li>
                                            <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> April 3,
                                                2023</li>
                                        </ul>
                                        <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best SPA Salons
                                                For Your Relaxation</a></h3>
                                        <p class="mb-0">Contrary to popular belief, Lorem Ipsum is not simply random
                                            text. It has roots in a piece of classical Latin literature from 45 BC,
                                            making it over 2000 years old. Richard</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('blog-details') }}">
                                        <img src="{{ URL::asset('/assets/img/blog/bloglistview-4.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="blog-category">
                                        <a href="javascript:void(0)"><span>Health</span></a><a
                                            href="javascript:void(0)"><span>Care</span></a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <ul class="entry-meta meta-item">
                                            <li>
                                                <div class="post-author">
                                                    <div class="post-author-img">
                                                        <img src="{{ URL::asset('/assets/img/profiles/avatar-01.jpg') }}"
                                                            alt="author">
                                                    </div>
                                                    <a href="javascript:void(0)"><span> John Doe </span></a>
                                                </div>
                                            </li>
                                            <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> February 4,
                                                2023</li>
                                        </ul>
                                        <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best SPA Salons
                                                For Your Relaxation</a></h3>
                                        <p class="mb-0">Contrary to popular belief, Lorem Ipsum is not simply random
                                            text. It has roots in a piece of classical Latin literature from 45 BC,
                                            making it over 2000 years old. Richard</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('blog-details') }}">
                                        <img src="{{ URL::asset('/assets/img/blog/bloglistview-1.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="blog-category">
                                        <a href="javascript:void(0)"><span>Health</span></a><a
                                            href="javascript:void(0)"><span>Care</span></a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <ul class="entry-meta meta-item">
                                            <li>
                                                <div class="post-author">
                                                    <div class="post-author-img">
                                                        <img src="{{ URL::asset('/assets/img/profiles/avatar-04.jpg') }}"
                                                            alt="author">
                                                    </div>
                                                    <a href="javascript:void(0)"><span> Scotty </span></a>
                                                </div>
                                            </li>
                                            <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> April 5,
                                                2023</li>
                                        </ul>
                                        <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best SPA
                                                Salons For Your Relaxation</a></h3>
                                        <p class="mb-0">Contrary to popular belief, Lorem Ipsum is not simply random
                                            text. It has roots in a piece of classical Latin literature from 45 BC,
                                            making it over 2000 years old. Richard</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('blog-details') }}">
                                        <img src="{{ URL::asset('/assets/img/blog/bloglistview-2.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="blog-category">
                                        <a href="javascript:void(0)"><span>Health</span></a><a
                                            href="javascript:void(0)"><span>Care</span></a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <ul class="entry-meta meta-item">
                                            <li>
                                                <div class="post-author">
                                                    <div class="post-author-img">
                                                        <img src="{{ URL::asset('/assets/img/profiles/avatar-05.jpg') }}"
                                                            alt="author">
                                                    </div>
                                                    <a href="javascript:void(0)"> <span> Kenneth </span></a>
                                                </div>
                                            </li>
                                            <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> May 17,
                                                2023</li>
                                        </ul>
                                        <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best SPA
                                                Salons For Your Relaxation</a></h3>
                                        <p class="mb-0">Contrary to popular belief, Lorem Ipsum is not simply random
                                            text. It has roots in a piece of classical Latin literature from 45 BC,
                                            making it over 2000 years old. Richard</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('blog-details') }}">
                                        <img src="{{ URL::asset('/assets/img/blog/bloglistview-3.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="blog-category">
                                        <a href="javascript:void(0)"><span>Health</span></a><a
                                            href="javascript:void(0)"><span>Care</span></a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <ul class="entry-meta meta-item">
                                            <li>
                                                <div class="post-author">
                                                    <div class="post-author-img">
                                                        <img src="{{ URL::asset('/assets/img/profiles/avatar-01.jpg') }}"
                                                            alt="author">
                                                    </div>
                                                    <a href="javascript:void(0)"><span> John Doe </span></a>
                                                </div>
                                            </li>
                                            <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> February 18,
                                                2023</li>
                                        </ul>
                                        <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best SPA
                                                Salons For Your Relaxation</a></h3>
                                        <p class="mb-0">Contrary to popular belief, Lorem Ipsum is not simply random
                                            text. It has roots in a piece of classical Latin literature from 45 BC,
                                            making it over 2000 years old. Richard</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('blog-details') }}">
                                        <img src="{{ URL::asset('/assets/img/blog/bloglistview-4.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="blog-category">
                                        <a href="javascript:void(0)"><span>Health</span></a><a
                                            href="javascript:void(0)"><span>Care</span></a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <ul class="entry-meta meta-item">
                                            <li>
                                                <div class="post-author">
                                                    <div class="post-author-img">
                                                        <img src="{{ URL::asset('/assets/img/profiles/avatar-06.jpg') }}"
                                                            alt="author">
                                                    </div>
                                                    <a href="javascript:void(0)"> <span> Orlando Diggs </span></a>
                                                </div>
                                            </li>
                                            <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> April 14,
                                                2023</li>
                                        </ul>
                                        <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best SPA
                                                Salons For Your Relaxation</a></h3>
                                        <p class="mb-0">Contrary to popular belief, Lorem Ipsum is not simply random
                                            text. It has roots in a piece of classical Latin literature from 45 BC,
                                            making it over 2000 years old. Richard</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @component('components.pagination')
                    @endcomponent
                </div>
                <div class="col-lg-4 theiaStickySidebar">
                    <div class="rightsidebar blogsidebar">
                        <div class="card">
                            <h4><img src="{{ URL::asset('/assets/img/details-icon.svg') }}" alt="details-icon">
                                Filter</h4>
                            <div class="filter-content looking-input form-group">
                                <input type="text" class="form-control" name="search" id="search"
                                    placeholder="To Search type and hit enter">
                            </div>
                        </div>
                        <div class="card">
                            <h4><img src="{{ URL::asset('/assets/img/category-icon.svg') }}" alt="details-icon">
                                Categories</h4>
                            <ul class="blogcategories-list">
                                <li><a href="javascript:void(0)">Accept Credit Cards</a></li>
                                <li><a href="javascript:void(0)">Smoking Allowed</a></li>
                                <li><a href="javascript:void(0)">Bike Parking</a></li>
                                <li><a href="javascript:void(0)">Street Parking</a></li>
                                <li><a href="javascript:void(0)">Wireless Internet</a></li>
                                <li><a href="javascript:void(0)">Pet Friendly</a></li>
                            </ul>
                        </div>
                        <div class="card tags-widget">
                            <h4><i class="feather-tag"></i> Tags</h4>
                            <ul class="tags">
                                <li>Travelling </li>
                                <li>Art </li>
                                <li>Vacation </li>
                                <li>Tourism </li>
                                <li>Culture </li>
                                <li>Lifestyle </li>
                                <li>Travelling </li>
                                <li>Art </li>
                                <li>vacation </li>
                                <li>Tourism </li>
                                <li>Culture </li>
                            </ul>
                        </div>
                        <div class="card">
                            <h4><i class="feather-tag"></i> Article</h4>
                            <div class="article">
                                <a href="{{ url('blog-details') }}" class="articleimg-1">
                                    <ul>
                                        <li>
                                            <h6>Great Business Tips in 2023</h6>
                                        </li>
                                        <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> January 22,
                                            2023</li>
                                    </ul>
                                </a>
                            </div>
                            <div class="article">
                                <a href="{{ url('blog-details') }}" class="articleimg-2">
                                    <ul>
                                        <li>
                                            <h6>Excited News About Fashion.</h6>
                                        </li>
                                        <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> March 9,
                                            2023</li>
                                    </ul>
                                </a>
                            </div>
                            <div class="article">
                                <a href="{{ url('blog-details') }}" class="articleimg-3">
                                    <ul>
                                        <li>
                                            <h6>8 Amazing Tricks About Business</h6>
                                        </li>
                                        <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> April 10,
                                            2023</li>
                                    </ul>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Blog List -->


    @component('components.scrolltotop')
    @endcomponent
@endsection
