<?php $page = 'add-listing'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Add Listing
        @endslot
        @slot('li_1')
            Add Listing
        @endslot
    @endcomponent
    <div class="dashboard-content">
        <div class="container">
            @component('components.nav')
            @endcomponent
            <!-- Profile Content -->
            <div class="profile-content">
                <div class="messages-form">
                    <div class="card">
                        <div class="card-header">
                            <h4>Basic Information</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="col-form-label" for="title">Listing Title <span>*</span></label>
                                <input type="text" class="form-control pass-input" name="title" id="title"
                                    placeholder="Title">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="description">Listing Description <span>*</span></label>
                                <textarea rows="6" class="form-control listingdescription" name="description" id="description"
                                    placeholder="Message"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label label-heading" for="wireless-internet">Category </label>
                                <div class="row category-listing">
                                    <div class="col-lg-4">
                                        <ul>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="wireless-internet" id="wireless-internet">
                                                    <span class="checkmark"></span> Automotive
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="accept-credit-card"
                                                        id="accept-credit-card">
                                                    <span class="checkmark"></span> Electronics
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="Coupouns" id="Coupouns">
                                                    <span class="checkmark"></span> Fashion Style
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="parking-street" id="parking-street">
                                                    <span class="checkmark"></span> Health Care
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-4">
                                        <ul>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="wireless-internet" id="wireless-internet1">
                                                    <span class="checkmark"></span> Job board
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="accept-credit-card"
                                                        id="accept-credit-card1">
                                                    <span class="checkmark"></span> Education
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="Coupouns" id="Coupouns2">
                                                    <span class="checkmark"></span> Real Estate
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="parking-street" id="parking-street1">
                                                    <span class="checkmark"></span> Travel
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-4">
                                        <ul>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="wireless-internet" id="wireless-internet2">
                                                    <span class="checkmark"></span> Sports & Game
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="accept-credit-card"
                                                        id="accept-credit-card2">
                                                    <span class="checkmark"></span> Magazines
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="Coupouns" id="Coupouns3">
                                                    <span class="checkmark"></span> Pet & Animal
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="parking-street" id="parking-street2">
                                                    <span class="checkmark"></span> Household
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group formlast-input">
                                <label class="col-form-label label-heading" for="tagline">Tagline</label>
                                <textarea rows="2" class="form-control tagline" name="tagline" id="tagline" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Basic Information</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="col-form-label" for="price">Price Range</label>
                                <input type="text" class="form-control pass-input" name="price" id="price"
                                    placeholder="$">
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group formlast-input">
                                        <label class="col-form-label" for="price-from">Price From</label>
                                        <select class="form-control select" name="price-from" id="price-from">
                                            <option>$65</option>
                                            <option>$75</option>
                                            <option>$85</option>
                                            <option>$95</option>
                                            <option>$105</option>
                                            <option>$110</option>
                                            <option>$115</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group formlast-input formlast-input-inner">
                                        <label class="col-form-label" for="price-to">Price To</label>
                                        <select class="form-control select" name="price-to" id="price-to">
                                            <option>$120</option>
                                            <option>$130</option>
                                            <option>$140</option>
                                            <option>$150</option>
                                            <option>$160</option>
                                            <option>$170</option>
                                            <option>$190</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Features Information</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group featuresform-list mb-0">
                                <ul>
                                    <li>
                                        <label class="custom_check">
                                            <input type="checkbox" name="wireless-internet" id="wireless-internet3">
                                            <span class="checkmark"></span> Accepts Credit Cards
                                        </label>
                                    </li>
                                    <li>
                                        <label class="custom_check">
                                            <input type="checkbox" name="accept-credit-card" id="accept-credit-card3">
                                            <span class="checkmark"></span> Bike Parking
                                        </label>
                                    </li>
                                    <li>
                                        <label class="custom_check">
                                            <input type="checkbox" name="Coupouns" id="Coupouns4">
                                            <span class="checkmark"></span> Parking Street
                                        </label>
                                    </li>
                                    <li>
                                        <label class="custom_check">
                                            <input type="checkbox" name="parking-street" id="parking-street3">
                                            <span class="checkmark"></span> Pets Friendly
                                        </label>
                                    </li>
                                    <li>
                                        <label class="custom_check">
                                            <input type="checkbox" name="Coupouns" id="Coupouns5">
                                            <span class="checkmark"></span> Wheelchair Accessible
                                        </label>
                                    </li>
                                    <li>
                                        <label class="custom_check">
                                            <input type="checkbox" name="parking-street" id="parking-street4">
                                            <span class="checkmark"></span> Wireless Internet
                                        </label>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Location Information</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="col-form-label" for="location">Location</label>
                                <input type="text" class="form-control" name="location" id="location"
                                    value="$">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="address">Address</label>
                                <input type="text" class="form-control" name="address" id="address"
                                    value="8697-8747 Stirling Rd, Florida" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="map-address">Map Address</label>
                                <input type="text" class="form-control" name="map-address" id="map-address"
                                    value="8697-8747 Stirling Rd, Florida">
                            </div>
                            <div class="listing-map">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3584.6461688381!2d-80.26548188573862!3d26.045130803169!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d9a862f9831459%3A0xafcb9384c02e8b75!2s8697%20Stirling%20Rd%2C%20Cooper%20City%2C%20FL%2033328%2C%20USA!5e0!3m2!1sen!2sin!4v1671519522101!5m2!1sen!2sin"
                                    width="100" height="430" style="border:0;" allowfullscreen="" loading="lazy"
                                    referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group formlast-input lat-input">
                                        <input type="text" class="form-control" name="demo-value" id="demo-value"
                                            value="26.045197767574102">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group formlast-input">
                                        <input type="text" class="form-control"name="demo-value1" id="demo-value1"
                                            value="-80.26095677163161">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Contact Information </h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="col-form-label" for="email">Email </label>
                                <input type="text" class="form-control pass-input" name="email" id="email"
                                    placeholder="" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="website">Website </label>
                                <input type="text" class="form-control pass-input" name="website" id="website"
                                    placeholder="" autocomplete="off">
                            </div>
                            <div class="form-group formlast-input">
                                <label class="col-form-label" for="phone">Phone </label>
                                <input type="text" class="form-control pass-input" name="phone" id="phone"
                                    placeholder="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Social Information </h4>
                        </div>
                        <div class="card-body">
                            <div class="row social-info">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label class="col-form-label" for="facebook">Facebook</label>
                                        <div class="pass-group group-img">
                                            <span class="lock-icon"><i class="fab fa-facebook-f"></i></span>
                                            <input type="text" class="form-control" name="facebook" id="facebook"
                                                value="http://facebook.com">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label class="col-form-label" for="twitter">Twitter</label>
                                        <div class="pass-group group-img">
                                            <span class="lock-icon"><i class="fab fa-twitter"></i></span>
                                            <input type="text" class="form-control" name="twitter" id="twitter"
                                                value="http://twitter.com">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row social-info">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group formlast-input lat-input">
                                        <label class="col-form-label" for="google">Google+</label>
                                        <div class="pass-group group-img">
                                            <span class="lock-icon"><i class="fa-brands fa-google-plus-g"></i></span>
                                            <input type="text" class="form-control" name="google" id="google"
                                                value="http://google.com">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group formlast-input">
                                        <label class="col-form-label" for="instagram">Instagram</label>
                                        <div class="pass-group group-img">
                                            <span class="lock-icon"><i class="fab fa-instagram"></i></span>
                                            <input type="text" class="form-control"name="instagram" id="instagram"
                                                value="http://instagram.com">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card media-section">
                        <div class="card-header">
                            <h4>Media Information </h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 featured-img1">
                                    <h6 class="media-title">Featured Image</h6>
                                    <div class="media-image ">
                                        <img src="{{ URL::asset('/assets/img/mediaimg-2.jpg') }}" alt="">
                                    </div>
                                    <div class="settings-upload-btn">
                                        <input type="file" accept="image/*" name="image"
                                            class="hide-input image-upload" id="file">
                                        <label for="file" class="file-upload" for="file">Upload File</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 featured-img2">
                                    <h6 class="Featured image">Logo</h6>
                                    <div class="media-image">
                                        <img src="{{ URL::asset('/assets/img/mediaimg-1.jpg') }}" alt="">
                                    </div>
                                    <div class="settings-upload-btn">
                                        <input type="file" accept="image/*" name="image"
                                            class="hide-input image-upload" id="file1">
                                        <label for="file1" class="file-upload" for="file1">Upload File</label>
                                    </div>
                                </div>
                            </div>
                            <div class="gallery-media">
                                <h6 class="media-title">Gallery</h6>
                                <div class="galleryimg-upload">
                                    <div class="gallery-upload">
                                        <img src="{{ URL::asset('/assets/img/gallery/gallerymedia-1.jpg') }}"
                                            class="img-fluid" alt="">
                                        <a href="javascript:void(0)" class="profile-img-del"><i
                                                class="feather-trash-2"></i></a>
                                    </div>
                                    <div class="gallery-upload">
                                        <img src="{{ URL::asset('/assets/img/gallery/gallerymedia-2.jpg') }}"
                                            class="img-fluid" alt="">
                                        <a href="javascript:void(0)" class="profile-img-del"><i
                                                class="feather-trash-2"></i></a>
                                    </div>
                                    <div class="gallery-upload">
                                        <img src="{{ URL::asset('/assets/img/gallery/gallerymedia-3.jpg') }}"
                                            class="img-fluid" alt="">
                                        <a href="javascript:void(0)" class="profile-img-del"><i
                                                class="feather-trash-2"></i></a>
                                    </div>
                                    <div class="gallery-upload">
                                        <img src="{{ URL::asset('/assets/img/gallery/gallerymedia-4.jpg') }}"
                                            class="img-fluid" alt="">
                                        <a href="javascript:void(0)" class="profile-img-del"><i
                                                class="feather-trash-2"></i></a>
                                    </div>
                                    <div class="gallery-upload">
                                        <img src="{{ URL::asset('/assets/img/gallery/gallerymedia-5.jpg') }}"
                                            class="img-fluid" alt="">
                                        <a href="javascript:void(0)" class="profile-img-del"><i
                                                class="feather-trash-2"></i></a>
                                    </div>
                                </div>
                                <div class="settings-upload-btn">
                                    <input type="file" accept="image/*" name="image"
                                        class="hide-input image-upload" id="file2">
                                    <label for="file2" class="file-upload" for="file2">Upload File</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit"> Submit</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /Profile Content -->
    @component('components.scrolltotop')
    @endcomponent
@endsection
