<?php $page = 'bookmarks'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Bookmarks
        @endslot
        @slot('li_1')
            Bookmarks
        @endslot
    @endcomponent

    <div class="dashboard-content">
        <div class="container">
            @component('components.nav')
            @endcomponent
            <!-- Bookmark Content -->
            <div class="bookmarks-content grid-view featured-slider">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 ">
                        <div class="card aos aos-init aos-animate" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/list/listgrid-1.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-02.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i>
                                                        Vehicles</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <img src="{{ URL::asset('/assets/img/eye.svg') }}"
                                                        alt="electronics">4000
                                                </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">2017 Gulfsteam Ameri-lite</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 06 Feb, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$350</span>
                                                <span>$450</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 ">
                        <div class="card aos aos-init aos-animate" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/list/listgrid-4.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-03.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i>
                                                        Electronics</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <img src="{{ URL::asset('/assets/img/eye.svg') }}"
                                                        alt="electronics">4000
                                                </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Fashion luxury Men date</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 08 May, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$250</span>
                                                <span>$350</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.6</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="card aos aos-init aos-animate" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/list/listgrid-8.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-04.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i>
                                                        Electronics</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <img src="{{ URL::asset('/assets/img/eye.svg') }}"
                                                        alt="electronics">4000
                                                </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Apple Iphone 6 16GB 4G LTE</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 09 Jan, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$550</span>
                                                <span>$400</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 ">
                        <div class="card aos aos-init aos-animate" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/list/listgrid-3.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-05.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i>
                                                        Electronics</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <img src="{{ URL::asset('/assets/img/eye.svg') }}"
                                                        alt="electronics">4000
                                                </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Customized Apple Imac </a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 10 Mar, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$450</span>
                                                <span>$300</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.5</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="card aos aos-init aos-animate" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/list/listgrid-2.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-06.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i>
                                                        Construction</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <img src="{{ URL::asset('/assets/img/eye.svg') }}"
                                                        alt="electronics">4000
                                                </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Villa 457 sq.m. In Benidorm
                                                Fully</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 11 May, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$650</span>
                                                <span>$600</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.5</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="card aos aos-init aos-animate" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/list/listgrid-5.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-03.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i>
                                                        Jobs</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <img src="{{ URL::asset('/assets/img/eye.svg') }}"
                                                        alt="electronics">4000
                                                </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">CDL A OTR Compnay Driver Job-N</a>
                                        </h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 12 Jan, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$550</span>
                                                <span>$450</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="card aos aos-init aos-animate" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/list/listgrid-6.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-04.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i>
                                                        Electronics</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <img src="{{ URL::asset('/assets/img/eye.svg') }}"
                                                        alt="electronics">4000
                                                </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">2012 Audi R8 GT Spyder
                                                Convertibl</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 02 Mar, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$450</span>
                                                <span>$350</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="card aos aos-init aos-animate" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/list/listgrid-7.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-07.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i>
                                                        Vehicles</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <img src="{{ URL::asset('/assets/img/eye.svg') }}"
                                                        alt="electronics">4000
                                                </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">HP Gaming 15.6 TouchScreen 12G</a>
                                        </h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 22 May, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$450</span>
                                                <span>$350</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 ">
                        <div class="card aos aos-init aos-animate" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/list/listgrid-1.jpg') }}" class="img-fluid"
                                            alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-02.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i>
                                                        Vehicles</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <img src="{{ URL::asset('/assets/img/eye.svg') }}"
                                                        alt="electronics">4000
                                                </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">2017 Gulfsteam Ameri-lite</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 9 Feb, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$350</span>
                                                <span>$450</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @component('components.pagination')
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
    <!-- /Bookmark Content -->

    @component('components.scrolltotop')
    @endcomponent
@endsection
