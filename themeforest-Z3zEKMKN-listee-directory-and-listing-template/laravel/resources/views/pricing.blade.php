<?php $page = 'pricing'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Pricing Plan
        @endslot
        @slot('li_1')
            Pricing Plan
        @endslot
    @endcomponent

    <!-- Pricing Plan Section -->
    <section class="pricingplan-section pricing-page">
        <div class="section-heading">
            <div class="container">
                <div class="row text-center">
                    <h2>Our Pricing <span>Pla</span>n</h2>
                    <p>checkout these latest cool ads from our members</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Intro</h6>
                            </div>
                            <h4>$10 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Basic</h6>
                            </div>
                            <h4>$25 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Popular</h6>
                            </div>
                            <h4>$50 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Enterprise</h6>
                            </div>
                            <h4>$100 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Pricing Plan Section -->

    @component('components.scrolltotop')
    @endcomponent
@endsection
