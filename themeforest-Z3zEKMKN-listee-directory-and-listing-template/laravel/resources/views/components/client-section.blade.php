<!-- CTA Section -->
<section class="cta-section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <div class="cta-content">
                    <h3>Earn Cash by <span>Selling</span> <br>
                        or Find Anything you desire
                    </h3>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                        alteration in some form, by injected humo or randomised words which don't look even slightlys
                    </p>
                    <div class="cta-btn">
                        <a href="{{ url('add-listing') }}" class="btn-primary postad-btn">Post Your Ads</a>
                        <a href="{{ url('listing-grid-sidebar') }}" class="browse-btn">Browse Ads</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="cta-img">
                    <img src="{{ URL::asset('/assets/img/cta-img.png') }}" class="img-fluid" alt="CTA">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /CTA Section -->

<!-- Client Testimonilas Section -->
<section class="testimonials-section">
    <div class="row">
        <div class="col-lg-5">
            <div class="testimonial-heading d-flex">
                <h4> Client <br> Testimonials</h4>
                <img src="{{ URL::asset('/assets/img/quotes.png') }}" alt="quotes">
            </div>
        </div>
        <div class="col-lg-7">
            <div class="rightimg"></div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="testimonials-slidersection">
                <div class="owl-nav mynav1"></div>
                <div class="owl-carousel testi-slider">
                    <div class="testimonial-info">
                        <div class="testimonialslider-heading d-flex">
                            <div class="testi-img">
                                <img src="{{ URL::asset('/assets/img/testimonial-1.jpg') }}" class="img-fluid"
                                    alt="testi-img">
                            </div>
                            <div class="testi-author">
                                <h6>Dev</h6>
                                <p>Lead Intranet Technician</p>
                            </div>

                        </div>
                        <div class="testimonialslider-content">
                            <p>Omnis totam molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod
                                vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum.
                                Alias dolores tempore illo accusantium est et voluptatem voluptas</p>
                        </div>
                    </div>
                    <div class="testimonial-info">
                        <div class="testimonialslider-heading d-flex">
                            <div class="testi-img">
                                <img src="{{ URL::asset('/assets/img/testimonial-2.jpg') }}" class="img-fluid"
                                    alt="testi-img">
                            </div>
                            <div class="testi-author">
                                <h6>Esther Hills</h6>
                                <p>Lead Intranet Technician</p>
                            </div>
                        </div>
                        <div class="testimonialslider-content">
                            <p>Omnis totam molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod
                                vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum.
                                Alias dolores tempore illo accusantium est et voluptatem voluptas</p>
                        </div>
                    </div>
                    <div class="testimonial-info">
                        <div class="testimonialslider-heading d-flex">
                            <div class="testi-img">
                                <img src="{{ URL::asset('/assets/img/testimonial-1.jpg') }}" class="img-fluid"
                                    alt="testi-img">
                            </div>
                            <div class="testi-author">
                                <h6>Dev</h6>
                                <p>Lead Intranet Technician</p>
                            </div>
                        </div>
                        <div class="testimonialslider-content">
                            <p>Omnis totam molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod
                                vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum.
                                Alias dolores tempore illo accusantium est et voluptatem voluptas</p>
                        </div>
                    </div>
                    <div class="testimonial-info">
                        <div class="testimonialslider-heading d-flex">
                            <div class="testi-img">
                                <img src="{{ URL::asset('/assets/img/testimonial-2.jpg') }}" class="img-fluid"
                                    alt="testi-img">
                            </div>
                            <div class="testi-author">
                                <h6>Esther Hills</h6>
                                <p>Lead Intranet Technician</p>
                            </div>
                        </div>
                        <div class="testimonialslider-content">
                            <p>Omnis totam molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod
                                vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum.
                                Alias dolores tempore illo accusantium est et voluptatem voluptas</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Client Testimonilas Section -->
<!-- Partners Section -->
<div class="partners-section">
    <div class="container">
        <p class="partners-heading">Over 5,26,000+ Sponsers being contact with us</p>
        <ul class="owl-carousel partnerslist d-flex">
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-1.svg') }}" alt="partners"></a></li>
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-2.svg') }}" alt="partners"></a></li>
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-3.svg') }}" alt="partners"></a></li>
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-4.svg') }}" alt="partners"></a></li>
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-5.svg') }}" alt="partners"></a></li>
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-6.svg') }}" alt="partners"></a></li>
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-1.svg') }}" alt="partners"></a></li>
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-2.svg') }}" alt="partners"></a></li>
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-3.svg') }}" alt="partners"></a></li>
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-4.svg') }}" alt="partners"></a></li>
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-5.svg') }}" alt="partners"></a></li>
            <li><a href="javascript:void(0)"><img class="img-fluid"
                        src="{{ URL::asset('/assets/img/partners/partners-6.svg') }}" alt="partners"></a></li>
        </ul>
    </div>
</div>
<!-- /Partners Section -->
