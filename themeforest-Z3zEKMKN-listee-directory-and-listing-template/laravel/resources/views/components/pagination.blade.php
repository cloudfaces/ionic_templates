                    <!--Pagination-->
                    <div class="blog-pagination">
                        <nav>
                            <ul class="pagination">
                                <li class="page-item previtem">
                                    <a class="page-link" href="#"><i class="fas fa-regular fa-arrow-left"></i>
                                        Prev</a>
                                </li>
                                <li class="justify-content-center pagination-center">
                                    <div class="pagelink">
                                        <ul>
                                            <li class="page-item">
                                                <a class="page-link" href="#">1</a>
                                            </li>
                                            <li class="page-item active">
                                                <a class="page-link" href="#">2 <span
                                                        class="visually-hidden">(current)</span></a>
                                            </li>
                                            <li class="page-item">
                                                <a class="page-link" href="#">3</a>
                                            </li>
                                            <li class="page-item">
                                                <a class="page-link" href="#">...</a>
                                            </li>
                                            <li class="page-item">
                                                <a class="page-link" href="#">14</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="page-item nextlink">
                                    <a class="page-link" href="#">Next <i
                                            class="fas fa-regular fa-arrow-right"></i></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!--Pagination-->
