<!-- scrollToTop start -->
@if (!Route::is(['index-9']))
    <div class="progress-wrap active-progress">
        <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"
                style="transition: stroke-dashoffset 10ms linear 0s; stroke-dasharray: 307.919px, 307.919px; stroke-dashoffset: 228.265px;">
            </path>
        </svg>
    </div>
@endif
<!-- scrollToTop end -->

@if (Route::is(['index-9']))
    <div class="home-nine progress-wrap active-progress">
        <svg class="progress-circle svg-content" width="99px" height="99px" viewBox="0 0 200 200">
            <path d="  M 0, 35.5 C 0, 0 0, 0 35.5, 0 S 71, 0 71, 35.5 71, 71 35.5, 71 0, 71 0, 35.5"
                style="transition: stroke-dashoffset 10ms linear 0s; stroke-dasharray: 307.919px, 307.919px; stroke-dashoffset: 228.265px;">
            </path>
        </svg>
    </div>
@endif
