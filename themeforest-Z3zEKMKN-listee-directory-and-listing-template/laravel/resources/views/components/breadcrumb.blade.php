<!--Inner Banner-->
@if (Route::is(['about']))
    <div class="aboutbanner innerbanner">
        <div class="inner-breadcrumb">
@endif
@if (!Route::is(['about', 'contact']))
    <div class="breadcrumb-bar">
@endif
@if (Route::is(['contact']))
    <div class="contactbanner innerbanner">
        <div class="inner-breadcrumb">
@endif
<div class="container">
    <div class="row align-items-center text-center">
        <div class="col-md-12 col-12 ">
            <h2 class="breadcrumb-title">{{ $title }}</h2>
            <nav aria-label="breadcrumb" class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('index') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ $li_1 }}</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

</div>

@if (Route::is(['about', 'contact']))
    </div>
@endif

<!--/Inner Banner-->
