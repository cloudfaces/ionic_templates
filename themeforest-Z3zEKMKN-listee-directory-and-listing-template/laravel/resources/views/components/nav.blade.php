<div class="{{ Request::is('dashboard', 'profile', 'bookmarks', 'messages', 'reviews', 'login') ? 'active' : '' }}">
    <ul class="dashborad-menus">
        <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
            <a href="{{ url('dashboard') }}">
                <i class="feather-grid"></i> <span>Dashboard</span>
            </a>
        </li>
        <li class="{{ Request::is('profile') ? 'active' : '' }}">
            <a href="{{ url('profile') }}">
                <i class="fa-solid fa-user"></i> <span>Profile</span>
            </a>
        </li>
        <li class="{{ Request::is('my-listing') ? 'active' : '' }}">
            <a href="{{ url('my-listing') }}">
                <i class="feather-list"></i> <span>My Listing</span>
            </a>
        </li>
        <li class="{{ Request::is('bookmarks') ? 'active' : '' }}">
            <a href="{{ url('bookmarks') }}">
                <i class="fas fa-solid fa-heart"></i> <span>Bookmarks</span>
            </a>
        </li>
        <li class="{{ Request::is('messages') ? 'active' : '' }}">
            <a href="{{ url('messages') }}">
                <i class="fa-solid fa-comment-dots"></i> <span>Messages</span>
            </a>
        </li>
        <li class="{{ Request::is('reviews') ? 'active' : '' }}">
            <a href="{{ url('reviews') }}">
                <i class="fas fa-solid fa-star"></i> <span>Reviews</span>
            </a>
        </li>
        <li class="{{ Request::is('login') ? 'active' : '' }}">
            <a href="{{ url('login') }}">
                <i class="fas fa-light fa-circle-arrow-left"></i> <span>Logout</span>
            </a>
        </li>
    </ul>
</div>
