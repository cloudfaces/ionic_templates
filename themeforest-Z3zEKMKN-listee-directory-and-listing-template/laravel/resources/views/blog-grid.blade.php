<?php $page = 'blog-grid'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Listings-Blog
        @endslot
        @slot('li_1')
            Blog
        @endslot
    @endcomponent

    <!-- Blog List -->
    <div class="bloglist-section blog-gridpage">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 d-lg-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-1.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0)"><span>Health</span></a><a
                                    href="javascript:void(0)"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-13.jpg') }}"
                                                alt="author">
                                        </div>
                                        <a href="javascript:void(0)"> <span> Mary </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> March 6, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best Spa Saloons for your
                                    relaxations?</a></h3>
                            <p class="blog-description">Dimply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry’s standard dumy text ever since the 1500s, when an
                                unknown printer took a galley of type ... </p>
                            <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-lg-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-4.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0)"><span>Health</span></a><a
                                    href="javascript:void(0)"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-14.jpg') }}"
                                                alt="author">
                                        </div>
                                        <a href="javascript:void(0)"> <span> Barbara </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> February 6, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best Spa Saloons for your
                                    relaxations?</a></h3>
                            <p class="blog-description">Dimply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry’s standard dumy text ever since the 1500s, when an
                                unknown printer took a galley of type ... </p>
                            <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-lg-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-5.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0)"><span>Health</span></a><a
                                    href="javascript:void(0)"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-12.jpg') }}"
                                                alt="author">
                                        </div>
                                        <a href="javascript:void(0)"> <span> Darryl </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> March 6, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best Spa Saloons for your
                                    relaxations?</a></h3>
                            <p class="blog-description">Dimply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry’s standard dumy text ever since the 1500s, when an
                                unknown printer took a galley of type ... </p>
                            <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-lg-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-1.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0)"><span>Health</span></a><a
                                    href="javascript:void(0)"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-11.jpg') }}"
                                                alt="author">
                                        </div>
                                        <a href="javascript:void(0)"> <span> Wilkerson </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> January 6, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best Spa Saloons for your
                                    relaxations?</a></h3>
                            <p class="blog-description">Dimply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry’s standard dumy text ever since the 1500s, when an
                                unknown printer took a galley of type ... </p>
                            <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-lg-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-4.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0)"><span>Health</span></a><a
                                    href="javascript:void(0)"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-10.jpg') }}"
                                                alt="author">
                                        </div>
                                        <a href="javascript:void(0)"> <span> Joseph </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> May 16, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best Spa Saloons for your
                                    relaxations?</a></h3>
                            <p class="blog-description">Dimply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry’s standard dumy text ever since the 1500s, when an
                                unknown printer took a galley of type ... </p>
                            <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-lg-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-5.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0)"><span>Health</span></a><a
                                    href="javascript:void(0)"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-09.jpg') }}"
                                                alt="author">
                                        </div>
                                        <a href="javascript:void(0)"> <span> Daniel </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> March 26, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best Spa Saloons for your
                                    relaxations?</a></h3>
                            <p class="blog-description">Dimply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry’s standard dumy text ever since the 1500s, when an
                                unknown printer took a galley of type ... </p>
                            <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-lg-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-1.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0)"><span>Health</span></a><a
                                    href="javascript:void(0)"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-08.jpg') }}"
                                                alt="author">
                                        </div>
                                        <a href="javascript:void(0)"> <span> Ashley </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> February 1, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best Spa Saloons for your
                                    relaxations?</a></h3>
                            <p class="blog-description">Dimply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry’s standard dumy text ever since the 1500s, when an
                                unknown printer took a galley of type ... </p>
                            <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-lg-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-4.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0)"><span>Health</span></a><a
                                    href="javascript:void(0)"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-07.jpg') }}"
                                                alt="author">
                                        </div>
                                        <a href="javascript:void(0)"> <span> Garcia </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> April 14, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best Spa Saloons for your
                                    relaxations?</a></h3>
                            <p class="blog-description">Dimply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry’s standard dumy text ever since the 1500s, when an
                                unknown printer took a galley of type ... </p>
                            <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-lg-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-5.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0)"><span>Health</span></a><a
                                    href="javascript:void(0)"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-06.jpg') }}"
                                                alt="author">
                                        </div>
                                        <a href="javascript:void(0)"> <span> Victor </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> January 2, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best Spa Saloons for your
                                    relaxations?</a></h3>
                            <p class="blog-description">Dimply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry’s standard dumy text ever since the 1500s, when an
                                unknown printer took a galley of type ... </p>
                            <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
            @component('components.pagination')
            @endcomponent
        </div>
    </div>
    <!-- /Blog List -->

    @component('components.scrolltotop')
    @endcomponent
@endsection
