<?php $page = 'error-404'; ?>
@extends('layout.mainlayout')
@section('content')
    <div class="error-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mx-auto">
                        <div class="error-wrap">

                            <div class="error-logo">
                                <a href="{{ url('/') }}"><img class="img-fluid"
                                        src="{{ URL::asset('/assets/img/logo.svg') }}" alt="img"></a>
                            </div>
                            <h2>Something went wrong</h2>
                            <div class="error-img">
                                <img class="img-fluid" src="{{ URL::asset('/assets/img/404-error.jpg') }}" alt="img">
                            </div>
                            <a href="{{ url('/') }}" class="btn btn-primary rounded-pill">Back to Home</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
