<?php $page = 'reviews'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Reviews
        @endslot
        @slot('li_1')
            Reviews
        @endslot
    @endcomponent

    <div class="dashboard-content">
        <div class="container">
            @component('components.nav')
            @endcomponent
            <!-- Reviews Content -->
            <div class="row dashboard-info reviewpage-content">
                <div class="col-lg-6 d-flex">
                    <div class="card dash-cards">
                        <div class="card-header">
                            <h4>Visitor Review</h4>
                            <div class="card-dropdown">
                                <ul>
                                    <li class="nav-item dropdown has-arrow logged-item">
                                        <a href="#" class="dropdown-toggle pageviews-link" data-bs-toggle="dropdown"
                                            aria-expanded="false">
                                            <span>All Listing</span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a class="dropdown-item" href="javascript:void(0)">Next Week</a>
                                            <a class="dropdown-item" href="javascript:void(0)">Last Month</a>
                                            <a class="dropdown-item" href="javascript:void(0)">Next Month</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul class="review-list">
                                <li class="review-box">
                                    <div class="review-profile">
                                        <div class="review-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-11.jpg') }}"
                                                class="img-fluid" alt="img">
                                        </div>
                                    </div>
                                    <div class="review-details">
                                        <h6>Joseph</h6>
                                        <div class="rating">
                                            <div class="rating-star">
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fa-regular fa-star rating-overall"></i>
                                            </div>
                                            <div><i class="fa-sharp fa-solid fa-calendar-days"></i> 4 months ago</div>
                                            <div>by: Joseph</div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. It
                                            has
                                            been the industry's standard dummy.</p>
                                        <ul class="review-gallery">
                                            <li>

                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-6.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-1.jpg') }}">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-9.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-9.jpg') }}">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-6.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-10.jpg') }}">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-5.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-2.jpg') }}">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="review-box">
                                    <div class="review-profile">
                                        <div class="review-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-02.jpg') }}"
                                                class="img-fluid" alt="img">
                                        </div>
                                    </div>
                                    <div class="review-details">
                                        <h6>Dev</h6>
                                        <div class="rating">
                                            <div class="rating-star">
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                            </div>
                                            <div><i class="fa-sharp fa-solid fa-calendar-days"></i> 6 months ago</div>
                                            <div>by: Joseph</div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. It
                                            has
                                            been the industry's standard dummy.</p>
                                    </div>
                                </li>
                                <li class="review-box">
                                    <div class="review-profile">
                                        <div class="review-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-01.jpg') }}"
                                                class="img-fluid" alt="img">
                                        </div>
                                    </div>
                                    <div class="review-details">
                                        <h6>Jonson</h6>
                                        <div class="rating">
                                            <div class="rating-star">
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fa-regular fa-star rating-overall"></i>
                                                <i class="fa-regular fa-star rating-overall"></i>
                                            </div>
                                            <div><i class="fa-sharp fa-solid fa-calendar-days"></i> 8 months ago</div>
                                            <div>by: Joseph</div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. It
                                            has
                                            been the industry's standard dummy.</p>
                                        <ul class="review-gallery">
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-6.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-1.jpg') }}">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-9.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-9.jpg') }}">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-10.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-10.jpg') }}">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-5.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-2.jpg') }}">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 d-flex">
                    <div class="card dash-cards">
                        <div class="card-header">
                            <h4>Your Review</h4>
                            <div class="card-dropdown">
                                <ul>
                                    <li class="nav-item dropdown has-arrow logged-item">
                                        <a href="#" class="dropdown-toggle pageviews-link"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            <span>All Listing</span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a class="dropdown-item" href="javascript:void(0)">Next Week</a>
                                            <a class="dropdown-item" href="javascript:void(0)">Last Month</a>
                                            <a class="dropdown-item" href="javascript:void(0)">Next Month</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul class="review-list">
                                <li class="review-box">
                                    <div class="review-profile">
                                        <div class="review-img">
                                            <img src="{{ URL::asset('/assets/img/profile-img.jpg') }}" class="img-fluid"
                                                alt="img">
                                        </div>
                                    </div>
                                    <div class="review-details">
                                        <h6>John Doe</h6>
                                        <div class="rating">
                                            <div class="rating-star">
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fa-regular fa-star rating-overall"></i>
                                                <i class="fa-regular fa-star rating-overall"></i>
                                            </div>
                                            <div><i class="fa-sharp fa-solid fa-calendar-days"></i> 4 months ago</div>
                                            <div>by: John Doe</div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. It
                                            has
                                            been the industry's standard dummy.</p>
                                        <ul class="review-gallery">
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-6.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-1.jpg') }}">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-9.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-9.jpg') }}">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-10.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-10.jpg') }}">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-5.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-2.jpg') }}">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="review-box">
                                    <div class="review-profile">
                                        <div class="review-img">
                                            <img src="{{ URL::asset('/assets/img/profile-img.jpg') }}" class="img-fluid"
                                                alt="img">
                                        </div>
                                    </div>
                                    <div class="review-details">
                                        <h6>John Doe</h6>
                                        <div class="rating">
                                            <div class="rating-star">
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fa-regular fa-star rating-overall"></i>
                                            </div>
                                            <div><i class="fa-sharp fa-solid fa-calendar-days"></i> 6 months ago</div>
                                            <div>by: John Doe</div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. It
                                            has
                                            been the industry's standard dummy.</p>
                                    </div>
                                </li>
                                <li class="review-box">
                                    <div class="review-profile">
                                        <div class="review-img">
                                            <img src="{{ URL::asset('/assets/img/profile-img.jpg') }}" class="img-fluid"
                                                alt="img">
                                        </div>
                                    </div>
                                    <div class="review-details">
                                        <h6>John Doe</h6>
                                        <div class="rating">
                                            <div class="rating-star">
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>

                                            </div>
                                            <div><i class="fa-sharp fa-solid fa-calendar-days"></i> 11 months ago</div>
                                            <div>by: John Doe</div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. It
                                            has
                                            been the industry's standard dummy.</p>
                                        <ul class="review-gallery">
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-6.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-1.jpg') }}">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-9.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-9.jpg') }}">
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-10.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-10.jpg') }}">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ URL::asset('/assets/img/gallery/gallery1/gallery-5.jpg') }}"
                                                    data-fancybox="gallery-2">
                                                    <img class="img-fluid" alt="Image"
                                                        src="{{ URL::asset('/assets/img/gallery/galleryimg-2.jpg') }}">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Reviews Content -->

    @component('components.scrolltotop')
    @endcomponent
@endsection
