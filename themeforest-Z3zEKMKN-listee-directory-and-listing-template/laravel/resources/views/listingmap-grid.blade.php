<?php $page = 'listingmap-grid'; ?>
@extends('layout.mainlayout')
@section('content')
    <!-- Main Content Section -->
    <div class="list-content listmap-grid ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 listingmappage-content">
                    <div class="listmapgrid-content">
                        <div class="row sorting-div">
                            <div class="col-lg-4 col-md-4 col-sm-4 align-items-center d-flex">
                                <div class="count-search">
                                    <p>Showing <span>1-8</span> of 10 Results</p>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8  align-items-center">
                                <div class="sortbyset">
                                    <span class="sortbytitle">Sort by</span>
                                    <div class="sorting-select">
                                        <select class="form-control select" name="price" id="price"
                                            autocomplete="off">
                                            <option>Default</option>
                                            <option>Price Low to High</option>
                                            <option>Price High to Low</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="grid-listview">
                                    <ul>
                                        <li>
                                            <a href="{{ url('listingmap-list') }}">
                                                <i class="feather-list"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('listingmap-grid') }}" class="active">
                                                <i class="feather-grid"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="grid-view">
                            <div class="row ">
                                <div class="col-lg-6 col-md-4">
                                    <div class="card">
                                        <div class="blog-widget">
                                            <div class="blog-img">
                                                <a href="{{ url('service-details') }}">
                                                    <img src="{{ URL::asset('/assets/img/list/listgrid-1.jpg') }}"
                                                        class="img-fluid" alt="blog-img">
                                                </a>
                                                <div class="fav-item">
                                                    <span class="Featured-text">Featured</span>
                                                    <a href="javascript:void(0)" class="fav-icon">
                                                        <i class="feather-heart"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="bloglist-content">
                                                <div class="card-body">
                                                    <div class="blogfeaturelink">
                                                        <div class="grid-author">
                                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-04.jpg') }}"
                                                                alt="author">
                                                        </div>
                                                        <div class="blog-features">
                                                            <a href="javascript:void(0)"><span> <i
                                                                        class="fa-regular fa-circle-stop"></i>
                                                                    Vehicles</span></a>
                                                        </div>
                                                        <div class="blog-author text-end">
                                                            <span> <i class="feather-eye"></i>4000 </span>
                                                        </div>
                                                    </div>
                                                    <h6><a href="{{ url('service-details') }}">2017 Gulfsteam
                                                            Ameri-lite</a></h6>
                                                    <div class="blog-location-details">
                                                        <div class="location-info">
                                                            <i class="feather-map-pin"></i> Los Angeles
                                                        </div>
                                                        <div class="location-info">
                                                            <i class="fa-solid fa-calendar-days"></i> 06 Apr, 2023
                                                        </div>
                                                    </div>
                                                    <div class="amount-details">
                                                        <div class="amount">
                                                            <span class="validrate">$350</span>
                                                            <span>$450</span>
                                                        </div>
                                                        <div class="ratings">
                                                            <span>4.0</span> (50)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="card">
                                        <div class="blog-widget">
                                            <div class="blog-img">
                                                <a href="{{ url('service-details') }}">
                                                    <img src="{{ URL::asset('/assets/img/list/listgrid-2.jpg') }}"
                                                        class="img-fluid" alt="blog-img">
                                                </a>
                                                <div class="fav-item">
                                                    <span class="Featured-text">Featured</span>
                                                    <a href="javascript:void(0)" class="fav-icon">
                                                        <i class="feather-heart"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="bloglist-content">
                                                <div class="card-body">
                                                    <div class="blogfeaturelink">
                                                        <div class="grid-author">
                                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-06.jpg') }}"
                                                                alt="author">
                                                        </div>
                                                        <div class="blog-features">
                                                            <a href="javascript:void(0)"><span> <i
                                                                        class="fa-regular fa-circle-stop"></i>
                                                                    Construction</span></a>
                                                        </div>
                                                        <div class="blog-author text-end">
                                                            <span> <i class="feather-eye"></i>9000 </span>
                                                        </div>
                                                    </div>
                                                    <h6><a href="{{ url('service-details') }}">Villa 457 sq.m. In
                                                            Benidorm Fully Qquipped House</a></h6>
                                                    <div class="blog-location-details">
                                                        <div class="location-info">
                                                            <i class="feather-map-pin"></i> Los Angeles
                                                        </div>
                                                        <div class="location-info">
                                                            <i class="fa-solid fa-calendar-days"></i> 26 Feb, 2023
                                                        </div>
                                                    </div>
                                                    <div class="amount-details">
                                                        <div class="amount">
                                                            <span class="validrate">$350</span>
                                                            <span>$450</span>
                                                        </div>
                                                        <div class="ratings">
                                                            <span>4.5</span> (50)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="card">
                                        <div class="blog-widget">
                                            <div class="blog-img">
                                                <a href="{{ url('service-details') }}">
                                                    <img src="{{ URL::asset('/assets/img/list/listgrid-3.jpg') }}"
                                                        class="img-fluid" alt="blog-img">
                                                </a>
                                                <div class="fav-item">
                                                    <span class="Featured-text">Featured</span>
                                                    <a href="javascript:void(0)" class="fav-icon">
                                                        <i class="feather-heart"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="bloglist-content">
                                                <div class="card-body">
                                                    <div class="blogfeaturelink">
                                                        <div class="grid-author">
                                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-07.jpg') }}"
                                                                alt="author">
                                                        </div>
                                                        <div class="blog-features">
                                                            <a href="javascript:void(0)"><span> <i
                                                                        class="fa-regular fa-circle-stop"></i>
                                                                    Electronics</span></a>
                                                        </div>
                                                        <div class="blog-author text-end">
                                                            <span> <i class="feather-eye"></i>5000 </span>
                                                        </div>
                                                    </div>
                                                    <h6><a href="{{ url('service-details') }}">Customized Apple iMac
                                                            21.5" All-In</a></h6>
                                                    <div class="blog-location-details">
                                                        <div class="location-info">
                                                            <i class="feather-map-pin"></i> Los Angeles
                                                        </div>
                                                        <div class="location-info">
                                                            <i class="fa-solid fa-calendar-days"></i> 18 Mar, 2023
                                                        </div>
                                                    </div>
                                                    <div class="amount-details">
                                                        <div class="amount">
                                                            <span class="validrate">$350</span>
                                                            <span>$450</span>
                                                        </div>
                                                        <div class="ratings">
                                                            <span>4.9</span> (50)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="card">
                                        <div class="blog-widget">
                                            <div class="blog-img">
                                                <a href="{{ url('service-details') }}">
                                                    <img src="{{ URL::asset('/assets/img/list/listgrid-4.jpg') }}"
                                                        class="img-fluid" alt="blog-img">
                                                </a>
                                                <div class="fav-item">
                                                    <span class="Featured-text">Featured</span>
                                                    <a href="javascript:void(0)" class="fav-icon">
                                                        <i class="feather-heart"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="bloglist-content">
                                                <div class="card-body">
                                                    <div class="blogfeaturelink">
                                                        <div class="grid-author">
                                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-04.jpg') }}"
                                                                alt="author">
                                                        </div>
                                                        <div class="blog-features">
                                                            <a href="javascript:void(0)"><span> <i
                                                                        class="fa-regular fa-circle-stop"></i>
                                                                    Electronics</span></a>
                                                        </div>
                                                        <div class="blog-author text-end">
                                                            <span> <i class="feather-eye"></i>8000 </span>
                                                        </div>
                                                    </div>
                                                    <h6><a href="{{ url('service-details') }}">Fashion Luxury Men
                                                            Date</a></h6>
                                                    <div class="blog-location-details">
                                                        <div class="location-info">
                                                            <i class="feather-map-pin"></i> Los Angeles
                                                        </div>
                                                        <div class="location-info">
                                                            <i class="fa-solid fa-calendar-days"></i> 14 Jan, 2023
                                                        </div>
                                                    </div>
                                                    <div class="amount-details">
                                                        <div class="amount">
                                                            <span class="validrate">$350</span>
                                                            <span>$450</span>
                                                        </div>
                                                        <div class="ratings">
                                                            <span>4.3</span> (50)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="card">
                                        <div class="blog-widget">
                                            <div class="blog-img">
                                                <a href="{{ url('service-details') }}">
                                                    <img src="{{ URL::asset('/assets/img/list/listgrid-5.jpg') }}"
                                                        class="img-fluid" alt="blog-img">
                                                </a>
                                                <div class="fav-item">
                                                    <span class="Featured-text">Featured</span>
                                                    <a href="javascript:void(0)" class="fav-icon">
                                                        <i class="feather-heart"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="bloglist-content">
                                                <div class="card-body">
                                                    <div class="blogfeaturelink">
                                                        <div class="grid-author">
                                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-05.jpg') }}"
                                                                alt="author">
                                                        </div>
                                                        <div class="blog-features">
                                                            <a href="javascript:void(0)"><span> <i
                                                                        class="fa-regular fa-circle-stop"></i>
                                                                    Jobs</span></a>
                                                        </div>
                                                        <div class="blog-author text-end">
                                                            <span> <i class="feather-eye"></i>6000 </span>
                                                        </div>
                                                    </div>
                                                    <h6><a href="{{ url('service-details') }}">CDL A OTR Compnay
                                                            Driver Job-N</a></h6>
                                                    <div class="blog-location-details">
                                                        <div class="location-info">
                                                            <i class="feather-map-pin"></i> Los Angeles
                                                        </div>
                                                        <div class="location-info">
                                                            <i class="fa-solid fa-calendar-days"></i> 16 May, 2023
                                                        </div>
                                                    </div>
                                                    <div class="amount-details">
                                                        <div class="amount">
                                                            <span class="validrate">$350</span>
                                                            <span>$450</span>
                                                        </div>
                                                        <div class="ratings">
                                                            <span>4.8</span> (50)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="card">
                                        <div class="blog-widget">
                                            <div class="blog-img">
                                                <a href="{{ url('service-details') }}">
                                                    <img src="{{ URL::asset('/assets/img/list/listgrid-6.jpg') }}"
                                                        class="img-fluid" alt="blog-img">
                                                </a>
                                                <div class="fav-item">
                                                    <span class="Featured-text">Featured</span>
                                                    <a href="javascript:void(0)" class="fav-icon">
                                                        <i class="feather-heart"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="bloglist-content">
                                                <div class="card-body">
                                                    <div class="blogfeaturelink">
                                                        <div class="grid-author">
                                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-06.jpg') }}"
                                                                alt="author">
                                                        </div>
                                                        <div class="blog-features">
                                                            <a href="javascript:void(0)"><span> <i
                                                                        class="fa-regular fa-circle-stop"></i>
                                                                    Vehicles</span></a>
                                                        </div>
                                                        <div class="blog-author text-end">
                                                            <span> <i class="feather-eye"></i>7000 </span>
                                                        </div>
                                                    </div>
                                                    <h6><a href="{{ url('service-details') }}">2023 Audi R8 GT Spyder
                                                            Convertible</a></h6>
                                                    <div class="blog-location-details">
                                                        <div class="location-info">
                                                            <i class="feather-map-pin"></i> Los Angeles
                                                        </div>
                                                        <div class="location-info">
                                                            <i class="fa-solid fa-calendar-days"></i> 09 Mar, 2023
                                                        </div>
                                                    </div>
                                                    <div class="amount-details">
                                                        <div class="amount">
                                                            <span class="validrate">$350</span>
                                                            <span>$450</span>
                                                        </div>
                                                        <div class="ratings">
                                                            <span>4.6</span> (50)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="card">
                                        <div class="blog-widget">
                                            <div class="blog-img">
                                                <a href="{{ url('service-details') }}">
                                                    <img src="{{ URL::asset('/assets/img/list/listgrid-8.jpg') }}"
                                                        class="img-fluid" alt="blog-img">
                                                </a>
                                                <div class="fav-item">
                                                    <span class="Featured-text">Featured</span>
                                                    <a href="javascript:void(0)" class="fav-icon">
                                                        <i class="feather-heart"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="bloglist-content">
                                                <div class="card-body">
                                                    <div class="blogfeaturelink">
                                                        <div class="grid-author">
                                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-03.jpg') }}"
                                                                alt="author">
                                                        </div>
                                                        <div class="blog-features">
                                                            <a href="javascript:void(0)"><span> <i
                                                                        class="fa-regular fa-circle-stop"></i>
                                                                    Electronics</span></a>
                                                        </div>
                                                        <div class="blog-author text-end">
                                                            <span> <i class="feather-eye"></i>3000 </span>
                                                        </div>
                                                    </div>
                                                    <h6><a href="{{ url('service-details') }}">Apple iphone6 16GB 4G
                                                            LTE</a></h6>
                                                    <div class="blog-location-details">
                                                        <div class="location-info">
                                                            <i class="feather-map-pin"></i> Los Angeles
                                                        </div>
                                                        <div class="location-info">
                                                            <i class="fa-solid fa-calendar-days"></i> 06 feb, 2023
                                                        </div>
                                                    </div>
                                                    <div class="amount-details">
                                                        <div class="amount">
                                                            <span class="validrate">$350</span>
                                                            <span>$450</span>
                                                        </div>
                                                        <div class="ratings">
                                                            <span>4.2</span> (50)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="card">
                                        <div class="blog-widget">
                                            <div class="blog-img">
                                                <a href="{{ url('service-details') }}">
                                                    <img src="{{ URL::asset('/assets/img/list/listgrid-7.jpg') }}"
                                                        class="img-fluid" alt="blog-img">
                                                </a>
                                                <div class="fav-item">
                                                    <span class="Featured-text">Featured</span>
                                                    <a href="javascript:void(0)" class="fav-icon">
                                                        <i class="feather-heart"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="bloglist-content">
                                                <div class="card-body">
                                                    <div class="blogfeaturelink">
                                                        <div class="grid-author">
                                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-07.jpg') }}"
                                                                alt="author">
                                                        </div>
                                                        <div class="blog-features">
                                                            <a href="javascript:void(0)"><span> <i
                                                                        class="fa-regular fa-circle-stop"></i>
                                                                    Electronics</span></a>
                                                        </div>
                                                        <div class="blog-author text-end">
                                                            <span> <i class="feather-eye"></i>9000 </span>
                                                        </div>
                                                    </div>
                                                    <h6><a href="{{ url('service-details') }}">HP Gaming 15.6
                                                            Touchscreen 12G</a></h6>
                                                    <div class="blog-location-details">
                                                        <div class="location-info">
                                                            <i class="feather-map-pin"></i> Los Angeles
                                                        </div>
                                                        <div class="location-info">
                                                            <i class="fa-solid fa-calendar-days"></i> 19 Mar, 2023
                                                        </div>
                                                    </div>
                                                    <div class="amount-details">
                                                        <div class="amount">
                                                            <span class="validrate">$350</span>
                                                            <span>$450</span>
                                                        </div>
                                                        <div class="ratings">
                                                            <span>4.7</span> (50)
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @component('components.pagination')
                        @endcomponent
                    </div>
                </div>
                <div class="col-lg-6 map-right">
                    <div id="map" class="map-listing"></div>
                    <div class="showfilter">
                        <button class="btn filterbtn">
                            <span class="showfilter-btn"><img src="{{ URL::asset('/assets/img/details-icon.svg') }}"
                                    alt="details-icon"> Show Filters</span>
                            <span class="hidefilter-btn"><i class="feather-eye-off"></i> Hide Filters</span>
                        </button>
                        <div class="listings-sidebar">
                            <div class="card">
                                <h4><img src="{{ URL::asset('/assets/img/details-icon.svg') }}" alt="details-icon">
                                    Filter</h4>
                                <form>
                                    <div class="filter-content looking-input form-group">
                                        <input type="text" class="form-control"
                                            placeholder="What are you looking for?" name="looking" id="looking">
                                    </div>
                                    <div class="filter-content form-group">
                                        <select class="form-control select category-select" name="category"
                                            id="category" autocomplete="off">
                                            <option value="">Choose Category</option>
                                            <option>Computer</option>
                                            <option>Electronics</option>
                                            <option>Car wash</option>
                                        </select>
                                    </div>
                                    <div class="filter-content looking-input form-group">
                                        <div class="group-img">
                                            <input type="text" class="form-control" placeholder="Where to look?"
                                                name="look" id="look">
                                            <i class="feather-map-pin"></i>
                                        </div>
                                    </div>
                                    <div class="filter-content form-group region">
                                        <select class="form-control select region-select" name="region" id="region"
                                            autocomplete="off">
                                            <option value="">Region</option>
                                            <option>Canada</option>
                                            <option>USA</option>
                                            <option>india</option>
                                        </select>
                                    </div>
                                    <div class="filter-content form-group amenities">
                                        <h4> Amenities</h4>
                                        <ul>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="wireless-internet"
                                                        id="wireless-internet">
                                                    <span class="checkmark"></span> Wireless Internet
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="accept-credit-card"
                                                        id="accept-credit-card">
                                                    <span class="checkmark"></span> Accepts Credit Cards
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="Coupouns" id="Coupouns">
                                                    <span class="checkmark"></span> Coupouns
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="parking-street" id="parking-street">
                                                    <span class="checkmark"></span> Parking Street
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="bike-parking" id="bike-parking">
                                                    <span class="checkmark"></span> Bike Parking
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom_check">
                                                    <input type="checkbox" name="Smoking-Allowed" id="Smoking-Allowed">
                                                    <span class="checkmark"></span> Smoking Allowed
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="filter-content form-group amenities radius">
                                        <div class="slidecontainer">
                                            <div class="slider-info">
                                                <h4> Radius</h4>
                                                <div class="demo"><span>50</span> Radius</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="filter-range">
                                                <input type="text" class="input-range" name="range" id="range">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter-content amenities mb-0">
                                        <h4> Price Range</h4>
                                        <div class="form-group mb-0">
                                            <input type="text" class="form-control" placeholder="Min" name="min"
                                                id="min">
                                            <input type="text" class="form-control me-0" placeholder="Max"
                                                name="max" id="max">
                                        </div>
                                        <div class="search-btn">
                                            <button class="btn btn-primary" type="submit"> <i class="fa fa-search"
                                                    aria-hidden="true"></i> Search</button>
                                            <button class="btn btn-reset mb-0" type="submit"> <i
                                                    class="fas fa-light fa-arrow-rotate-right"></i> Reset
                                                Filters</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Main Content Section -->
@endsection
