<?php $page = 'blog-list'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Listings-Blog
        @endslot
        @slot('li_1')
            Blog
        @endslot
    @endcomponent

    <!-- Blog List -->
    <div class="bloglisting">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="bloglist-widget">
                        <div class="blog grid-blog">
                            <div class="blog-image">
                                <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                        src="{{ URL::asset('/assets/img/blog/bloglistingimg-1.jpg') }}" alt="Post Image"></a>
                            </div>
                            <div class="blog-content">
                                <p class="blog-category">
                                    <a href="javascript:void(0)"><span>Health</span></a><a
                                        href="javascript:void(0)"><span>Care</span></a>
                                </p>
                                <ul class="entry-meta meta-item">
                                    <li>
                                        <div class="post-author">
                                            <div class="post-author-img">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-13.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <a href="javascript:void(0)"><span> Mary </span></a>
                                        </div>
                                    </li>
                                    <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> May 6, 2023</li>
                                </ul>
                                <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best SPA Salons For Your
                                        Relaxation</a></h3>
                                <p class="mb-0">Contrary to popular belief, Lorem Ipsum is not simply random text. It has
                                    roots in a piece of classical Latin literature from 45 BC, making it over 2000 years
                                    old. Richard</p>
                                <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                            class="feather-arrow-right"></i></a></div>
                            </div>
                        </div>
                        <div class="blog grid-blog">
                            <div class="blog-image">
                                <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                        src="{{ URL::asset('/assets/img/blog/bloglistingimg-2.jpg') }}"
                                        alt="Post Image"></a>
                            </div>
                            <div class="blog-content">
                                <p class="blog-category">
                                    <a href="javascript:void(0)"><span>Health</span></a><a
                                        href="javascript:void(0)"><span>Care</span></a>
                                </p>
                                <ul class="entry-meta meta-item">
                                    <li>
                                        <div class="post-author">
                                            <div class="post-author-img">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-14.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <a href="javascript:void(0)"> <span> Barbara </span></a>
                                        </div>
                                    </li>
                                    <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> March 9, 2023</li>
                                </ul>
                                <h3 class="blog-title"><a href="{{ url('blog-details') }}">Great Business Tips in 2023</a>
                                </h3>
                                <p class="mb-0">Contrary to popular belief, Lorem Ipsum is not simply random text. It has
                                    roots in a piece of classical Latin literature from 45 BC, making it over 2000 years
                                    old. Richard</p>
                                <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                            class="feather-arrow-right"></i></a></div>
                            </div>
                        </div>
                        <div class="blog grid-blog">
                            <div class="blog-image">
                                <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                        src="{{ URL::asset('/assets/img/blog/bloglistingimg-3.jpg') }}"
                                        alt="Post Image"></a>
                            </div>
                            <div class="blog-content">
                                <p class="blog-category">
                                    <a href="javascript:void(0)"><span>Health</span></a><a
                                        href="javascript:void(0)"><span>Care</span></a>
                                </p>
                                <ul class="entry-meta meta-item">
                                    <li>
                                        <div class="post-author">
                                            <div class="post-author-img">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-12.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <a href="javascript:void(0)"> <span> Darryl </span></a>
                                        </div>
                                    </li>
                                    <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> April 7, 2023</li>
                                </ul>
                                <h3 class="blog-title"><a href="{{ url('blog-details') }}">8 Amazing Tricks About
                                        Business</a></h3>
                                <p class="mb-0">Contrary to popular belief, Lorem Ipsum is not simply random text. It has
                                    roots in a piece of classical Latin literature from 45 BC, making it over 2000 years
                                    old. Richard</p>
                                <div class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                            class="feather-arrow-right"></i></a></div>
                            </div>
                        </div>
                    </div>
                    @component('components.pagination')
                    @endcomponent
                </div>
                <div class="col-lg-4 theiaStickySidebar">
                    <div class="rightsidebar blogsidebar">
                        <div class="card">
                            <h4><img src="{{ URL::asset('/assets/img/details-icon.svg') }}" alt="details-icon"> Filter
                            </h4>
                            <div class="filter-content looking-input form-group">
                                <input type="text" class="form-control" name="search" id="search"
                                    placeholder="To Search type and hit enter">
                            </div>
                        </div>
                        <div class="card">
                            <h4><img src="{{ URL::asset('/assets/img/category-icon.svg') }}" alt="details-icon">
                                Categories</h4>
                            <ul class="blogcategories-list">
                                <li><a href="javascript:void(0)">Accept Credit Cards</a></li>
                                <li><a href="javascript:void(0)">Smoking Allowed</a></li>
                                <li><a href="javascript:void(0)">Bike Parking</a></li>
                                <li><a href="javascript:void(0)">Street Parking</a></li>
                                <li><a href="javascript:void(0)">Wireless Internet</a></li>
                                <li><a href="javascript:void(0)">Pet Friendly</a></li>
                            </ul>
                        </div>
                        <div class="card tags-widget">
                            <h4><i class="feather-tag"></i> Tags</h4>
                            <ul class="tags">
                                <li>Travelling </li>
                                <li>Art </li>
                                <li>Vacation </li>
                                <li>Tourism </li>
                                <li>Culture </li>
                                <li>Lifestyle </li>
                                <li>Travelling </li>
                                <li>Art </li>
                                <li>vacation </li>
                                <li>Tourism </li>
                                <li>Culture </li>
                            </ul>
                        </div>
                        <div class="card">
                            <h4><i class="feather-tag"></i> Article</h4>
                            <div class="article">
                                <a href="{{ url('blog-details') }}" class="articleimg-1">
                                    <ul>
                                        <li>
                                            <h6>Great Business Tips in 2023</h6>
                                        </li>
                                        <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> January 6, 2023
                                        </li>
                                    </ul>
                                </a>
                            </div>
                            <div class="article">
                                <a href="{{ url('blog-details') }}" class="articleimg-2">
                                    <ul>
                                        <li>
                                            <h6>Excited News About Fashion.</h6>
                                        </li>
                                        <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> March 9, 2023
                                        </li>
                                    </ul>
                                </a>
                            </div>
                            <div class="article">
                                <a href="{{ url('blog-details') }}" class="articleimg-3">
                                    <ul>
                                        <li>
                                            <h6>8 Amazing Tricks About Business</h6>
                                        </li>
                                        <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> February 10, 2023
                                        </li>
                                    </ul>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Blog List -->

    @component('components.scrolltotop')
    @endcomponent
@endsection
