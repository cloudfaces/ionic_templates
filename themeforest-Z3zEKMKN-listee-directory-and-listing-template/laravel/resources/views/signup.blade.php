<?php $page = 'signup'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Create an Account
        @endslot
        @slot('li_1')
            Register
        @endslot
    @endcomponent

    <!-- Login Section -->
    <div class="login-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-5 mx-auto">
                    <div class="login-wrap register-form">

                        <div class="login-header">
                            <h3>Create an Account</h3>
                            <p>Lets start with <span>Listee</span></p>
                        </div>

                        <!-- Login Form -->
                        <form action="{{ route('register.custom') }}" method="POST">
                            @csrf
                            <div class="form-group group-img">
                                <div class="group-img">
                                    <i class="feather-user"></i>
                                    <input type="text" class="form-control" placeholder="Full Name" name="name"
                                        id="name" autocomplete="off">
                                        <div class="text-danger pt-2">
                                            @error('name')
                                                {{ $message }}
                                            @enderror
                                        </div>
                                </div>
                            </div>
                            <div class="form-group group-img">
                                <div class="group-img">
                                    <i class="feather-mail"></i>
                                    <input type="text" class="form-control" placeholder="Email Address" name="email"
                                        id="email" autocomplete="off">
                                        <div class="text-danger pt-2">
                                            @error('email')
                                                {{ $message }}
                                            @enderror
                                        </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="pass-group group-img">
                                    <i class="feather-lock"></i>
                                    <input type="password" class="form-control pass-input" placeholder="Password"
                                        name="password" id="password" autocomplete="off">
                                    <span class="toggle-password feather-eye-off"></span>
                                </div>
                                <div class="text-danger pt-2">
                                    @error('password')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                            <button class="btn btn-primary w-100 login-btn" type="submit">Create Account</button>
                            <div class="register-link text-center">
                                <p>Already have an account? <a class="forgot-link" href="{{ url('login') }}">Sign
                                        In</a></p>
                            </div>
                            <div class="login-or">
                                <span class="or-line"></span>
                                <span class="span-or">Sign in with Social Media Accounts</span>
                            </div>
                            <div class="social-login">
                                <a href="#" class="btn btn-apple w-100"><img
                                        src="{{ URL::asset('/assets/img/apple.svg') }}" class="me-1" alt="img">Sign
                                    in with Apple</a>
                            </div>
                            <div class="social-login">
                                <a href="#" class="btn btn-google w-100"><img
                                        src="{{ URL::asset('/assets/img/google.svg') }}" class="me-1" alt="img">Sign
                                    in with Google</a>
                            </div>
                            <div class="social-login">
                                <a href="#" class="btn btn-facebook w-100 mb-0"><img
                                        src="{{ URL::asset('/assets/img/facebook.svg') }}" class="me-2"
                                        alt="img">Continue with Facebook</a>
                            </div>
                        </form>
                        <!-- /Login Form -->

                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /Login Section -->

    @component('components.scrolltotop')
    @endcomponent
@endsection
