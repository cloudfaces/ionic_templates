<?php $page = 'index'; ?>
@extends('layout.mainlayout')
@section('content')
    <!-- Banner Section -->
    <section class="banner-section">
        <div class="banner-circle">
            <img src="{{ URL::asset('/assets/img/bannerbg.png') }}" class="img-fluid" alt="bannercircle">
        </div>
        <div class="container">
            <div class="home-banner">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <div class="section-search aos" data-aos="fade-up">
                            <p class="explore-text"> <span>Explore top-rated attractions</span></p>
                            <img src="{{ URL::asset('/assets/img/arrow-banner.png') }}" class="arrow-img" alt="arrow">
                            <h1>Let us help you <br>
                                <span>Find, Buy</span> & Own Dreams
                            </h1>
                            <p>Countrys most loved and trusted classified ad listing website classified ad.randomised words
                                which don't look even slightly Browse thousand of items near you.</p>
                            <div class="search-box">
                                <form action="{{ url('listing-grid-sidebar') }}" class="d-flex">
                                    <div class="search-input line">
                                        <div class="form-group">
                                            <select class="form-control select category-select" name="category"
                                                id="category">
                                                <option value="">Choose Category</option>
                                                <option>Computer</option>
                                                <option>Automobile</option>
                                                <option>Car Wash</option>
                                                <option>Cleaning</option>
                                                <option>Electrical</option>
                                                <option>Construction</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="search-input">
                                        <div class="form-group">
                                            <div class="group-img">
                                                <input type="text" class="form-control" name="location" id="location"
                                                    autocomplete="off" placeholder="Choose Location">
                                                <i class="feather-map-pin"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="search-btn">
                                        <button class="btn btn-primary" type="submit"> <i class="fa fa-search"
                                                aria-hidden="true"></i> Search</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="banner-imgs">
                            <img src="{{ URL::asset('/assets/img/Right-img.png') }}" class="img-fluid" alt="bannerimage">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{ URL::asset('/assets/img/bannerellipse.png') }}" class="img-fluid banner-elipse" alt="arrow">
        <img src="{{ URL::asset('/assets/img/banner-arrow.png') }}" class="img-fluid bannerleftarrow" alt="arrow">
    </section>
    <!-- /Banner Section -->

    <!-- Category Section -->
    <section class="category-section">
        <div class="container">
            <div class="section-heading">
                <div class="row align-items-center">
                    <div class="col-md-6 aos aos-init aos-animate" data-aos="fade-up">
                        <h2>Our <span class="title-left">Cat</span>egory</h2>
                        <p>Buy and Sell Everything from Used Our Top Category</p>
                    </div>
                    <div class="col-md-6 text-md-end aos aos-init aos-animate" data-aos="fade-up">
                        <a href="{{ url('categories') }}" class="btn  btn-view">View All</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>Automotive</h5>
                        <span>09 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-1.svg') }}" alt="icons">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>Electronics</h5>
                        <span>07 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-2.svg') }}" alt="icons">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>Fashion Style</h5>
                        <span>10 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-3.svg') }}" alt="icons">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>Health Care</h5>
                        <span>08 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-4.svg') }}" alt="icons">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>Job Board</h5>
                        <span>06 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-5.svg') }}" alt="icons">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>Education</h5>
                        <span>09 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-6.svg') }}" alt="icons">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>Real Estate</h5>
                        <span>07 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-7.svg') }}" alt="icons">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>Travel</h5>
                        <span>05 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-8.svg') }}" alt="icons">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>Sports & Game</h5>
                        <span>08 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-9.svg') }}" alt="icons">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>Magazines</h5>
                        <span>10 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-10.svg') }}" alt="icons">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>Pet & Animal</h5>
                        <span>09 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-11.svg') }}" alt="icons">
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <a href="{{ url('categories') }}" class="category-links">
                        <h5>House Hold</h5>
                        <span>07 Ads</span>
                        <img src="{{ URL::asset('/assets/img/icons/category-12.svg') }}" alt="icons">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- /Category Section -->

    
    <!-- Featured ADS Section -->
    <section class="featured-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 aos aos-init aos-animate" data-aos="fade-up">
                    <div class="section-heading">
                        <h2>Featu<span class="title-right">red</span> Ads</h2>
                        <p>Checkout these latest coo ads from our members</p>
                    </div>
                </div>
                <div class="col-md-6 text-md-end aos" data-aos="fade-up">
                    <div class="owl-nav mynav2"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel featured-slider grid-view">
                        <div class="card aos" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-9.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-02.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Education</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <i class="feather-eye"></i>4000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">2017 Gulfsteam Ameri-lite</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-regular fa-calendar-days"></i> 06 Apr, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$350</span>
                                                <span>$450</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card aos" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-2.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-05.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Electronics</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <i class="feather-eye"></i>6000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Fashion luxury Men date</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 08 Jan, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$250</span>
                                                <span>$350</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.6</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card aos" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-3.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-04.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Electronics</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <i class="feather-eye"></i>5000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Apple Iphone 6 16GB 4G LTE</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 09 May, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$550</span>
                                                <span>$400</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card aos" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-4.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-05.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Gadgets</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <i class="feather-eye"></i>8000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Customized Apple Imac </a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 10 Feb, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$450</span>
                                                <span>$300</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.5</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card aos" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-9.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-02.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Education</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <i class="feather-eye"></i>9000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">2017 Gulfsteam Ameri-lite</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 06 May, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$350</span>
                                                <span>$450</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card aos" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-5.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-06.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Construction</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span><i class="feather-eye"></i> 6000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Villa 457 sq.m. In Benidorm Fully</a>
                                        </h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 11 Apr, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$650</span>
                                                <span>$600</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.5</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Featured ADS Section -->

    <!-- Popular Location Section -->
    <section class="popular-locations">
        <div class="popular-circleimg">
            <img class="img-fluid" src="{{ URL::asset('/assets/img/popular-img.png') }}" alt="Popular-sections">
        </div>
        <div class="container">
            <div class="section-heading">
                <h2>Popular <span>Loc</span>ations</h2>
                <p>Start by selecting your favuorite location and explore the goods</p>
            </div>
            <div class="location-details d-flex">
                <div class="row">
                    <div class="location-info col-lg-4 col-md-6">
                        <div class="location-info-details d-flex align-items-center">
                            <div class="location-img">
                                <a href="{{ url('listing-grid-sidebar') }}"><img class="img-fluid"
                                        src="{{ URL::asset('/assets/img/locations/usa.jpg') }}" alt="locations"></a>
                            </div>
                            <div class="location-content">
                                <a href="{{ url('listing-grid-sidebar') }}">USA</a>
                                <p>20+ Ads Posted</p>
                                <a href="{{ url('listing-grid-sidebar') }}" class="view-detailsbtn">View Details</a>
                            </div>
                        </div>
                    </div>
                    <div class="location-info col-lg-4 col-md-6">
                        <div class="location-info-details d-flex align-items-center">
                            <div class="location-img">
                                <a href="{{ url('listing-grid-sidebar') }}"><img class="img-fluid"
                                        src="{{ URL::asset('/assets/img/locations/canada.jpg') }}" alt="locations"></a>
                            </div>
                            <div class="location-content">
                                <a href="{{ url('listing-grid-sidebar') }}">Canada</a>
                                <p>10+ Ads Posted</p>
                                <a href="{{ url('listing-grid-sidebar') }}" class="view-detailsbtn">View Details</a>
                            </div>
                        </div>
                    </div>
                    <div class="location-info col-lg-4 col-md-6">
                        <div class="location-info-details d-flex align-items-center">
                            <div class="location-img">
                                <a href="{{ url('listing-grid-sidebar') }}"><img class="img-fluid"
                                        src="{{ URL::asset('/assets/img/locations/china.jpg') }}" alt="locations"></a>
                            </div>
                            <div class="location-content">
                                <a href="{{ url('listing-grid-sidebar') }}">China</a>
                                <p>40+ Ads Posted</p>
                                <a href="{{ url('listing-grid-sidebar') }}" class="view-detailsbtn">View Details</a>
                            </div>
                        </div>
                    </div>
                    <div class="location-info col-lg-4 col-md-6">
                        <div class="location-info-details d-flex align-items-center">
                            <div class="location-img">
                                <a href="{{ url('listing-grid-sidebar') }}"><img class="img-fluid"
                                        src="{{ URL::asset('/assets/img/locations/uk.jpg') }}" alt="locations"></a>
                            </div>
                            <div class="location-content">
                                <a href="{{ url('listing-grid-sidebar') }}">United Kingdom</a>
                                <p>30+ Ads Posted</p>
                                <a href="{{ url('listing-grid-sidebar') }}" class="view-detailsbtn">View Details</a>
                            </div>
                        </div>
                    </div>
                    <div class="location-info col-lg-4 col-md-6">
                        <div class="location-info-details d-flex align-items-center">
                            <div class="location-img">
                                <a href="{{ url('listing-grid-sidebar') }}"><img class="img-fluid"
                                        src="{{ URL::asset('/assets/img/locations/australia.jpg') }}"
                                        alt="locations"></a>
                            </div>
                            <div class="location-content">
                                <a href="{{ url('listing-grid-sidebar') }}">Australia</a>
                                <p>50+ Ads Posted</p>
                                <a href="{{ url('listing-grid-sidebar') }}" class="view-detailsbtn">View Details</a>
                            </div>
                        </div>
                    </div>
                    <div class="location-info col-lg-4 col-md-6">
                        <div class="location-info-details d-flex align-items-center">
                            <div class="location-img">
                                <a href="{{ url('listing-grid-sidebar') }}"><img class="img-fluid"
                                        src="{{ URL::asset('/assets/img/locations/france.jpg') }}" alt="locations"></a>
                            </div>
                            <div class="location-content">
                                <a href="{{ url('listing-grid-sidebar') }}">France</a>
                                <p>10+ Ads Posted</p>
                                <a href="{{ url('listing-grid-sidebar') }}" class="view-detailsbtn">View Details</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="align-items-center"><a href="{{ url('listing-grid-sidebar') }}" class="browse-btn">Browse
                    Ads</a></div>
        </div>
    </section>
    <!-- /Popular Locations Section -->

    <!-- Latest Ads Section -->
    <section class="latestad-section grid-view featured-slider">
        <div class="container">
            <div class="section-heading">
                <div class="row align-items-center">
                    <div class="col-md-6 aos aos-init aos-animate" data-aos="fade-up">
                        <h2>Lat<span class="title-right">est</span> Ads</h2>
                        <p>checkout these latest cool ads from our members</p>
                    </div>
                    <div class="col-md-6 text-md-end aos aos-init aos-animate" data-aos="fade-up">
                        <a href="{{ url('service-details') }}" class="btn  btn-view">View All</a>
                    </div>
                </div>
            </div>
            <div class="lateestads-content">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6 d-flex">
                        <div class="card aos flex-fill" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-9.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-02.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Education</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <i class="feather-eye"></i> 4000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">2017 Gulfsteam Ameri-lite</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 06 Jan, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$350</span>
                                                <span>$450</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 d-flex">
                        <div class="card aos flex-fill" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-2.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-03.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Electronics</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <i class="feather-eye"></i> 8000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Fashion luxury Men date</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 08 Feb, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$250</span>
                                                <span>$350</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.6</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 d-flex">
                        <div class="card aos flex-fill" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-3.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-04.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Electronics</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <i class="feather-eye"></i> 5000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Apple Iphone 6 16GB 4G LTE</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 09 Apr, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$550</span>
                                                <span>$400</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 d-flex">
                        <div class="card aos flex-fill" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-4.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-05.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Gadgets</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span><i class="feather-eye"></i> 7000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Customized Apple Imac </a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 10 May, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$450</span>
                                                <span>$300</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.5</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 d-flex">
                        <div class="card aos flex-fill" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-5.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-06.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Construction</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <i class="feather-eye"></i>6000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">Villa 457 sq.m. In Benidorm Fully</a>
                                        </h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 11 Mar, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$650</span>
                                                <span>$600</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.5</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 d-flex">
                        <div class="card aos flex-fill" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-6.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-03.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Jobs</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <i class="feather-eye"></i>4000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">CDL A OTR Compnay Driver Job-N</a>
                                        </h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 12 Jan, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$550</span>
                                                <span>$450</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 d-flex">
                        <div class="card aos flex-fill" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-7.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-06.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span><i
                                                            class="fa-regular fa-circle-stop"></i> Electronics</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span><i class="feather-eye"></i> 9000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">HP Gaming 15.6 Touchscren 12G</a></h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 02 Apr, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$450</span>
                                                <span>$350</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.9</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 d-flex">
                        <div class="card aos flex-fill" data-aos="fade-up">
                            <div class="blog-widget">
                                <div class="blog-img">
                                    <a href="{{ url('service-details') }}">
                                        <img src="{{ URL::asset('/assets/img/featured/feature-8.jpg') }}"
                                            class="img-fluid" alt="blog-img">
                                    </a>
                                    <div class="fav-item">
                                        <span class="Featured-text">Featured</span>
                                        <a href="javascript:void(0)" class="fav-icon">
                                            <i class="feather-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bloglist-content">
                                    <div class="card-body">
                                        <div class="blogfeaturelink">
                                            <div class="grid-author">
                                                <img src="{{ URL::asset('/assets/img/profiles/avatar-07.jpg') }}"
                                                    alt="author">
                                            </div>
                                            <div class="blog-features">
                                                <a href="javascript:void(0)"><span> <i
                                                            class="fa-regular fa-circle-stop"></i> Vehicle</span></a>
                                            </div>
                                            <div class="blog-author text-end">
                                                <span> <i class="feather-eye"></i>5000 </span>
                                            </div>
                                        </div>
                                        <h6><a href="{{ url('service-details') }}">2012 AudiR8 GT Spider Convrtibile</a>
                                        </h6>
                                        <div class="blog-location-details">
                                            <div class="location-info">
                                                <i class="feather-map-pin"></i> Los Angeles
                                            </div>
                                            <div class="location-info">
                                                <i class="fa-solid fa-calendar-days"></i> 02 Mar, 2023
                                            </div>
                                        </div>
                                        <div class="amount-details">
                                            <div class="amount">
                                                <span class="validrate">$450</span>
                                                <span>$350</span>
                                            </div>
                                            <div class="ratings">
                                                <span>4.7</span> (50)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Latest Ads Section -->

    @component('components.client-section')
    @endcomponent

    <!-- Pricing Plan Section -->
    <section class="pricingplan-section">
        <div class="section-heading">
            <div class="container">
                <div class="row text-center">
                    <h2>Our Pricing <span>Pla</span>n</h2>
                    <p>checkout these latest cool ads from our members</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Intro</h6>
                            </div>
                            <h4>$10 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Basic</h6>
                            </div>
                            <h4>$25 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Popular</h6>
                            </div>
                            <h4>$50 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Enterprise</h6>
                            </div>
                            <h4>$100 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Pricing Plan Section -->

    <!-- Blog  Section -->
    <section class="blog-section">
        <div class="section-heading">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 aos aos-init aos-animate" data-aos="fade-up">
                        <h2>Lat<span class="title-right">est</span> Blog</h2>
                        <p>people are giving these goods for free so check them out</p>
                    </div>
                    <div class="col-md-6 text-md-end aos aos-init aos-animate" data-aos="fade-up">
                        <a href="{{ url('blog-grid') }}" class="btn  btn-view">View All</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 d-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-1.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0);"><span>Health</span></a><a
                                    href="javascript:void(0);"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-14.jpg') }}"
                                                alt="Post Author">
                                        </div>
                                        <a href="javascript:void(0);" class="mb-0"> <span> Amara </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> May 4, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">The Best Spa Saloons for your
                                    relaxations?</a></h3>
                            <p class="blog-description">Lorem ipsum dolor sit amet, consectetur em adipiscing elit, sed do
                                eiusmod tempor. Lorem ipsum dolor sit amet, consectetur em adipiscing elit,</p>
                            <p class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-2.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0);"><span>Health</span></a><a
                                    href="javascript:void(0);"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-12.jpg') }}"
                                                alt="Post Author">
                                        </div>
                                        <a href="javascript:void(0);" class="mb-0"><span> Darryl </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> Mar 6, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">Three Powerful Tricks For Online
                                    Advertising</a></h3>
                            <p class="blog-description">Lorem ipsum dolor sit amet, consectetur em adipiscing elit, sed do
                                eiusmod tempor. Lorem ipsum dolor sit amet, consectetur em adipiscing elit,</p>
                            <p class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-flex">
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ url('blog-details') }}"><img class="img-fluid"
                                    src="{{ URL::asset('/assets/img/blog/blog-3.jpg') }}" alt="Post Image"></a>
                        </div>
                        <div class="blog-content">
                            <p class="blog-category">
                                <a href="javascript:void(0);"><span>Health</span></a><a
                                    href="javascript:void(0);"><span>Care</span></a>
                            </p>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        <div class="post-author-img">
                                            <img src="{{ URL::asset('/assets/img/profiles/avatar-13.jpg') }}"
                                                alt="Post Author">
                                        </div>
                                        <a href="javascript:void(0);" class="mb-0"> <span> Mary </span></a>
                                    </div>
                                </li>
                                <li class="date-icon"><i class="fa-solid fa-calendar-days"></i> Apr 10, 2023</li>
                            </ul>
                            <h3 class="blog-title"><a href="{{ url('blog-details') }}">Competitive Analysis for
                                    Enterprerneurs in 20232</a></h3>
                            <p class="blog-description">Lorem ipsum dolor sit amet, consectetur em adipiscing elit, sed do
                                eiusmod tempor. Lorem ipsum dolor sit amet, consectetur em adipiscing elit,</p>
                            <p class="viewlink"><a href="{{ url('blog-details') }}">View Details <i
                                        class="feather-arrow-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Blog  Section -->
    @component('components.scrolltotop')
    @endcomponent
@endsection
