<?php $page = 'gallery'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Gallery
        @endslot
        @slot('li_1')
            Gallery
        @endslot
    @endcomponent

    <!-- Gallery section-->
    <div class="gallerypage-info">
        <div class="container">
            <div class="gallery-content">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-1.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-1.jpg') }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-2.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-2.jpg') }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-3.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-3.jpg') }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget me-0">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-4.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-4.jpg') }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-5.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-5.jpg') }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-6.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-6.jpg') }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-7.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-7.jpg') }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget me-0">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-8.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-8.jpg') }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget me-0">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-9.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-9.jpg') }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget me-0">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-10.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-10.jpg') }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget me-0">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-12.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-12.jpg') }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="gallery-widget me-0">
                            <a href="{{ URL::asset('/assets/img/gallery/gallery2/gallerypop-3.jpg') }}"
                                data-fancybox="gallery2">
                                <img class="img-fluid" alt="Image"
                                    src="{{ URL::asset('/assets/img/gallery/gallery2/galleryimage-3.jpg') }}">
                            </a>
                        </div>
                    </div>
                    @component('components.pagination')
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
    <!-- /Gallery section-->

    @component('components.scrolltotop')
    @endcomponent
@endsection
