<?php $page = 'about'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            About Us
        @endslot
        @slot('li_1')
            About us
        @endslot
    @endcomponent

    <!--About Content-->
    <section class="about-content">
        <div class="container">
            <div class="about-listee">
                <div class="about-img">
                    <img src="{{ URL::asset('/assets/img/about-img.jpg') }}" class="img-fluid" alt="">
                </div>
                <div class="about-info">
                    <h4> <span>About</span> Listee</h4>
                    <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla
                        fermentum malesuada pulvinar. Vestibulum laoreet rutrum semper. Vivamus malesuada, nisl in
                        consectetur semper, mauris nulla aliquam risus, nec ultricies sapien elit sed ante. Aenean luctus
                        felis in sem</p>
                    <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla
                        fermentum malesuada pulvinar. Vestibulum laoreet rutrum semper. Vivamus malesuada, </p>
                </div>
            </div>
        </div>
    </section>
    <!--/About Content-->

    <!--How It Works-->
    <section class="howitworks">
        <div class="container">
            <h3>How It Work</h3>
            <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla fermentum
                malesuada pulvinar. Vestibulum laoreet rutrum semper. Vivamus malesuada, nisl in consectetur semper, mauris
                nulla aliquam risus, nec ultricies sapien elit sed ante. Aenean luctus felis in sem consequat auctor. Nulla
                turpis enim, scelerisque sit amet consectetur vel, lacinia sed augue. Proin ultricies dui id ex fringilla
                porta.</p>
            <p>Morbi nisi justo, venenatis ac nibh at, bibendum mattis risus. Maecenas tincidunt, ligula sed congue tempus,
                magna augue cursus ipsum, in malesuada justo risus nec lorem. Nam augue augue, mollis nec condimentum
                euismod, lacinia ultricies leo.</p>
            <div class="row">
                <div class="col-lg-4 col-md-4 d-flex">
                    <div class="howitwork-info">
                        <h5>01</h5>
                        <h6>Create Account</h6>
                        <p>Morbi nisi justo, venenatis ac nibh at, bibendum mattis risus. Maecenas tincidunt, ligula sed
                            congue tempus, magna augue cursus ipsum, in malesuada justo risus nec lorem. Nam augue augue,
                            mollis nec condimentum euismod, lacinia ultricies leo.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-flex">
                    <div class="howitwork-info">
                        <h5>02</h5>
                        <h6>Post An Ad</h6>
                        <p>Morbi nisi justo, venenatis ac nibh at, bibendum mattis risus. Maecenas tincidunt, ligula sed
                            congue tempus, magna augue cursus ipsum, in malesuada justo risus nec lorem. Nam augue augue,
                            mollis nec condimentum euismod, lacinia ultricies leo.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-flex">
                    <div class="howitwork-info">
                        <h5>03</h5>
                        <h6>Find,Buy & Own Dreams</h6>
                        <p>Morbi nisi justo, venenatis ac nibh at, bibendum mattis risus. Maecenas tincidunt, ligula sed
                            congue tempus, magna augue cursus ipsum, in malesuada justo risus nec lorem. Nam augue augue,
                            mollis nec condimentum euismod, lacinia ultricies leo.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/How It Works-->

    <!--Popular Location-->
    <div class="aboutpopular-locations">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="aboutlocation-details usa">
                        <div class="aboutloc-img">
                            <img src="{{ URL::asset('/assets/img/locations/london-1.jpg') }}" alt="">
                        </div>
                        <a href="{{ url('listing-grid-sidebar') }}">
                            <div class="aboutlocations-info">
                                <h3>London</h3>
                                <span>20+ Ads Posted</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="aboutlocation-details usa">
                        <div class="aboutloc-img">
                            <img src="{{ URL::asset('/assets/img/locations/usa-1.jpg') }}" alt="">
                        </div>
                        <a href="{{ url('listing-grid-sidebar') }}">
                            <div class="aboutlocations-info">
                                <h3>USA</h3>
                                <span>15+ Ads Posted</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="aboutlocation-details canada">
                        <div class="aboutloc-img">
                            <img src="{{ URL::asset('/assets/img/locations/canada-1.jpg') }}" alt="">
                        </div>
                        <a href="{{ url('listing-grid-sidebar') }}">
                            <div class="aboutlocations-info">
                                <h3>Canada</h3>
                                <span>25+ Ads Posted</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="aboutlocation-details china">
                        <div class="aboutloc-img">
                            <img src="{{ URL::asset('/assets/img/locations/china-1.jpg') }}" alt="">
                        </div>
                        <a href="{{ url('listing-grid-sidebar') }}">
                            <div class="aboutlocations-info">
                                <h3>China</h3>
                                <span>17+ Ads Posted</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="aboutlocation-details uk">
                        <div class="aboutloc-img">
                            <img src="{{ URL::asset('/assets/img/locations/uk-1.jpg') }}" alt="">
                        </div>
                        <a href="{{ url('listing-grid-sidebar') }}">
                            <div class="aboutlocations-info">
                                <h3>United Kingdom</h3>
                                <span>12+ Ads Posted</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="aboutlocation-details australi-loc">
                        <div class="aboutloc-img">
                            <img src="{{ URL::asset('/assets/img/locations/australia-1.jpg') }}" alt="">
                        </div>
                        <a href="{{ url('listing-grid-sidebar') }}">
                            <div class="aboutlocations-info">
                                <h3>Australia</h3>
                                <span>25+ Ads Posted</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="aboutlocation-details france">
                        <div class="aboutloc-img">
                            <img src="{{ URL::asset('/assets/img/locations/france-1.jpg') }}" alt="">
                        </div>
                        <a href="{{ url('listing-grid-sidebar') }}">
                            <div class="aboutlocations-info">
                                <h3>France</h3>
                                <span>14+ Ads Posted</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/Popular location-->
    @component('components.client-section')
    @endcomponent
    @component('components.scrolltotop')
    @endcomponent
@endsection
