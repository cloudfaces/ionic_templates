<?php $page = 'howitworks'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            How it Works
        @endslot
        @slot('li_1')
            How it Works
        @endslot
    @endcomponent

    <!-- How It Works section -->
    <section class="work-section">
        <div class="container">
            <div class="work-heading">
                <h4>Steps to improve your Business</h4>
                <p class="description">Add your business to Listee, so customers can easily find</p>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 d-flex">
                    <div class="work-info card">
                        <img src="{{ URL::asset('/assets/img/icons/work1.jpg') }}" class="img-fluid" alt="">
                        <h5>01</h5>
                        <h6>Create Account</h6>
                        <p>Morbi nisi justo, venenatis ac nibh at, bibendum mattis risus. Maecenas tincidunt, ligula sed
                            congue tempus, magna augue cursus ipsum, in malesuada justo risus nec lorem. Nam augue
                            augue, mollis nec condimentum euismod, lacinia ultricies leo.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-flex">
                    <div class="work-info card">
                        <h5 class="mt-0">02</h5>
                        <h6>Post An Ad </h6>
                        <p>Morbi nisi justo, venenatis ac nibh at, bibendum mattis risus. Maecenas tincidunt, ligula sed
                            congue tempus, magna augue cursus ipsum, in malesuada justo risus nec lorem. Nam augue
                            augue, mollis nec condimentum euismod, lacinia ultricies leo.</p>
                        <img src="{{ URL::asset('/assets/img/icons/work2.jpg') }}" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 d-flex">
                    <div class="work-info card">
                        <img src="{{ URL::asset('/assets/img/icons/work3.jpg') }}" class="img-fluid" alt="">
                        <h5>03</h5>
                        <h6>Find, Buy and Own Dreams</h6>
                        <p>Morbi nisi justo, venenatis ac nibh at, bibendum mattis risus. Maecenas tincidunt, ligula sed
                            congue tempus, magna augue cursus ipsum, in malesuada justo risus nec lorem. Nam augue
                            augue, mollis nec condimentum euismod, lacinia ultricies leo.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /How It Works section -->

    <!-- Pricing Plan Section -->
    <section class="pricingplan-section how-work">
        <div class="section-heading">
            <div class="container">
                <div class="row text-center">
                    <h2>Our Pricing <span>Pla</span>n</h2>
                    <p>You can set to charge users for Packages, Subscriptions, Hybrid or Free</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Intro</h6>
                            </div>
                            <h4>$10 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Basic</h6>
                            </div>
                            <h4>$25 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Popular</h6>
                            </div>
                            <h4>$50 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex col-md-6">
                    <div class="price-card flex-fill">
                        <div class="price-head">
                            <div class="price-level">
                                <h6>Enterprise</h6>
                            </div>
                            <h4>$100 <span>/ month</span></h4>
                        </div>
                        <div class="price-body">
                            <p>For most business that want to optimize web queries</p>
                            <ul>
                                <li class="active">Basic listing submission</li>
                                <li class="active">One Listing</li>
                                <li class="active">30 days Availabilty</li>
                                <li class="inactive">Limited Support</li>
                                <li class="inactive">Edit your listing</li>
                            </ul>
                            <div>
                                <a href="{{ url('login') }}" class="btn viewdetails-btn">View details</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Pricing Plan Section -->


    @component('components.scrolltotop')
    @endcomponent
@endsection
