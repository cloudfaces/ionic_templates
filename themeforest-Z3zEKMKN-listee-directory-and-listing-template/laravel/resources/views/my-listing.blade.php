<?php $page = 'my-listing'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            My Listing
        @endslot
        @slot('li_1')
            My Listing
        @endslot
    @endcomponent


    <div class="dashboard-content">
        <div class="container">
            @component('components.nav')
            @endcomponent
            <!-- Dashboard Content -->
            <div class="dash-listingcontent dashboard-info">
                <div class="dash-cards card">
                    <div class="card-header">
                        <h4>My Listings</h4>
                        <a class="nav-link header-login add-listing" href="{{ url('add-listing') }}"><i
                                class="fa-solid fa-plus"></i> Add Listing</a>
                    </div>
                    <div class="card-body">
                        <div class="listing-search">
                            <div class="filter-content form-group">
                                <div class="group-img">
                                    <input type="text" class="form-control" placeholder="Search..." name="search"
                                        id="search">
                                    <i class="feather-search"></i>
                                </div>
                            </div>
                            <div class="sorting-div">
                                <div class="sortbyset">
                                    <span class="sortbytitle">Sort by</span>
                                    <div class="sorting-select">
                                        <select class="form-control select" name="sort" id="sort"
                                            autocomplete="off">
                                            <option>Newest</option>
                                            <option>Newest</option>
                                            <option>Oldest</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="listing-table datatable" id="listdata-table">
                                <thead>
                                    <tr>
                                        <th class="no-sort">Image</th>
                                        <th class="no-sort">Details</th>
                                        <th>Status</th>
                                        <th class="no-sort">Views</th>
                                        <th class="no-sort">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="listingtable-img"> <a href="{{ url('service-details') }}"><img
                                                        class="img-fluid avatar-img"
                                                        src="{{ URL::asset('/assets/img/list/tablelist-2.jpg') }}"
                                                        alt=""></a></div>
                                        </td>
                                        <td>
                                            <h6><a href="{{ url('service-details') }}">Villa 457 sq.m. In Benidorm Fully
                                                    Qquipped House</a></h6>
                                            <div class="listingtable-rate"><a href="javascript:void(0)" class="cat-icon"><i
                                                        class="fa-regular fa-circle-stop"></i> Electronics</a> <span
                                                    class="discount-amt">$350000.00</span><span
                                                    class="fixed-amt">$90000</span>
                                            </div>
                                            <p>Mauris vestibulum lorem a condimentum vulputate.</p>
                                        </td>
                                        <td><span class="status-text">Published</span></td>
                                        <td><Span class="views-count">1523</span></td>
                                        <td>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="action-btn btn-view"><i
                                                        class="feather-eye"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-edit"><i
                                                        class="feather-edit-3"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-trash"><i
                                                        class="feather-trash-2"></i></a>

                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="listingtable-img"><a href="{{ url('service-details') }}"><img
                                                        class="img-fluid avatar-img"
                                                        src="{{ URL::asset('/assets/img/list/tablelist-3.jpg') }}"
                                                        alt=""></a></div>
                                        </td>
                                        <td>
                                            <h6><a href="{{ url('service-details') }}">CDL A OTR Compnay Driver Job-N</a>
                                            </h6>
                                            <div class="listingtable-rate"><a href="javascript:void(0)" class="cat-icon"><i
                                                        class="fa-regular fa-circle-stop"></i> Electronics</a> <span
                                                    class="discount-amt">$550000.00</span><span
                                                    class="fixed-amt">$50000</span>
                                            </div>
                                            <p>Mauris vestibulum lorem a condimentum vulputate.</p>
                                        </td>
                                        <td><span class="status-text">Published</span></td>
                                        <td><Span class="views-count">1673</span></td>
                                        <td>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="action-btn btn-view"><i
                                                        class="feather-eye"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-edit"><i
                                                        class="feather-edit-3"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-trash"><i
                                                        class="feather-trash-2"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="listingtable-img"><a href="{{ url('service-details') }}"><img
                                                        class="img-fluid avatar-img"
                                                        src="{{ URL::asset('/assets/img/list/tablelist-1.jpg') }}"
                                                        alt=""></a></div>
                                        </td>
                                        <td>
                                            <h6><a href="{{ url('service-details') }}">HP Gaming 15.6 Touchscren 12G</a>
                                            </h6>
                                            <div class="listingtable-rate"><a href="javascript:void(0)" class="cat-icon"><i
                                                        class="fa-regular fa-circle-stop"></i> Electronics</a> <span
                                                    class="discount-amt">$480000.00</span><span
                                                    class="fixed-amt">$80000</span>
                                            </div>
                                            <p>Mauris vestibulum lorem a condimentum vulputate.</p>
                                        </td>
                                        <td><span class="status-text">Published</span></td>
                                        <td><Span class="views-count">1827</span></td>
                                        <td>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="action-btn btn-view"><i
                                                        class="feather-eye"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-edit"><i
                                                        class="feather-edit-3"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-trash"><i
                                                        class="feather-trash-2"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="listingtable-img"> <a href="{{ url('service-details') }}"><img
                                                        class="img-fluid avatar-img"
                                                        src="{{ URL::asset('/assets/img/list/tablelist-4.jpg') }}"
                                                        alt=""></a></div>
                                        </td>
                                        <td>
                                            <h6><a href="{{ url('service-details') }}">2012 AudiR8 GT Spider
                                                    Convrtibile</a>
                                            </h6>
                                            <div class="listingtable-rate"><a href="javascript:void(0)"
                                                    class="cat-icon"><i class="fa-regular fa-circle-stop"></i>
                                                    Electronics</a> <span class="discount-amt">$550000.00</span><span
                                                    class="fixed-amt">$40000</span></div>
                                            <p>Mauris vestibulum lorem a condimentum vulputate.</p>
                                        </td>
                                        <td><span class="status-text">Published</span></td>
                                        <td><Span class="views-count">1423</span></td>
                                        <td>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="action-btn btn-view"><i
                                                        class="feather-eye"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-edit"><i
                                                        class="feather-edit-3"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-trash"><i
                                                        class="feather-trash-2"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="listingtable-img"> <a href="{{ url('service-details') }}"><img
                                                        class="img-fluid avatar-img"
                                                        src="{{ URL::asset('/assets/img/list/tablelist-5.jpg') }}"
                                                        alt=""></a></div>
                                        </td>
                                        <td>
                                            <h6><a href="{{ url('service-details') }}">2017 Gulfsteam Ameri-Lite</a></h6>
                                            <div class="listingtable-rate"><a href="javascript:void(0)"
                                                    class="cat-icon"><i class="fa-regular fa-circle-stop"></i>
                                                    Electronics</a> <span class="discount-amt">$359000.00</span><span
                                                    class="fixed-amt">$70000</span></div>
                                            <p>Mauris vestibulum lorem a condimentum vulputate.</p>
                                        </td>
                                        <td><span class="status-text">Published</span></td>
                                        <td><Span class="views-count">1513</span></td>
                                        <td>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="action-btn btn-view"><i
                                                        class="feather-eye"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-edit"><i
                                                        class="feather-edit-3"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-trash"><i
                                                        class="feather-trash-2"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="listingtable-img"> <a href="{{ url('service-details') }}"><img
                                                        class="img-fluid avatar-img"
                                                        src="{{ URL::asset('/assets/img/list/tablelist-6.jpg') }}"
                                                        alt=""></a></div>
                                        </td>
                                        <td>
                                            <h6><a href="{{ url('service-details') }}">Fashion Luxury Men Date</a></h6>
                                            <div class="listingtable-rate"><a href="javascript:void(0)"
                                                    class="cat-icon"><i class="fa-regular fa-circle-stop"></i>
                                                    Electronics</a> <span class="discount-amt">$250000.00</span><span
                                                    class="fixed-amt">$60000</span></div>
                                            <p>Mauris vestibulum lorem a condimentum vulputate.</p>
                                        </td>
                                        <td><span class="status-text">Published</span></td>
                                        <td><Span class="views-count">1729</span></td>
                                        <td>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="action-btn btn-view"><i
                                                        class="feather-eye"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-edit"><i
                                                        class="feather-edit-3"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-trash"><i
                                                        class="feather-trash-2"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="listingtable-img"> <a href="{{ url('service-details') }}"><img
                                                        class="img-fluid avatar-img"
                                                        src="{{ URL::asset('/assets/img/list/tablelist-7.jpg') }}"
                                                        alt=""></a></div>
                                        </td>
                                        <td>
                                            <h6><a href="{{ url('service-details') }}">Apple iPhone 6 16GB 4G LTE </a>
                                            </h6>
                                            <div class="listingtable-rate"><a href="javascript:void(0)"
                                                    class="cat-icon"><i class="fa-regular fa-circle-stop"></i>
                                                    Electronics</a> <span class="discount-amt">$150000.00</span><span
                                                    class="fixed-amt">$40000</span></div>
                                            <p>Mauris vestibulum lorem a condimentum vulputate.</p>
                                        </td>
                                        <td><span class="status-text">Published</span></td>
                                        <td><Span class="views-count">2523</span></td>
                                        <td>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="action-btn btn-view"><i
                                                        class="feather-eye"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-edit"><i
                                                        class="feather-edit-3"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-trash"><i
                                                        class="feather-trash-2"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="listingtable-img"> <a href="{{ url('service-details') }}"><img
                                                        class="img-fluid avatar-img"
                                                        src="{{ URL::asset('/assets/img/list/tablelist-8.jpg') }}"
                                                        alt=""></a></div>
                                        </td>
                                        <td>
                                            <h6><a href="{{ url('service-details') }}">Customized Apple iMac 21.5″ All-In
                                                </a>
                                            </h6>
                                            <div class="listingtable-rate"><a href="javascript:void(0)"
                                                    class="cat-icon"><i class="fa-regular fa-circle-stop"></i>
                                                    Electronics</a> <span class="discount-amt">$156000.00</span><span
                                                    class="fixed-amt">$60000</span></div>
                                            <p>Mauris vestibulum lorem a condimentum vulputate.</p>
                                        </td>
                                        <td><span class="status-text unpublish">Un Published</span></td>
                                        <td><Span class="views-count">1223</span></td>
                                        <td>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="action-btn btn-view"><i
                                                        class="feather-eye"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-edit"><i
                                                        class="feather-edit-3"></i></a>
                                                <a href="javascript:void(0)" class="action-btn btn-trash"><i
                                                        class="feather-trash-2"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @component('components.pagination')
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Dashboard Content -->

    @component('components.scrolltotop')
    @endcomponent
@endsection
