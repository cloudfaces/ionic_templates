<?php $page = 'profile'; ?>
@extends('layout.mainlayout')
@section('content')
    @component('components.breadcrumb')
        @slot('title')
            Profile
        @endslot
        @slot('li_1')
            Profile
        @endslot
    @endcomponent
    <div class="dashboard-content">
        <div class="container">
            @component('components.nav')
            @endcomponent
            <!-- Profile Content -->
            <div class="profile-content">
                <div class="row dashboard-info">
                    <div class="col-lg-9">
                        <div class="card dash-cards">
                            <div class="card-header">
                                <h4>Profile Details</h4>
                            </div>
                            <div class="card-body">
                                <div class="profile-photo">
                                    <div class="profile-img">
                                        <div class="settings-upload-img">
                                            <img src="{{ URL::asset('/assets/img/profile-img.jpg') }}" alt="profile">
                                        </div>
                                        <div class="settings-upload-btn">
                                            <input type="file" accept="image/*" name="image"
                                                class="hide-input image-upload" id="file">
                                            <label for="file" class="file-upload" for="file">Upload New
                                                photo</label>
                                        </div>
                                        <span>Max file size: 10 MB</span>
                                    </div>
                                    <a href="javascript:void(0)" class="profile-img-del"><i class="feather-trash-2"></i></a>
                                </div>
                                <div class="profile-form">
                                    <form>
                                        <div class="form-group">
                                            <label class="col-form-label" for="user">Your Full Name</label>
                                            <div class="pass-group group-img">
                                                <span class="lock-icon"><i class="feather-user"></i></span>
                                                <input type="text" class="form-control" value="John Doe" name="user"
                                                    id="user">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label class="col-form-label" for="phone">Phone Number</label>
                                                    <div class="pass-group group-img">
                                                        <span class="lock-icon"><i class="feather-phone-call"></i></span>
                                                        <input type="tel" class="form-control" value="+44 215346 1223"
                                                            name="phone" id="phone" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label class="col-form-label" for="email">Email Address</label>
                                                    <div class="group-img">
                                                        <i class="feather-mail"></i>
                                                        <input type="text" class="form-control" value="johndoe@email.com"
                                                            name="email" id="email" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label" for="note">Notes</label>
                                            <div class="pass-group group-img">
                                                <textarea rows="4" class="form-control" name="note" id="note">Mauris vestibulum lorem a condimentum vulputate. Integer vitae turpis turpis. Cras at tincidunt urna. Aenean leo justo, congue et felis a, elementum rutrum felis. Fusce rutrum ipsum eu pretium faucibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean leo justo, congue et felis a.  Aenean leo justo, congue et felis a.	</textarea>
                                            </div>
                                        </div>
                                        <div class="row socialmedia-info">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label class="col-form-label" for="facebook">Facebook</label>
                                                    <div class="pass-group group-img">
                                                        <span class="lock-icon"><i class="fab fa-facebook-f"></i></span>
                                                        <input type="text" class="form-control"
                                                            value="https://www.facebook.com/" name="facebook"
                                                            id="facebook">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label class="col-form-label" for="twitter">Twitter</label>
                                                    <div class="pass-group group-img">
                                                        <span class="lock-icon"><i class="fab fa-twitter"></i></span>
                                                        <input type="text" class="form-control"
                                                            value="https://twitter.com/" name="twitter" id="twitter">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row socialmedia-info">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group formlast-input">
                                                    <label class="col-form-label" for="google">Google+</label>
                                                    <div class="pass-group group-img">
                                                        <span class="lock-icon"><i
                                                                class="fa-brands fa-google-plus-g"></i></span>
                                                        <input type="text" class="form-control"
                                                            value="https://www.google.com/" name="google"
                                                            id="google">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group formlast-input">
                                                    <label class="col-form-label" for="instagram">Instagram</label>
                                                    <div class="pass-group group-img">
                                                        <span class="lock-icon"><i class="fab fa-instagram"></i></span>
                                                        <input type="text" class="form-control"
                                                            value="https://www.instagram.com/" name="instagram"
                                                            id="instagram">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="profile-sidebar">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Change Password</h4>
                                </div>
                                <div class="card-body">
                                    <form>
                                        <div class="form-group">
                                            <label class="col-form-label" for="password">Current Password</label>
                                            <div class="pass-group group-img">
                                                <span class="lock-icon"><i class="feather-lock"></i></span>
                                                <input type="password" class="form-control pass-input"
                                                    placeholder="Password" name="password" id="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label" for="new-password">New Password</label>
                                            <div class="pass-group group-img">
                                                <i class="feather-lock"></i>
                                                <input type="password" class="form-control pass-input"
                                                    placeholder="Password" name="new-password" id="new-password"
                                                    value="12345">
                                                <span class="toggle-password feather-eye-off"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label" for="confirm-password">Confirm New
                                                Password</label>
                                            <div class="pass-group group-img">
                                                <i class="feather-lock"></i>
                                                <input type="password" class="form-control pass-new-input"
                                                    placeholder="Password" name="confirm-password" id="confirm-password"
                                                    value="12345">
                                                <span class="toggle-password1 feather-eye-off"></span>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary" type="submit"> Change Password</button>
                                    </form>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Profile Content -->

    @component('components.scrolltotop')
    @endcomponent
@endsection
