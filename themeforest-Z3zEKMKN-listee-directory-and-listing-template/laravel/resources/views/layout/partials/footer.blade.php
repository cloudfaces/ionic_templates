<!-- Footer -->
@if (!Route::is(['index-2', 'index-3', 'index-4', 'index-5', 'index-6', 'index-7', 'index-8', 'index-9']))
    <footer class="footer">
        <div class="container">
            <div class="stay-tuned">
                <h3>Stay Tuned With Us</h3>
                <p>Subcribe to our newletter and never miss our latest news and promotions. Our newsletter is sent
                    once
                    a
                    week, every thursday.</p>
                <form>
                    <div class="form-group">
                        <div class="group-img">
                            <i class="feather-mail"></i>
                            <input type="text" class="form-control" name="footer-email" id="footer-email"
                                placeholder="Enter Email Address">
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit"> Subscribe</button>
                </form>
            </div>
        </div>

        <!-- Footer Top -->

        <div class="footer-top aos" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget">
                            <div class="footer-logo">
                                <a href="#"><img src="{{ URL::asset('/assets/img/footerlogo.svg') }}"
                                        alt="logo"></a>
                            </div>
                            <div class="footer-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt
                                    et magna aliqua. </p>
                            </div>
                            <div class="social-icon">
                                <ul>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">About us</h2>
                            <ul>
                                <li>
                                    <a href="{{ url('about') }}">Our product</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Documentation</a>
                                </li>
                                <li>
                                    <a href="{{ url('service-details') }}">Our Services</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Get Started Us</a>
                                </li>
                                <li>
                                    <a href="{{ url('contact') }}">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Quick links</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Market Place</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Documentation</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Customers</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Carriers</a>
                                </li>
                                <li>
                                    <a href="{{ url('blog-list') }}">Our Blog</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Top Cities</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Manhatten</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Los Angeles</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Houston</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Chicago</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Alabama</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget">
                            <h2 class="footer-title">Communication</h2>
                            <div class="footer-contact-info">
                                <div class="footer-address">
                                    <img src="{{ URL::asset('/assets/img/call-calling.svg') }}" alt="Callus">
                                    <p><span>Call Us</span> <br> +017 123 456 78 </p>
                                </div>
                                <div class="footer-address">
                                    <img src="{{ URL::asset('/assets/img/sms-tracking.svg') }}" alt="Callus">
                                    <p><span>Send Message</span> <br> listee@example.com </p>
                                </div>
                            </div>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                </div>

                <!-- Footer Counter Section-->
                <div class="footercount">
                    <div class="row">
                        <div class="col-lg-3 col-md-3">
                            <div class="vistors-details">
                                <p>Our Unique Visitor</p>
                                <p class="visitors-value">25,329,532</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="vistors-details">
                                <p>Our Unique Visitor</p>
                                <p class="visitors-value">25,329,53264546</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="vistors-details">
                                <p>Our Unique Visitor</p>
                                <p class="visitors-value">25,329,53264546</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="vistors-details">
                                <p>We Accept</p>
                                <ul class="d-flex">
                                    <li><a href="javascript:void(0)"><img class="img-fluid"
                                                src="{{ URL::asset('/assets/img/amex-pay.svg') }}" alt="amex"></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><img class="img-fluid"
                                                src="{{ URL::asset('/assets/img/apple-pay.svg') }}" alt="pay"></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><img class="img-fluid"
                                                src="{{ URL::asset('/assets/img/gpay.svg') }}" alt="gpay"></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><img class="img-fluid"
                                                src="{{ URL::asset('/assets/img/master.svg') }}" alt="paycard"></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><img class="img-fluid"
                                                src="{{ URL::asset('/assets/img/phone.svg') }}" alt="spay"></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><img class="img-fluid"
                                                src="{{ URL::asset('/assets/img/visa.svg') }}" alt="visa"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Footer  Counter Section-->

            </div>
        </div>
        <!-- /Footer Top -->

        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <!-- Copyright -->
                <div class="copyright">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="copyright-text">
                                <p class="mb-0">All Copyrights Reserved &copy;
                                    <script>
                                        document.write(new Date().getFullYear())
                                    </script> - Listee.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- Copyright Menu -->
                            <div class="copyright-menu">
                                <ul class="policy-menu">
                                    <li>
                                        <a href="{{ url('privacy-policy') }}">Privacy </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('faq') }}">Faq </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('terms-condition') }}">Terms</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /Copyright Menu -->
                        </div>
                    </div>
                </div>
                <!-- /Copyright -->
            </div>
        </div>
        <!-- /Footer Bottom -->

    </footer>
    <!-- /Footer -->
@endif
@if (Route::is(['index-2']))
    <footer class="footer footer-two">
        <div class="footer-social">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h6>Would you like to connect with us</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="social-icon">
                            <ul>
                                <li>
                                    <a href="#" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Top -->
        <div class="footer-top aos" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget">
                            <div class="footer-logo">
                                <a href="{{ url('index') }}"><img
                                        src="{{ URL::asset('/assets/img/footerlogo.svg') }}" alt="logo"></a>
                            </div>
                            <div class="footer-content">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s, when
                                    an
                                    unknown printer took a galley of type and scrambled</p>
                            </div>
                            <div class="update-form">
                                <form action="#">
                                    <span><i class="feather-mail"></i></span>
                                    <input type="email" class="form-control" name="footer-email" id="footer-email"
                                        placeholder="Enter You Email Here">
                                    <button type="submit" class="btn btn-primary">Subscribe</button>
                                </form>
                            </div>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">About us</h2>
                            <ul>
                                <li>
                                    <a href="{{ url('about') }}">Our product</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Documentation</a>
                                </li>
                                <li>
                                    <a href="{{ url('service-details') }}">Our Services</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Get Started Us</a>
                                </li>
                                <li>
                                    <a href="{{ url('contact') }}">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Quick links</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Market Place</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Documentation</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Customers</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Carriers</a>
                                </li>
                                <li>
                                    <a href="{{ url('blog-list') }}">Our Blog</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Top Cities</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Manhatten</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Los Angeles</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Houston</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Chicago</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Alabama</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget">
                            <h2 class="footer-title">Communication</h2>
                            <div class="footer-contact-info">
                                <div class="footer-address">
                                    <img src="{{ URL::asset('/assets/img/call-calling.svg') }}" alt="Callus">
                                    <p><span>Call Us</span> <br> +017 123 456 78 </p>
                                </div>
                                <div class="footer-address">
                                    <img src="{{ URL::asset('/assets/img/sms-tracking.svg') }}" alt="Callus">
                                    <p><span>Send Message</span> <br> listee@example.com </p>
                                </div>
                            </div>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                </div>

                <!-- Footer Counter Section-->
                <div class="footercount">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="vistors-details">
                                <ul class="d-flex">
                                    <li><a href="javascript:void(0)"><img class="img-fluid"
                                                src="{{ URL::asset('/assets/img/icons/visa-01.svg') }}"
                                                alt="visa"></a></li>
                                    <li><a href="javascript:void(0)"><img class="img-fluid"
                                                src="{{ URL::asset('/assets/img/icons/master-01.svg') }}"
                                                alt="paycard"></a></li>
                                    <li><a href="javascript:void(0)"><img class="img-fluid"
                                                src="{{ URL::asset('/assets/img/icons/discover.svg') }}"
                                                alt="spay"></a></li>
                                    <li><a href="javascript:void(0)"><img class="img-fluid"
                                                src="{{ URL::asset('/assets/img/icons/stripe.svg') }}"
                                                alt="spay"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="selection-list ">
                                <div class="lang-select">
                                    <span class="select-icon"><i class="feather-globe"></i></span>
                                    <select class="select" name="country" id="country1" autocomplete="off">
                                        <option>India</option>
                                        <option>UK</option>
                                        <option>Japan</option>
                                    </select>
                                </div>
                                <div class="currency-select">
                                    <select class="select" name="currency" id="currency1" autocomplete="off">
                                        <option>USD</option>
                                        <option>Euro</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /Footer  Counter Section-->

            </div>
        </div>
        <!-- /Footer Top -->

        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <!-- Copyright -->
                <div class="copyright">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright-text text-center">
                                <p class="mb-0">All Copyrights Reserved &copy;
                                    <script>
                                        document.write(new Date().getFullYear())
                                    </script> - Listee.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Copyright -->
            </div>
        </div>
        <!-- /Footer Bottom -->

    </footer>
    <!-- /Footer -->
@endif
@if (Route::is(['index-3']))
    <!-- Footer Banner -->
    <footer class="section-blk footer-blk footer-three">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="top-foo-widget">
                        <div class="help-info">
                            <p>Toll Free Customer Care +(1) 123 456 7890</p>
                            <p>Need live support? listee@example.com</p>
                        </div>
                        <div class="social-info">
                            <ul class="d-flex">
                                <li>
                                    <a href="javscript:;"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a href="javscript:;"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="javscript:;"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                                <li>
                                    <a href="javscript:;"><i class="fab fa-youtube"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="middle-foo-widget">
                        <div class="row sm-row-gap-23">
                            <div class="col-md-7">
                                <div class="links-blk">
                                    <div class="row sm-row-gap-23">
                                        <div class="col-auto col-md-3">
                                            <div class="col-style">
                                                <h5>About us</h5>
                                                <ul>
                                                    <li>
                                                        <a href="{{ url('about') }}">Our product</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Documentation</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ url('service-details') }}">Our Services</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Get Started Us</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ url('contact') }}">Contact Us</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-auto col-md-3">
                                            <div class="col-style">
                                                <h5>Quick links</h5>
                                                <ul>
                                                    <li>
                                                        <a href="javascript:void(0)">Market Place</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Documentation</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Customers</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Carriers</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ url('blog-list') }}">Our Blog</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-auto col-md-3">
                                            <div class="col-style">
                                                <h5>Services</h5>
                                                <ul>
                                                    <li><a href="{{ url('service-details') }}">Hotel</a></li>
                                                    <li><a href="{{ url('service-details') }}">Activity Finder</a>
                                                    </li>
                                                    <li><a href="{{ url('service-details') }}">Flight finder</a></li>
                                                    <li><a href="{{ url('service-details') }}">Holiday Rental</a></li>
                                                    <li><a href="{{ url('service-details') }}">Travel Agents</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-auto col-md-3">
                                            <div class="col-style">
                                                <h5>Destinations</h5>
                                                <ul>
                                                    <li>
                                                        <a href="javascript:void(0)">Manhatten</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Los Angeles</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Houston</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Chicago</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Alabama</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="drop-blk">
                                    <h5>Gallery</h5>
                                    <ul class="gallery-blk d-flex align-items-center">
                                        <li>
                                            <a href="{{ URL::asset('/assets/img/gallery/galleryimg-1.jpg') }}"
                                                data-fancybox="gallery1">
                                                <img src="{{ URL::asset('/assets/img/gallery/galleryimg-1.jpg') }}"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::asset('/assets/img/gallery/galleryimg-2.jpg') }}"
                                                data-fancybox="gallery1">
                                                <img src="{{ URL::asset('/assets/img/gallery/galleryimg-2.jpg') }}"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::asset('/assets/img/gallery/galleryimg-9.jpg') }}"
                                                data-fancybox="gallery1">
                                                <img src="{{ URL::asset('/assets/img/gallery/galleryimg-9.jpg') }}"
                                                    alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::asset('/assets/img/gallery/galleryimg-10.jpg') }}"
                                                data-fancybox="gallery1">
                                                <img src="{{ URL::asset('/assets/img/gallery/galleryimg-10.jpg') }}"
                                                    alt="">
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="navbar-nav selection-list mt-4">
                                        <li class="nav-item dropdown">
                                            <div class="lang-select">
                                                <span class="select-icon"><i class="feather-globe"></i></span>
                                                <select class="select" name="country" id="country"
                                                    autocomplete="off">
                                                    <option>India</option>
                                                    <option>UK</option>
                                                    <option>Japan</option>
                                                </select>
                                            </div>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <div class="lang-select">
                                                <span class="select-icon"><i class="fa-solid fa-coins"></i></span>
                                                <select class="select" name="currency" id="currency"
                                                    autocomplete="off">
                                                    <option>Euro</option>
                                                    <option>USD</option>
                                                </select>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btm-foo-widget">
                        <div class="copy-info">
                            <p>Copyright ©
                                <script>
                                    document.write(new Date().getFullYear())
                                </script> Listee. All rights are reserved.
                            </p>
                        </div>
                        <div class="payment-info">
                            <ul class="d-flex">
                                <li>
                                    <img class="img-fluid" src="{{ URL::asset('/assets/img/icons/visa-01.svg') }}"
                                        alt="visa">
                                </li>
                                <li>
                                    <img class="img-fluid" src="{{ URL::asset('/assets/img/icons/master-01.svg') }}"
                                        alt="paycard">
                                </li>
                                <li>
                                    <img class="img-fluid" src="{{ URL::asset('/assets/img/icons/discover.svg') }}"
                                        alt="spay">
                                </li>
                                <li>
                                    <img class="img-fluid" src="{{ URL::asset('/assets/img/icons/stripe.svg') }}"
                                        alt="spay">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endif

@if (Route::is(['index-4']))
    <!-- Footer -->
    <footer class="footer footer-four">

        <!-- Footer Top -->
        <div class="footer-top aos" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget">
                            <div class="footer-logo">
                                <a href="#"><img src="{{ URL::asset('/assets/img/logo.png') }}"
                                        alt="logo"></a>
                            </div>
                            <div class="footer-content">
                                <p>Discover amazing things to do everywhere you go.</p>
                            </div>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">About us</h2>
                            <ul>
                                <li>
                                    <a href="{{ url('about') }}">Our product</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Documentation</a>
                                </li>
                                <li>
                                    <a href="{{ url('service-details') }}">Our Services</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Get Started Us</a>
                                </li>
                                <li>
                                    <a href="{{ url('contact') }}">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Quick links</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Market Place</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Documentation</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Customers</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Carriers</a>
                                </li>
                                <li>
                                    <a href="{{ url('blog-list') }}">Our Blog</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget">
                            <h2 class="footer-title">Communication</h2>
                            <div class="footer-contact-info">
                                <ul>
                                    <li>Email: listee@example.com</li>
                                    <li>Phone: 1 (00) 832 2342</li>
                                </ul>
                            </div>

                            <div class="social-icon">
                                <ul>
                                    <li>
                                        <a href="#" target="_blank"><i class="fa-brands fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fa-brands fa-linkedin"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /Footer Top -->

        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <!-- Copyright -->
                <div class="copyright">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright-text text-center">
                                <p class="mb-0">
                                    <script>
                                        document.write(new Date().getFullYear())
                                    </script> &copy; Listee. All rights reserved.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Copyright -->
            </div>
        </div>
        <!-- /Footer Bottom -->

    </footer>
    <!-- /Footer -->
@endif

@if (Route::is(['index-5']))
    <!-- Footer -->
    <footer class="footer footer-five">

        <!-- Footer Top -->
        <div class="footer-top aos" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget">
                            <div class="footer-logo">
                                <a href="#"><img src="{{ URL::asset('/assets/img/footerlogo.svg') }}"
                                        alt="logo"></a>
                            </div>
                            <div class="footer-content">
                                <p>Lörem ipsum od ohet dilogi. Bell trabel, samuligt, ohöbel utom diska. Jinesade bel
                                    när feras redorade i belogi. FAR paratyp i muvåning, och pesask vyfisat. </p>
                            </div>
                            <div class="social-icon">
                                <ul>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Pages</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Market Place</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Documentaion</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Customers</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Carrers</a>
                                </li>
                                <li>
                                    <a href="{{ url('blog-list') }}">Our Blog</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Categories</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Restaurant</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Beauty</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Fitness</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Night Life</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Shopping</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Top Cities</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Manhattan</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Los Angeles</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Houston</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Chicago</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Alabama</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget">
                            <h2 class="footer-title">Communication</h2>
                            <div class="footer-contact-info">
                                <div class="footer-address">
                                    <i class="feather-phone me-2"></i>
                                    <p> (406) 555-0120 </p>
                                </div>
                                <div class="footer-address">
                                    <i class="feather-mail me-2"></i>
                                    <p> listee@example.com </p>
                                </div>
                                <div class="footer-address">
                                    <i class="feather-map-pin me-2"></i>
                                    <p> 2972 Westheimer Rd. Santa Ana, Illinois 85486 </p>
                                </div>
                            </div>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /Footer Top -->

        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <!-- Copyright -->
                <div class="copyright">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright-text text-center">
                                <p class="mb-0">
                                    <script>
                                        document.write(new Date().getFullYear())
                                    </script> &copy; Listee. All rights reserved.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Copyright -->
            </div>
        </div>
        <!-- /Footer Bottom -->

    </footer>
    <!-- /Footer -->
@endif

@if (Route::is(['index-6']))
    <!-- Footer -->
    <section class="footer-six common-padding">
        <div class="footer-six-bg">
            <img src="{{ URL::asset('/assets/img/footer-six-bg.png') }}" alt="">
        </div>
        <div class="container">
            <div class="footer-six-top">
                <div class="row">
                    <div class="col-lg-2 col-md-6 col-sm-12">
                        <div class="footer-six-top-list">
                            <h4>Network</h4>
                            <ul>
                                <li><a href="javascript:void(0);">Browse by Make</a></li>
                                <li><a href="javascript:void(0);">Add Car</a></li>
                                <li><a href="javascript:void(0);">Sitemap</a></li>
                                <li><a href="javascript:void(0);">My Favourite</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-12">
                        <div class="footer-six-top-list">
                            <h4>Company</h4>
                            <ul>
                                <li><a href="{{ url('about') }}">About us</a></li>
                                <li><a href="{{ url('contact') }}">Contact</a></li>
                                <li><a href="{{ url('contact') }}">Testimonials</a></li>
                                <li><a href="{{ url('blog-grid') }}">Blog</a></li>
                                <li><a href="{{ url('signup') }}">Become a dealer</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-12">
                        <div class="footer-six-top-list">
                            <h4>Help Center</h4>
                            <ul>
                                <li><a href="{{ url('faq') }}">Faq’s</a></li>
                                <li><a href="{{ url('pricing') }}">Pricing Plan</a></li>
                                <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                                <li><a href="{{ url('terms-condition') }}">Terms & Conditions</a></li>
                                <li><a href="{{ url('howitworks') }}">How it works</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="footer-six-right">
                            <div class="footer-send-mail">
                                <input type="text" class="form-control" name="footer-email" id="footer-email"
                                    placeholder="Subscribe to Get Updates">
                                <a href="javascript:void(0);">Send mail</a>
                            </div>
                        </div>
                        <div class="social-icon-six">
                            <h3>Follow us on</h3>
                            <ul>
                                <li>
                                    <a href="javascript:void(0);" target="_blank"><i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" target="_blank"><i class="fab fa-twitter"></i> </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" target="_blank"><i
                                            class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" target="_blank"><i
                                            class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-six-center">
                <div class="footer-six-center-content">
                    <h6>Our Total Visitors</h6>
                    <a href="javascript:void(0);">4,56,6597</a>
                </div>
                <div class="footer-six-center-content">
                    <h6>Need live support?</h6>
                    <a href="javascript:void(0);">listee@example.com</a>
                </div>
                <div class="footer-six-center-content">
                    <h6>Toll Free Customer Care</h6>
                    <a href="javascript:void(0);">+91 26447 99875</a>
                </div>
                <div class="footer-six-center-list">
                    <ul>
                        <li><a href="{{ url('index-6') }}"> Home</a></li>
                        <li><a href="javascript:void(0);"> Site Map</a></li>
                        <li><a href="{{ url('privacy-policy') }}"> Privacy policy</a></li>
                        <li><a href="{{ url('privacy-policy') }}"> Cookie Policy</a></li>
                    </ul>
                </div>
            </div>
            <hr class="custom-line mb-0">
            <div class="footer-six-bottom">
                <p>©
                    <script>
                        document.write(new Date().getFullYear())
                    </script> Listee. All Rights Reserved.
                </p>
            </div>
        </div>
    </section>
    <!-- /Footer -->
@endif

@if (Route::is(['index-7']))
    <!-- Footer -->
    <footer class="footer footer-seven">
        <div class="container footer-container">
            <div class="stay-tuned">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="footer-left-part pull-left">
                            <h4>Newsletter</h4>
                            <p>Be the first one to know about discounts, offers and events</p>

                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="footer-right-part">
                            <form>
                                <div class="form-group">
                                    <div class="group-img">
                                        <i class="feather-mail"></i>
                                        <input type="text" class="form-control" name="footer-email"
                                            id="footer-email" placeholder="Enter your email">
                                        <button class="btn btn-primary" type="submit"> Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Top -->
        <div class="footer-top aos" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">About</h2>
                            <ul>
                                <li>
                                    <a href="{{ url('about') }}">About us</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Pages</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Listing</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Category</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">In Press</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Support</h2>
                            <ul>
                                <li>
                                    <a href="{{ url('contact') }}">Contact us</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Online Chat</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Whatsapp</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Telegram</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Ticketing</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">FAQ</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Account</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Manage Deliveries</a>
                                </li>
                                <li>
                                    <a href="{{ url('service-details') }}">Orders</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Payments</a>
                                </li>
                                <li>
                                    <a href="{{ url('contact') }}">Returns</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Category</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Broast</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Pizza </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Chicken</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Burgers</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Sandwiches</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Location</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">San Francisco
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"> London</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Tokyo</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Miami </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Los Angeles</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget">
                            <h2 class="footer-title">Contact Us </h2>
                            <div class="footer-contact-info">
                                <div class="footer-address">
                                    <p class="footer-icon"><i class="fa-solid fa-phone"></i></p>
                                    <p> (406) 555-0120 </p>
                                </div>
                                <div class="footer-address">
                                    <p class="footer-icon"><i class="fa-solid fa-envelope"></i></p>
                                    <p> listee@example.com</p>
                                </div>
                                <div class="footer-address">
                                    <p class="footer-icon"><i class="fa-solid fa-location-dot"></i></p>
                                    <p> 2972 Westheimer Rd. Santa Ana, Illinois 85486</p>
                                </div>
                            </div>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                </div>

                <!-- Footer Counter Section-->
                <div class="footercount">
                    <div class="row d-flex align-items-center">
                        <div class="col-lg-4 col-md-6">
                            <div class="vistors-details">
                                <img src="{{ URL::asset('/assets/img/logo-new.png') }}" alt="img">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 ">
                            <div class="vistors-details text-lg-center">
                                <div class="social-icon">
                                    <ul>
                                        <li>
                                            <a href="#" target="_blank"><i class="fab fa-facebook-square"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank"><i class="fab fa-youtube"></i> </a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank"><i class="fab fa-dribbble"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank"><i class="fab fa-figma"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank"><i class="fab fa-whatsapp"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 ">
                            <div class="vistors-details text-lg-end">
                                <p>©
                                    <script>
                                        document.write(new Date().getFullYear())
                                    </script>, All Rights Reserved
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Footer  Counter Section-->

            </div>
        </div>
        <!-- /Footer Top -->

    </footer>
    <!-- /Footer -->
@endif

@if (Route::is(['index-8']))
    <!-- Footer -->
    <footer class="footer footer-eight">
        <!-- Footer Top -->
        <div class="footer-top aos aos-init aos-animate" data-aos="fade-up">
            <div class="container">
                <!-- /Footer  Counter Section-->
                <div class="row">
                    <div class="col-lg-2 col-md-6 aos" data-aos="fade-up">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Company</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">About Us</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Careers</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Press</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">News</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Media Kit</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Contact</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6 aos" data-aos="fade-up">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Product</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Overview</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Features</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Solutions</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Tutorials</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Pricing</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Releases</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6 aos" data-aos="fade-up">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Resources</h2>
                            <ul>
                                <li>
                                    <a href="{{ url('blog-details') }}">Blog</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Newsletter</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Events</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Help Centre</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Tutorials</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Support</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6 aos" data-aos="fade-up">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Social</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Twitter</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">LinkedIn </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Facebook</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">GitHub</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">AngelList</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Dribble</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-4 col-md-6 aos" data-aos="fade-up">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h2 class="footer-title">Subscribe</h2>
                            <p>Subscribe to stay tuned for new web design and latest updates. Let's do it!</p>
                            <div class="update-form">
                                <form action="#">
                                    <input type="email" class="form-control" name="footer-email" id="footer-email"
                                        placeholder="Enter you email">
                                    <button type="submit" class="btn btn-primary">Subscribe</button>
                                </form>
                            </div>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="container">
                        <!-- Copyright -->
                        <div class="copyright">
                            <div class="row">
                                <div class="col-md-6 aos" data-aos="fade-up">
                                    <div class="copyright-text">
                                        <p class="mb-0"> ©
                                            <script>
                                                document.write(new Date().getFullYear())
                                            </script> Listee. All Rights Reserved.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6 aos" data-aos="fade-up">
                                    <!-- Copyright Menu -->
                                    <div class="copyright-menu">
                                        <ul class="policy-menu">
                                            <li>
                                                <a href="{{ url('about') }}">About Us </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">Careers </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">Press </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">News </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">Media Kit </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);">Contact</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /Copyright Menu -->
                                </div>
                            </div>
                        </div>
                        <!-- /Copyright -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /Footer Top -->
    </footer>
    <!-- /Footer -->
@endif

@if (Route::is(['index-9']))
    <!-- Footer -->
    <section class="footer-six footer-nine common-padding">
        <div class="footer-six-bg footer-nine-bg">
            <img src="{{ URL::asset('/assets/img/bg/footer-bg.png') }}" alt="">
        </div>
        <div class="container">
            <div class="footer-six-top footer-nine-top">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-12">
                        <div class="footer-six-right">
                            <div class="footer-send-mail">
                                <input type="text" class="form-control" name="footer-email" id="footer-email"
                                    placeholder="Subscribe to Get Updates">
                                <a href="javascript:void(0);">Send mail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="footer-six-center foot-nine-list">
                            <div class="footer-six-center-content">
                                <h6>Our Total Visitors</h6>
                                <a href="javascript:void(0);">4,56,6597</a>
                            </div>
                            <div class="footer-six-center-content">
                                <h6>Need live support?</h6>
                                <a href="javascript:void(0);">listee@example.com</a>
                            </div>
                            <div class="footer-six-center-content">
                                <h6>Toll Free Customer Care</h6>
                                <a href="javascript:void(0);">+91 26447 99875</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" row position-relative">
                <div class="col-lg-4 col-md-6  col-sm-12">
                    <div class="foot-nine-logo">
                        <a href="{{ url('index-9') }}"><img
                                src="{{ URL::asset('/assets/img/footer-logo-nine.svg') }}" alt=""></a>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of
                            classical Latin literature from 45 BC, making it over 2000 years old. Richard...</p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <div class="footer-six-top-list">
                        <h4>Discover</h4>
                        <ul>
                            <li><a href="javascript:void(0);">Chicago</a></li>
                            <li><a href="javascript:void(0);">Los Angeles</a></li>
                            <li><a href="javascript:void(0);">Miami</a></li>
                            <li><a href="javascript:void(0);">New York</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <div class="footer-six-top-list">
                        <h4>Lists by Category</h4>
                        <ul>
                            <li><a href="javascript:void(0);">Apartments</a></li>
                            <li><a href="javascript:void(0);">Condos</a></li>
                            <li><a href="javascript:void(0);">Houses</a></li>
                            <li><a href="javascript:void(0);">Offices</a></li>
                            <li><a href="javascript:void(0);">Retail</a></li>
                            <li><a href="javascript:void(0);">Villas</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <div class="footer-six-top-list">
                        <h4>Quick Links</h4>
                        <ul>
                            <li><a href="{{ url('about') }}">About Us</a></li>
                            <li><a href="{{ url('terms-condition') }}">Terms & Conditions</a></li>
                            <li><a href="{{ url('privacy-policy') }}">User’s Guide</a></li>
                            <li><a href="{{ url('contact') }}">Support Center</a></li>
                            <li><a href="{{ url('about') }}">Press Info</a></li>
                            <li><a href="{{ url('contact') }}">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <div class="social-icon-six social-icon-nine">
                        <h3>Follow us on</h3>
                        <ul>
                            <li>
                                <a href="javascript:void(0);" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" target="_blank"><i class="fab fa-twitter"></i> </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" target="_blank"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr class="custom-line foot-nine-line">
            <div class="footer-six-bottom pt-0">
                <p class="mb-0">©
                    <script>
                        document.write(new Date().getFullYear())
                    </script> Listee. All Rights Reserved.
                </p>
                <div class="footer-six-center-list">
                    <ul>
                        <li><a href="{{ url('index-9') }}"> Home</a></li>
                        <li><a href="javascript:void(0);"> Site Map</a></li>
                        <li><a href="{{ url('privacy-policy') }}"> Privacy policy</a></li>
                        <li><a href="{{ url('privacy-policy') }}"> Cookie Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- /Footer -->
@endif
