<!-- jQuery -->
<script src="{{ URL::asset('/assets/js/jquery-3.7.1.min.js') }}"></script>

<!-- Bootstrap Core JS -->
<script src="{{ URL::asset('/assets/js/bootstrap.bundle.min.js') }}"></script>

<!-- Fearther JS -->
<script src="{{ URL::asset('/assets/js/feather.min.js') }}"></script>

<!-- Aos -->
<script src="{{ URL::asset('/assets/plugins/aos/aos.js') }}"></script>

@if (Route::is([
        'index-2',
        'index-3',
        'index-4',
        'index-5',
        'index-6',
        'index-7',
        'index-8',
        'index-9',
        'about',
        'index',
        'listing-grid-sidebar',
        'listing-list-sidebar',
    ]))
    <!-- Owl Carousel JS -->
    <script src="{{ URL::asset('/assets/js/owl.carousel.min.js') }}"></script>
@endif

@if (Route::is(['index-3', 'index-6', 'index-9']))
    <!-- Datetimepicker JS -->
    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
@endif

@if (Route::is([
        'index',
        'service-details',
        'reviews',
        'profile',
        'privacy-policy',
        'my-listing',
        'messages',
        'listingmap-list',
        'listingmap-grid',
        'listing-list-sidebar',
        'gallery',
        'forget-password',
        'dashboard',
        'contact',
        'categories',
        'bookmarks',
        'blog-list',
        'add-listing',
        'index-2',
        'index-3',
        'index-4',
        'index-5',
        'index-6',
        'index-7',
        'index-8',
        'index-9',
        'listing-grid-sidebar',
        'forgot-password',
    ]))
    <!-- Select2 JS -->
    <script src="{{ URL::asset('/assets/plugins/select2/js/select2.min.js') }}"></script>
@endif
@if (Route::is([
        'service-details',
        'listing-list-sidebar',
        'listing-grid-sidebar',
        'blog-list',
        'blog-list-sidebar',
        'blog-grid-sidebar',
    ]))
    <!-- Sticky Sidebar JS -->
    <script src="{{ URL::asset('/assets/plugins/theia-sticky-sidebar/ResizeSensor.js') }}"></script>
    <script src="{{ URL::asset('/assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js') }}"></script>
@endif

@if (Route::is(['service-details', 'gallery', 'index-2', 'index-3','reviews']))
    <!-- Fancybox JS -->
    <script src="{{ URL::asset('/assets/plugins/fancybox/jquery.fancybox.min.js') }}"></script>
@endif

@if (Route::is(['listingmap-list', 'listingmap-grid']))
    <!-- Custom JS -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6adZVdzTvBpE2yBRK8cDfsss8QXChK0I"></script>
    <script src="{{ URL::asset('/assets/js/map.js') }}"></script>
@endif
@if (Route::is(['listingmap-list', 'listingmap-grid', 'listing-list-sidebar', 'listing-grid-sidebar']))
    <!-- Rangeslider JS -->
    <script src="{{ URL::asset('/assets/plugins/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/plugins/ion-rangeslider/js/custom-rangeslider.js') }}"></script>
@endif

@if (Route::is(['my-listing']))
    <!-- Datatables JS -->
    <script src="{{ URL::asset('/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/plugins/datatables/datatables.min.js') }}"></script>
@endif

@if (Route::is(['listing-grid-sidebar']))
    <!-- Rangeslider JS -->
    <script src="{{ URL::asset('/assets/js/rangeslider.min.js') }}"></script>
@endif

@if (Route::is(['dashboard']))
    <!-- Apex Chart -->
    <script src="{{ URL::asset('/assets/plugins/apex/apexcharts.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/plugins/apex/chart-data.js') }}"></script>
@endif
@if (Route::is(['index-8', 'index-9']))
    <!-- counterup JS -->
    <script src="assets/js/jquery.waypoints.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
@endif
@if (!Route::is(['listingmap-grid', 'listingmap-list', 'index-3', 'error-404', 'error-500']))
    <!-- Top JS -->
    <script src="{{ URL::asset('/assets/js/backToTop.js') }}"></script>
@endif

{{-- @livewireScripts
@livewireScriptConfig --}}

<!-- Custom JS -->
<script src="{{ URL::asset('/assets/js/script.js') }}"></script>
