<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}">

<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{ url('assets/plugins/fontawesome/css/fontawesome.min.css') }}">
<link rel="stylesheet" href="{{ url('assets/plugins/fontawesome/css/all.min.css') }}">

<!-- Fearther CSS -->
<link rel="stylesheet" href="{{ url('assets/css/feather.css') }}">
<!-- Aos CSS -->
<link rel="stylesheet" href="{{ url('assets/plugins/aos/aos.css') }}">

@if (Route::is(['index-3']))
    <!-- Material Icon CSS -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
@endif
@if (Route::is(['index-3', 'index-6', 'index-9']))
    <!-- Datetimepicker CSS -->
    <link rel="stylesheet" href="{{ url('assets/css/bootstrap-datetimepicker.min.css') }}">
@endif
@if (Route::is([
        'index-2',
        'index-3',
        'index-4',
        'index-5',
        'index-6',
        'index-7',
        'index-8',
        'index-9',
        'about',
        'contact',
        'index',
        'listing-grid-sidebar',
        'listing-list-sidebar',
    ]))
    <!-- Owl carousel CSS -->
    <link rel="stylesheet" href="{{ url('assets/css/owl.carousel.min.css') }}">
@endif

@if (Route::is([
        'index-2',
        'index-3',
        'index-4',
        'index-5',
        'index-6',
        'index-7',
        'index-8',
        'index-9',
        'add-listing',
        'blog-details',
        'index',
        'contact',
        'listing-grid-sidebar',
        'listing-list-sidebar',
        'listingmap-grid',
        'listingmap-list',
        'messages',
        'my-listing',
        'profile',
        'reviews',
        'service-details',
    ]))
    <!-- Select2 CSS -->
    <link rel="stylesheet" href="{{ url('assets/plugins/select2/css/select2.min.css') }}">
@endif

@if (Route::is(['index-2', 'index-3', 'blog-details', 'gallery', 'service-details','reviews']))
    <!-- Fancybox CSS -->
    <link rel="stylesheet" href="{{ url('assets/plugins/fancybox/jquery.fancybox.min.css') }}">
@endif

@if (Route::is(['listing-grid-sidebar', 'listing-list-sidebar', 'contact', 'listingmap-grid', 'listingmap-list']))
    <!-- Rangeslider CSS -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/plugins/ion-rangeslider/css/ion.rangeSlider.min.css') }}">
@endif

@if (Route::is(['dashboard', 'index-3', 'index-8']))
    <!-- Owl carousel CSS -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/plugins/apex/apexcharts.css') }}">
@endif

@if (Route::is(['my-listing']))
    <!-- Datatables CSS -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/plugins/datatables/datatables.min.css') }}">
@endif

<!-- Main CSS -->
<link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
