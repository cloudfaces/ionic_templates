@if (!Route::is(['index-2', 'index-3', 'index-4', 'index-5', 'index-6', 'index-7', 'index-8', 'index-9']))
    <header class="header">
@endif
@if (Route::is(['index-2']))
    <header class="header header-two">
        <div class="header-top">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-4">
                        <div class="selection-list">
                            <div class="lang-select">
                                <span class="select-icon"><i class="feather-globe"></i></span>
                                <select class="select" name="country" id="country" autocomplete="off">
                                    <option>India</option>
                                    <option>UK</option>
                                    <option>Japan</option>
                                </select>
                            </div>
                            <div class="currency-select">
                                <select class="select" name="currency" name="currency" autocomplete="off">
                                    <option>USD</option>
                                    <option>Euro</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <ul class="d-flex justify-content-end">
                            <li class="d-flex align-items-center"><i class="feather-map-pin me-1"></i> 4998 Elk Creek
                                Road Canton GA 30114</li>
                            <li class="d-flex align-items-center"><i class="feather-mail me-1"></i> admin@example.com
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
@endif
@if (Route::is(['index-3']))
    <header class="header header-three">
@endif
@if (Route::is(['index-4', 'index-5']))
    <header class="header header-four">
@endif
@if (Route::is(['index-6']))
    <header class="header header-six">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="selection-list">
                            <div class="head-contact-six">
                                <i class="feather-phone me-1"></i>
                                <a href="javascript:void(0);">+1 56622 49854</a>
                            </div>
                            <div class="head-contact-six">
                                <i class="feather-mail me-1 "></i>
                                <a href="javascript:void(0);">listee@example.com</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <ul class="d-flex justify-content-end car-top-left">
                            <li class="d-flex align-items-center"><a href="{{ url('login') }}">Already a User</a></li>
                            <li class="d-flex align-items-center"><a href="{{ url('signup') }}">Become a dealer</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
@endif
@if (Route::is(['index-7']))
    <header class="header header-four header-seven">
@endif
@if (Route::is(['index-8']))
    <header class="header header-eight">
@endif
@if (Route::is(['index-9']))
    <header class="header header-nine">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <ul class="d-flex justify-content-start car-top-left">
                            <li class="d-flex align-items-center"><a href="{{ url('categories') }}">Find an agent</a>
                            </li>
                            <li class="d-flex align-items-center"><a href="{{ url('signup') }}">Become a dealer</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="selection-list">
                            <div class="head-contact">
                                <i class="feather-lock me-1"></i>
                                <a href="{{ url('login') }}">Login</a>
                            </div>
                            <span>/</span>
                            <div class="head-contact">
                                <i class="feather-user me-1 "></i>
                                <a href="{{ url('signup') }}">Sign Up</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
@endif
<div class="container">
    <nav class="navbar navbar-expand-lg header-nav">
        <div class="navbar-header">
            <a id="mobile_btn" href="javascript:void(0);">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>
            @if (!Route::is(['index-6', 'index-7', 'index-9']))
                <a href="{{ url('/') }}" class="navbar-brand logo">
                    <img src="{{ URL::asset('/assets/img/logo.svg') }}" class="img-fluid" alt="Logo">
                </a>
            @endif
            @if (Route::is(['index-9']))
                <a href="{{ url('/') }}" class="navbar-brand logo">
                    <img src="{{ URL::asset('/assets/img/icons/logo.svg') }}" class="img-fluid" alt="Logo">
                </a>
                <a href="{{ url('/') }}" class="navbar-brand logo-small">
                    <img src="{{ URL::asset('/assets/img/logo.png') }}" class="img-fluid" alt="Logo">
                </a>
            @endif
            @if (Route::is(['index-6']))
                <a href="{{ url('/') }}" class="navbar-brand logo">
                    <img src="{{ URL::asset('/assets/img/logo-white.svg') }}" class="img-fluid" alt="Logo">
                </a>
                <a href="{{ url('/') }}" class="navbar-brand logo-small">
                    <img src="{{ URL::asset('/assets/img/logo.png') }}" class="img-fluid" alt="Logo">
                </a>
            @endif
            @if (Route::is(['index-7']))
                <a href="{{ url('/') }}" class="navbar-brand logo">
                    <img src="{{ URL::asset('/assets/img/logo-new.png') }}" class="img-fluid" alt="Logo">
                </a>
            @endif
        </div>
        <div class="main-menu-wrapper">
            <div class="menu-header">
                <a href="{{ url('/') }}" class="menu-logo">
                    @if (!Route::is(['index-7']))
                        <img src="{{ URL::asset('/assets/img/logo.svg') }}" class="img-fluid" alt="Logo">
                    @endif
                    @if (Route::is(['index-7']))
                        <img src="{{ URL::asset('/assets/img/logo-new.png') }}" class="img-fluid" alt="Logo">
                    @endif
                </a>
                <a id="menu_close" class="menu-close" href="javascript:void(0);"> <i class="fas fa-times"></i></a>
            </div>
            @if (!Route::is(['index-6', 'index-9']))
                <ul class="main-nav">
            @endif
            @if (Route::is(['index-6', 'index-9']))
                <ul class="navbar-nav main-nav my-2 my-lg-0">
            @endif
            <li
                class="has-submenu megamenu {{ Request::is('/', 'index-2', 'index-3', 'index-4', 'index-5', 'index-6', 'index-7', 'index-8', 'index-9') ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    @if (Route::is(['index-8']))
                        <i class="feather-home"></i>
                    @endif
                    Home
                    @if (!Route::is(['index-8']))
                        <i class="fas fa-chevron-down"></i>
                    @endif
                </a>
                <ul class="submenu mega-submenu">
                    <li
                        class="{{ Request::is('/', 'index-2', 'index-3', 'index-4', 'index-5', 'index-6', 'index-7', 'index-8', 'index-9') ? 'active' : '' }}">
                        <div class="megamenu-wrapper">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="single-demo {{ Request::is('/') ? 'active' : '' }}">
                                        <div class="demo-img">
                                            <a href="{{ url('/') }}"><img
                                                    src="{{ URL::asset('/assets/img/home-01.jpg') }}"
                                                    class="img-fluid" alt="img"></a>
                                        </div>
                                        <div class="demo-info">
                                            <a href="{{ url('/') }}">Classified Home</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single-demo {{ Request::is('index-2') ? 'active' : '' }}">
                                        <div class="demo-img">
                                            <a href="{{ url('index-2') }}"><img
                                                    src="{{ URL::asset('/assets/img/home-02.jpg') }}"
                                                    class="img-fluid" alt="img"></a>
                                        </div>
                                        <div class="demo-info">
                                            <a href="{{ url('index-2') }}">Wedding Home</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single-demo {{ Request::is('index-3') ? 'active' : '' }}">
                                        <div class="demo-img">
                                            <a href="{{ url('index-3') }}"><img
                                                    src="{{ URL::asset('/assets/img/home-03.jpg') }}"
                                                    class="img-fluid" alt="img"></a>
                                        </div>
                                        <div class="demo-info">
                                            <a href="{{ url('index-3') }}">Tour Home</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single-demo {{ Request::is('index-4') ? 'active' : '' }}">
                                        <div class="demo-img">
                                            <a href="{{ url('index-4') }}"><img
                                                    src="{{ URL::asset('/assets/img/home-04.jpg') }}"
                                                    class="img-fluid" alt="img"></a>
                                        </div>
                                        <div class="demo-info">
                                            <a href="{{ url('index-4') }}">Workspace Home</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single-demo {{ Request::is('index-5') ? 'active' : '' }}">
                                        <div class="demo-img">
                                            <a href="{{ url('index-5') }}"><img
                                                    src="{{ URL::asset('/assets/img/home-05.jpg') }}"
                                                    class="img-fluid" alt="img"></a>
                                        </div>
                                        <div class="demo-info">
                                            <a href="{{ url('index-5') }}">Business Home</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single-demo {{ Request::is('index-6') ? 'active' : '' }}">
                                        <div class="demo-img">
                                            <a href="{{ url('index-6') }}"><img
                                                    src="{{ URL::asset('/assets/img/home-06.jpg') }}"
                                                    class="img-fluid" alt="img"></a>
                                        </div>
                                        <div class="demo-info">
                                            <a href="{{ url('index-6') }}">Car Rentals Home</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single-demo {{ Request::is('index-7') ? 'active' : '' }}">
                                        <div class="demo-img">
                                            <a href="{{ url('index-7') }}"><img
                                                    src="{{ URL::asset('/assets/img/home-07.jpg') }}"
                                                    class="img-fluid" alt="img"></a>
                                        </div>
                                        <div class="demo-info">
                                            <a href="{{ url('index-7') }}">Restaurant Home</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single-demo {{ Request::is('index-8') ? 'active' : '' }}">
                                        <div class="demo-img">
                                            <a href="{{ url('index-8') }}"><img
                                                    src="{{ URL::asset('/assets/img/home-08.jpg') }}"
                                                    class="img-fluid" alt="img"></a>
                                        </div>
                                        <div class="demo-info">
                                            <a href="{{ url('index-8') }}">Job Listing Home</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single-demo {{ Request::is('index-9') ? 'active' : '' }}">
                                        <div class="demo-img">
                                            <a href="{{ url('index-9') }}"><img
                                                    src="{{ URL::asset('/assets/img/home-09.jpg') }}"
                                                    class="img-fluid" alt="img"></a>
                                        </div>
                                        <div class="demo-info">
                                            <a href="{{ url('index-9') }}">Realestate Home</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li
                class="has-submenu {{ Request::is('listing-grid', 'listing-grid-sidebar', 'listing-list-sidebar', 'listingmap-list', 'listingmap-grid') ? 'active' : '' }}">
                <a href="">
                    @if (Route::is(['index-8']))
                        <i class="feather-search"></i>
                        Find Jobs
                </a>
                @endif
                @if (!Route::is(['index-8']))
                    Listings
                    <i class="fas fa-chevron-down"></i></a>
                @endif
                <ul class="submenu">
                    <li class="{{ Request::is('listing-grid') ? 'active' : '' }}"><a
                            href="{{ url('listing-grid') }}">Listing Grid</a></li>
                    <li class="{{ Request::is('listing-grid-sidebar') ? 'active' : '' }}"><a
                            href="{{ url('listing-grid-sidebar') }}">Listing Grid Sidebar</a></li>
                    <li class="{{ Request::is('listing-list-sidebar') ? 'active' : '' }}"><a
                            href="{{ url('listing-list-sidebar') }}">Listing List Sidebar</a></li>
                    <li class="{{ Request::is('listingmap-list') ? 'active' : '' }}"><a
                            href="{{ url('listingmap-list') }}">Listing List Map</a></li>
                    <li class="{{ Request::is('listingmap-grid') ? 'active' : '' }}"><a
                            href="{{ url('listingmap-grid') }}">Listing Grid Map</a></li>
                </ul>
            </li>
            <li
                class="has-submenu {{ Request::is('about', 'service-details', 'pricing', 'faq', 'gallery', 'categories', 'howitworks', 'terms-condition', 'privacy-policy', 'error-404', 'error-500') ? 'active' : '' }}">
                <a href="">
                    @if (Route::is(['index-8']))
                        <i class="feather-list"></i>
                        Services
                </a>
                @endif
                @if (!Route::is(['index-8']))
                    Pages <i class="fas fa-chevron-down"></i></a>
                @endif
                <ul class="submenu">
                    <li class="{{ Request::is('about') ? 'active' : '' }}"><a href="{{ url('about') }}">About
                            Us</a></li>
                    <li class="{{ Request::is('service-details') ? 'active' : '' }}"><a
                            href="{{ url('service-details') }}">Service Details </a></li>
                    <li class="{{ Request::is('pricing') ? 'active' : '' }}"><a
                            href="{{ url('pricing') }}">Pricing</a></li>
                    <li class="{{ Request::is('faq') ? 'active' : '' }}"><a href="{{ url('faq') }}">FAQ</a>
                    </li>
                    <li class="{{ Request::is('gallery') ? 'active' : '' }}"><a
                            href="{{ url('gallery') }}">Gallery</a></li>
                    <li class="{{ Request::is('categories') ? 'active' : '' }}"><a
                            href="{{ url('categories') }}">Category</a></li>
                    <li class="{{ Request::is('howitworks') ? 'active' : '' }}"><a
                            href="{{ url('howitworks') }}">How it Works</a></li>
                    <li class="{{ Request::is('terms-condition') ? 'active' : '' }}"><a
                            href="{{ url('terms-condition') }}">Terms & Conditions</a></li>
                    <li class="{{ Request::is('privacy-policy') ? 'active' : '' }}"><a
                            href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                    <li class="{{ Request::is('error-404') ? 'active' : '' }}"><a href="{{ url('error-404') }}">404
                            Error</a></li>
                    <li class="{{ Request::is('error-500') ? 'active' : '' }}"><a href="{{ url('error-500') }}">500
                            Error</a></li>
                </ul>
            </li>
            @if (Route::is(['index-8']))
                <li
                    class="has-submenu {{ Request::is('blog-list', 'blog-grid', 'blog-details', 'blog-list-sidebar', 'blog-grid-sidebar') ? 'active' : '' }}">
                    <a href="">
                        @if (Route::is(['index-8']))
                            <i class="feather-edit"></i>
                        @endif
                        Blog
                        @if (!Route::is(['index-8']))
                            <i class="fas fa-chevron-down"></i>
                        @endif
                    </a>
                    <ul class="submenu">
                        <li class="{{ Request::is('blog-list') ? 'active' : '' }}"><a
                                href="{{ url('blog-list') }}">Blog List</a></li>
                        <li class="{{ Request::is('blog-grid') ? 'active' : '' }}"><a
                                href="{{ url('blog-grid') }}">Blog Grid</a></li>
                        <li class="{{ Request::is('blog-details') ? 'active' : '' }}"><a
                                href="{{ url('blog-details') }}">Blog Details</a></li>
                        <li class="{{ Request::is('blog-list-sidebar') ? 'active' : '' }}"><a
                                href="{{ url('blog-list-sidebar') }}">Blog List Sidebar</a></li>
                        <li class="{{ Request::is('blog-grid-sidebar') ? 'active' : '' }}"><a
                                href="{{ url('blog-grid-sidebar') }}">Blog Grid Sidebar</a></li>
                    </ul>
                </li>
            @endif
            <li
                class="has-submenu {{ Request::is('dashboard', 'profile', 'my-listing', 'bookmarks', 'messages', 'reviews', 'add-listing') ? 'active' : '' }}">
                <a href="">
                    @if (Route::is(['index-8']))
                        <i class="feather-info"></i>
                        About
                </a>
                @endif
                @if (!Route::is(['index-8']))
                    User Pages <i class="fas fa-chevron-down"></i></a>
                @endif

                <ul class="submenu">
                    <li class="{{ Request::is('dashboard') ? 'active' : '' }}"><a
                            href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="{{ Request::is('profile') ? 'active' : '' }}"><a
                            href="{{ url('profile') }}">Profile</a></li>
                    <li class="{{ Request::is('my-listing') ? 'active' : '' }}"><a
                            href="{{ url('my-listing') }}">My Listing</a></li>
                    <li class="{{ Request::is('bookmarks') ? 'active' : '' }}"><a
                            href="{{ url('bookmarks') }}">Bookmarks</a></li>
                    <li class="{{ Request::is('messages') ? 'active' : '' }}"><a
                            href="{{ url('messages') }}">Messages</a></li>
                    <li class="{{ Request::is('reviews') ? 'active' : '' }}"><a
                            href="{{ url('reviews') }}">Reviews</a></li>
                    <li class="{{ Request::is('add-listing') ? 'active' : '' }}"><a
                            href="{{ url('add-listing') }}">Add Listing</a></li>
                </ul>
            </li>
            @if (!Route::is(['index-8']))
                <li
                    class="has-submenu {{ Request::is('blog-list', 'blog-grid', 'blog-details', 'blog-list-sidebar', 'blog-grid-sidebar') ? 'active' : '' }}">
                    <a href="">
                        @if (Route::is(['index-8']))
                            <i class="feather-edit"></i>
                        @endif
                        Blog
                        @if (!Route::is(['index-8']))
                            <i class="fas fa-chevron-down"></i>
                        @endif
                    </a>
                    <ul class="submenu">
                        <li class="{{ Request::is('blog-list') ? 'active' : '' }}"><a
                                href="{{ url('blog-list') }}">Blog List</a></li>
                        <li class="{{ Request::is('blog-grid') ? 'active' : '' }}"><a
                                href="{{ url('blog-grid') }}">Blog Grid</a></li>
                        <li class="{{ Request::is('blog-details') ? 'active' : '' }}"><a
                                href="{{ url('blog-details') }}">Blog Details</a></li>
                        <li class="{{ Request::is('blog-list-sidebar') ? 'active' : '' }}"><a
                                href="{{ url('blog-list-sidebar') }}">Blog List Sidebar</a></li>
                        <li class="{{ Request::is('blog-grid-sidebar') ? 'active' : '' }}"><a
                                href="{{ url('blog-grid-sidebar') }}">Blog Grid Sidebar</a></li>
                    </ul>
                </li>
            @endif
            <li class="{{ Request::is('contact') ? 'active' : '' }}"><a href="{{ url('contact') }}">
                    <a href="{{ url('contact') }}">
                        @if (Route::is(['index-8']))
                            <i class="feather-send"></i>
                        @endif
                        Contact
                    </a>
            </li>
            @if (!Route::is(['index-3']))
                <li class="login-link">
                    <a href="{{ url('signup') }}">Sign Up</a>
                </li>
                @if (!Route::is(['index-4', 'index-5', 'index-7']))
                    <li class="login-link">
                        <a href="{{ url('login') }}">Sign In</a>
                    </li>
                @endif
            @endif
            </ul>

            @if (Route::is(['index-4', 'index-7']))
                <ul class="nav header-navbar-rht nav">
                    @if (!Route::is(['index-7']))
                        <li class="nav-item">
                            <a class="nav-link header-reg" href="{{ url('signup') }}">Sign Up</a>
                        </li>
                    @endif

                    <li class="nav-item">
                        @if (!Route::is(['index-7']))
                            <a class="nav-link header-login" href="{{ url('login') }}"> Sign In</a>
                    </li>
            @endif
            @if (Route::is(['index-7']))
                <a class="nav-link header-login" href="{{ url('login') }}"> <i class="fa-solid fa-plus"></i>
                    Sign In</a>
            @endif
            <li class="nav-item">
                @if (!Route::is(['index-7']))
                    <a class="nav-link header-login" href="{{ url('add-listing') }}">Add Listing <i
                            class="fa-solid fa-plus"></i></a>
                @endif
                @if (Route::is(['index-7']))
                    <a class="nav-link header-login" href="{{ url('add-listing') }}"><i
                            class="fa-solid fa-plus"></i> Add Listing
                    </a>
                @endif
            </li>
            </ul>
            @endif
        </div>
        @if (Route::is(['index-3', 'index-6', 'index-9']))
            <div class="d-flex align-items-center block-e">
                @if (!Route::is(['index-6', 'index-9']))
                    <div class="cta-btn">
                        <a href="{{ url('login') }}" class="btn">sign in /</a>
                        <a href="{{ url('signup') }}" class="btn ms-1"> register</a>
                    </div>
                @endif
                @if (!Route::is(['index-3', 'index-9']))
                    <a href="{{ url('categories') }}" class="car-list-btn">
                        <img src="{{ URL::asset('/assets/img/icons/search-icon.svg') }}" alt="">
                        <span>List a Car</span>
                    </a>
                @endif
                @if (Route::is(['index-9']))
                    <a href="{{ url('categories') }}" class="car-list-btn header-phone">
                        <i class="feather-phone"></i>
                        <span>+1 56622 49854</span>
                    </a>
                    <a href="{{ url('categories') }}" class="car-list-btn">
                        <i class="feather-plus-circle"></i>
                        <span>Submit Property</span>
                    </a>
                @endif
            </div>
        @endif
        @if (!Route::is(['index-6', 'index-9', 'index-4', 'index-7']))
            <ul class="nav header-navbar-rht">
                @if (!Route::is(['index-2', 'index-3', 'index-5']))
                    @if (!Route::is(['add-listing', 'dashboard', 'profile', 'my-listing', 'bookmarks', 'messages', 'reviews']))
                        @if (!Route::is(['index-7']))
                            <li class="nav-item">
                                @if (!Route::is(['index-8']))
                                    <a class="nav-link header-reg" href="{{ url('signup') }}">Sign Up</a>
                                @endif
                                @if (Route::is(['index-8']))
                                    <a class="nav-link header-reg" href="{{ url('login') }}"><i
                                            class="feather-unlock"></i> Login
                                    </a>
                                @endif
                            </li>
                        @endif
                        <li class="nav-item">
                            @if (!Route::is(['index-7', 'index-8']))
                                <a class="nav-link header-login" href="{{ url('login') }}"> Sign In</a>
                            @endif
                            @if (Route::is(['index-8']))
                                <a class="nav-link header-reg" href="{{ url('signup') }}"><i
                                        class="feather-user"></i> Sign
                                    Up</a>
                            @endif
                        </li>

                    @endif
                    <li class="nav-item">
                        @if (!Route::is(['index-4', 'index-8']))
                            <a class="nav-link header-login add-listing" href="{{ url('add-listing') }}"><i
                                    class="fa-solid fa-plus"></i> Add Listing</a>
                        @endif
                        @if (Route::is(['index-4', 'index-7']))
                            <a class="nav-link header-login" href="{{ url('add-listing') }}">Add Listing <i
                                    class="fa-solid fa-plus"></i></a>
                        @endif
                        @if (Route::is(['index-8']))
                            <a class="nav-link header-login add-listing" href="{{ url('add-listing') }}"> Post a
                                job</a>
                        @endif
                    </li>
                @endif
                @if (Route::is(['index-2']))
                    <li class="nav-item">
                        <a class="nav-link header-login" href="{{ url('login') }}">Get Started Now</a>
                    </li>
                @endif
                @if (Route::is(['add-listing', 'dashboard', 'profile', 'my-listing', 'bookmarks', 'messages', 'reviews']))
                    <li class="nav-item dropdown has-arrow logged-item">
                        <a href="#" class="dropdown-toggle profile-userlink" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            <img src="{{ URL::asset('/assets/img/profile-img.jpg') }}" alt="">
                            <span>John Doe</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end">
                            <a class="dropdown-item" href="{{ url('dashboard') }}">Dashboard</a>
                            <a class="dropdown-item" href="{{ url('profile') }}">Profile Settings</a>
                            <a class="dropdown-item" href="{{ url('login') }}">Logout</a>
                        </div>
                    </li>
                @endif
                @if (Route::is(['index-5']))
                    <li class="nav-item">
                        <div class="cta-btn">
                            <a href="{{ url('login') }}" class="btn"><i class="feather-user"></i> sign in /</a>
                            <a href="{{ url('signup') }}" class="btn ms-1"> register</a>
                        </div>
                    </li>
                @endif
            </ul>
        @endif
    </nav>
</div>
</header>
<!-- /Header -->
