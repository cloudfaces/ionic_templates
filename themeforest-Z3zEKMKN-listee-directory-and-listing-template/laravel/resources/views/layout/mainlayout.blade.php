<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    @if (!Route::is(['index-3']))
        <title>Listee | Template</title>
    @endif
    @if (Route::is(['index-3']))
        <title>Dreamstrip | Template</title>
    @endif
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon.png') }}">
    @include('layout.partials.head')
</head>


@if (!Route::is(['index-3']))

    <body>
@endif
@if (Route::is(['index-3']))

    <body class="body-three">
@endif
<!-- Main Wrapper -->
@if (
    !Route::is([
        'blog-details',
        'blog-grid-sidebar',
        'blog-grid',
        'blog-list',
        'blog-list-sidebar',
        'bookmarks',
        'categories',
        'contact',
        'dashboard',
        'faq',
        'forgot-password',
        'gallery',
        'listing-grid-sidebar',
        'listing-grid',
        'listing-list-sidebar',
        'listingmap-grid',
        'listingmap-list',
        'login',
        'messages',
        'my-listing',
        'profile',
        'reviews',
        'service-details',
        'signup',
        'index-6',
        'index-9',
    ]))
    <div class="main-wrapper">
@endif
@if (Route::is([
        'blog-details',
        'blog-grid-sidebar',
        'blog-grid',
        'blog-list',
        'blog-list-sidebar',
        'bookmarks',
        'categories',
        'contact',
        'dashboard',
        'faq',
        'forgot-password',
        'gallery',
        'listing-grid-sidebar',
        'listing-grid',
        'listing-list-sidebar',
        'listingmap-grid',
        'listingmap-list',
        'login',
        'messages',
        'my-listing',
        'profile',
        'reviews',
        'service-details',
        'signup',
        'forgot-password',
    ]))
    <div class="main-wrapper innerpagebg">
@endif
@if (Route::is(['index-6']))
    <div class="main-wrapper home-six">
@endif
@if (Route::is(['index-9']))
    <div class="main-wrapper home-nine">
@endif
@if (Route::is(['howitworks', 'pricing']))
    <div class="main-wrapper pricinginner-page">
@endif
@if (!Route::is(['error-404', 'error-500']))
    @include('layout.partials.header')
@endif
@yield('content')
@if (!Route::is(['listingmap-grid', 'listingmap-list', 'error-404', 'error-500']))
    @include('layout.partials.footer')
@endif

</div>
<!-- /Main Wrapper -->
@include('layout.partials.footer-scripts')
</body>

</html>
