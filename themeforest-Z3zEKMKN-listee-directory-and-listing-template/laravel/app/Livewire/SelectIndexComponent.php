<?php

namespace App\Livewire;

use Livewire\Component;

class SelectIndexComponent extends Component
{
    public $selectedOption1;
    public $options1;

    public function mount() {
        $this->options1 = [ 'Bangalore, India', 'Delhi', 'Hyderabad' ];
    }
    public function render()
    {
        return view('livewire.select-index-component');
    }
}
