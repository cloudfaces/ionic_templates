Hi,

First of all - thank you for buying our template! 

Before you use template, please read documentation for better understanding how to use it.
http://csform.com/ionic4/ionic4-UI-billy-theme/documentation/

Also you can contact us at dev@csform.com if you need.

Happy coding! :)


CSForm Dev Team




CREDITS:
Images from unsplash.com


FONT:
https://developer.apple.com/fonts/

ICONS:
https://materialdesignicons.com/ 
+ default Ionic icons